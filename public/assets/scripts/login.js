//Login file
 $(document).ready(function(){
    document.getElementById("btn-register").disabled = true;
    maxDate = new Date(new Date().setFullYear(new Date().getFullYear() - 18));
    $('#group-fecha').datetimepicker({
        format: 'YYYY-MM-DD',
        ignoreReadonly: true,
        maxDate: maxDate,
    });

    var loginForm = $("#loginForm");
    var ruta = '/';
    loginForm.submit(function(e) {
        $("#form-login-errors").empty();
        e.preventDefault();
        var formData = loginForm.serialize();

        $("#email-div").removeClass("has-error");
        $("#password-div").removeClass("has-error");
        $("#login-errors").removeClass("has-error");
        $("#modal-loading").modal("show");
        $.ajax({
            url: ruta+'login',
            type: 'POST',
            data: formData,
            success: function(data) {
                $("#modal-loading").modal("hide");
                $("#loginModal").modal("hide");
                location.reload(true);
            },
            error: function(data) {
                if (data.status != 500) {
                    var obj = jQuery.parseJSON(data.responseText);
                    $("#modal-loading").modal("hide");
                    console.log(obj.error);
                    if (obj.email) {
                        $("#email-div").addClass("has-error");
                        $("#form-errors-email").html(obj.email);
                    }
                    if (obj.password) {
                        $("#password-div").addClass("has-error");
                        $("#form-errors-password").html(obj.password);
                    }
                    if (obj.error) {
                        $("#login-errors").addClass("has-error");
                        $("#loginerrors").html(obj.error);
                        $("#loginerrors").addClass("error-login")
                    }
                }
                else{
                    session_expired(data);
                }
            }
        });
    });

        var registerForm = $("#registerForm");
        registerForm.submit(function(e){
            e.preventDefault();
            var formData = registerForm.serialize();
            $("#register-errors-name").html( "" );
            $("#register-errors-email").html( "" );
            $("#register-errors-password").html( "" );
            $("#register-errors-identification").html( "" );
            $("#register-errors-birth-date").html( "" );
            $("#register-name").removeClass("has-error");
            $("#register-email").removeClass("has-error");
            $("#register-password").removeClass("has-error");
            $("#register-identification").removeClass("has-error");
            $("#register-birth-date").removeClass("has-error");
            $("#register-phone").removeClass("has-error");
            $("#modal-loading").modal("show");
            $.ajax({
                url: ruta+'register',
                type:'POST',
                data:formData,
                success:function(data){
                    $("#modal-loading").modal("hide");
                    $('#registerModal').modal( 'hide' );
                    location.reload(true);
                },
                error: function (data) {
                    $("#modal-loading").modal("hide");
                    console.log(data.responseText);
                    var obj = jQuery.parseJSON( data.responseText );
                   if(obj.name){
                        $("#register-name").addClass("has-error");
                        $("#register-errors-name").html( obj.name );
                        $("#register-errors-name").addClass("error-login")
                    }
                    if(obj.email){
                        $("#register-email").addClass("has-error");
                        $("#register-errors-email").html( obj.email );
                        $("#register-errors-email").addClass("error-login")
                    }
                    if(obj.identification){
                        $("#register-identification").addClass("has-error");
                        $("#register-errors-identification").html( obj.identification );
                        $("#register-errors-identification").addClass("error-login")
                    }
                    if(obj.password){
                        $("#register-password").addClass("has-error");
                        $("#register-errors-password").html( obj.password );
                        $("#register-errors-password").addClass("error-login")
                    }
                    if(obj.birth_date){
                        $("#register-birth_date").addClass("has-error");
                        $("#register-errors-birth_date").html( obj.birth_date );
                        $("#register-errors-birth_date").addClass("error-login")
                    }
                    if(obj.phone){
                        $("#register-phone").addClass("has-error");
                        $("#register-errors-phone").html( obj.phone );
                        $("#register-errors-phone").addClass("error-login")
                    }
                }
            });
        });
    });

function enableBtn(){
    document.getElementById("btn-register").disabled = false;
}