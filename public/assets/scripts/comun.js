

//Comun file
var ruta = "/";
var img_loading = '<div class="text-center"><img class="img-responsive img-loading" src="../images/loading1.gif"></div>';
var tipoLogro = $("#tipoLogro").val();

$(function() {
	initDatePicker();
	initTime();
});

$(".menu-nav").click(function (){
     $('li').removeClass('active');
     $(this).parent().addClass('active');
});

function format_date(input){
    var d = new Date(Date.parse(input.replace(/-/g, "/")));
    var month = "0";
    var day = "0";
    var date = "";
    var hours = "0";
    var minutes = "0";
    var am = "AM";

    if (d.getDate() > 9)
    {
    	day = d.getDate();
    } else
    {
    	day += d.getDate();
    }

    if (d.getMonth()+1 > 9)
    {
    	month = d.getMonth()+1;
    } else
    {
    	month += d.getMonth()+1;
    }
	date = day + "-" + month + "-" + d.getFullYear();



    if (d.getHours() > 12)
    {
    	hours = d.getHours() - 12;
    	am = "PM";
    }
    else
    {
    	hours = d.getHours();
    }

    if(hours < 10)
    {
    	hours = "0" + hours;
    }

    if (d.getMinutes() < 9)
    {
    	minutes += d.getMinutes();
    }
    else
    {
    	minutes = d.getMinutes();
    }

    var time = hours + ":" + minutes + " " + am;
    return (date + " " + time);
};

function numberWithCommas(x) {
    return x.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function seleccion(games_probs){
    var cantidad = "";
    //Ganadores
    if(games_probs.tipo_probabilidad==1 || games_probs.tipo_probabilidad==8 || games_probs.tipo_probabilidad==11 || games_probs.tipo_probabilidad==15)
    {
        salida = games_probs.tipo_apuesta.nombre + ": " + games_probs.equipo.nombre + " (" + logro(games_probs.probabilidad, tipoLogro) + ")";
    }
    //Runlines
    else if (games_probs.tipo_probabilidad==3 || games_probs.tipo_probabilidad==7 || games_probs.tipo_probabilidad==12 || games_probs.tipo_probabilidad==16) {
        if (games_probs.cantidad == 0) {
            cantidad = "PK";
        }
        else
        {
            var signo = games_probs.games_probs_signo.signo;


            cantidad = signo + games_probs.cantidad;
        }

        salida = games_probs.tipo_apuesta.nombre + ": " + games_probs.equipo.nombre + " (" + cantidad + " " + logro(games_probs.probabilidad, tipoLogro) + ")";
    }
    //Totales
    else if(games_probs.tipo_probabilidad==4 || games_probs.tipo_probabilidad==10 || games_probs.tipo_probabilidad==13 || games_probs.tipo_probabilidad==17 || games_probs.tipo_probabilidad==18) {
        var signo = (games_probs.orden == 1) ? "+" : "-";
        salida = games_probs.tipo_apuesta.nombre + " (" + signo + games_probs.cantidad + " " + logro(games_probs.probabilidad, tipoLogro) + ")";
    }
    else if(games_probs.tipo_probabilidad==6 || games_probs.tipo_probabilidad==14){
        salida = games_probs.tipo_apuesta.nombre + " (" + logro(games_probs.probabilidad, tipoLogro) + ")";
    }
    else if(games_probs.tipo_probabilidad==9) {
        salida = games_probs.tipo_apuesta.nombre + ": ";
        salida += (games_probs.orden == 1) ? "SI" : "NO";
        salida += " " + logro(games_probs.probabilidad, tipoLogro) + ")";

    }
    else if(games_probs.tipo_probabilidad == 19){
        salida = games_probs.equipo.nombre;
    }
    return salida;
}

function session_expired(data){
    $(".modal").modal("hide");
    $("#modal-message").modal("show");
    if(data.status == 401)
    {
        $("#title-message").html("<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Sesión Expirada");
        $("#message").html(" Su sesión expiró, por favor inicie sesión nuevamente");
        $("#modal-message").on('hide.bs.modal', function () {
            window.location.href = "/login";
        });
    }
    else{
        $("#title-message").html("<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Error " + data.status);
        $("#message").html(data.statusText);
    }
}

$(".input-number").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
         // Allow: Ctrl+C
        (e.keyCode == 67 && e.ctrlKey === true) ||
         // Allow: Ctrl+X
        (e.keyCode == 88 && e.ctrlKey === true) ||
         // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

function carga_bancos(){
    if (bancos ==0 )
    {
        $.ajax({
            url: ruta +'banco',
            type: 'GET',
            dataType: 'json',
        })
        .done(function(data) {
            $(".banco").html("<option value='0'>Seleccione el banco</option>");
            for (var i = 0; i < data.length; i++) {
                $(".banco").append("<option value='" + data[i].id + "'>" + data[i].nombre +"</option>");
            }
            bancos = 1;
        })
        .fail(function(error) {

        })
        .always(function() {
        });
    }
}

function ticketSorteo(bets){
    $.ajax({
        url: ruta + 'ticket',
        type: 'GET',
        dataType: 'json',
        data:{
            bets: bets
        }
    })
    .done(function(data) {
        var bets = data.bet;
        var played = '';
        var total = 0;

        for (var i = 0; i < bets.length; i++) {
            var bets_detail = bets[i].bets_detail;
            played += bets_detail[0].games_probs.game.event + ' '
                + seleccion(bets_detail[0].games_probs).substring(0, 6)
                + ' ('+ bets[i].id +') '
                + parseFloat(bets[i].monto).toFixed(2) + '<br>' ;
            total += +bets[i].monto;
        }

        var content = '<table class="text-uppercase text-ticket ">'
            + '<tr>'
            + '<td><b>Fecha:</b></th>'
            + '<td>'+ format_date(bets[0].fecha_registro, 'd-m-Y') +'</td>'
            + '</tr><tr>'
            + '<td><b>Jugada:</b></th>'
            + '<td>'+ played +'</td>'
            + '</tr><tr>'
            + '<td><b>Total Jugado:</b></th>'
            + '<td>'+ total.toFixed(2) +'</td>'
            + '</tr>'
            +'</table>';
        $("#ticket").html(content);

    })
    .fail(function() {
        session_expired(data);
    })
    .always(function() {
    });
}

function ticket(id, reprint){
    $.ajax({
        url: ruta + 'bet/' + id,
        type: 'GET',
        dataType: 'json',
    })
		.done(function (data) {
			console.log(data);
		if (reprint == 1) {
            $("#ticket").empty();
		}				

		$("#ticket").append(loadTicket(data));
		
        if (reprint == 1) {
            print_ticket();
        }

    })
    .fail(function() {
        session_expired(data);
    })
    .always(function() {
    });
}

function loadTicket(data) {
	var response = data.bet;
	var bets_detail = response.bets_detail
	var ganancia = 0;
	var monto = parseFloat(response.monto );		

	var head = `<h4 class="modal-title text-ticket">Miapuestaweb.com</h4>
				<div class="ticket-header">
				<p><b>Ticket #: </b>` + response.id + `</p>
				<p><b>Fecha y hora: </b>` + format_date(response.fecha_registro) + `</p>`;
	
	var body = `<div class="ticket-body">`;

	for (var i = 0; i < bets_detail.length; i++) {
		var jugada = i + 1;
		body += `<b>#` + jugada + ' ' + bets_detail[i].games_probs.game.event + `: </b>`
				+ 	seleccion(bets_detail[i].games_probs)
		if(i==0) {

			ganancia = monto * bets_detail[i].games_probs.probabilidad;
		} else {

			ganancia = ganancia * bets_detail[i].games_probs.probabilidad;
		}
	
		body += jugada != bets_detail.length ? ' / ' : '';
	}

	body += `</p>
			</div>`;

	head += `<p><b>Monto: </b>` + numberWithCommas(monto) + `
				<b>Total a Ganar: </b>` + numberWithCommas(ganancia) + `
			 </p>
		</div>`; //end header
	
	var content = head + body + `<br>`;

    return content;
}

function print_ticket(){
    window.print();
    setTimeout(function () { window.close(); }, 100);
}

function logro(value, tipo){
    if (tipo == 1){
         if (value >= 2)
        {
            americano=(value-1)*100;
            mostrar = "+" + americano.toPrecision(3);
        }
        else
        {
           americano= -100/(value-1);
            if (value==0)
            {
                mostrar = '+' + americano.toPrecision(3);
            }
            else
            {
                mostrar = americano.toPrecision(3);
            }
        }
    }
    else{
        mostrar = value;
    }
    return mostrar;
}


function initDatePicker() {

	$('.common-date-picker').datetimepicker({
	  locale: 'es',
	  format: 'YYYY-MM-DD',
	  ignoreReadonly: true,
  });
}

//Time
function initTime() {
	
	var date = new Date(),
		hour = date.getHours(),
		minute = checkTime(date.getMinutes()),
		ss = checkTime(date.getSeconds())
		path = window.location.pathname;
  
	function checkTime(i) {
	  if( i < 10 ) {
		i = "0" + i;
	  }
	  return i;
	}
	
	if (path == '/') {
	
		if ( hour > 12 ) {
		  hour = hour - 12;
		  if ( hour == 12 ) {
			  hour = checkTime(hour);
		  document.getElementById("lbl-time").innerHTML = hour+":"+minute+":"+ss+" AM";
		  }
		  else {
			  hour = checkTime(hour);
			document.getElementById("lbl-time").innerHTML = hour+":"+minute+":"+ss+" PM";
		  }
		}
		else {
		  document.getElementById("lbl-time").innerHTML = hour+":"+minute+":"+ss+" AM";;
		}
		var time = setTimeout(initTime,1000);
	}  
  }