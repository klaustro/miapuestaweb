//Cuentas file
var bancos = 0;
var idcuenta = 0;

function cargar(){
	$("#panel-cuentas").html(img_loading);
	carga_bancos();
	$.ajax({
		url: ruta + 'cuentasbancarias',
		type: 'GET',
		dataType: 'json',
	})
	.done(function(data) {
		var content = "";
		for (var i = 0; i < data.length; i++) {
			content += "<div class='panel panel-default panel-ticket'>"
							+"<div class='panel-heading height-bet'><strong id='idbanco' data-banco_id='" + data[i].banco_id + "'>Banco </strong>" + data[i].banco.nombre +"</div>"
							+"<div class='panel-body'>"
								+"<ul class='list-group'>"
									+"<p class='pull-left'><strong>Número de Cuenta "+data[i].tipo+":</strong> <text id='text-nrocuenta'>" + data[i].nrocuenta + "</p>"
									+"<p class='pull-right'>"
										+"<button class='btn btn-info btn-xs' title='Editar cuenta' onclick='edit(" + data[i].id + ")' data-toggle='popover'><i class='fa fa-edit' aria-hidden='true'></i></button>"
										+" <button class='btn btn-danger btn-xs' title='Eliminar cuenta' onclick='confirmation(" + data[i].id + ")' data-toggle='popover'><i class='fa fa-trash' aria-hidden='true'></i></button>"
									+"</p><div class='clearfix'></div>"
								+"</ul>"
							+"</div>"
						+"</div>";
		}
		$("#panel-cuentas").html(content);
	})
	.fail(function(data) {
	//	session_expired(data);
	})
	.always(function() {
		console.log("complete");
	});

    $("#banco-cuenta").change(function(event) {
        validate();
    });
    $("#nrocuenta").keyup(function(event) {
        validate();
    });
    $("#tipo-cuenta").change(function(event) {
        validate();
    });
	$('[data-toggle="tooltip"]').tooltip()	

}

function edit(id){
	idcuenta = id;
	$("#btn-cuenta").prop('disabled', false);
	$.ajax({
		url: ruta + 'cuentasbancarias/'+id,
		type: 'GET',
		dataType: 'json'
	})
	.done(function(data) {
		$("#cuentasModal").modal("show");
		var banco_id = data.banco_id;
		var nrocuenta = data.nrocuenta;
		var tipo = data.tipo;
		$("#banco-cuenta").val(banco_id);
		$("#nrocuenta").val(nrocuenta);
		$("#tipo-cuenta").val(tipo);
		$(".title").html("Modificar Cuenta");

	})
	.fail(function(data) {
		session_expired(data);
	})
	.always(function() {
		console.log("complete");
	});
}

function confirmation(id){
	idcuenta = id;
	$("#modal-confirmation").modal("show");
	$("#title-confirmation").html('<i class="fa fa-bank" aria-hidden="true"></i> Eliminar Cuenta');
	$("#message-confirmation").html("Seguro que desea eliminar ésta cuenta");
}

function borrar(){
	$.ajax({
		url: ruta + 'cuentasbancarias/' + idcuenta,
		type: 'DELETE',
		dataType: 'json',
        headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
	})
	.done(function(data) {
		var title = data.title;
		var message = data.message;
		$("#modal-confirmation").modal('hide');
		$("#modal-message").modal("show");
		$("#title-message").html('<i class="fa fa-bank" aria-hidden="true"></i> "' + title + '"');
		$("#message").html(message);
		cargar();
	})
	.fail(function(data) {
		session_expired(data);
	})
	.always(function() {
		console.log("complete");
	});

}

function add(){
	$("#btn-cuenta").prop('disabled', true);
	$("#banco-cuenta").val(0);
	$("#tipo-cuenta").val(0);
	$("#nrocuenta").val("");
	$(".title").html("Registrar Cuenta");
	$("#cuentasModal").modal("show");
}

function register(id){
	var banco = $("#banco-cuenta").val();
	var nrocuenta = $("#nrocuenta").val();
	var tipo = $("#tipo-cuenta").val();
	if ($(".title").html() == "Registrar Cuenta") {
		$.ajax({
			url: ruta + 'cuentasbancarias',
			type: 'POST',
			dataType: 'json',
			data: {
				banco : banco,
				nrocuenta: nrocuenta,
				tipo: tipo
			},
	        headers: {
	                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                 },
		})
		.done(function(data) {
		    var title = data.title;
		    var message = data.message;
		    $("#cuentasModal").modal('hide');
		    $("#modal-message").modal("show");
		    $("#title-message").html('<i class="fa fa-bank" aria-hidden="true"></i> ' + title);
		    $("#message").html(message);
		    cargar();
		})
		.fail(function(data) {
			session_expired(data);
		})
		.always(function() {
			console.log("complete");
		});
	}
	else{
		$.ajax({
			url: ruta + 'cuentasbancarias/' + idcuenta,
			type: 'PUT',
			dataType: 'json',
			data: {
				banco : banco,
				nrocuenta: nrocuenta,
				cuenta: tipo
			},
	        headers: {
	                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                 },
		})
		.done(function(data) {
		    var title = data.title;
		    var message = data.message;
		    $("#cuentasModal").modal('hide');
		    $("#modal-message").modal("show");
		    $("#title-message").html('<i class="fa fa-bank" aria-hidden="true"></i> "' + title + '"');
		    $("#message").html(message);
		    cargar();
		})
		.fail(function(data) {
			session_expired(data);
		})
		.always(function() {
			console.log("complete");
		});

	}


}

    function validate(){
        if($("#nrocuenta").val().length < 20 || $("#banco-cuenta").val() == 0 || $("#tipo-cuenta").val() == 0)
        {
            $("#btn-cuenta").prop('disabled', true);
        }
        else{
            $("#btn-cuenta").prop('disabled', false);
        }
    }

	$(".input-number").keydown(function (e) {
	    // Allow: backspace, delete, tab, escape, enter and .
	    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
	         // Allow: Ctrl+A
	        (e.keyCode == 65 && e.ctrlKey === true) ||
	         // Allow: Ctrl+C
	        (e.keyCode == 67 && e.ctrlKey === true) ||
	         // Allow: Ctrl+X
	        (e.keyCode == 88 && e.ctrlKey === true) ||
	         // Allow: home, end, left, right
	        (e.keyCode >= 35 && e.keyCode <= 39)) {
	             // let it happen, don't do anything
	             return;
	    }
	    // Ensure that it is a number and stop the keypress
	    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
	        e.preventDefault();
	    }
	});