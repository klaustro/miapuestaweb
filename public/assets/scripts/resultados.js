//Resultados file

    $("#group-fecha-result").on("dp.hide", function(e) {
		var fecha = $("#fecha-result").val();
		cargarResultados(fecha);
    });

    function cargarResultados(fecha){

    	$('#group-fecha-result').datetimepicker({
       		format: 'YYYY-MM-DD',
       	 	ignoreReadonly: true,
    	});

    	$("#panel-resultados").html(img_loading);
    	$.ajax({
    		url: ruta + 'resultado',
    		type: 'GET',
    		dataType: 'json',
    		data: {'fecha_cierre': fecha},
    	})
    	.done(function(response) {
    		console.log(response);
			var content ="<div class='panel-group' id='accordion' role='tablist' aria-multiselectable='true'>";
			if(response.length >0){
				var swCategoria = 0;
				var swSubCategoria = 0;
				for (var i = 0; i < response.length; i++) {
				var ganancia = 0;
					if (swCategoria == 0) {
						content += "<div class='panel panel-default panel-game'>"
							+"<div class='panel-heading text-uppercase heading-categoria'></strong>" + response[i].menu.nombre + "</p></div>"
							+"<div class='panel-body'>"
						swCategoria = 1
					}
					if(swSubCategoria==0){
						content += "<div class='panel panel-default panel-game'>"
							+"<div class='panel-heading text-uppercase heading-subcategoria'></strong> " + response[i].submenu.nombre + "</p></div>"
							+"<div class='panel-body'>"
						swSubCategoria = 1
					}

                var title = response[i].equipo.length > 2
                    ? response[i].submenu.nombre + ' ' + format_date(response[i].fecha_cierre)
                    : response[i].equipo[0].nombre + " - " + response[i].equipo[1].nombre;

					content += "<div class='panel panel-warning panel-game'>"
							+"<div class='panel-heading text-uppercase'>"
							+ title
							+"</div>"
							+"<div class='panel-body'>";
					var resultado = response[i].resultado;
					var sw = 0;
					for (var j = 0; j < resultado.length; j++) {
						if (resultado[j].tipo == 1 || resultado[j].tipo == 3) {
							if (sw == 0) {
								content +="<ul class='list-group'>";
								content += "<li class='list-group-item text-uppercase'><strong>" + resultado[j].tipo_resultado.nombre + ": </strong> " + resultado[j].equipo.nombre + " "+ resultado[j].score;
								sw=1;
							}
							else {
								content += " - " + resultado[j].score + " " + resultado[j].equipo.nombre + "</li>"
								content += "</ul>"
								sw=0;
							}
						}
						else if(resultado[j].tipo == 2 || resultado[j].tipo == 6){
							content +="<ul class='list-group'>";
							content += "<li class='list-group-item text-uppercase'><strong>" + resultado[j].tipo_resultado.nombre + ": </strong> " + resultado[j].equipo.nombre;
							content += "</ul>";
						}
						else if(resultado[j].tipo == 5){
							content +="<ul class='list-group'>";
							content += "<li class='list-group-item text-uppercase'><strong>" + resultado[j].tipo_resultado.nombre + ": </strong> " + resultado[j].score;
							content += "</ul>";
						}
						else if(resultado[j].tipo == 6){
							var sino = "SI";
							if (resultado[j].score == 2) {
								sino = "NO";
							}
							content +="<ul class='list-group'>";
							content += "<li class='list-group-item text-uppercase'><strong>" + resultado[j].tipo_resultado.nombre + ": </strong> " + sino;
							content += "</ul>";
						}
                        else if(resultado[j].tipo == 7){
                            console.log(resultado[j]);
                            content +="<ul class='list-group'>";
                            content += "<li class='list-group-item text-uppercase'><strong>" + resultado[j].tipo_resultado.nombre + ": </strong> " + resultado[j].equipo.nombre;
                            content += "</ul>";
                        }

					}
					content += '</div></div>';
					if (i < response.length -1) {
						if(response[i].subcategoria.id != response[i+1].subcategoria.id){
							content += "</div>"
									+"</div>";
						swSubCategoria = 0;
						}
						if(response[i].categoria.id != response[i+1].categoria.id){
							content += "</div>"
									+"</div>";
						swCategoria = 0;
						}
					}
					else{
						content += "</div>"
								+"</div>";
						content += "</div>"
								+"</div>"
								+"</div>";//Accordion
					}
				}
			}
			$("#panel-resultados").html(content);
    	})
    	.fail(function(data) {
    		console.log(data);
    	})
    	.always(function() {
    		$("#modal-loading").modal("hide");
    	});/**/

    }