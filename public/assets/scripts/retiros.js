//Retiros
balance = $("#saldo").data('saldo');

$(document).ready(function(){
    cargar_cuentas();
});

$("#retiro").keyup(function(event) {
    validate();
});

$("#retiro-cuenta").change(function(event) {
    validate();
});

$('#retiroModal').on('shown', function() {
    cargar_cuentas();
    $("#retiro").val("");
    $("#btn-retiro").prop('disabled', true);
})

$("#btn-retiro").click(function(){
    $("#modal-loading").modal("show");
    $.ajax({
        url: ruta + 'retiro',
        type: 'POST',
        dataType: 'json',
        data: {
                monto : $("#retiro").val(),
                cuenta_id : $("#retiro-cuenta").val(),
              },
        headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
    })
    .done(function(data) {
        var message = data.message;
        var saldo = data.saldo;
        $("#retiroModal").modal('hide');
        $("#modal-message").modal("show");
        $("#title-message").html('<i class="fa fa-check" aria-hidden="true"></i> Retiro Exitoso');
        $("#message").html(message);
        $("#saldo").html(numberWithCommas(saldo))
        balance = saldo;
    })
    .fail(function(data) {
        session_expired(data);

    })
    .always(function() {
        $("#modal-loading").modal("hide");
    });
});

function validate(){
    if($("#retiro").val()=="" || $("#retiro").val() <= 0 || $("#retiro-cuenta").val() == 0)
    {
        $("#btn-retiro").prop('disabled', true);
    }
    else{
        if($("#retiro").val() > balance)
        {
            $("#retiro-errors-monto").html("El monto a retirar supera su saldo disponible");
            $("#btn-retiro").prop('disabled', true);
        }
        else
        {
            $("#retiro-errors-monto").empty();
            $("#btn-retiro").prop('disabled', false);
        }

    }
}

function cargar_cuentas(){
    $.ajax({
        url: ruta + 'cuentasbancarias',
        type: 'GET',
        dataType: 'json',
    })
    .done(function(data) {
            $("#retiro-cuenta").html("<option value='0'>Seleccione la cuenta</option>");
            for (var i = 0; i < data.length; i++) {
                $("#retiro-cuenta").append("<option value='" + data[i].id + "'>" + data[i].banco.nombre + " - " + data[i].nrocuenta  +"</option>");
            }
    })
    .fail(function() {
        console.log("error");
    })
}