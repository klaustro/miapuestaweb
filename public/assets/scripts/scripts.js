//Comun file
var ruta = "/";
var img_loading = '<div class="text-center"><img class="img-responsive img-loading" src="../images/loading1.gif"></div>';
var tipoLogro = $("#tipoLogro").val();

$(".menu-nav").click(function (){
     $('li').removeClass('active');
     $(this).parent().addClass('active');
});

function format_date(input){
    var d = new Date(Date.parse(input.replace(/-/g, "/")));
    var today = new Date();
    var month = "0";
    var day = "0";
    var date = "";
    var hours = "0";
    var minutes = "0";
    var am = "AM";

    if (d.getDate() > 9)
    {
    	day = d.getDate();
    } else
    {
    	day += d.getDate();
    }

    if (d.getMonth()+1 > 9)
    {
    	month = d.getMonth()+1;
    } else
    {
    	month += d.getMonth()+1;
    }
	date = day + "-" + month + "-" + d.getFullYear();



    if (d.getHours() > 12)
    {
    	hours = d.getHours() - 12;
    	am = "PM";
    }
    else
    {
    	hours = d.getHours();
    }

    if(hours < 10)
    {
    	hours = "0" + hours;
    }

    if (d.getMinutes() < 9)
    {
    	minutes += d.getMinutes();
    }
    else
    {
    	minutes = d.getMinutes();
    }

    var time = hours + ":" + minutes + " " + am;
    return (date + " " + time);
};

function numberWithCommas(x) {
    return x.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function seleccion(games_probs){
    //console.log(games_probs);
    var cantidad = "";
    //Ganadores
    if(games_probs.tipo_probabilidad==1 || games_probs.tipo_probabilidad==8 || games_probs.tipo_probabilidad==11 || games_probs.tipo_probabilidad==15)
    {
        salida = games_probs.tipo_apuesta.nombre + ": " + games_probs.equipo.nombre + " (" + logro(games_probs.probabilidad, tipoLogro) + ")";
    }
    //Runlines
    else if (games_probs.tipo_probabilidad==3 || games_probs.tipo_probabilidad==7 || games_probs.tipo_probabilidad==12 || games_probs.tipo_probabilidad==16) {
        if (games_probs.cantidad == 0) {
            cantidad = "PK";
        }
        else
        {
            var signo = games_probs.games_probs_signo.signo;


            cantidad = signo + games_probs.cantidad;
        }

        salida = games_probs.tipo_apuesta.nombre + ": " + games_probs.equipo.nombre + " (" + cantidad + " " + logro(games_probs.probabilidad, tipoLogro) + ")";
    }
    //Totales
    else if(games_probs.tipo_probabilidad==4 || games_probs.tipo_probabilidad==10 || games_probs.tipo_probabilidad==13 || games_probs.tipo_probabilidad==17 || games_probs.tipo_probabilidad==18) {
        var signo = (games_probs.orden == 1) ? "+" : "-";
        salida = games_probs.tipo_apuesta.nombre + " (" + signo + games_probs.cantidad + " " + logro(games_probs.probabilidad, tipoLogro) + ")";
    }
    else if(games_probs.tipo_probabilidad==6 || games_probs.tipo_probabilidad==14){
        salida = games_probs.tipo_apuesta.nombre + " (" + logro(games_probs.probabilidad, tipoLogro) + ")";
    }
    else if(games_probs.tipo_probabilidad==9) {
        salida = games_probs.tipo_apuesta.nombre + ": ";
        salida += (games_probs.orden == 1) ? "SI" : "NO";
        salida += " " + logro(games_probs.probabilidad, tipoLogro) + ")";

    }
    return salida;
}

function session_expired(data){
    $(".modal").modal("hide");
    $("#modal-message").modal("show");
    if(data.status == 401)
    {
        $("#title-message").html("<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Sesión Expirada");
        $("#message").html(" Su sesión expiró, por favor inicie sesión nuevamente");
        $("#modal-message").on('hide.bs.modal', function () {
            window.location.href = "/login";
        });
    }
    else{
        $("#title-message").html("<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Error " + data.status);
        $("#message").html(data.statusText);
    }
}

$(".input-number").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
         // Allow: Ctrl+C
        (e.keyCode == 67 && e.ctrlKey === true) ||
         // Allow: Ctrl+X
        (e.keyCode == 88 && e.ctrlKey === true) ||
         // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

function carga_bancos(){
    if (bancos ==0 )
    {
        $.ajax({
            url: ruta +'banco',
            type: 'GET',
            dataType: 'json',
        })
        .done(function(data) {
            $(".banco").html("<option value='0'>Seleccione el banco</option>");
            for (var i = 0; i < data.length; i++) {
                $(".banco").append("<option value='" + data[i].id + "'>" + data[i].nombre +"</option>");
            }
            bancos = 1;
        })
        .fail(function(error) {
            
        })
        .always(function() {
        });
    }
}

function ticket(id, reprint){
    $.ajax({
        url: ruta + 'bet/' + id,
        type: 'GET',
        dataType: 'json',
    })
    .done(function(data) {
        var response = data.bet;
        var bets_detail = response.bets_detail
        var ganancia = 0;
        var monto = parseFloat(response.monto );
        //console.log(data);

        if (reprint == 1) 
        {
            $("#ticket").empty();
        }
        var content ="<table class='text-uppercase text-ticket'>"
                +   "<tr>"
                +       "<td><strong>Ticket #:</strong></td>"
                +       "<td>"+id+"</td>"
                +   "</tr>"
                +   "<tr>"
                +       "<td><strong>Fecha y hora:</strong></td>"
                +       "<td>"+ format_date(response.fecha_registro) +"</td>"
                +   "</tr>"

        for (var i = 0; i < bets_detail.length; i++) {
        var jugada = i + 1;
        content += "<tr>"
                +       "<td><strong>Evento # "+ jugada +":</strong></td>"
                +       "<td>" + bets_detail[i].games_probs.game.equipo[0].nombre + " - " + bets_detail[i].games_probs.game.equipo[1].nombre + "</td>"
                +   "</tr>"
                +   "<tr>"
                +       "<td><strong>jugada # "+ jugada +":</strong></td>"
                +       "<td>" + seleccion(bets_detail[i].games_probs) + "</td>"
                +   "</tr>";
                    if(i==0)
                        ganancia = monto * bets_detail[i].games_probs.probabilidad;
                    else            
                        ganancia = ganancia * bets_detail[i].games_probs.probabilidad;

        //console.log("acumulado" + response.monto +" * "+ bets_detail[i].games_probs.probabilidad)
        }
        //console.log(response.monto +" - "+ ganancia);
        content +=  "<tr>"
                +       "<td><strong>Monto:</strong></td>"
                +       "<td>"+ numberWithCommas(monto) +"</td>"
                +   "</tr>"
                +   "<tr>"
                +       "<td><strong>Total a Ganar:</td></strong>"
                +       "<td>"+ numberWithCommas(ganancia) +"</td>"
                +   "</tr>"
        content +="</table><br>";
        $("#ticket").append(content);
        if (reprint == 1) {
            print_ticket();
        }
        
    })
    .fail(function() {
        session_expired(data);
    })
    .always(function() {
        //console.log("complete");
    });
}

function print_ticket(){
    window.print();
    setTimeout(function () { window.close(); }, 100);
}

function logro(value, tipo){  
    if (tipo == 1){
         if (value >= 2) 
        {
            americano=(value-1)*100;
            mostrar = "+" + americano.toPrecision(3);
        }
        else
        {
           americano= -100/(value-1);
            if (probabilidad==0)
            {
                mostrar = '+' + americano.toPrecision(3);
            }
            else
            {
                mostrar = americano.toPrecision(3);
            }   
        }
    }
    else{        
        mostrar = value;
    }
    return mostrar;
}
//Admin file
$('#corredor').change(function (){
	if ($(this).val() == 1) {
		$('#corredor-comision').removeClass('hidden');
		$("#btn-update-user").prop('disabled', true);
	}
	else{
		$('#corredor-comision').addClass('hidden');
	}

});

function enableButton()
{
	if($('#tipoComision').val() != '' &&  $('#ventas').val() != '')
	{
		$("#btn-update-user").prop('disabled', false);
	}
	else{
		$("#btn-update-user").prop('disabled', true);
	}
}


//bets file
var bets = [];
var machos = [];
var total = 0;
var compuesta = 0;
var swNotify = 0;
var arr_games = [];
var forbidden = [];
var balance = $("#saldo").data('saldo');
var corredor = $("#saldo").data('corredor');
var swCompuesta = 0;
var ganancia = 0;

function reset(){
	$('[data-toggle="tooltip"]').tooltip();
	swCompuesta = 0;
	bets = [];
	machos = [];
	total = 0;
	compuesta = 0;
	swNotify = 0;
	$("#btn-bet").prop('disabled', false);
	$('.in').collapse('hide');
	if($("#bets-monto").val() > balance && corredor == 0)
	{
		$("#bet-errors-monto").html("Saldo insuficiente");
		$("#btn-bet").prop('disabled', true);
	}
	$(".sw-tipo").prop('checked', compuesta);
	$(".lbl-tipo-apuesta").html("  Directa");	
}

$(".sw-tipo").change(function(event) {
	if(bets.length>1){
		if ($(".lbl-tipo-apuesta").html() == "  Directa") {
			$(".lbl-tipo-apuesta").html("  Compuesta");
			compuesta = 1;
		} else {
			$(".lbl-tipo-apuesta").html("  Directa");
			compuesta = 0;
		}
		swCompuesta = 1;
		calcule_bets();
	}
	else{
		$("#bet-forbidden").modal("show");
		$("#forbiden-label").html("<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Apuesta Compuesta");
		$("#forbidden-message").html("Por favor seleccione al menos dos logros para realizar una apuesta compuesta");
		$(".sw-tipo").prop('checked', compuesta);
	}
});

$("#modal-bets").on('show.bs.modal', function () {
	$('#sw-tipo-apuesta-modal').prop('checked', compuesta);
});

$("#modal-bets").on('hide.bs.modal', function () {
	$('#sw-tipo-apuesta').prop('checked', compuesta);

});

function subcategorias(menu){
	if ($.trim($("#submenu"+menu).html())=='')
	{
		//$("#modal-loading").modal("show");
		$("#submenu"+menu).html(img_loading);

		$.ajax({
			url: ruta + 'submenu',
			type: 'GET',
			dataType: 'json',
			data: {'menu': menu},
		})
		.done(function(data) {
			//console.log(data[0]);
			var content = "";
			for (var i = 0; i < data.length; i++) {
				if (data[i].games_count >0){
					content +="<button class='btn btn-primary btn-sub' type='button' data-toggle='collapse' data-target='#collapse-submenu" + data[i].id + "' aria-expanded='false' aria-controls='collapse-submenu" + data[i].id + "' onClick='games(" + data[i].id + ")'>"
								+ data[i].nombre
								+"</button>"
								+"<div class='collapse talfi' id='collapse-submenu" + data[i].id + "'>"
									+"<div class='well col-md-11' id='games" + data[i].id + "'>"
									+"</div>"
								+"</div>";					
				}					
			$("#submenu"+menu).html(content);
			}
		})
		.fail(function(data) {
			session_expired(data);			
		})
		.always(function() {
			//console.log("complete");
			//$("#modal-loading").modal("hide");
		});
	}
}

function games(submenu){
	if ($.trim($("#games"+submenu).html())=='')
	{
		//$("#modal-loading").modal("show");
		$("#games"+submenu).html(img_loading);
		$.ajax({
			url: ruta + 'game',
			type: 'GET',
			dataType: 'json',
			data: {'submenu': submenu},
		})
		.done(function(data) {
			var swhora = 0;
			var content = "";
			var fecha = "0";
			//console.log(data);
			for (var i = 0; i < data.length; i++) {
				if (fecha != data[i].fecha_cierre)
				{
					if (fecha !=  "0")
						content += "</div>";
					content += "<div class='panel panel-warning'>"
									+"<div class='panel-heading'>"
										+"<h3 class='panel-title hora-game'>" + format_date(data[i].fecha_cierre) + "</h3>"
									+"</div>";
				}
				fecha = data[i].fecha_cierre

				content = content + "<div class='panel-body panel-game'>"
									+"<ul class='list-group'>"
										+"<li class='list-group-item disabled'>"
											+"<table width='100%'>"
												+"<tr>"
													+"<th width='55%'><div class='hidden-phone' title='" + data[i].id + "'>EQUIPO</div></th>"
													+"<th class='th-bets' width='15%'><div class='visible-phone'>G</div><div class='hidden-phone'>GANADOR</div></th>"
													+"<th class='th-bets' width='15%'><div class='visible-phone'>T</div><div class='hidden-phone'>TOTALES</div></th>"
													+"<th class='th-bets' width='15%'><div class='visible-phone'>RL</div><div class='hidden-phone'>RUNLINE</div></th>"
												+"</tr>"
											+"</table>"
										+"</li>";
											$(".td-bet").click(function(){
		var id = $(this).data("id");
		var idgame = $(this).data("game");
		var prob = $(this).data("prob");
		var detalle = $(this).data("detalle");
		var tipo = $(this).data("tipo");

		saveBet(id, idgame, prob, detalle, tipo);
	});

				for (var j = 0; j < data[i].equipo.length; j++) {
					content += 	"<li class='list-group-item'>"
									+"<table width='100%' class='table-bets'>"
										+"<tr>"
											+"<td width='55%'><strong class='text-uppercase'>" + data[i].equipo[j].nombre + "</strong></td>"
											+"<td class='td-bet bet-off' width='15%' id='" + getId(data[i].games_probs, data[i].equipo[j].id, 1) 
												+ "' onClick='selectBet("+ getId(data[i].games_probs, data[i].equipo[j].id, 1) 
												+ ", " + data[i].id 
												+ ", "+ probabilidad(data[i].games_probs, data[i].equipo[j].id, 1)  
												+ ", \""+ data[i].equipo[0].nombre.substring(0, 3) + " - " + data[i].equipo[1].nombre.substring(0, 3) + " GANADOR " + data[i].equipo[j].nombre + " " + probabilidad_text(data[i].games_probs, data[i].equipo[j].id, 1) + "\""
												+ ", 1)'>"
												+ probabilidad_text(data[i].games_probs, data[i].equipo[j].id, 1) + "</a>"
											+"</td>"

											+"<td class='td-bet bet-off' width='15%' id='" + getId(data[i].games_probs, data[i].equipo[j].id, 4) 
												+ "' onClick='selectBet("+ getId(data[i].games_probs, data[i].equipo[j].id, 4) 
												+ ", " + data[i].id 
												+ ", "+ probabilidad(data[i].games_probs, data[i].equipo[j].id, 4)  
												+ ", \""+ data[i].equipo[0].nombre.substring(0, 3) + " - " + data[i].equipo[1].nombre.substring(0, 3) + " TOTALES " + data[i].equipo[j].nombre + " " + probabilidad_text(data[i].games_probs, data[i].equipo[j].id, 4) + "\""
												+ ", 4)'>"
												+ probabilidad_text(data[i].games_probs, data[i].equipo[j].id, 4)
											+"</td>"

											+"<td class='td-bet bet-off' width='15%' id='" + getId(data[i].games_probs, data[i].equipo[j].id, 3) 
												+ "' onClick='selectBet("+ getId(data[i].games_probs, data[i].equipo[j].id, 3) 
												+ ", " + data[i].id 
												+ ", "+ probabilidad(data[i].games_probs, data[i].equipo[j].id, 3)  
												+ ", \""+ data[i].equipo[0].nombre.substring(0, 3) + " - " + data[i].equipo[1].nombre.substring(0, 3) + " RUNLINE " + data[i].equipo[j].nombre + " " + probabilidad_text(data[i].games_probs, data[i].equipo[j].id, 3) + "\""
												+ ", 3)'>"
												+ probabilidad_text(data[i].games_probs, data[i].equipo[j].id, 3)
											+"</td>"											
											
										+"</tr>"
									+"</table>"
								+"</li>";
				}
				for (var j = 0; j < data[i].games_probs.length; j++) {
					//console.log(data[i].games_probs[j]);
					if(data[i].games_probs[j].tipo_probabilidad == '6')
					{
						content += 	"<li class='list-group-item'>"
										+"<table width='100%' class='table-bets'>" 
											+"<tr>"
												+"<td width='55%'><strong>EMPATE</strong></td>"
												+"<td class='td-bet bet-off' width='15%' id='" + data[i].games_probs[j].id 
													+ "' onClick='selectBet("+ data[i].games_probs[j].id
													+ ", " + data[i].id
													+ ", "+ data[i].games_probs[j].probabilidad
													+ ", \""+ data[i].equipo[0].nombre.substring(0, 3) + " - " + data[i].equipo[1].nombre.substring(0, 3) + " EMPATE "+ data[i].games_probs[j].probabilidad + "\""
													+ ", 6)'>"
													+ logro(data[i].games_probs[j].probabilidad, tipoLogro)
												+"</td>"													
												//+"<td class='td-bet bet-off' width='15%' id='" + data[i].games_probs[j].id + "' data-id='" + data[i].games_probs[j].id + "' data-prob='" + data[i].games_probs[j].probabilidad + "' data-detalle='" + data[i].equipo[0].nombre.substring(0, 3) + " - " + data[i].equipo[1].nombre.substring(0, 3) + " EMPATE "+ data[i].games_probs[j].probabilidad + "' data-game='" + data[i].id + "' data-tipo='6'>" +  data[i].games_probs[j].probabilidad  + "</td>"
												+"<td rowspan='2' width='30%'></td>"
											+"</tr>"
										+"</table>"
									+"</li>"
					}
				}

				if (data[i].exoticas_count != null)
				{
					content +="<button class='btn btn-default btn-xs' type='button' data-toggle='collapse' data-target='#collapse-exoticas"+ data[i].id +"' aria-expanded='false' aria-controls='collapse-exoticas"+ data[i].id +"' onClick='exoticas(" + data[i].id + ")'>"
								+"Exóticas <span class='badge badge-warning'>+" + data[i].exoticas_count.count + "</span>"
							+"</button>"
						+"<div class='collapse' id='collapse-exoticas"+ data[i].id +"'>"
							+"<div class='well col-md-11 col-xs-11' id='well-exoticas" + data[i].id + "'>"
							+"</div>"
						+"</div>"
				}

				content +="</ul>"
					+"</div>";
	}
		$("#games"+submenu).html(content);

		})
		.fail(function(data) {
			session_expired(data);			
		})
		.always(function() {
			//console.log("complete");
			//$("#modal-loading").modal("hide");
		});
	}
}



function selectBet(id, idgame, prob, detalle, tipo)	{
	saveBet(id, idgame, prob, detalle, tipo);
}

function exoticas(idgame){
	if ($.trim($("#well-exoticas"+idgame).html())=='')
	{
		//$("#modal-loading").modal("show");
		$("#well-exoticas"+idgame).html(img_loading);
		$.ajax({
			url: ruta+'games_prob',
			type: 'GET',
			dataType: 'json',
			data: {'idgame': idgame},
		})
		.done(function(data) {
			var content = "";
			var evento = data[0].equipo.nombre.substring(0, 3) + " - " + data[0].equipo.nombre.substring(0, 3);
			for (var i = 0; i < data.length; i++) {
				/*Descripcon de equipos*/
				if (data[i].tipo_probabilidad == 7 || data[i].tipo_probabilidad == 8 || data[i].tipo_probabilidad == 11 || data[i].tipo_probabilidad == 12 || data[i].tipo_probabilidad == 15 || data[i].tipo_probabilidad == 16)
					equipo = data[i].equipo.nombre;
				else if (data[i].tipo_probabilidad == 10 || data[i].tipo_probabilidad == 13 || data[i].tipo_probabilidad == 14 || data[i].tipo_probabilidad == 17 || data[i].tipo_probabilidad == 18)
					equipo = "";
				else if (data[i].tipo_probabilidad == 9)
				{
					equipo = "SI";
					if (data[i].orden == 2)
						equipo = "NO";
				}

				/*Cantidades*/
				if (data[i].tipo_probabilidad == 1 || data[i].tipo_probabilidad == 8 || data[i].tipo_probabilidad == 9 || data[i].tipo_probabilidad == 11 || data[i].tipo_probabilidad == 14 || data[i].tipo_probabilidad == 15)
				{
					cantidad = "";
				}
				else
				{
					cantidad = data[i].cantidad;
				}

				/*Signos*/
				if (data[i].tipo_probabilidad == 10 || data[i].tipo_probabilidad == 13 || data[i].tipo_probabilidad == 17 || data[i].tipo_probabilidad == 18)
				{
					signo = "+";
					if(data[i].orden == 2)
						signo = "-";
				}
				else if (data[i].tipo_probabilidad == 7 || data[i].tipo_probabilidad == 12 || data[i].tipo_probabilidad == 16)
				{
					signo = data[i].games_probs_signo.signo;
					if(data[i].cantidad == '0'){
						signo = 'PK ';
						cantidad = '';
					}
				}
				else if (data[i].tipo_probabilidad == 16)
				{
					signo = "+";
					if(data[i].orden == 2)
						signo = "-";
				}
				else
				{
					signo = "";
				}
				var detalle = "\"" + evento + " " + data[i].tipo_apuesta.nombre + " " +  equipo + " " + signo + cantidad + " " +  parseFloat(data[i].probabilidad).toFixed(2) +"\"" ;
				var whereClick =  "onClick='selectBet("
									+ data[i].id
									+ ", " + data[i].idgame
									+ ", " + data[i].probabilidad
									+ ", " + detalle
									+ ", " + data[i].tipo_probabilidad
									+")'";				
				if (data[i].orden == 1)
				{									
					content += "<ul class='list-group list-exoticas col-md-3 col-xs-11'>"
									+"<li class='list-group-item disabled text-uppercase'>"+data[i].tipo_apuesta.nombre+"</li>"
									+"<li class='list-group-item li-exotica li-exotica-off text-uppercase' id='" + data[i].id + "' " + whereClick + ">"
									+ equipo + " <strong>" + signo + cantidad + "</strong> " + logro(parseFloat(data[i].probabilidad).toFixed(2), tipoLogro)									
									+ "</li>"
				}
				else
				{
					content += "<li class='list-group-item li-exotica bet-off text-uppercase' id='" + data[i].id + "' " + whereClick + "'>"
								+ equipo + " <strong>" + signo + cantidad + "</strong> " + logro(parseFloat(data[i].probabilidad).toFixed(2), tipoLogro)
							+ "</li>"
							+"</ul>";
				}
			}

			$("#well-exoticas"+idgame).html(content);
		})
		.fail(function() {
			session_expired(data);			
		})
		.always(function() {
			//console.log("complete");
			//$("#modal-loading").modal("hide");
		});
	}
}

function get_macho(games_probs){
	var prob =0;
	var equipo = 0;
	for (var i = 0; i < games_probs.length; i++) {
		if (games_probs[i].tipo_probabilidad == 1)
		{
			if (equipo ==0)
			{
				equipo = games_probs[i].equipo;
				prob = games_probs[i].probabilidad;
			}
			else
			{
				if (games_probs[i].probabilidad < prob)
				{
					equipo = games_probs[i].equipo;
				}
			}
		}
	}
	if (no_repetido(games_probs[0].idgame) == 0) {
		macho_game = new macho(games_probs[0].idgame, equipo);
		machos.push(macho_game);
	}

	return equipo;
}

//CONSTRUCTORS
function macho(idgame, equipo) {
    this.idgame = idgame;
    this.equipo = equipo;
}

function bet(id, idgame, probabilidad, monto, tipo, detalle) {
    this.id = id;
    this.idgame = idgame;
    this.probabilidad = probabilidad;
    this.monto = monto;
    this.tipo = tipo;
    this.detalle = detalle;
}

function bet_forbidden(id, message){
	this.id = id;
	this.message = message;
}

function no_repetido(idgame) {
    resp = 0;
    //console.log(idgame);
    for (i = 0; i < machos.length; i++) {
        if (machos[i].idgame == idgame)
            resp = 1;
    }
    return resp;
}

function probabilidad_text(games_probs, equipo, tipo){
	var value = ""

	vmacho = get_macho(games_probs);

	for (var i = 0; i < games_probs.length; i++) {

		if (games_probs[i].equipo == equipo && games_probs[i].tipo_probabilidad == tipo)
		{
			var cantidad = "";
			var signo = "";
			//console.log(games_probs[i]);
			if (tipo == 3){
				signo = games_probs[i].games_probs_signo.signo;
			if(games_probs[i].cantidad == '0')
				signo = "PK";
			}

			if(tipo==4)
			{
				signo = "+";
				if(games_probs[i].orden == 2)
					signo = "-";
			}

			if (games_probs[i].cantidad >0)
				cantidad = games_probs[i].cantidad;
			var value = signo + cantidad + " "  + logro(parseFloat(games_probs[i].probabilidad).toFixed(2), tipoLogro);
		}
	}
	return value;
}

function probabilidad(games_probs, equipo, tipo){
	var value = 0

	for (var i = 0; i < games_probs.length; i++) {
		if (games_probs[i].equipo == equipo && games_probs[i].tipo_probabilidad == tipo)
			var value =  games_probs[i].probabilidad;
	}
	return value;
}

function getId(games_probs, equipo, tipo){
		prob =0;

		for (var i = 0; i < games_probs.length; i++) {
			if (games_probs[i].equipo == equipo && games_probs[i].tipo_probabilidad == tipo)
			{
				var value = games_probs[i].id;
			}
		}
	return value;
}

$(".bets-monto").keyup(function(event) {
	if($(this).val() > balance && corredor == 0)
	{
		$("#bet-errors-monto").html("Saldo insuficiente");
		$("#btn-bet").prop('disabled', true);
	}
	else
	{
		$("#bet-errors-monto").html("");
		$("#btn-bet").prop('disabled', false);
		$(".bets-monto-simple").val($(this).val())
		for (var i = 0; i < bets.length; i++) {
			bets[i].monto =$(this).val();
		}
		calcule_bets();

	}

});

function monto_simple(id, probabilidad){
	var monto = $("#bets-monto-"+id).val();
	for (var i = 0; i < bets.length; i++){
		if(bets[i].id == id)
			bets[i].monto = monto;
	}
	$("#ganancia"+id).html(numberWithCommas(monto*probabilidad));
	calcule_bets();
}

function rules(idgame, tipo, idgames_probs){
	var count_bets = 0;
	var tipos_apuesta = [];
	var repetido = 0;

	for (var i = 0; i < bets.length; i++) {
		if(bets[i].idgame == idgame)
		{
			count_bets += 1;
			tipos_apuesta.push(bets[i].tipo);
			if(bets[i].tipo == tipo)
				repetido += 1;
		}
	}

	if(count_bets > 0)
	{
		if(repetido>1)
		{
			saveBet(idgames_probs,0,0,0);
			$("#bet-forbidden").modal("show");
			$("#forbiden-label").html("<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Logro Repetido");
			$("#forbidden-message").html("No puede jugar el mismo tipo de apuesta en el mismo juego");
		}
		else{
			$.ajax({
				url: ruta+'regla',
				type: 'GET',
				dataType: 'json',
				data: {
						game: idgame,
						'tipos_apuesta[]' : tipos_apuesta
					  },
			})
			.done(function(data) {
				var message = data.message;
				//console.log(message);
				if(message != "Valida")
				{
					saveBet(idgames_probs,0,0,0);
					$("#bet-forbidden").modal("show");
					$("#forbiden-label").html("<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Apuesta Prohibida");
					$("#forbidden-message").html(message);

				}
			})
			.fail(function() {
				session_expired(data);				
			})
			.always(function() {
				//console.log("complete");
			});
		}
	}
}

function saveBet(id, idgame, prob, detalle, tipo){
	var index = -1;
	var monto = $("#bets-monto-modal").val()
	for (var i = 0; i < bets.length; i++){
		if(bets[i].id == id)
			index = i;
	}

	if (index > -1)
	{
    	bets.splice(index, 1);
		$("#"+id).removeClass('bet-on');
		$("#"+id).addClass('bet-off');
		$("#alert-"+id).remove();
		if(bets.length == 0)
			$("#modal-bets").modal("hide");
		if(bets.length == 1){
			compuesta = 0;
			$(".sw-tipo").prop('checked', compuesta);
			$(".lbl-tipo-apuesta").html("  Directa");
		}
	}
	else
	{
		new_bet = new bet(id, idgame, prob, monto, tipo, detalle);
		bets.push(new_bet);

		$("#bets-detalle").append("<div class='alert alert-warning alert-dismissible alert-bet' role='alert' id='alert-" + id + "'>"
								+"  <button type='button' class='close' data-dismiss='alert' aria-label='Close' onClick='saveBet("+id+",0,0,0)'><span aria-hidden='true'>&times;</span>"
								+"  </button>"
								+   "  <div class='pull-left text-uppercase text-bet'><b>" + detalle + "</b></div>"
								+ "<div class='pull-right'><input type='number' placeholder='Monto' class='bets-monto-simple input-number' id='bets-monto-" + id + "' onKeyup='monto_simple(" + id + ", " + prob + ")' value='" + monto + "'/>"
								+ "<b class='bets-monto-simple' id='ganancia"+ id +"'>" + prob*monto + "</b></div><div class='clearfix'></div>"
								+"</div>");
		$("#"+id).removeClass('bet-off');
		$("#"+id).addClass('bet-on');

		if (compuesta == 1)
		{
			$(".bets-monto-simple").addClass('hidden');
			rules(idgame, tipo, id);
		}
	}
	calcule_bets();
}

function calcule_bets(){
	total = 0;
	arriesgado = 0;
	$("#bets-arriesgado").empty();
	if (compuesta == 0)
	{
		for (var i = 0; i < bets.length; i++) {
				total += bets[i].probabilidad * bets[i].monto;
				arriesgado += bets[i].monto*1;
				$("#ganancia"+bets[i].id).html(numberWithCommas(bets[i].monto*bets[i].probabilidad));
		}
		$(".bets-monto-simple").removeClass('hidden');
		$("#bets-arriesgado").html("Monto arriesgado: " + numberWithCommas(arriesgado));
	}
	else
	{
		for (var i = 0; i < bets.length; i++) {
			if(i==0)
				total = bets[i].probabilidad * bets[i].monto;
			else
				total = total * bets[i].probabilidad;
		}

		$(".bets-monto-simple").addClass('hidden');
	}

	$(".bets-total").html(numberWithCommas(total));
	ganancia = total;

	if (bets.length == 1 )
	{
		message = "<strong>" + bets.length + "</strong> logro, con una ganancia posible de <strong>" + numberWithCommas(total) + "</strong>";
	}
	else
	{
		message = "<strong>" + bets.length + "</strong> logros, con una ganancia posible de <strong>" + numberWithCommas(total) + "</strong>";
	}
	//console.log('swNotify:' + swNotify + ' bets.length: ' + bets.length);
	if (swNotify == 0 && bets.length > 0)
	{
		notify =$.notify({
							message: message
									+" <button class='btn btn-primary btn-text' data-toggle='modal' href='#modal-bets'>"
										+"<i class='fa fa-caret-square-o-right' aria-hidden='true'></i> "
										+"Continuar"
									+"</button>"
						},
						{
							type: "warning",
							delay: 0,
							allow_dismiss: false,
							placement:
								{
									from: "bottom",
									align: "center"
								}
						});
	swNotify = 1;
	}
	else
	{
		if(bets.length == 0)
		{
			notify.close();
			swNotify = 0;
		}
		else
		{
					notify.update("message", message
						+" <button class='btn btn-primary btn-text' data-toggle='modal' href='#modal-bets'>"
							+"<i class='fa fa-caret-square-o-right' aria-hidden='true'></i> "
							+"Continuar"
						+"</button>");
		}
	}
}

function make_bet(){
	$("#btn-bet").prop('disabled', true);
	$("#ticket").empty();
	$("#modal-loading").modal("show");
		//console.log(compuesta);
	$.ajax({
		url: ruta+'bet',
		type: 'POST',
		dataType: 'json',
		data: {
				compuesta : compuesta,
				bets : bets,
				ganancia : ganancia
			   },
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        },
	})
	.done(function(data) {
		var title = data.title;
		var message = data.message;
		var saldo = data.saldo;
		var id = data.id;
		if(title == "valida"){
			$("#modal-bets").modal("hide");
			$("#modal-loading").modal("hide");
			$("#bet-result").modal("show");
			$("#bet-message").html(message);

			for (var i = 0; i < id.length; i++) {
				ticket(id[i], 0);
			}

			bets = [];
			$(".td-bet").removeClass('bet-on').addClass('bet-off');
			$("#bets-count").html("");
			$(".alert-bet").remove();
			notify.close();
			$("#saldo").html("<i class='fa fa-money' aria-hidden='true'></i> " + numberWithCommas(saldo))
			balance = saldo;
		}
		else{
			$("#modal-loading").modal("hide");
			$("#bet-forbidden").modal("show");
			$("#forbiden-label").html("<i class='fa fa-exclamation-circle' aria-hidden='true'></i> " + title);
			$("#forbidden-message").html(message);
			$("#modal-bets").modal("hide");
			notify.close();
			compuesta = 0;
		}
		reset();
	})
	.fail(function(data) {
		//session_expired(data);		
	})
	.always(function() {
		//console.log("complete");
	});
}

$(".input-number").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
         // Allow: Ctrl+C
        (e.keyCode == 67 && e.ctrlKey === true) ||
         // Allow: Ctrl+X
        (e.keyCode == 88 && e.ctrlKey === true) ||
         // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});
//Carousel file
function startCamera() 
{
	$('#camera_wrap').camera({ 
		fx: 'scrollLeft', time: 2000, loader: 'none', playPause: false, navigation: true, height: '35%', pagination: true
	});
}

$(function() {
	startCamera();
});
//Contacto file
function contacto() {	
	var name = $('#name').val() ;
	var email = $('#email').val() ;
	var content = $('#content').val() ;
	$.ajax({
		url: ruta+'contacto',
		type: 'POST',
		dataType: 'json',
		data: { name: name, 
				email: email, 
				content: content
			},
        headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },			
	})
	.done(function(data) {
		console.log(data);
		$('#title-message').html('Contacto MiApuestaWeb');
		$('#message').html(data.message);
		$('#modal-message').modal('show');	
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
        $("#modal-message").on('hide.bs.modal', function () {
            location.reload();
        });	
	});
}

//Cuentas file
var bancos = 0;
var idcuenta = 0;

function cargar(){
	$("#panel-cuentas").html(img_loading);
	carga_bancos();
	$.ajax({
		url: ruta + 'cuentasbancarias',
		type: 'GET',
		dataType: 'json',
	})
	.done(function(data) {
		var content = "";
		for (var i = 0; i < data.length; i++) {
			content += "<div class='panel panel-default panel-ticket'>"
							+"<div class='panel-heading height-bet'><strong id='idbanco' data-banco_id='" + data[i].banco_id + "'>Banco </strong>" + data[i].banco.nombre +"</div>"
							+"<div class='panel-body'>"
								+"<ul class='list-group'>"
									+"<p class='pull-left'><strong>Número de Cuenta "+data[i].tipo+":</strong> <text id='text-nrocuenta'>" + data[i].nrocuenta + "</p>"
									+"<p class='pull-right'>"
										+"<button class='btn btn-info btn-xs' title='Editar cuenta' onclick='edit(" + data[i].id + ")' data-toggle='popover'><i class='fa fa-edit' aria-hidden='true'></i></button>"
										+" <button class='btn btn-danger btn-xs' title='Eliminar cuenta' onclick='confirmation(" + data[i].id + ")' data-toggle='popover'><i class='fa fa-trash' aria-hidden='true'></i></button>"
									+"</p><div class='clearfix'></div>"
								+"</ul>"
							+"</div>"
						+"</div>";
		}
		$("#panel-cuentas").html(content);
	})
	.fail(function(data) {
	//	session_expired(data);
	})
	.always(function() {
		console.log("complete");
	});

    $("#banco-cuenta").change(function(event) {
        validate();
    });
    $("#nrocuenta").keyup(function(event) {
        validate();
    });
    $("#tipo-cuenta").change(function(event) {
        validate();
    });
	$('[data-toggle="tooltip"]').tooltip()	

}

function edit(id){
	idcuenta = id;
	$("#btn-cuenta").prop('disabled', false);
	$.ajax({
		url: ruta + 'cuentasbancarias/'+id,
		type: 'GET',
		dataType: 'json'
	})
	.done(function(data) {
		$("#cuentasModal").modal("show");
		var banco_id = data.banco_id;
		var nrocuenta = data.nrocuenta;
		var tipo = data.tipo;
		$("#banco-cuenta").val(banco_id);
		$("#nrocuenta").val(nrocuenta);
		$("#tipo-cuenta").val(tipo);
		$(".title").html("Modificar Cuenta");

	})
	.fail(function(data) {
		session_expired(data);
	})
	.always(function() {
		console.log("complete");
	});
}

function confirmation(id){
	idcuenta = id;
	$("#modal-confirmation").modal("show");
	$("#title-confirmation").html('<i class="fa fa-bank" aria-hidden="true"></i> Eliminar Cuenta');
	$("#message-confirmation").html("Seguro que desea eliminar ésta cuenta");
}

function borrar(){
	$.ajax({
		url: ruta + 'cuentasbancarias/' + idcuenta,
		type: 'DELETE',
		dataType: 'json',
        headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
	})
	.done(function(data) {
		var title = data.title;
		var message = data.message;
		$("#modal-confirmation").modal('hide');
		$("#modal-message").modal("show");
		$("#title-message").html('<i class="fa fa-bank" aria-hidden="true"></i> "' + title + '"');
		$("#message").html(message);
		cargar();
	})
	.fail(function(data) {
		session_expired(data);
	})
	.always(function() {
		console.log("complete");
	});

}

function add(){
	$("#btn-cuenta").prop('disabled', true);
	$("#banco-cuenta").val(0);
	$("#tipo-cuenta").val(0);
	$("#nrocuenta").val("");
	$(".title").html("Registrar Cuenta");
	$("#cuentasModal").modal("show");
}

function register(id){
	var banco = $("#banco-cuenta").val();
	var nrocuenta = $("#nrocuenta").val();
	var tipo = $("#tipo-cuenta").val();
	if ($(".title").html() == "Registrar Cuenta") {
		$.ajax({
			url: ruta + 'cuentasbancarias',
			type: 'POST',
			dataType: 'json',
			data: {
				banco : banco,
				nrocuenta: nrocuenta,
				tipo: tipo
			},
	        headers: {
	                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                 },
		})
		.done(function(data) {
		    var title = data.title;
		    var message = data.message;
		    $("#cuentasModal").modal('hide');
		    $("#modal-message").modal("show");
		    $("#title-message").html('<i class="fa fa-bank" aria-hidden="true"></i> ' + title);
		    $("#message").html(message);
		    cargar();
		})
		.fail(function(data) {
			session_expired(data);
		})
		.always(function() {
			console.log("complete");
		});
	}
	else{
		$.ajax({
			url: ruta + 'cuentasbancarias/' + idcuenta,
			type: 'PUT',
			dataType: 'json',
			data: {
				banco : banco,
				nrocuenta: nrocuenta,
				cuenta: tipo
			},
	        headers: {
	                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                 },
		})
		.done(function(data) {
		    var title = data.title;
		    var message = data.message;
		    $("#cuentasModal").modal('hide');
		    $("#modal-message").modal("show");
		    $("#title-message").html('<i class="fa fa-bank" aria-hidden="true"></i> "' + title + '"');
		    $("#message").html(message);
		    cargar();
		})
		.fail(function(data) {
			session_expired(data);
		})
		.always(function() {
			console.log("complete");
		});

	}


}

    function validate(){
        if($("#nrocuenta").val().length < 20 || $("#banco-cuenta").val() == 0 || $("#tipo-cuenta").val() == 0)
        {
            $("#btn-cuenta").prop('disabled', true);
        }
        else{
            $("#btn-cuenta").prop('disabled', false);
        }
    }

	$(".input-number").keydown(function (e) {
	    // Allow: backspace, delete, tab, escape, enter and .
	    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
	         // Allow: Ctrl+A
	        (e.keyCode == 65 && e.ctrlKey === true) ||
	         // Allow: Ctrl+C
	        (e.keyCode == 67 && e.ctrlKey === true) ||
	         // Allow: Ctrl+X
	        (e.keyCode == 88 && e.ctrlKey === true) ||
	         // Allow: home, end, left, right
	        (e.keyCode >= 35 && e.keyCode <= 39)) {
	             // let it happen, don't do anything
	             return;
	    }
	    // Ensure that it is a number and stop the keypress
	    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
	        e.preventDefault();
	    }
	});
//Forgot file
function forgot(){
	$.ajax({
		url: ruta + 'password/email',
		type: 'POST',
		dataType: 'json',
        headers: {
        	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },		
		data: {'email' : $('#email1').val()},
	})
	.done(function(data) {
		console.log(data);
	})
	.fail(function(response) {
		console.log("error");
		console.log(response);
	})
	.always(function() {
		console.log("complete");
	});
	
}
//Historial file
var weeks = 0;
var status = 0;
var page = 1;
var ticket_id = "";
var corredor = $("#corredor").val();

function searchHistory()
{
	ticket_id = $("#ticket-id").val();
	weeks = $("#select-weeks").val();
	status =  $("#select-status").val();
	cargarHistory(weeks, status, page, ticket_id);
	$("#ticket-id").val('');
	console.log('corredor = ' + corredor);
}

function cargarHistory(weeks, status, current_page, ticket_id){
	swView();
	$("#panel-historial").html(img_loading);
	$.ajax({
		url: ruta + 'history?page='+ current_page,
		type: 'GET',
		dataType: 'json',
		data:{
			'weeks' : weeks,
			'status' : status,
			'id': ticket_id
		}
	})
	.done(function(data) {
		response = data.bets.data;
		balance = data.balance;
		totales = data.totales;
		rango = data.rango;
		//console.log(data);
		var content ="";
		if(response.length >0){
				 content +="<div class='accordion-group panel-ticket center-block'>"
								+"<div class='accordion-heading'>"
									+"<a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion2' href='#collapse1' onclick=''>"
									+ "<h6 class='orange-txt text-center'><i class='fa fa-plus-circle' aria-hidden='true'></i> Balance Semanal del " + rango + "</h6>"
									+"</a>"
								+"</div>"
								+"<div id='collapse1' class='accordion-body collapse'>"
									+"<div class='accordion-inner'>"
						var total = 0;
						for (var i = 0; i < balance.length; i++)
						{
							content += "<ul class='list-group col-md-2 list-balance'>"
										    +"<li class='list-group-item disabled text-center'><strong>" + balance[i].dia + "</strong></li>"
										    +"<li class='list-group-item'><strong>Utilidad: </strong>" + balance[i].gana + "</li>";
							if (corredor == 0) {
								content +=  "<li class='list-group-item'><strong>Entradas / Salidas: </strong>" + balance[i].movimiento + "</li>"
										    +"<li class='list-group-item'><strong>Balance: </strong>" + balance[i].saldo + "</li>";
							}
							else{
								content += "<li class='list-group-item'><strong>Ventas: </strong>" + balance[i].ventas + "</li>"
										    +"<li class='list-group-item'><strong>Comisión: </strong>" + balance[i].comision + "</li>"	;						
							}

							content += 	"</ul>";
						}
						content += "<ul class='list-group col-md-2 list-balance'>"
									    +"<li class='list-group-item disabled text-center active'><strong>TOTAL SEMANAL</strong></li>"
									    +"<li class='list-group-item'><strong>Utilidad: " + totales.gana + "</strong></li>";
					    	if (corredor == 0) {
							content += 		"<li class='list-group-item'><strong>Entradas / Salidas: " + totales.movimiento + " </strong></li>"
										    +"<li class='list-group-item'><strong>Balance: " + totales.saldo + "</strong></li>";
					    	} 
					    	else {
							content += 		"<li class='list-group-item'><strong>Ventas: " + totales.ventas + "</strong></li>"
										    +"<li class='list-group-item'><strong>Comisión: " + totales.comision + "</strong></li>"					    		
					    	}

						content += 		"</ul>";
						content +="</div>"
							+"</div>"
							+"<div class='clearfix'></div>"
						+"</div>";

			for (var i = 0; i < response.length; i++) {
			var ganancia = 0;
			content += "<div class='panel panel-default panel-ticket'>"
				+"<div class='panel-heading height-bet'><strong>Ticket </strong>#" + response[i].id 
				+ "<p class='pull-right'><strong>Fecha: </strong> " + format_date(response[i].fecha_registro) + "</p><div class='clearfix'></div></div>"
				+"<div class='panel-body'>"
					+"<ul class='list-group'>";
				var bets_detail = response[i].bets_detail;			
				for (var j = 0; j < bets_detail.length; j++) {
					var class_badge = "";
					var txt_badge = "";
					var resultado = " - ";					
					if (bets_detail[j].games_probs.game.status==0) {
						txt_badge = "Pendiente";
					}
					else{
						if(bets_detail[j].games_probs.acierto == 1){
							var class_badge = "badge-success";
							var txt_badge = "Acertó";
						}
						else{
							var class_badge = "badge-important";
							var txt_badge = "No acertó";
						}
						if (bets_detail[j].games_probs.game.resultado.length >1) {
							resultado = "<strong> " + bets_detail[j].games_probs.game.resultado[0].score + "- " + bets_detail[j].games_probs.game.resultado[1].score + "</strong> ";
						}

					}
					content += "<li class='list-group-item height-bet text-uppercase'><span class='badge " + class_badge + " text-capitalize'>" + txt_badge + "</span>" + bets_detail[j].games_probs.game.equipo[0].nombre + resultado + bets_detail[j].games_probs.game.equipo[1].nombre + " | <strong>" + seleccion(bets_detail[j].games_probs) + "</strong></li>"
					ganancia = ganancia + (response[i].monto * bets_detail[j].games_probs.probabilidad);
				}
				var button = "";
				var comision = "";
				if (corredor == 1) {
					//comision = ' <strong>Comision:</strong> ' + numberWithCommas(parseFloat(response[i].comision))
					if (response[i].status==2){
						button = " <button class='btn btn-primary btn-text btn-xs' onClick='pay(" + response[i].id + ")'><i class='fa fa-dollar' aria-hidden='true'></i> Pagar</button>";
					} 
					if (response[i].status==5) {
						button = " <span class='badge badge-warning text-capitalize'>Pagada</span>" ;
					}
					if (response[i].status==1){
						var button = " <button class='btn btn-primary btn-text btn-xs' onClick='ticket(" + response[i].id + ", 1)'><i class='fa fa-print' aria-hidden='true'></i> Imprimir</button>";			
					}
				} 

				content += "</ul>"
				+"</div>"
				+"<div class='panel-footer height-bet'>"
					+"<strong>Monto Arriesgado: </strong>" + numberWithCommas(parseFloat(response[i].monto)) 
					+comision
					+ " <p class='pull-right'>"
						+"<strong>A Ganar: </strong>" + numberWithCommas(parseFloat(response[i].ganancia)) 
						+button
					+"</p>"
					+"<div class='clearfix'></div></div>"
			+"</div>";
			}

			if(data.bets.last_page > 1)
			{
				previus = current_page - 1;
				next = current_page + 1;
				content += "<div class='pagination pagination-small text-center'>"
	                    +"<ul>";
	            if (current_page != 1)
	            	content +=	"<li><a class='disabled' href='##' onClick='cargarHistory(" + weeks + ", " + status + ", " + previus + ")'>«</a></li>";

	            for (var i = 1; i <=  data.bets.last_page; i++) {
	            	active = "";
	            	if(current_page == i)
	            		active = "btn-primary";
	            	content +=	 "<li><a class='" + active + "' href='##' onClick='cargarHistory(" + weeks + ", " + status + ", " + i + ")'>" + i + "</a></li>"
	            }
	            if (current_page != data.bets.last_page)
	            	content +="<li><a href='##' onClick='cargarHistory(" + weeks + ", " + status + ", " + next + ")'>»</a></li>";

	            content +="</ul>"
	                +"</div>";
	            content += "<p class='text-center rows'>Desde " + data.bets.from + " hasta " + data.bets.to + " de " + data.bets.total + " apuestas</p>";
			}
		}
		else {
			var txtWeek = "en ésta semana";
			if(weeks > 1)
				txtWeek = "de hace "+  weeks + " Semanas";

			content = "<div class='alert alert-error'>No tiene apuestas registradas " + txtWeek + "</div>"
		}


		$("#panel-historial").html(content);
	})
	.fail(function(data) {
		console.log(data.statusText + " (" + data.status +")");
	})
	.always(function() {
		//console.log("complete");
		$("#modal-loading").modal("hide");
	});

}

$(window).resize(function () {
    swView();
});

function swView() {
    //console.log($(window).width());
    if ($(window).width() < 992)
    {
    	$("#west").removeClass('text-left').addClass('text-center');
    	$("#noerht").removeClass('text-center').addClass('text-center');
    	$("#east").removeClass('text-right').addClass('text-center');
    	$("#saldo").removeClass('text-right').addClass('text-center');
    }
    else
    {
    	$("#west").removeClass('text-center').addClass('text-left');
    	$("#noerht").removeClass('text-center').addClass('text-center');
    	$("#east").removeClass('text-center').addClass('text-right');
    	$("#saldo").removeClass('text-center').addClass('text-right');
    }
}

function pay(id){
	$.ajax({
		url: ruta + 'bet/ '+id,
		type: 'PUT',
		dataType: 'json',
		data: {status: '5'},
		headers: {
		    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},		
	})
	.done(function(data) {
		console.log(data);
		message = data.message;
            $("#modal-message").modal("show");
            $("#title-message").html('<i class="fa fa-check" aria-hidden="true"></i> Apuesta pagada');
            $("#message").html(message);		
            searchHistory();
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});

}

$( "#ticket-id" ).keypress(function(e) {
  if ( e.which == 13 ) {
     searchHistory();
  }
  $("#select-status").val('0');
});

//Login file
 $(document).ready(function(){
    document.getElementById("btn-register").disabled = true;
    maxDate = new Date(new Date().setFullYear(new Date().getFullYear() - 18));
    $('#group-fecha').datetimepicker({
        format: 'YYYY-MM-DD',
        ignoreReadonly: true,
        maxDate: maxDate,
    });

    var loginForm = $("#loginForm");
    var ruta = '/';
    loginForm.submit(function(e) {
        $("#form-login-errors").empty();
        e.preventDefault();
        var formData = loginForm.serialize();

        $("#email-div").removeClass("has-error");
        $("#password-div").removeClass("has-error");
        $("#login-errors").removeClass("has-error");
        $("#modal-loading").modal("show");
        $.ajax({
            url: ruta+'login',
            type: 'POST',
            data: formData,
            success: function(data) {
                $("#modal-loading").modal("hide");
                $("#loginModal").modal("hide");
                location.reload(true);
            },
            error: function(data) {
                if (data.status != 500) {
                    var obj = jQuery.parseJSON(data.responseText);
                    $("#modal-loading").modal("hide");
                    console.log(obj.error);
                    if (obj.email) {
                        $("#email-div").addClass("has-error");
                        $("#form-errors-email").html(obj.email);
                    }
                    if (obj.password) {
                        $("#password-div").addClass("has-error");
                        $("#form-errors-password").html(obj.password);
                    }
                    if (obj.error) {
                        $("#login-errors").addClass("has-error");
                        $("#loginerrors").html(obj.error);
                        $("#loginerrors").addClass("error-login")
                    }
                }
                else{
                    session_expired(data);
                }
            }
        });
    });

        var registerForm = $("#registerForm");
        registerForm.submit(function(e){
            e.preventDefault();
            var formData = registerForm.serialize();
            $("#register-errors-name").html( "" );
            $("#register-errors-email").html( "" );
            $("#register-errors-password").html( "" );
            $("#register-errors-identification").html( "" );
            $("#register-errors-birth-date").html( "" );
            $("#register-name").removeClass("has-error");
            $("#register-email").removeClass("has-error");
            $("#register-password").removeClass("has-error");
            $("#register-identification").removeClass("has-error");
            $("#register-birth-date").removeClass("has-error");
            $("#register-phone").removeClass("has-error");
            $("#modal-loading").modal("show");
            $.ajax({
                url: ruta+'register',
                type:'POST',
                data:formData,
                success:function(data){
                    $("#modal-loading").modal("hide");
                    $('#registerModal').modal( 'hide' );
                    location.reload(true);
                },
                error: function (data) {
                    $("#modal-loading").modal("hide");
                    console.log(data.responseText);
                    var obj = jQuery.parseJSON( data.responseText );
                   if(obj.name){
                        $("#register-name").addClass("has-error");
                        $("#register-errors-name").html( obj.name );
                        $("#register-errors-name").addClass("error-login")
                    }
                    if(obj.email){
                        $("#register-email").addClass("has-error");
                        $("#register-errors-email").html( obj.email );
                        $("#register-errors-email").addClass("error-login")
                    }
                    if(obj.identification){
                        $("#register-identification").addClass("has-error");
                        $("#register-errors-identification").html( obj.identification );
                        $("#register-errors-identification").addClass("error-login")
                    }
                    if(obj.password){
                        $("#register-password").addClass("has-error");
                        $("#register-errors-password").html( obj.password );
                        $("#register-errors-password").addClass("error-login")
                    }
                    if(obj.birth_date){
                        $("#register-birth_date").addClass("has-error");
                        $("#register-errors-birth_date").html( obj.birth_date );
                        $("#register-errors-birth_date").addClass("error-login")
                    }
                    if(obj.phone){
                        $("#register-phone").addClass("has-error");
                        $("#register-errors-phone").html( obj.phone );
                        $("#register-errors-phone").addClass("error-login")
                    }
                }
            });
        });
    });

function enableBtn(){
    document.getElementById("btn-register").disabled = false;
}
//Pagos file
var bancos = 0;

$('#pagoModal').on('shown', function() {
    pagosInit();
    $("#fecha_hora").val("");
    $("#monto").val("");
    $("#tipo").val("0");
    $("#referencia").val("");
    $("#banco").val("0");
    $("#titular").val("");
    $("#ultimos_digitos").val("");
    carga_bancos();
    show_cuentas();
})

function pagosInit()
{
    $('#pago-group-fecha').datetimepicker({
        format: 'YYYY-MM-DD',
        ignoreReadonly: true,
    });

    $("#group-fecha").on("dp.change", function(e) {
        validate();
    });
    $("#monto").keyup(function(event) {
        validate();
    });
    $("#tipo").change(function(event) {
        validate();
    });
    $("#referencia").keyup(function(event) {
        validate();
    });
    $("#banco").change(function(event) {
        validate();
    });
    $("#titular").keyup(function(event) {
        validate();
    });
    $("#ultimos_digitos").keyup(function(event) {
        validate();
    });

    $("#btn-pago").click(function(){
        $("#modal-loading").modal("show");
        $.ajax({
            url: ruta + 'pago',
            type: 'POST',
            dataType: 'json',
            data: {
                    fecha_hora : $("#fecha_hora").val(),
                    monto : $("#monto").val(),
                    tipo : $("#tipo").val(),
                    referencia : $("#referencia").val(),
                    banco : $("#banco").val(),
                    titular : $("#titular").val(),
                    ultimos_digitos : $("#ultimos_digitos").val()
                  },
            headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     },
        })
        .done(function(data) {
            var message = data.message;
            $("#pagoModal").modal('hide');
            $("#modal-message").modal("show");
            $("#title-message").html('<i class="fa fa-check" aria-hidden="true"></i> Pago Exitoso');
            $("#message").html(message);
        })
        .fail(function(data) {
            session_expired(data);
        })
        .always(function() {
            $("#modal-loading").modal("hide");
        });

    });

    function validate(){
        if($("#fecha_hora").val()=="" ||
        $("#monto").val()=="" ||
        $("#tipo").val() == 0 ||
        $("#referencia").val()=="" ||
        $("#banco").val() == 0 ||
        $("#titular").val()=="" ||
        $("#ultimos_digitos").val()=="")
        {
            $("#btn-pago").prop('disabled', true);
        }
        else{
            $("#btn-pago").prop('disabled', false);
        }
    }    
}


function show_cuentas(){
    $("#modal-message").modal("show");
    $("#title-message").html('<i class="fa fa-bank" aria-hidden="true"></i> Cuentas para depositar');
    content = +"<table class='subt1'>"
                +"<tbody><tr>"
                    +"<td colspan='3'>Deposite en las siguientes cuentas corrientes a nombre de <strong>Juan Francisco Rivas</strong> C.I. <strong>14123639</strong></td>"
                +"</tr>"
                +"<tr>"
                    +"<td><img class='img-banco' src='images/mercantil.jpg' alt='Mercantil'></td>"
                    +"<td>Mercantil</td>"
                    +"<td>01050151210151066256</td>"
                +"</tr>"
                +"<tr>"
                    +"<td><img class='img-banco' src='images/provincial.png' alt='Provincial'></td>"
                    +"<td>Provincial</td>"
                    +"<td>01082425700100055543</td>"
                +"</tr>"
                +"<tr>"
                    +"<td><img class='img-banco' src='images/banesco.jpg' alt='Banesco'></td>"
                    +"<td>Banesco</td>"
                    +"<td>01340031870313254399</td>"
                +"</tr>"
            +"</tbody></table>"
    $("#message").html(content);
}
//Resultados file

    $("#group-fecha-result").on("dp.hide", function(e) {
		var fecha = $("#fecha-result").val();
		cargarResultados(fecha);
    });

    function cargarResultados(fecha){
    
    	$('#group-fecha-result').datetimepicker({
       		format: 'YYYY-MM-DD',
       	 	ignoreReadonly: true,
    	});

    	$("#panel-resultados").html(img_loading);
    	$.ajax({
    		url: ruta + 'resultado',
    		type: 'GET',
    		dataType: 'json',
    		data: {'fecha_cierre': fecha},
    	})
    	.done(function(response) {
    		console.log(response);
			var content ="<div class='panel-group' id='accordion' role='tablist' aria-multiselectable='true'>";
			if(response.length >0){
				var swCategoria = 0;
				var swSubCategoria = 0;
				console.log(response);
				for (var i = 0; i < response.length; i++) {
				var ganancia = 0;
					if (swCategoria == 0) {
						content += "<div class='panel panel-default panel-game'>"
							+"<div class='panel-heading text-uppercase heading-categoria'></strong>" + response[i].menu.nombre + "</p></div>"
							+"<div class='panel-body'>"
						swCategoria = 1
					}
					if(swSubCategoria==0){
						content += "<div class='panel panel-default panel-game'>"
							+"<div class='panel-heading text-uppercase heading-subcategoria'></strong> " + response[i].submenu.nombre + "</p></div>"
							+"<div class='panel-body'>"
						swSubCategoria = 1
					}


					content += "<div class='panel panel-warning panel-game'>"
							+"<div class='panel-heading text-uppercase'>"
							+ response[i].equipo[0].nombre + " - " + response[i].equipo[1].nombre
							+"</div>"
							+"<div class='panel-body'>";
					var resultado = response[i].resultado;
					var sw = 0;
					for (var j = 0; j < resultado.length; j++) {
						if (resultado[j].tipo == 1 || resultado[j].tipo == 3) {
							if (sw == 0) {
								content +="<ul class='list-group'>";
								content += "<li class='list-group-item text-uppercase'><strong>" + resultado[j].tipo_resultado.nombre + ": </strong> " + resultado[j].equipo.nombre + " "+ resultado[j].score;
								sw=1;
							}
							else {
								content += " - " + resultado[j].score + " " + resultado[j].equipo.nombre + "</li>"
								content += "</ul>"
								sw=0;
							}
						}
						else if(resultado[j].tipo == 2 || resultado[j].tipo == 6){
							content +="<ul class='list-group'>";
							content += "<li class='list-group-item text-uppercase'><strong>" + resultado[j].tipo_resultado.nombre + ": </strong> " + resultado[j].equipo.nombre;
							content += "</ul>";
						}
						else if(resultado[j].tipo == 5){
							content +="<ul class='list-group'>";
							content += "<li class='list-group-item text-uppercase'><strong>" + resultado[j].tipo_resultado.nombre + ": </strong> " + resultado[j].score;
							content += "</ul>";
						}
						else{
							var sino = "SI";
							if (resultado[j].score == 2) {
								sino = "NO";
							}
							content +="<ul class='list-group'>";
							content += "<li class='list-group-item text-uppercase'><strong>" + resultado[j].tipo_resultado.nombre + ": </strong> " + sino;
							content += "</ul>";
						}

					}
					content += '</div></div>';
					if (i < response.length -1) {
						if(response[i].subcategoria.id != response[i+1].subcategoria.id){
							content += "</div>"
									+"</div>";
						swSubCategoria = 0;
						}
						if(response[i].categoria.id != response[i+1].categoria.id){
							content += "</div>"
									+"</div>";
						swCategoria = 0;
						}
					}
					else{
						content += "</div>"
								+"</div>";
						content += "</div>"
								+"</div>"
								+"</div>";//Accordion
					}
				}
			}
			$("#panel-resultados").html(content);
    	})
    	.fail(function(data) {
    		console.log(data);
    	})
    	.always(function() {
    		$("#modal-loading").modal("hide");
    	});/**/

    }
//Retiros
balance = $("#saldo").data('saldo');

$("#retiro").keyup(function(event) {
    validate();
});

$("#retiro-cuenta").change(function(event) {
    validate();
});

$('#retiroModal').on('shown', function() {
    cargar_cuentas();
    $("#retiro").val("");
    $("#btn-retiro").prop('disabled', true);
})

$("#btn-retiro").click(function(){
    $("#modal-loading").modal("show");
    $.ajax({
        url: ruta + 'retiro',
        type: 'POST',
        dataType: 'json',
        data: {
                monto : $("#retiro").val(),
                cuenta_id : $("#retiro-cuenta").val(),
              },
        headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },
    })
    .done(function(data) {
        var message = data.message;
        var saldo = data.saldo;
        $("#retiroModal").modal('hide');
        $("#modal-message").modal("show");
        $("#title-message").html('<i class="fa fa-check" aria-hidden="true"></i> Retiro Exitoso');
        $("#message").html(message);
        $("#saldo").html(numberWithCommas(saldo))
        balance = saldo;
    })
    .fail(function(data) {
        session_expired(data);

    })
    .always(function() {
        $("#modal-loading").modal("hide");
    });
});

function validate(){
    console.log( $("#retiro-cuenta").val());
    if($("#retiro").val()=="" || $("#retiro").val() <= 0 || $("#retiro-cuenta").val() == 0)
    {
        $("#btn-retiro").prop('disabled', true);
    }
    else{
        if($("#retiro").val() > balance)
        {
            $("#retiro-errors-monto").html("El monto a retirar supera su saldo disponible");
            $("#btn-retiro").prop('disabled', true);
        }
        else
        {
            $("#retiro-errors-monto").empty();
            $("#btn-retiro").prop('disabled', false);
        }

    }
}

function cargar_cuentas(){
    $.ajax({
        url: ruta + 'cuentasbancarias',
        type: 'GET',
        dataType: 'json',
    })
    .done(function(data) {
            $("#retiro-cuenta").html("<option value='0'>Seleccione la cuenta</option>");
            for (var i = 0; i < data.length; i++) {
                $("#retiro-cuenta").append("<option value='" + data[i].id + "'>" + data[i].banco.nombre + " - " + data[i].nrocuenta  +"</option>");
            }
    })
    .fail(function() {
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });
}