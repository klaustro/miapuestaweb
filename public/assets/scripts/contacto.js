//Contacto file
function contacto() {	
	var name = $('#name').val() ;
	var email = $('#email').val() ;
	var content = $('#content').val() ;
	$.ajax({
		url: ruta+'contacto',
		type: 'POST',
		dataType: 'json',
		data: { name: name, 
				email: email, 
				content: content
			},
        headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 },			
	})
	.done(function(data) {
		console.log(data);
		$('#title-message').html('Contacto MiApuestaWeb');
		$('#message').html(data.message);
		$('#modal-message').modal('show');	
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
        $("#modal-message").on('hide.bs.modal', function () {
            location.reload();
        });	
	});
}
