//bets file
var bets = [];
var machos = [];
var total = 0;
var compuesta = 0;
var swNotify = 0;
var arr_games = [];
var forbidden = [];
var balance = $("#saldo").data('saldo');
var corredor = $("#saldo").data('corredor');
var swCompuesta = 0;
var ganancia = 0;

function reset(){
	$('[data-toggle="tooltip"]').tooltip();
	swCompuesta = 0;
	bets = [];
	machos = [];
	total = 0;
	compuesta = 0;
	swNotify = 0;
	$("#btn-bet").prop('disabled', false);
	$('.in').collapse('hide');
	if($("#bets-monto").val() > balance && corredor == 0)
	{
		$("#bet-errors-monto").html("Saldo insuficiente");
		$("#btn-bet").prop('disabled', true);
	}
	$(".sw-tipo").prop('checked', compuesta);
	$(".lbl-tipo-apuesta").html("  Directa");
}

function areSimple(){
	for (var i = 0; i < bets.length; i++) {
		if(bets[i].tipo == 19){
			return true;
		}
	}
}

$(".sw-tipo").change(function(event) {
	if(areSimple()){
		$("#bet-forbidden").modal("show");
		$("#forbiden-label").html("<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Apuesta Compuesta");
		$("#forbidden-message").html("No se puede combinar su selección");
		$(".sw-tipo").prop('checked', compuesta);
	}
	else if(bets.length>1){
		if ($(".lbl-tipo-apuesta").html() == "  Directa") {
			$(".lbl-tipo-apuesta").html("  Compuesta");
			compuesta = 1;
		} else {
			$(".lbl-tipo-apuesta").html("  Directa");
			compuesta = 0;
		}
		swCompuesta = 1;
		calcule_bets();
	}
	else{
		$("#bet-forbidden").modal("show");
		$("#forbiden-label").html("<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Apuesta Compuesta");
		$("#forbidden-message").html("Por favor seleccione al menos dos logros para realizar una apuesta compuesta");
		$(".sw-tipo").prop('checked', compuesta);
	}
});

$("#modal-bets").on('show.bs.modal', function () {
	$('#sw-tipo-apuesta-modal').prop('checked', compuesta);
});

$("#modal-bets").on('hide.bs.modal', function () {
	$('#sw-tipo-apuesta').prop('checked', compuesta);

});

function subcategorias(menu){
	if ($.trim($("#submenu"+menu).html())=='')
	{
		//$("#modal-loading").modal("show");
		$("#submenu"+menu).html(img_loading);

		$.ajax({
			url: ruta + 'submenu',
			type: 'GET',
			dataType: 'json',
			data: {'menu': menu},
		})
		.done(function(data) {
			var content = "";
			for (var i = 0; i < data.length; i++) {
				if (data[i].games_count >0){
					content +="<button class='btn btn-primary btn-sub' type='button' data-toggle='collapse' data-target='#collapse-submenu" + data[i].id + "' aria-expanded='false' aria-controls='collapse-submenu" + data[i].id + "' onClick='games(" + data[i].id + ")'>"
								+ data[i].nombre
								+"</button>"
								+"<div class='collapse talfi' id='collapse-submenu" + data[i].id + "'>"
									+"<div class='well col-md-11' id='games" + data[i].id + "'>"
									+"</div>"
								+"</div>";
				}
			$("#submenu"+menu).html(content);
			}
		})
		.fail(function(data) {
			session_expired(data);
		})
		.always(function() {
			//$("#modal-loading").modal("hide");
		});
	}
}

function games(submenu){
	if ($.trim($("#games"+submenu).html())=='')
	{
		//$("#modal-loading").modal("show");
		$("#games"+submenu).html(img_loading);
		$.ajax({
			url: ruta + 'game',
			type: 'GET',
			dataType: 'json',
			data: {'submenu': submenu},
		})
		.done(function(data) {
			var swhora = 0;
			var content = "";
			var fecha = "0";
			for (var i = 0; i < data.length; i++) {
				if (fecha != data[i].fecha_cierre)
				{
					if (fecha !=  "0")
						content += "</div>";
					content += "<div class='panel panel-warning'>"
									+"<div class='panel-heading'>"
										+"<h3 class='panel-title hora-game'>" + format_date(data[i].fecha_cierre) + "</h3>"
									+"</div>";
				}
				fecha = data[i].fecha_cierre

			if (data[i].categoria != 12) {
				content = content + "<div class='panel-body panel-game'>"
									+"<ul class='list-group'>"
										+"<li class='list-group-item disabled'>"
											+"<table width='100%'>"
												+"<tr>"
													+"<th width='55%'><div class='hidden-phone' title='" + data[i].id + "'>EQUIPO</div></th>"
													+"<th class='th-bets' width='15%'><div class='visible-phone'>G</div><div class='hidden-phone'>GANADOR</div></th>"
													+"<th class='th-bets' width='15%'><div class='visible-phone'>T</div><div class='hidden-phone'>TOTALES</div></th>"
													+"<th class='th-bets' width='15%'><div class='visible-phone'>RL</div><div class='hidden-phone'>RUNLINE</div></th>"
												+"</tr>"
											+"</table>"
										+"</li>";
											$(".td-bet").click(function(){
				var id = $(this).data("id");
				var idgame = $(this).data("game");
				var prob = $(this).data("prob");
				var detalle = $(this).data("detalle");
				var tipo = $(this).data("tipo");

				saveBet(id, idgame, prob, detalle, tipo);
				});

				for (var j = 0; j < data[i].equipo.length; j++) {
					content += 	"<li class='list-group-item'>"
									+"<table width='100%' class='table-bets'>"
										+"<tr>"
											+"<td width='55%' class='team-title text-uppercase'>" + data[i].equipo[j].nombre + "</td>"
											+"<td class='td-bet bet-off' width='15%' id='" + getId(data[i].games_probs, data[i].equipo[j].id, data[i].categoria == 12 ? 19 : 1)
												+ "' onClick='selectBet("+ getId(data[i].games_probs, data[i].equipo[j].id, data[i].categoria == 12 ? 19 : 1)
												+ ", " + data[i].id
												+ ", "+ probabilidad(data[i].games_probs, data[i].equipo[j].id, data[i].categoria == 12 ? 19 : 1)
												+ ", \""+ data[i].equipo[0].nombre.substring(0, 3) + " - " + data[i].equipo[1].nombre.substring(0, 3) + " GANADOR " + data[i].equipo[j].nombre + " " + probabilidad_text(data[i].games_probs, data[i].equipo[j].id, data[i].categoria == 12 ? 19 : 1) + "\""
												+ ", 1)'>"
												+ probabilidad_text(data[i].games_probs, data[i].equipo[j].id, data[i].categoria == 12 ? 19 : 1) + "</a>"
											+"</td>"

											+"<td class='td-bet bet-off' width='15%' id='" + getId(data[i].games_probs, data[i].equipo[j].id, 4)
												+ "' onClick='selectBet("+ getId(data[i].games_probs, data[i].equipo[j].id, 4)
												+ ", " + data[i].id
												+ ", "+ probabilidad(data[i].games_probs, data[i].equipo[j].id, 4)
												+ ", \""+ data[i].equipo[0].nombre.substring(0, 3) + " - " + data[i].equipo[1].nombre.substring(0, 3) + " TOTALES " + data[i].equipo[j].nombre + " " + probabilidad_text(data[i].games_probs, data[i].equipo[j].id, 4) + "\""
												+ ", 4)'>"
												+ probabilidad_text(data[i].games_probs, data[i].equipo[j].id, 4)
											+"</td>"

											+"<td class='td-bet bet-off' width='15%' id='" + getId(data[i].games_probs, data[i].equipo[j].id, 3)
												+ "' onClick='selectBet("+ getId(data[i].games_probs, data[i].equipo[j].id, 3)
												+ ", " + data[i].id
												+ ", "+ probabilidad(data[i].games_probs, data[i].equipo[j].id, 3)
												+ ", \""+ data[i].equipo[0].nombre.substring(0, 3) + " - " + data[i].equipo[1].nombre.substring(0, 3) + " RUNLINE " + data[i].equipo[j].nombre + " " + probabilidad_text(data[i].games_probs, data[i].equipo[j].id, 3) + "\""
												+ ", 3)'>"
												+ probabilidad_text(data[i].games_probs, data[i].equipo[j].id, 3)
											+"</td>"

										+"</tr>"
									+"</table>"
								+"</li>";
				}
				for (var j = 0; j < data[i].games_probs.length; j++) {
					if(data[i].games_probs[j].tipo_probabilidad == '6')
					{
						content += 	"<li class='list-group-item'>"
										+"<table width='100%' class='table-bets'>"
											+"<tr>"
												+"<td width='55%'><strong>EMPATE</strong></td>"
												+"<td class='td-bet bet-off' width='15%' id='" + data[i].games_probs[j].id
													+ "' onClick='selectBet("+ data[i].games_probs[j].id
													+ ", " + data[i].id
													+ ", "+ data[i].games_probs[j].probabilidad
													+ ", \""+ data[i].equipo[0].nombre.substring(0, 3) + " - " + data[i].equipo[1].nombre.substring(0, 3) + " EMPATE "+ data[i].games_probs[j].probabilidad + "\""
													+ ", 6)'>"
													+ logro(data[i].games_probs[j].probabilidad, tipoLogro)
												+"</td>"
												//+"<td class='td-bet bet-off' width='15%' id='" + data[i].games_probs[j].id + "' data-id='" + data[i].games_probs[j].id + "' data-prob='" + data[i].games_probs[j].probabilidad + "' data-detalle='" + data[i].equipo[0].nombre.substring(0, 3) + " - " + data[i].equipo[1].nombre.substring(0, 3) + " EMPATE "+ data[i].games_probs[j].probabilidad + "' data-game='" + data[i].id + "' data-tipo='6'>" +  data[i].games_probs[j].probabilidad  + "</td>"
												+"<td rowspan='2' width='30%'></td>"
											+"</tr>"
										+"</table>"
									+"</li>"
					}
				}

				if (data[i].exoticas_count != null)
				{
					content +="<button class='btn btn-default btn-xs' type='button' data-toggle='collapse' data-target='#collapse-exoticas"+ data[i].id +"' aria-expanded='false' aria-controls='collapse-exoticas"+ data[i].id +"' onClick='exoticas(" + data[i].id + ")'>"
								+"Exóticas <span class='badge badge-warning'>+" + data[i].exoticas_count.count + "</span>"
							+"</button>"
						+"<div class='collapse' id='collapse-exoticas"+ data[i].id +"'>"
							+"<div class='well col-md-11 col-xs-11' id='well-exoticas" + data[i].id + "'>"
							+"</div>"
						+"</div>"
				}

				content +="</ul>"
					+"</div>";
			}
			else{
				for (var j = 0; j < data[i].games_probs.length; j++) {
					content += "<box class='td-bet bet-off col-lg-1 col-md-3 col-sm-5 col-xs-10' style='font-size:11px' id='" + getId(data[i].games_probs, data[i].equipo[j].id, 19)
						+ "' onClick='selectBet("+ getId(data[i].games_probs, data[i].equipo[j].id, 19)
						+ ", " + data[i].id
						+ ", "+ probabilidad(data[i].games_probs, data[i].equipo[j].id, 19)
						+ ", \""+ "LOTO ACTIVO - " + format_date(data[i].fecha_cierre) + " " + data[i].equipo[j].nombre + "\""
						+ ", 19)'>"
						+ data[i].equipo[j].nombre + "</a>"
					+"</box>";
				}
				content += '<div class="clearfix"></div>'
			}
	}
		$("#games"+submenu).html(content);

		})
		.fail(function(data) {
			session_expired(data);
		})
		.always(function() {
			//$("#modal-loading").modal("hide");
		});
	}
}



function selectBet(id, idgame, prob, detalle, tipo)	{
	if (compuesta == 1 && tipo == 19) {
		alert('No se puede combinar éste tipo de apuesta');
	} else{
		saveBet(id, idgame, prob, detalle, tipo);
	}
	//alert('lanzate');
}

function exoticas(idgame){
	if ($.trim($("#well-exoticas"+idgame).html())=='')
	{
		//$("#modal-loading").modal("show");
		$("#well-exoticas"+idgame).html(img_loading);
		$.ajax({
			url: ruta+'games_prob',
			type: 'GET',
			dataType: 'json',
			data: {'idgame': idgame},
		})
		.done(function(data) {
			var content = "";
			var evento = data[0].equipo.nombre.substring(0, 3) + " - " + data[0].equipo.nombre.substring(0, 3);
			for (var i = 0; i < data.length; i++) {
				/*Descripcon de equipos*/
				if (data[i].tipo_probabilidad == 7 || data[i].tipo_probabilidad == 8 || data[i].tipo_probabilidad == 11 || data[i].tipo_probabilidad == 12 || data[i].tipo_probabilidad == 15 || data[i].tipo_probabilidad == 16)
					equipo = data[i].equipo.nombre;
				else if (data[i].tipo_probabilidad == 10 || data[i].tipo_probabilidad == 13 || data[i].tipo_probabilidad == 14 || data[i].tipo_probabilidad == 17 || data[i].tipo_probabilidad == 18)
					equipo = "";
				else if (data[i].tipo_probabilidad == 9)
				{
					equipo = "SI";
					if (data[i].orden == 2)
						equipo = "NO";
				}

				/*Cantidades*/
				if (data[i].tipo_probabilidad == 1 || data[i].tipo_probabilidad == 8 || data[i].tipo_probabilidad == 9 || data[i].tipo_probabilidad == 11 || data[i].tipo_probabilidad == 14 || data[i].tipo_probabilidad == 15)
				{
					cantidad = "";
				}
				else
				{
					cantidad = data[i].cantidad;
				}

				/*Signos*/
				if (data[i].tipo_probabilidad == 10 || data[i].tipo_probabilidad == 13 || data[i].tipo_probabilidad == 17 || data[i].tipo_probabilidad == 18)
				{
					signo = "+";
					if(data[i].orden == 2)
						signo = "-";
				}
				else if (data[i].tipo_probabilidad == 7 || data[i].tipo_probabilidad == 12 || data[i].tipo_probabilidad == 16)
				{
					signo = data[i].games_probs_signo.signo;
					if(data[i].cantidad == '0'){
						signo = 'PK ';
						cantidad = '';
					}
				}
				else if (data[i].tipo_probabilidad == 16)
				{
					signo = "+";
					if(data[i].orden == 2)
						signo = "-";
				}
				else
				{
					signo = "";
				}
				var detalle = "\"" + evento + " " + data[i].tipo_apuesta.nombre + " " +  equipo + " " + signo + cantidad + " " +  parseFloat(data[i].probabilidad).toFixed(2) +"\"" ;
				var whereClick =  "onClick='selectBet("
									+ data[i].id
									+ ", " + data[i].idgame
									+ ", " + data[i].probabilidad
									+ ", " + detalle
									+ ", " + data[i].tipo_probabilidad
									+")'";
				if (data[i].orden == 1)
				{
					content += "<ul class='list-group list-exoticas col-md-3 col-xs-11'>"
									+"<li class='list-group-item disabled text-uppercase'>"+data[i].tipo_apuesta.nombre+"</li>"
									+"<li class='list-group-item li-exotica li-exotica-off text-uppercase' id='" + data[i].id + "' " + whereClick + ">"
									+ equipo + " <strong>" + signo + cantidad + "</strong> " + logro(parseFloat(data[i].probabilidad).toFixed(2), tipoLogro)
									+ "</li>"
				}
				else
				{
					content += "<li class='list-group-item li-exotica bet-off text-uppercase' id='" + data[i].id + "' " + whereClick + "'>"
								+ equipo + " <strong>" + signo + cantidad + "</strong> " + logro(parseFloat(data[i].probabilidad).toFixed(2), tipoLogro)
							+ "</li>"
							+"</ul>";
				}
			}

			$("#well-exoticas"+idgame).html(content);
		})
		.fail(function() {
			session_expired(data);
		})
		.always(function() {
			//$("#modal-loading").modal("hide");
		});
	}
}

function get_macho(games_probs){
	var prob =0;
	var equipo = 0;
	for (var i = 0; i < games_probs.length; i++) {
		if (games_probs[i].tipo_probabilidad == 1)
		{
			if (equipo ==0)
			{
				equipo = games_probs[i].equipo;
				prob = games_probs[i].probabilidad;
			}
			else
			{
				if (games_probs[i].probabilidad < prob)
				{
					equipo = games_probs[i].equipo;
				}
			}
		}
	}
	if (no_repetido(games_probs[0].idgame) == 0) {
		macho_game = new macho(games_probs[0].idgame, equipo);
		machos.push(macho_game);
	}

	return equipo;
}

//CONSTRUCTORS
function macho(idgame, equipo) {
    this.idgame = idgame;
    this.equipo = equipo;
}

function bet(id, idgame, probabilidad, monto, tipo, detalle) {
    this.id = id;
    this.idgame = idgame;
    this.probabilidad = probabilidad;
    this.monto = monto;
    this.tipo = tipo;
    this.detalle = detalle;
}

function bet_forbidden(id, message){
	this.id = id;
	this.message = message;
}

function no_repetido(idgame) {
    resp = 0;
    for (i = 0; i < machos.length; i++) {
        if (machos[i].idgame == idgame)
            resp = 1;
    }
    return resp;
}

function probabilidad_text(games_probs, equipo, tipo){
	var value = ""

	vmacho = get_macho(games_probs);

	for (var i = 0; i < games_probs.length; i++) {

		if (games_probs[i].equipo == equipo && games_probs[i].tipo_probabilidad == tipo)
		{
			var cantidad = "";
			var signo = "";
			if (tipo == 3){
				signo = games_probs[i].games_probs_signo.signo;
			if(games_probs[i].cantidad == '0')
				signo = "PK";
			}

			if(tipo==4)
			{
				signo = "+";
				if(games_probs[i].orden == 2)
					signo = "-";
			}

			if (games_probs[i].cantidad >0)
				cantidad = games_probs[i].cantidad;
			var value = signo + cantidad + " "  + logro(parseFloat(games_probs[i].probabilidad).toFixed(2), tipoLogro);
		}
	}
	return value;
}

function probabilidad(games_probs, equipo, tipo){
	var value = 0

	for (var i = 0; i < games_probs.length; i++) {
		if (games_probs[i].equipo == equipo && games_probs[i].tipo_probabilidad == tipo)
			var value =  games_probs[i].probabilidad;
	}
	return value;
}

function getId(games_probs, equipo, tipo){
		prob =0;

		for (var i = 0; i < games_probs.length; i++) {
			if (games_probs[i].equipo == equipo && games_probs[i].tipo_probabilidad == tipo)
			{
				var value = games_probs[i].id;
			}
		}
	return value;
}

$(".bets-monto").keyup(function(event) {
	if($(this).val() > balance && corredor == 0)
	{
		$("#bet-errors-monto").html("Saldo insuficiente");
		$("#btn-bet").prop('disabled', true);
	}
	else
	{
		$("#bet-errors-monto").html("");
		$("#btn-bet").prop('disabled', false);
		$(".bets-monto-simple").val($(this).val())
		for (var i = 0; i < bets.length; i++) {
			bets[i].monto =$(this).val();
		}
		calcule_bets();

	}

});

function monto_simple(id, probabilidad){
	var monto = $("#bets-monto-"+id).val();
	for (var i = 0; i < bets.length; i++){
		if(bets[i].id == id)
			bets[i].monto = monto;
	}
	$("#ganancia"+id).html(numberWithCommas(monto*probabilidad));
	calcule_bets();
}

function rules(idgame, tipo, idgames_probs){
	var count_bets = 0;
	var tipos_apuesta = [];
	var repetido = 0;

	for (var i = 0; i < bets.length; i++) {
		if(bets[i].idgame == idgame)
		{
			count_bets += 1;
			tipos_apuesta.push(bets[i].tipo);
			if(bets[i].tipo == tipo)
				repetido += 1;
		}
	}

	if(count_bets > 0)
	{
		if(repetido>1)
		{
			saveBet(idgames_probs,0,0,0);
			$("#bet-forbidden").modal("show");
			$("#forbiden-label").html("<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Logro Repetido");
			$("#forbidden-message").html("No puede jugar el mismo tipo de apuesta en el mismo juego");
		}
		else{
			$.ajax({
				url: ruta+'regla',
				type: 'GET',
				dataType: 'json',
				data: {
						game: idgame,
						'tipos_apuesta[]' : tipos_apuesta
					  },
			})
			.done(function(data) {
				var message = data.message;
				if(message != "Valida")
				{
					saveBet(idgames_probs,0,0,0);
					$("#bet-forbidden").modal("show");
					$("#forbiden-label").html("<i class='fa fa-exclamation-circle' aria-hidden='true'></i> Apuesta Prohibida");
					$("#forbidden-message").html(message);

				}
			})
			.fail(function() {
				session_expired(data);
			})
			.always(function() {
			});
		}
	}
}

function saveBet(id, idgame, prob, detalle, tipo){
	var index = -1;
	var monto = $("#bets-monto-modal").val()
	for (var i = 0; i < bets.length; i++){
		if(bets[i].id == id)
			index = i;
	}

	if (index > -1)
	{
    	bets.splice(index, 1);
		$("#"+id).removeClass('bet-on');
		$("#"+id).addClass('bet-off');
		$("#alert-"+id).remove();
		if(bets.length == 0)
			$("#modal-bets").modal("hide");
		if(bets.length == 1){
			compuesta = 0;
			$(".sw-tipo").prop('checked', compuesta);
			$(".lbl-tipo-apuesta").html("  Directa");
		}
	}
	else
	{
		new_bet = new bet(id, idgame, prob, monto, tipo, detalle);
		bets.push(new_bet);

		$("#bets-detalle").append("<div class='alert alert-warning alert-dismissible alert-bet' role='alert' id='alert-" + id + "'>"
								+"  <button type='button' class='close' data-dismiss='alert' aria-label='Close' onClick='saveBet("+id+",0,0,0)'><span aria-hidden='true'>&times;</span>"
								+"  </button>"
								+   "  <div class='pull-left text-uppercase text-bet'><b>" + detalle + "</b></div>"
								+ "<div class='pull-right'><input type='number' placeholder='Monto' class='bets-monto-simple input-number' id='bets-monto-" + id + "' onKeyup='monto_simple(" + id + ", " + prob + ")' value='" + monto + "'/>"
								+ "<b class='bets-monto-simple' id='ganancia"+ id +"'>" + prob*monto + "</b></div><div class='clearfix'></div>"
								+"</div>");
		$("#"+id).removeClass('bet-off');
		$("#"+id).addClass('bet-on');

		if (compuesta == 1)
		{
			$(".bets-monto-simple").addClass('hidden');
			rules(idgame, tipo, id);
		}
	}
	calcule_bets();
}

function calcule_bets(){
	total = 0;
	arriesgado = 0;
	$("#bets-arriesgado").empty();
	if (compuesta == 0)
	{
		for (var i = 0; i < bets.length; i++) {
				total += bets[i].probabilidad * bets[i].monto;
				arriesgado += bets[i].monto*1;
				$("#ganancia"+bets[i].id).html(numberWithCommas(bets[i].monto*bets[i].probabilidad));
		}
		$(".bets-monto-simple").removeClass('hidden');
		$("#bets-arriesgado").html("Monto arriesgado: " + numberWithCommas(arriesgado));
	}
	else
	{
		for (var i = 0; i < bets.length; i++) {
			if(i==0)
				total = bets[i].probabilidad * bets[i].monto;
			else
				total = total * bets[i].probabilidad;
		}

		$(".bets-monto-simple").addClass('hidden');
	}

	$(".bets-total").html(numberWithCommas(total));
	ganancia = total;

	if (bets.length == 1 )
	{
		message = "<strong>" + bets.length + "</strong> logro, con una ganancia posible de <strong>" + numberWithCommas(total) + "</strong>";
	}
	else
	{
		message = "<strong>" + bets.length + "</strong> logros, con una ganancia posible de <strong>" + numberWithCommas(total) + "</strong>";
	}
	if (swNotify == 0 && bets.length > 0)
	{
		notify =$.notify({
							message: message
									+" <button class='btn btn-primary btn-text' data-toggle='modal' href='#modal-bets'>"
										+"<i class='fa fa-caret-square-o-right' aria-hidden='true'></i> "
										+"Continuar"
									+"</button>"
						},
						{
							type: "warning",
							delay: 0,
							allow_dismiss: false,
							placement:
								{
									from: "bottom",
									align: "center"
								}
						});
	swNotify = 1;
	}
	else
	{
		if(bets.length == 0)
		{
			notify.close();
			swNotify = 0;
		}
		else
		{
					notify.update("message", message
						+" <button class='btn btn-primary btn-text' data-toggle='modal' href='#modal-bets'>"
							+"<i class='fa fa-caret-square-o-right' aria-hidden='true'></i> "
							+"Continuar"
						+"</button>");
		}
	}
}

function make_bet(){
	$("#btn-bet").prop('disabled', true);
	$("#ticket").empty();
	
	$("#modal-bets").modal("hide");
	$("#modal-loading").modal("show");
	$.ajax({
		url: ruta+'bet',
		type: 'POST',
		dataType: 'json',
		data: {
				compuesta : compuesta,
				bets : bets,
				ganancia : ganancia
			   },
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        },
	})
	.done(function(data) {
		var title = data.title;
		var message = data.message;
		var saldo = data.saldo;
		var id = data.id;
		if(title == "valida"){			
			$("#modal-loading").modal("hide");			
			$("#bet-result").modal("show");
			$("#bet-message").html(message);

			if (bets[0].tipo != 19){
				for (var i = 0; i < id.length; i++) {
					ticket(id[i], 0);
				}
			} else{
				ticketSorteo(id);
			}


			bets = [];
			$(".td-bet").removeClass('bet-on').addClass('bet-off');
			$("#bets-count").html("");
			$(".alert-bet").remove();
			notify.close();
			$("#saldo").html("<i class='fa fa-money' aria-hidden='true'></i> " + numberWithCommas(saldo))
			balance = saldo;
		}
		else{
			$("#modal-loading").modal("hide");
			$("#bet-forbidden").modal("show");
			$("#forbiden-label").html("<i class='fa fa-exclamation-circle' aria-hidden='true'></i> " + title);
			$("#forbidden-message").html(message);
			$("#modal-bets").modal("hide");
			notify.close();
			compuesta = 0;
		}
		reset();
	})
}

$(".input-number").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
         // Allow: Ctrl+C
        (e.keyCode == 67 && e.ctrlKey === true) ||
         // Allow: Ctrl+X
        (e.keyCode == 88 && e.ctrlKey === true) ||
         // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

$('#bet-close').on('click', function (){
	//$('.modal-backdrop.fade.in').remove();	
});