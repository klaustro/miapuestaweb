//Pagos file
var bancos = 0;


$(function() {
	InitCloseModal()
})  


function showPagos()
{
    pagosInit();
    $("#fecha_hora").val("");
    $("#monto").val("");
    $("#tipo").val("0");
    $("#referencia").val("");
    $("#banco").val("0");
    $("#titular").val("");
    $("#ultimos_digitos").val("");
    carga_bancos();
    show_cuentas();
}

function pagosInit()
{
    $('#pago-group-fecha').datetimepicker({
        format: 'YYYY-MM-DD',
        ignoreReadonly: true,
    });

    $("#group-fecha").on("dp.change", function(e) {
        validate();
    });
    $("#monto").keyup(function(event) {
        validate();
    });
    $("#tipo").change(function(event) {
        validate();
    });
    $("#referencia").keyup(function(event) {
        validate();
    });
    $("#banco").change(function(event) {
        validate();
    });
    $("#titular").keyup(function(event) {
        validate();
    });
    $("#ultimos_digitos").keyup(function(event) {
        validate();
    });

    $("#btn-pago").click(function(){
        $("#modal-loading").modal("show");
        $.ajax({
            url: ruta + 'pago',
            type: 'POST',
            dataType: 'json',
            data: {
                    fecha_hora : $("#fecha_hora").val(),
                    monto : $("#monto").val(),
                    tipo : $("#tipo").val(),
                    referencia : $("#referencia").val(),
                    banco : $("#banco").val(),
                    titular : $("#titular").val(),
                    ultimos_digitos : $("#ultimos_digitos").val()
                  },
            headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                     },
        })
        .done(function(data) {
            var message = data.message;
            $("#pagoModal").modal('hide');
            $("#modal-message").modal("show");
            $("#title-message").html('<i class="fa fa-check" aria-hidden="true"></i> Pago Exitoso');
            $("#message").html(message);
        })
        .fail(function(data) {
            console.log(data);
            if (data.status == 406) {
                alert(data.responseText);
            } 
            else{
                session_expired(data);                
            }
        })
        .always(function() {
            $("#modal-loading").modal("hide");
        });

    });

    function validate(){
        if($("#fecha_hora").val()=="" ||
        $("#monto").val()=="" ||
        $("#tipo").val() == 0 ||
        $("#referencia").val()=="" ||
        $("#banco").val() == 0 ||
        $("#titular").val()=="" ||
        $("#ultimos_digitos").val()=="")
        {
            $("#btn-pago").prop('disabled', true);
        }
        else{
            $("#btn-pago").prop('disabled', false);
        }
    }
}


function show_cuentas(){
    $("#modal-message").modal("show");
    $("#title-message").html('<i class="fa fa-bank" aria-hidden="true"></i> Cuentas para depositar');
    content = +"<table class='subt1'>"
                +"<tbody><tr>"
                    +"<td colspan='3'>Deposite en la siguiente cuenta corriente o pago móvil a nombre de <strong>Fernando Rivas</strong> C.I. <strong>17266490</strong></td>"
                +"</tr>"
                +"<tr>"
					+"<td><img class='img-banco' src='/images/banesco.jpg' alt='Banesco' title='Banesco'></td>"
					+"<td>Banesco</td>"
					+"<td>01340532875321113240</td>"
				+"</tr>"
				+"<tr>"
					+"<td><img class='img-banco' src='/images/banesco-pago-movil.png' alt='Banesco' title='Pago Móvil'></td>"
					+"<td>Banesco</td>"
					+"<td><b>Tlf:</b> 04122985469 <b>CI:</b> 17266490</td>"
				+"</tr>"				
            +"</tbody></table>"
	$("#message").html(content);
}

function InitCloseModal() {
	
	$('.modal').on('hidden.bs.modal', function (e) {
		$('.modal-backdrop').trigger('click');
	})		
}