//Historial file
var weeks = 0;
var status = 0;
var page = 1;
var ticket_id = "";
var corredor = $("#corredor").val();

function searchHistory()
{
	ticket_id = $("#ticket-id").val();
	weeks = $("#select-weeks").val();
	status =  $("#select-status").val();
	cargarHistory(weeks, status, page, ticket_id);
	$("#ticket-id").val('');
	console.log('corredor = ' + corredor);
}

function cargarHistory(weeks, status, current_page, ticket_id){
	swView();
	$("#panel-historial").html(img_loading);
	$.ajax({
		url: ruta + 'history?page='+ current_page,
		type: 'GET',
		dataType: 'json',
		data:{
			'weeks' : weeks,
			'status' : status,
			'id': ticket_id
		}
	})
	.done(function(data) {
		response = data.bets.data;
		balance = data.balance;
		totales = data.totales;
		rango = data.rango;
		//console.log(data);
		var content ="";
		if(response.length >0){
				 content +="<div class='accordion-group panel-ticket center-block'>"
								+"<div class='accordion-heading'>"
									+"<a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion2' href='#collapse1' onclick=''>"
									+ "<h6 class='orange-txt text-center'><i class='fa fa-plus-circle' aria-hidden='true'></i> Balance Semanal del " + rango + "</h6>"
									+"</a>"
								+"</div>"
								+"<div id='collapse1' class='accordion-body collapse'>"
									+"<div class='accordion-inner'>"
						var total = 0;
						for (var i = 0; i < balance.length; i++)
						{
							content += "<ul class='list-group col-md-2 list-balance'>"
										    +"<li class='list-group-item disabled text-center'><strong>" + balance[i].dia + "</strong></li>"
										    +"<li class='list-group-item'><strong>Utilidad: </strong>" + balance[i].gana + "</li>";
							if (corredor == 0) {
								content +=  "<li class='list-group-item'><strong>Entradas / Salidas: </strong>" + balance[i].movimiento + "</li>"
										    +"<li class='list-group-item'><strong>Balance: </strong>" + balance[i].saldo + "</li>";
							}
							else{
								content += "<li class='list-group-item'><strong>Ventas: </strong>" + balance[i].ventas + "</li>"
										    +"<li class='list-group-item'><strong>Comisión: </strong>" + balance[i].comision + "</li>"	;						
							}

							content += 	"</ul>";
						}
						content += "<ul class='list-group col-md-2 list-balance'>"
									    +"<li class='list-group-item disabled text-center active'><strong>TOTAL SEMANAL</strong></li>"
									    +"<li class='list-group-item'><strong>Utilidad: " + totales.gana + "</strong></li>";
					    	if (corredor == 0) {
							content += 		"<li class='list-group-item'><strong>Entradas / Salidas: " + totales.movimiento + " </strong></li>"
										    +"<li class='list-group-item'><strong>Balance: " + totales.saldo + "</strong></li>";
					    	} 
					    	else {
							content += 		"<li class='list-group-item'><strong>Ventas: " + totales.ventas + "</strong></li>"
										    +"<li class='list-group-item'><strong>Comisión: " + totales.comision + "</strong></li>"					    		
					    	}

						content += 		"</ul>";
						content +="</div>"
							+"</div>"
							+"<div class='clearfix'></div>"
						+"</div>";

			for (var i = 0; i < response.length; i++) {
			var ganancia = 0;
			content += "<div class='panel panel-default panel-ticket'>"
				+"<div class='panel-heading height-bet'><strong>Ticket </strong>#" + response[i].id 
				+ "<p class='pull-right'><strong>Fecha: </strong> " + format_date(response[i].fecha_registro) + "</p><div class='clearfix'></div></div>"
				+"<div class='panel-body'>"
					+"<ul class='list-group'>";
				var bets_detail = response[i].bets_detail;	

				for (var j = 0; j < bets_detail.length; j++) {
					var class_badge = "";
					var txt_badge = "";
					var resultado = " - ";					
					if (bets_detail[j].games_probs.game.status==0) {
						txt_badge = "Pendiente";
					}
					else{
						if(bets_detail[j].games_probs.acierto == 1){
							var class_badge = "badge-success";
							var txt_badge = "Acertó";
						}
						else{
							var class_badge = "badge-important";
							var txt_badge = "No acertó";
						}
						if (bets_detail[j].games_probs.game.resultado.length >1) {
							resultado = "<strong> " + bets_detail[j].games_probs.game.resultado[0].score + "- " + bets_detail[j].games_probs.game.resultado[1].score + "</strong> ";
						}

					}
					evento = bets_detail[j].games_probs.tipo_probabilidad == 19 ? 'LOTTO ACTIVO - ' + format_date(bets_detail[j].games_probs.game.fecha_cierre) : bets_detail[j].games_probs.game.equipo[0].nombre + resultado + bets_detail[j].games_probs.game.equipo[1].nombre;
					content += "<li class='list-group-item height-bet text-uppercase'><span class='badge " + class_badge + " text-capitalize'>" + txt_badge + "</span>" + evento + " | <strong>" + seleccion(bets_detail[j].games_probs) + "</strong></li>"
					ganancia = ganancia + (response[i].monto * bets_detail[j].games_probs.probabilidad);
				}
				var button = "";
				var comision = "";
				if (corredor == 1) {
					//comision = ' <strong>Comision:</strong> ' + numberWithCommas(parseFloat(response[i].comision))
					if (response[i].status==2){
						button = " <button class='btn btn-primary btn-text btn-xs' onClick='pay(" + response[i].id + ")'><i class='fa fa-dollar' aria-hidden='true'></i> Pagar</button>";
					} 
					if (response[i].status==5) {
						button = " <span class='badge badge-warning text-capitalize'>Pagada</span>" ;
					}
					if (response[i].status==1){
						var button = " <button class='btn btn-primary btn-text btn-xs' onClick='ticket(" + response[i].id + ", 1)'><i class='fa fa-print' aria-hidden='true'></i> Imprimir</button>";			
					}
				} 

				content += "</ul>"
				+"</div>"
				+"<div class='panel-footer height-bet'>"
					+"<strong>Monto Arriesgado: </strong>" + numberWithCommas(parseFloat(response[i].monto)) 
					+comision
					+ " <p class='pull-right'>"
						+"<strong>A Ganar: </strong>" + numberWithCommas(parseFloat(response[i].ganancia)) 
						+button
					+"</p>"
					+"<div class='clearfix'></div></div>"
			+"</div>";
			}

			if(data.bets.last_page > 1)
			{
				previus = current_page - 1;
				next = current_page + 1;
				content += "<div class='pagination pagination-small text-center'>"
	                    +"<ul>";
	            if (current_page != 1)
	            	content +=	"<li><a class='disabled' href='##' onClick='cargarHistory(" + weeks + ", " + status + ", " + previus + ")'>«</a></li>";

	            for (var i = 1; i <=  data.bets.last_page; i++) {
	            	active = "";
	            	if(current_page == i)
	            		active = "btn-primary";
	            	content +=	 "<li><a class='" + active + "' href='##' onClick='cargarHistory(" + weeks + ", " + status + ", " + i + ")'>" + i + "</a></li>"
	            }
	            if (current_page != data.bets.last_page)
	            	content +="<li><a href='##' onClick='cargarHistory(" + weeks + ", " + status + ", " + next + ")'>»</a></li>";

	            content +="</ul>"
	                +"</div>";
	            content += "<p class='text-center rows'>Desde " + data.bets.from + " hasta " + data.bets.to + " de " + data.bets.total + " apuestas</p>";
			}
		}
		else {
			var txtWeek = "en ésta semana";
			if(weeks > 1)
				txtWeek = "de hace "+  weeks + " Semanas";

			content = "<div class='alert alert-error'>No tiene apuestas registradas " + txtWeek + "</div>"
		}


		$("#panel-historial").html(content);
	})
	.fail(function(data) {
		console.log(data.statusText + " (" + data.status +")");
	})
	.always(function() {
		//console.log("complete");
		$("#modal-loading").modal("hide");
	});

}

$(window).resize(function () {
    swView();
});

function swView() {
    //console.log($(window).width());
    if ($(window).width() < 992)
    {
    	$("#west").removeClass('text-left').addClass('text-center');
    	$("#noerht").removeClass('text-center').addClass('text-center');
    	$("#east").removeClass('text-right').addClass('text-center');
    	$("#saldo").removeClass('text-right').addClass('text-center');
    }
    else
    {
    	$("#west").removeClass('text-center').addClass('text-left');
    	$("#noerht").removeClass('text-center').addClass('text-center');
    	$("#east").removeClass('text-center').addClass('text-right');
    	$("#saldo").removeClass('text-center').addClass('text-right');
    }
}

function pay(id){
	$.ajax({
		url: ruta + 'bet/ '+id,
		type: 'PUT',
		dataType: 'json',
		data: {status: '5'},
		headers: {
		    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},		
	})
	.done(function(data) {
		console.log(data);
		message = data.message;
            $("#modal-message").modal("show");
            $("#title-message").html('<i class="fa fa-check" aria-hidden="true"></i> Apuesta pagada');
            $("#message").html(message);		
            searchHistory();
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});

}

$( "#ticket-id" ).keypress(function(e) {
  if ( e.which == 13 ) {
     searchHistory();
  }
  $("#select-status").val('0');
});
