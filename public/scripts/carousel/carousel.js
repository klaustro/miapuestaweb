//Carousel file
function startCamera() 
{
	$('#camera_wrap').camera({ 
		fx: 'scrollLeft', time: 2000, loader: 'none', playPause: false, navigation: true, height: '35%', pagination: true
	});
}

$(function() {
	startCamera();
});