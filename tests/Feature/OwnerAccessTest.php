<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class OwnerAccessTest extends TestCase
{    
    use DatabaseTransactions;

    protected $admin;
    protected $owner;
    protected $user;
 
    protected function setUp()
    {
        parent::setUp();

        $this->admin = factory(User::class)->create([
            'role' => 'admin',
        ]);


        $this->owner = factory(User::class)->create([
            'role' => 'owner',
        ]);

        $this->user = factory(User::class)->create([
            'role' => 'user',
        ]);

    }

    /** @test */

    function an_owner_user_can_see_owner_menu ()
    {
        $this->actingAs($this->owner)
            ->get('/')
            ->assertSee('Apuestas')
            ->assertSee('Comisiones por Corredor')
            ->assertDontSee('Datos');
    }

    /** @test */
    function an_guest_user_cannot_see_owner_menu()
    {
        $this->get('/')
            ->assertDontSee('Apuestas')
            ->assertDontSee('Comisiones por Corredor')
            ->assertDontSee('Datos');
    }
  

    /** @test */
    function an_normal_user_cannot_see_owner_menu()
    {
        $this->actingAs($this->user)
            ->get('/')
            ->assertDontSee('Apuestas')
            ->assertDontSee('Comisiones por Corredor')
            ->assertDontSee('Datos');
    }    

    //Test Routes

    /** @test */
    function an_owner_user_can_enter_in_owners_pages()
    {           
        $this->actingAs($this->owner)
            ->get('owner/runner')
            ->assertStatus(200);
    }

    /** @test */
    function an_admin_user_can_enter_in_owners_pages()
    {   
        $this->actingAs($this->admin)
            ->get('owner/runner')
            ->assertStatus(200);
    }    

    /** @test */
    function an_owner_user_cannot_enter_in_forbiddeen_page()
    {   
        $this->actingAs($this->owner)
            ->get('admin/menu')
            ->assertStatus(403)
            ->assertSee('Acceso no autorizado');
    }

    /** @test */
    function a_normal_user_cannnot_enter_in_owners_pages()
    {   
        $this->actingAs($this->user)
            ->get('owner/runner')
            ->assertStatus(403)
            ->assertSee('Acceso no autorizado');
    } 

    
    /** @test */
    function a_guest_user_cannnot_enter_in_owners_pages()
    {   
        $this->get('admin/runner')
            ->assertStatus(404)
            ->assertSee('Página no encontrada');
    }    

}
