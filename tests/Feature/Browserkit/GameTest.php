<?php

namespace Tests\Feature;

use Tests\FeatureTestCase;

class GameTest extends FeatureTestCase
{

	function testBasicTest()
	{
	  $this->assertTrue(true);
	}

    function no_test_if_game_is_listed()
    {
    	//Having
    	$admin = factory(\App\User::class)->create([
    		'role' => 'admin'
    	]);

    	$this->actingAs($admin);

    	$status = factory(\App\Stsgame::class)->create([
    		'id' => 0,
    		'nombre' => 'pendiente'
    	]);

    	$game =  factory(\App\Game::class)->create([
    		'status' => $status->id,
    	]);

    	$gamesProbs = factory(\App\GamesProb::class)->times(2)->create([
    		'idgame' => $game->id,
    		'tipo_probabilidad' => 1
    	]);

    	$this->visit(route('admin.game.index'))
    		->seeInElement('li', $game->event);
    }

	function no_test_game_list_is_paginated()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);

	    	$games = [];

	    	for ($i=0; $i<16; $i++) { 
	    		$games[$i] =  factory(\App\Game::class)->create([
	    			'status' => 0
	    		]);

			factory(\App\GamesProb::class)->times(2)->create([
				'idgame' => $games[$i]->id,
	    			'tipo_probabilidad' => '1',
	    			'activa' => '1',
	    		]);	    		
	    	}

		//When
		$this->visit(route('admin.game.index'));
		//dd($games[0]->event, $games[19]->event);
		//Then
		$this->see($games[15]->event)	
			->dontSee($games[0]->event);
	}    

	function no_test_game_data_can_be_showed()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);

    		$game =  factory(\App\Game::class)->create([
    			'status' => 0
    		]);

		factory(\App\GamesProb::class)->times(2)->create([
			'idgame' => $game->id,
    			'tipo_probabilidad' => '1',
    			'activa' => '1',
    		]);
		//dd($game->teams);
		
		//When
		$this->visit("admin/game/{$game->id}");

		//Then
		$this->seeInElement('h6', 'Resultados')
			->see($game->id )
			->see("{$game->event}")
			->seeInElement('li', $game->teams->pluck('nombre')[0])
			->seeInElement('li', $game->teams->pluck('nombre')[1]);		
	}		

	function no_test_game_can_be_finded_by_event()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);

    		$searchedGame =  factory(\App\Game::class)->create([
    			'status' => 0,
    			'categoria' => 1
    		]);

		$searcerdGamesProbs =factory(\App\GamesProb::class)->times(2)->create([
			'idgame' => $searchedGame->id,
    			'tipo_probabilidad' => '1',
    			'activa' => '1',
    		]);    	

    		$games =  factory(\App\Game::class)->times(15)->create([
    			'status' => 0,
    			'categoria' => 1
    		]);

    		foreach ($games as $key => $game) {
			$gamesProbs =factory(\App\GamesProb::class)->times(2)->create([
				'idgame' => $game->id,
	    			'tipo_probabilidad' => '1',
	    			'activa' => '1',
	    		]);   
    		}			

		$searchTeam = $searchedGame->teams->pluck('nombre')[0];
		//When
		$this->visit('/admin/game')
			->dontSeeInElement('li',"#{$searchedGame->id}")
			->type($searchTeam , 'search')
			->press('Buscar');



		//Then
		$this->seeInElement('li', $searchedGame->event);
	}

	function no_test_game_can_be_finded_by_id()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);

    		$searchedGame =  factory(\App\Game::class)->create([
    			'status' => 0,
    			'categoria' => 1
    		]);

		$searcerdGamesProbs =factory(\App\GamesProb::class)->times(2)->create([
			'idgame' => $searchedGame->id,
    			'tipo_probabilidad' => '1',
    			'activa' => '1',
    		]);    	

    		$games =  factory(\App\Game::class)->times(15)->create([
    			'status' => 0,
    			'categoria' => 1
    		]);

    		foreach ($games as $key => $game) {
			$gamesProbs =factory(\App\GamesProb::class)->times(2)->create([
				'idgame' => $game->id,
	    			'tipo_probabilidad' => '1',
	    			'activa' => '1',
	    		]);   
    		}			

		$searchTeam = $searchedGame->teams->pluck('nombre')[0];

		//When
		$this->visit('/admin/game')
			->dontSeeInElement('li',"#{$searchedGame->id}")
			->type($searchedGame->id , 'search')
			->press('Buscar');



		//Then
		$this->seeInElement('li', $searchedGame->event);
	}		

	function no_test_category_filter()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);

    		$searchedGames =  factory(\App\Game::class)->times(5)->create([
    			'status' => 0
    		]);

    		foreach ($searchedGames as $key => $game) { 
			factory(\App\GamesProb::class)->times(2)->create([
				'idgame' => $game->id,
	    			'tipo_probabilidad' => '1',
	    			'activa' => '1',
	    		]);    	   		
    		}

    		$games =  factory(\App\Game::class)->times(15)->create([
    			'status' => 0,
    			'categoria' => 3
    		]);

    		foreach ($games as $key => $game) {
			factory(\App\GamesProb::class)->times(2)->create([
				'idgame' => $game->id,
	    			'tipo_probabilidad' => '1',
	    			'activa' => '1',
	    		]);   
    		}				

		//When
		$this->visit('/admin/game')
			->dontSeeInElement('li',"#{$searchedGames[0]->id}")
			->select($searchedGames[0]->categoria, 'categoria')
			->press('Buscar');

		//Then
		$this->seeInElement('li', $searchedGames[0]->event);		
		$this->dontSeeInElement('li', $games[0]->event);		
	}

	function no_test_if_games_can_be_filtered_by_status()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);

	    	factory(\App\Stsgame::class)->create([
	    		'id' => '0',
	    		'nombre' => 'PENDIENTE'
	    	]);

	    	factory(\App\Stsgame::class)->create([
	    		'id' => '1',
	    		'nombre' => 'CERRADO'
	    	]);	    	

    		$openGames =  factory(\App\Game::class)->times(5)->create([
    			'status' => 0
    		]);

    		foreach ($openGames as $key => $game) { 
			factory(\App\GamesProb::class)->times(2)->create([
				'idgame' => $game->id,
	    			'tipo_probabilidad' => '1',
	    			'activa' => '1',
	    		]);    	   		
    		}

    		$closedGames =  factory(\App\Game::class)->times(5)->create([
    			'status' => 1,
    			'categoria' => 3
    		]);

    		foreach ($closedGames as $key => $game) {
			factory(\App\GamesProb::class)->times(2)->create([
				'idgame' => $game->id,
	    			'tipo_probabilidad' => '1',
	    			'activa' => '1',
	    		]);   
    		}				

		//When
		$this->visit('/admin/game')
			->dontSeeInElement('li',"#{$openGames[0]->id}")
			->select('0', 'status')
			->press('Buscar');

		//Then
		$this->seeInElement('li', $openGames[0]->event);		
		$this->dontSeeInElement('li', $closedGames[0]->event);			
	}
}

