<?php

namespace Tests\Feature;

use Tests\FeatureTestCase;

class PayTest extends FeatureTestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_pay_view_exists()
    {
    	//Having
    	$admin = factory(\App\User::class)->create([
    		'role' => 'admin'
    	]);

    	$this->actingAs($admin);

    	//When
        $this->visit('/admin/pago')
        	->seeInElement('h4', 'Lista de pagos');
    }

    public function test_if_pays_is_listed()
    {
    	//Having
    	$admin = factory(\App\User::class)->create([
    		'role' => 'admin'
    	]);

    	$this->actingAs($admin);

    	$pay = factory(\App\Pago::class)->create();

    	$this->visit('/admin/pago')
    		->seeInElement('li', $pay->id);
    }

	public function test_pay_list_is_paginated()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);

		$first = factory(\App\Pago::class)->create();

		factory(\App\Pago::class)->times(15)->create();

		$last = factory(\App\Pago::class)->create();

		//When
		$this->visit('/admin/pago');

		//Then
		$this->see($last->user->name)	
			->dontSee($first->user->name);
	}    

	public function test_pay_data_can_be_showed()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);

		$banco = \App\Banco::create([
			'nombre' => 'Banesco'
		]);

		$pago = factory(\App\Pago::class)->create([
			'banco' => $banco->id,
			'tipo' => '0'
		]);

		//When
		$this->visit("/admin/pago/{$pago->id}");

		//Then
		$this->seeInElement('li',$pago->id )
			->seeInElement('li', $pago->user->name)
			->seeInElement('li', 'Depósito')
			->seeInElement('li', $pago->referencia)
			->seeInElement('li', $pago->bank->nombre)
			->seeInElement('li', $pago->titular) 
			->seeInElement('li', $pago->ultimos_digitos)
			->seeInElement('li', $pago->monto);
	}	

	public function test_pay_can_be_approved()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);

		$banco = \App\Banco::create([
			'nombre' => 'Banesco'
		]);

		$pay = factory(\App\Pago::class)->create([
			'banco' => $banco->id,
			'status' => '0'
		]);

		//When 
		$this->visit("/admin/pago/{$pay->id}")
			->press('Aprobar');

		//Then
		$this->seeInDatabase('pagos',[
			'id' => $pay->id,
			'status' =>'1'
		]);

		$this->dontSeeInDatabase('pagos',[
			'id' => $pay->id+1,
			'status' =>'0'
		]);

		$this->seePageIs('/admin/pago')
			->seeInElement('span ', 'Aprobado');

	}

	public function test_pay_can_be_finded_by_id()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);

		$searched = factory(\App\Pago::class)->create();	

		factory(\App\Pago::class)->times(15)->create();		

		//When
		$this->visit('/admin/pago')
			->dontSeeInElement('li',"#{$searched->id}")
			->type($searched->id, 'search')
			->press('Buscar');

		//Then
		$this->seeInElement('li', $searched->id);
	}

	public function test_pay_can_be_finded_by_user_name()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);

		$searched = factory(\App\Pago::class)->create();	
		
		factory(\App\Pago::class)->times(15)->create();		

		//When
		$this->visit('/admin/pago')
			->dontSeeInElement('li',$searched->user->name)
			->type($searched->user->name, 'search')
			->press('Buscar');

		//Then
		$this->seeInElement('li', $searched->user->name);
	}	

	public function test_pay_can_be_finded_by_user_name_like()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);

		$searched1 = factory(\App\Pago::class)->create([			
			'id_usuario' => function (){
            		return factory(\App\User::class)->create([
            			'name' => 'Alan Brito'
            		])->id;
       		 }
		]);	

		$searched2 = factory(\App\Pago::class)->create([			
			'id_usuario' => function (){
            		return factory(\App\User::class)->create([
            			'name' => 'Alanis Morisete'
            		])->id;
       		 }
		]);	
		
		factory(\App\Pago::class)->times(15)->create();		

		//When
		$this->visit('/admin/pago')
			->dontSeeInElement('li', 'Alan Brito')
			->dontSeeInElement('li','Alanis Morisete')
			->type('alan', 'search')
			->press('Buscar');

		//Then
		$this->seeInElement('li', 'Alan Brito')
			->seeInElement('li','Alanis Morisete');
	}		

	public function test_pay_can_be_disapproved()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);
		$banco = \App\Banco::create([
			'nombre' => 'Banesco'
		]);

		$pay = factory(\App\Pago::class)->create([
			'banco' => $banco->id,
			'status' => '1'
		]);

		//When 
		$this->visit("/admin/pago/{$pay->id}")
			->press('Desaprobar');

		//Then
		$this->seeInDatabase('pagos',[
			'id' => $pay->id,
			'status' =>'0'
		]);

		$this->seePageIs('/admin/pago')
			->seeInElement('span ', 'Pendiente');
	}
}
