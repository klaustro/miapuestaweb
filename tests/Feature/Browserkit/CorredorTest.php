<?php
namespace Tests\Feature;

use Tests\FeatureTestCase;

class CorredorTest extends FeatureTestCase
{
	protected $admin;
	protected $tipoComision;
	protected $comision;
	protected $game;
	protected $betType;
	protected $gamesProb;
	protected $ten;
	protected $corredor;
	protected $otherRunner;
	protected $corredorComision;

	function setUp()
	{
		parent::setUp();

		$this->admin = factory(\App\User::class)->create([
			'role' => 'admin'
		]);

		$this->actingAs($this->admin);

		$this->tipoComision = factory(\App\TipoComision::class)->create([
			'id' => '1',
		]);		

		$this->comision = factory(\App\Comision::class)->create([
			'tipo' => '1'
		]);		

	  	$this->game = factory(\App\Game::class)->create();

	  	$this->betType =  factory(\App\TipoApuesta::class)->create([
	  		'id' => '1',
	  		'nombre' => 'Ganador'
	  	]);  	

	  	$this->gamesProb = factory(\App\GamesProb::class)->times(2)->create([
	  		'idgame' => $this->game->id,
	  		'tipo_probabilidad' => $this->betType->id,
	  		'acierto' => '1',
	  	]);  	

	  	$this->ten = factory(\App\Comision::class)->create([
	  		'tipo' => $this->tipoComision->id,
	  		'desde' => '0',
	  		'hasta' => '9999',
	  		'comision' => '10'
	  	]);

	  	$this->corredor = factory(\App\User::class)->create([
	  		'name' => 'Juan Palencia',
	  		'corredor' => '1'
	  	]);	  	

	  	$this->otherRunner = factory(\App\User::class)->create([
	  		'corredor' => '1'
	  	]);

	  	$this->corredorComision = factory(\App\CorredorComision::class)->create([
		'idusuario' => $this->corredor->id,
		'idtipo_comision' => $this->tipoComision->id,
		'ventas' => '1'	
	  	]); 	
	}

	public function test_if_commission_runner_can_be_setted()
	{
	  	//Having

		$user = factory(\App\User::class)->create([
			'name' => 'Usuario',
			'corredor' => '1'
		]);		


		//When
		$this->visit('admin/user/' . $user->id)
			->select('1', 'tipoComision')
			->select('1', 'ventas')
			->press('Enviar');

		//Then
		$this->seeInDatabase('corredor_comision',[
			'idusuario' => $user->id,
			'idtipo_comision' => '1',
			'ventas' => '1',
		]);

		$this->seePageIs('admin/user');
	}

	public function test_if_commission_runner_can_be_showed()
	{
		 //Having
		$user = factory(\App\User::class)->create([
			'name' => 'Usuario',
			'corredor' => '1'
		]);	

		factory(\App\CorredorComision::class)->create([
			'idusuario' => $user->id,
			'idtipo_comision' => '1',
			'ventas' => '0',
		]);

		//dd($user->sells, $user->tipoComision);

		//When
		$this->visit('admin/user/' . $user->id);

		//Then
		$this->see('<option value="1" selected>')
			->see('<option value="0" selected>');

	}    

	public function test_runner_commission_is_paginate()
	{
	  	//Having
	  	$status = factory(\App\Stsbet::class)->create();

		$first = factory(\App\Bet::class)->create([
			'usuario' => $this->corredor->id,
			'monto' => '10000.00',
			'ganancia' => '14900.00',
			'fecha' => '2017-06-21'
		]);

		 factory(\App\Bet::class)->times(14)->create([
		 	'usuario' => $this->corredor->id,
		 	'monto' => '10000.00',
			'ganancia' => '14900.00',
		 ]);

		$last = factory(\App\Bet::class)->create([
			'usuario' => $this->corredor->id,
			'monto' => '10000.00',
			'ganancia' => '14900.00',
			'fecha' => '2017-06-21'
		]);


		$firstDetail = factory(\App\BetsDetail::class)->create([
			'idbets' => $first->id,
			'idgames_probs' => $this->gamesProb[0]->id
		]);

		$lastDetail = factory(\App\BetsDetail::class)->create([
			'idbets' => $last->id,
			'idgames_probs' => $this->gamesProb[1]->id
		]);

		//When
		$this->visit("/owner/runner")
			->dontSeeInElement('td', $first->id)
			->seeInElement('td',$last->id);

	}

	public function test_runner_commision_can_be_filter_by_date()
	{

	  	//Having
		 factory(\App\CorredorComision::class)->create([
			'idusuario' => $this->otherRunner->id,
			'idtipo_comision' => $this->tipoComision->id,
			'ventas' => '1'	
		  	]);

		  	$status = factory(\App\Stsbet::class)->create();

		$searched = factory(\App\Bet::class)->create([
			'usuario' => $this->corredor->id,
			'monto' => '10000.00',
			'ganancia' => '14900.00',
			'fecha' => '2017-06-08'
		]);

		 factory(\App\Bet::class)->times(15)->create([
		 	'usuario' => $this->otherRunner->id,
		 	'monto' => '10000.00',
			'ganancia' => '14900.00',
			'fecha' => '2017-06-15'
		 ]);


		$fsearchedDetail = factory(\App\BetsDetail::class)->create([
			'idbets' => $searched->id,
			'idgames_probs' => $this->gamesProb[0]->id
		]);


		//When
		$this->visit("/owner/runner")
			->dontSee('<td>' . $searched->id . '</td> ')
			->type('2017-06-01', 'from')
			->type('2017-06-08', 'to')
			->press('Buscar');

		//Then
		$this->see('<td>' . $searched->id . '</td> ');
	}	

	public function test_runner_commision_can_be_filter_by_runner()
	{

	  	//Having
	  	factory(\App\CorredorComision::class)->create([
		'idusuario' => $this->otherRunner->id,
		'idtipo_comision' => $this->tipoComision->id,
		'ventas' => '1'	
	  	]);

	  	$status = factory(\App\Stsbet::class)->create();

		$searched = factory(\App\Bet::class)->create([
			'usuario' => $this->corredor->id,
			'monto' => '10000.00',
			'ganancia' => '14900.00',
			'fecha' => '2017-06-21'
		]);

		 factory(\App\Bet::class)->times(15)->create([
		 	'usuario' => $this->otherRunner->id,
		 	'monto' => '10000.00',
			'ganancia' => '14900.00',
		 ]);


		$fsearchedDetail = factory(\App\BetsDetail::class)->create([
			'idbets' => $searched->id,
			'idgames_probs' => $this->gamesProb[0]->id
		]);


		//When
		$this->visit("/owner/runner")
			->dontSeeInElement('td', $searched->id)
			->select($this->corredor->id, 'runner')
			->press('Buscar');

		//Then
		$this->seeInElement('td', $searched->id);
	}	

	public function test_runner_commision_can_be_filter_by_status()
	{

	  	//Having
	  	factory(\App\CorredorComision::class)->create([
		'idusuario' => $this->otherRunner->id,
		'idtipo_comision' => $this->tipoComision->id,
		'ventas' => '1'	
	  	]);

	  	$status = factory(\App\Stsbet::class)->create([
	  		'id' => '1'
	  	]);

	  	$otherStatus=factory(\App\Stsbet::class)->create([
	  		'id' => '2'
	  	]);

		$searched = factory(\App\Bet::class)->create([
			'usuario' => $this->corredor->id,
			'monto' => '10000.00',
			'ganancia' => '14900.00',
			'fecha' => '2017-06-21',
			'status' => '1'
		]);

		 factory(\App\Bet::class)->times(15)->create([
		 	'usuario' => $this->otherRunner->id,
		 	'monto' => '10000.00',
			'ganancia' => '14900.00',
			'status' => '2'
		 ]);


		$fsearchedDetail = factory(\App\BetsDetail::class)->create([
			'idbets' => $searched->id,
			'idgames_probs' => $this->gamesProb[0]->id
		]);


		//When
		$this->visit("/owner/runner")
			->dontSeeInElement('td', $searched->id)
			->select('1', 'status')
			->press('Buscar');

		//Then
		$this->seeInElement('td', $searched->id);
	}

}
