<?php
namespace Tests\Feature;

use Tests\FeatureTestCase;

class BetTypeTest extends FeatureTestCase
{
	public function test_type_bet_can_be_created()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);

	    	$primero = factory(\App\Menu::class)->create();   

	    	$segundo = factory(\App\Menu::class)->create();   

		//When 
		$this->visit(route('admin.typebet.create'))
			->see('Crear tipo de apuesta')
			->submitForm('Crear', [
			    'nombre' => 'Ganador',
			    'max_apuesta' => '400000',
			    'exotica' => '1',
			    'cantidad' => '1',
			    'probabilidades' => '2',
			    'pk' => '1',
			    'menu'  => [$primero->id, $segundo->id],
			]);			

		//Then
		$this->seeInDatabase('tipo_apuesta',[
			'nombre' =>'Ganador',
			'max_apuesta' => '400000',
			'exotica' => '1',
			'cantidad' => '1',
			'probabilidades' => '2',
			'pk' => '1',
		]);

		$this->seeInDatabase('apuestas_categorias',[
			'categoria' => $primero->id
		]);

		$this->seeInDatabase('apuestas_categorias',[
			'categoria' => $segundo->id
		]);


		$this->seePageIs('admin/typebet')
			->seeInElement('li ', 'Ganador');

	}

	public function test_type_bet_list_is_paginated()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);

		$first = factory(\App\TipoApuesta::class)->create([
			'nombre' => 'Primer tipo'
		]);

		factory(\App\TipoApuesta::class)->times(15)->create();

		$last = factory(\App\TipoApuesta::class)->create([
			'nombre' => 'Ultimo tipo'
		]);

		//When
		$this->visit('/admin/typebet');

		//Then
		$this->see($first->nombre)	
			->dontSee($last->nombre);
	}    	

	public function test_type_bet_data_can_be_showed()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);

		$typebet = factory(\App\TipoApuesta::class)->create([
			'exotica' => '1',
			'cantidad' => '0',
		]);

		//When
		$this->visit("/admin/typebet/{$typebet->id}");

		//Then
		$this->see($typebet->nombre )
			->see($typebet->max_apuesta)
			->see('<option value="1" selected>')
			->see('<option value="0" selected>')
			->seeInField('probabilidades', $typebet->probabilidades)
			->see('<option value="'.$typebet->pk.'" selected>');		
	}	

	public function test_type_bet_can_be_updated()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);


		$typebet = factory(\App\TipoApuesta::class)->create([
			'nombre' => 'Ganador'
		]);

	    	$primero = factory(\App\Menu::class)->create();   

	    	$segundo = factory(\App\Menu::class)->create();		

		//When 
		$this->visit("/admin/typebet/{$typebet->id}")
			->submitForm('Actualizar', [
			    'nombre' => 'Runline',
			    'max_apuesta' => '5000000',
			    'exotica' => '1',
			    'cantidad' => '1',
			    'probabilidades' => '2',
			    'pk' => '1',
			    'menu'  => [$primero->id, $segundo->id],
			]);		

		//Then
		$this->seeInDatabase('tipo_apuesta',[
			'nombre' =>'Runline',
			'max_apuesta' => '5000000',
			'exotica' => '1',
			'cantidad' => '1',
			'probabilidades' => '2',
			'pk' => '1',
		]);

		$this->seeInDatabase('apuestas_categorias',[
			'categoria' => $primero->id
		]);

		$this->seeInDatabase('apuestas_categorias',[
			'categoria' => $segundo->id
		]);		

		$this->seePageIs('/admin/typebet')
			->seeInElement('li ', 'Runline');

	}	

	public function test_type_bet_can_be_finded_by_name()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);

		factory(\App\TipoApuesta::class)->times(15)->create();		

		$searched = factory(\App\TipoApuesta::class)->create([
			'nombre' => 'Ganador'
		]);


		//When
		$this->visit('/admin/typebet')
			->dontSeeInElement('li',"#{$searched->id}")
			->type('Ganador', 'search')
			->press('Buscar');

		//Then
		$this->seeInElement('li', 'Ganador');
	}

}
