<?php

namespace Tests\Feature;

use Tests\FeatureTestCase;

use App\Equipo;

class TeamTest extends FeatureTestCase
{
    public function test_submenu_list_is_paginated()
    {
            //Having
            $admin = factory(\App\User::class)->create([
                'role' => 'admin'
            ]);

            $this->actingAs($admin);

        $first = factory(\App\Equipo::class)->create([
            'nombre' => 'Real Madrid'
        ]);

        factory(\App\Equipo::class)->times(15)->create();

        $last = factory(\App\Equipo::class)->create([
            'nombre' => 'Inter de Milan'
        ]);

        //When
        $this->visit('/admin/team');

        //Then
        $this->see('Real Madrid')   
            ->dontSee('Inter de Milan');
    }

    public function test_team_can_be_finded_by_name()
    {
            //Having
            $admin = factory(\App\User::class)->create([
                'role' => 'admin'
            ]);

            $this->actingAs($admin);

        factory(\App\Equipo::class)->times(15)->create();      

        $searched = factory(\App\Equipo::class)->create([
            'nombre' => 'Inter de Milan',
        ]); 

        //When
        $this->visit('/admin/team')
            ->dontSeeInElement('li', 'Inter de Milan')
            ->type('inter', 'search')
            ->press('Buscar');

        //Then
        $this->seeInElement('li', 'Inter de Milan');
    }    

    public function test_team_can_be_finded_by_id()
    {
            //Having
            $admin = factory(\App\User::class)->create([
                'role' => 'admin'
            ]);

            $this->actingAs($admin);

            factory(\App\Equipo::class)->times(15)->create();

            $searchedTeam =  factory(\App\Equipo::class)->create();

        //When
        $this->visit('/admin/team')
            ->dontSeeInElement('li',"#{$searchedTeam->nombre}")
            ->type($searchedTeam->id , 'search')
            ->press('Buscar');

        //Then
        $this->seeInElement('li', $searchedTeam->nombre);
    }    

    public function test_team_can_be_created()
    {
            //Having
            $admin = factory(\App\User::class)->create([
                'role' => 'admin'
            ]);

            $this->actingAs($admin);

        $menu = factory(\App\Menu::class)->create();            

        $primera = factory(\App\Submenu::class)->create([
            'id_menu' => $menu->id,
        ]);

        $segunda = factory(\App\Submenu::class)->create([
            'id_menu' => $menu->id,
        ]);

        //When 
        $this->visit(route('admin.team.create'))
            ->see('Crear equipo')
            ->submitForm('Crear', [
                'nombre' => 'Inter de Milan',
                'submenu'  => [$primera->id, $segunda->id],
                'abbr' => 'IM'
            ]);

        //Then
        $this->seeInDatabase('submenu2',[
            'nombre' =>'Inter de Milan',
            'abbr' => 'IM'
        ]);

        $this->seeInDatabase('equipo_subcategoria',[
            'id_submenu' => $primera->id
        ]);  
        
        $this->seeInDatabase('equipo_subcategoria',[
            'id_submenu' => $segunda->id
        ]);   

        $this->seePageIs('/admin/team')
            ->seeInElement('li ', 'Inter de Milan');

    }    

    public function test_team_data_can_be_showed()
    {
        //Having
        $admin = factory(\App\User::class)->create([
            'role' => 'admin'
        ]);

        $this->actingAs($admin);
        
        $menu = factory(\App\Menu::class)->create(); 
        
        $submenu = factory(\App\Submenu::class)->create([
            'id_menu' => $menu->id,
        ]);

        $team = factory(\App\Equipo::class)->create([
        'nombre' => 'Inter de Milan',
        'abbr' => 'IM'
        ]);

        $team->equipoSubcategoria()->create([
        'id_submenu' => $submenu->id
        ]);

        //When
        $this->visit("/admin/team/{$team->id}");

        //Then
        $this->seeInField('nombre', 'Inter de Milan')
        ->see('<option value="' .  $submenu->id . '" selected>')
        ->seeInField('abbr', 'IM'); 
    } 

    public function test_team_can_be_updated()
    {
        //Having
        $admin = factory(\App\User::class)->create([
            'role' => 'admin'
        ]);

        $this->actingAs($admin);

        $menu = factory(\App\Menu::class)->create();            

        $primera = factory(\App\Submenu::class)->create([
            'id_menu' => $menu->id,
        ]);

        $segunda = factory(\App\Submenu::class)->create([
            'id_menu' => $menu->id,
        ]);

        $tercera = factory(\App\Submenu::class)->create([
            'id_menu' => $menu->id,
        ]);        

        $team = factory(\App\Equipo::class)->create([
        'nombre' => 'Inter de Milan',
        'abbr' => 'IM'
        ]);

        $team->equipoSubcategoria()->create([
        'id_submenu' => $primera->id
        ]);


        //When 
        $this->visit('admin/team/' . $team->id)
            ->see('Actualizar equipo')
            ->submitForm('Actualizar', [
                'nombre' => 'Juventus',
                'submenu'  => [$primera->id, $segunda->id, $tercera->id],
                'abbr' => 'JUV'
            ]);            

        //Then
        $this->seeInDatabase('submenu2',[
            'nombre' =>'Juventus',
            'abbr' => 'JUV'
        ]);
        
        $this->seeInDatabase('equipo_subcategoria',[
            'id_equipo' => $team->id,
            'id_submenu' => $segunda->id
        ]);   

        $this->seePageIs('/admin/team')
            ->seeInElement('li ', 'Juventus');

    }        
}
