<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\FeatureTestCase;


class ResultTotalTest extends FeatureTestCase
{
    use DatabaseTransactions;

 protected function setUp()
    {
        parent::setUp();

        $this->admin = factory(\App\User::class)->create([
            'role' => 'admin'
        ]);

        $this->actingAs($this->admin);        

        $this->game =  factory(\App\Game::class)->create([
            'status' => 0,
            'categoria' => 1,
        ]);

        $this->male = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '1',
            'probabilidad' => '1',
            'orden' => '1',
            'activa' => '1',
            'acierto' => 0,
        ]);     

        $this->female = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '1',
            'probabilidad' => '2',
            'orden' => '2',
            'activa' => '1',
            'acierto' => 0,
        ]);   

    }

    function test_totals_up()     
    {
        $up = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '4', 
            'cantidad' => '2',
            'orden' => 1,      
            'activa' => '1',
            'acierto' => 0,
        ]);    

        $down = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '4', 
            'cantidad' => '2',
            'orden' => 2,      
            'activa' => '1',
            'acierto' => 0,
        ]);  

        $this->visit('/admin/game/' . $this->game->id)
            ->type('2', 'resultF[0]')
            ->type('1', 'resultF[1]')            
            ->press('Actualizar');   

        $this->seeInDatabase('games_probs',[
            'id' => $up->id,
            'acierto' => '1',
        ]);   

        $this->seeInDatabase('games_probs',[
            'id' => $down->id,
            'acierto' => '0',
        ]);           
    }

    function test_totals_up_half()     
    {
        $up = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '13', 
            'cantidad' => '2',
            'orden' => 1,      
            'activa' => '1',
            'acierto' => 0,
        ]);    

        $down = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '13', 
            'cantidad' => '2',
            'orden' => 2,      
            'activa' => '1',
            'acierto' => 0,
        ]);  

        $this->visit('/admin/game/' . $this->game->id)
            ->type('2', 'resultH[0]')
            ->type('1', 'resultH[1]')            
            ->press('Actualizar');   

        $this->seeInDatabase('games_probs',[
            'id' => $up->id,
            'acierto' => '1',
        ]);   

        $this->seeInDatabase('games_probs',[
            'id' => $down->id,
            'acierto' => '0',
        ]);           
    }

    function test_totals_down()     
    {
        $up = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '4', 
            'cantidad' => '2',
            'orden' => 1,      
            'activa' => '1',
            'acierto' => 0,
        ]);    

        $down = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '4', 
            'cantidad' => '2',
            'orden' => 2,      
            'activa' => '1',
            'acierto' => 0,
        ]);  

        $this->visit('/admin/game/' . $this->game->id)
            ->type('1', 'resultF[0]')
            ->type('0', 'resultF[1]')            
            ->press('Actualizar');   

        $this->seeInDatabase('games_probs',[
            'id' => $up->id,
            'acierto' => '0',
        ]);   

        $this->seeInDatabase('games_probs',[
            'id' => $down->id,
            'acierto' => '1',
        ]);           
    }  

    function test_totals_down_half()     
    {
        $up = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '13', 
            'cantidad' => '2',
            'orden' => 1,      
            'activa' => '1',
            'acierto' => 0,
        ]);    

        $down = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '13', 
            'cantidad' => '2',
            'orden' => 2,      
            'activa' => '1',
            'acierto' => 0,
        ]);  

        $this->visit('/admin/game/' . $this->game->id)
            ->type('1', 'resultH[0]')
            ->type('0', 'resultH[1]')            
            ->press('Actualizar');   

        $this->seeInDatabase('games_probs',[
            'id' => $up->id,
            'acierto' => '0',
        ]);   

        $this->seeInDatabase('games_probs',[
            'id' => $down->id,
            'acierto' => '1',
        ]);           
    }     

    function test_totals_PK()     
    {
        $up = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '4', 
            'cantidad' => '2',
            'orden' => 1,      
            'activa' => '1',
            'acierto' => 0,
        ]);    

        $down = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '4', 
            'cantidad' => '2',
            'orden' => 2,      
            'activa' => '1',
            'acierto' => 0,
        ]);  

        $this->visit('/admin/game/' . $this->game->id)
            ->type('1', 'resultF[0]')
            ->type('1', 'resultF[1]')            
            ->press('Actualizar');   

        $this->seeInDatabase('games_probs',[
            'id' => $up->id,
            'probabilidad' => 1,
            'acierto' => '1',
        ]);   

        $this->seeInDatabase('games_probs',[
            'id' => $down->id,
            'probabilidad' => 1,
            'acierto' => '1',
        ]);           
    }  

    function test_totals_PK_half()     
    {
        $up = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '13', 
            'cantidad' => '2',
            'orden' => 1,      
            'activa' => '1',
            'acierto' => 0,
        ]);    

        $down = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '13', 
            'cantidad' => '2',
            'orden' => 2,      
            'activa' => '1',
            'acierto' => 0,
        ]);  

        $this->visit('/admin/game/' . $this->game->id)
            ->type('1', 'resultH[0]')
            ->type('1', 'resultH[1]')            
            ->press('Actualizar');   

        $this->seeInDatabase('games_probs',[
            'id' => $up->id,
            'probabilidad' => 1,
            'acierto' => '1',
        ]);   

        $this->seeInDatabase('games_probs',[
            'id' => $down->id,
            'probabilidad' => 1,
            'acierto' => '1',
        ]);           
    }  

    function test_totals_home_up()     
    {
        $up = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '17', 
            'equipo' => $this->game->male->equipo,
            'cantidad' => '2',
            'orden' => 1,      
            'activa' => '1',
            'acierto' => 0,
        ]);    

        $down = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '17', 
            'equipo' => $this->game->female->equipo,
            'cantidad' => '2',
            'orden' => 2,      
            'activa' => '1',
            'acierto' => 0,
        ]);  

        $this->visit('/admin/game/' . $this->game->id)
            ->type('4', 'resultF[0]')
            ->type('1', 'resultF[1]')            
            ->press('Actualizar');   

        $this->seeInDatabase('games_probs',[
            'id' => $up->id,
            'acierto' => '1',
        ]);   

        $this->seeInDatabase('games_probs',[
            'id' => $down->id,
            'acierto' => '0',
        ]);           
    }

    function test_totals_visitor_up()     
    {
        $up = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '18', 
            'equipo' => $this->game->male->equipo,
            'cantidad' => '2',
            'orden' => 1,      
            'activa' => '1',
            'acierto' => 0,
        ]);    

        $down = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '18', 
            'equipo' => $this->game->female->equipo,
            'cantidad' => '2',
            'orden' => 2,      
            'activa' => '1',
            'acierto' => 0,
        ]);  

        $this->visit('/admin/game/' . $this->game->id)
            ->type('4', 'resultF[0]')
            ->type('3', 'resultF[1]')            
            ->press('Actualizar');   

        $this->seeInDatabase('games_probs',[
            'id' => $up->id,
            'acierto' => '1',
        ]);   

        $this->seeInDatabase('games_probs',[
            'id' => $down->id,
            'acierto' => '0',
        ]);           
    }    

    function test_home_totals_down()     
    {
        $up = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '17', 
            'cantidad' => '2',
            'orden' => 1,      
            'activa' => '1',
            'acierto' => 0,
        ]);    

        $down = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '17', 
            'cantidad' => '2',
            'orden' => 2,      
            'activa' => '1',
            'acierto' => 0,
        ]);  

        $this->visit('/admin/game/' . $this->game->id)
            ->type('1', 'resultF[0]')
            ->type('0', 'resultF[1]')            
            ->press('Actualizar');   

        $this->seeInDatabase('games_probs',[
            'id' => $up->id,
            'acierto' => '0',
        ]);   

        $this->seeInDatabase('games_probs',[
            'id' => $down->id,
            'acierto' => '1',
        ]);           
    }  

    function test_visitor_totals_down()     
    {
        $up = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '18', 
            'cantidad' => '2',
            'orden' => 1,      
            'activa' => '1',
            'acierto' => 0,
        ]);    

        $down = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '18', 
            'cantidad' => '2',
            'orden' => 2,      
            'activa' => '1',
            'acierto' => 0,
        ]);  

        $this->visit('/admin/game/' . $this->game->id)
            ->type('1', 'resultF[0]')
            ->type('1', 'resultF[1]')            
            ->press('Actualizar');   

        $this->seeInDatabase('games_probs',[
            'id' => $up->id,
            'acierto' => '0',
        ]);   

        $this->seeInDatabase('games_probs',[
            'id' => $down->id,
            'acierto' => '1',
        ]);           
    }      

    function test_home_totals_PK()     
    {
        $up = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '17', 
            'cantidad' => '2',
            'orden' => 1,      
            'activa' => '1',
            'acierto' => 0,
        ]);    

        $down = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '17', 
            'cantidad' => '2',
            'orden' => 2,      
            'activa' => '1',
            'acierto' => 0,
        ]);  

        $this->visit('/admin/game/' . $this->game->id)
            ->type('2', 'resultF[0]')
            ->type('1', 'resultF[1]')            
            ->press('Actualizar');   

        $this->seeInDatabase('games_probs',[
            'id' => $up->id,
            'probabilidad' => 1,
            'acierto' => '1',
        ]);   

        $this->seeInDatabase('games_probs',[
            'id' => $down->id,
            'probabilidad' => 1,
            'acierto' => '1',
        ]);           
    } 

    function test_visitor_totals_PK()     
    {
        $up = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '18', 
            'cantidad' => '2',
            'orden' => 1,      
            'activa' => '1',
            'acierto' => 0,
        ]);    

        $down = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '18', 
            'cantidad' => '2',
            'orden' => 2,      
            'activa' => '1',
            'acierto' => 0,
        ]);  

        $this->visit('/admin/game/' . $this->game->id)
            ->type('1', 'resultF[0]')
            ->type('2', 'resultF[1]')            
            ->press('Actualizar');   

        $this->seeInDatabase('games_probs',[
            'id' => $up->id,
            'probabilidad' => 1,
            'acierto' => '1',
        ]);   

        $this->seeInDatabase('games_probs',[
            'id' => $down->id,
            'probabilidad' => 1,
            'acierto' => '1',
        ]);           
    } 
}
