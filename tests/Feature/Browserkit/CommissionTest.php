<?php
namespace Tests\Feature;

use Tests\FeatureTestCase;

class CommissionTest extends FeatureTestCase
{
	public function test_comsion_can_be_created()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);

 		factory(\App\TipoComision::class)->create([
 			'id' => '1',
 		]);

		//When 
		$this->visit(route('admin.comision.create'))
			->see('Crear comisión')
			->select('1', 'tipo')
			->type('0.00', 'desde')
			->type('80.99', 'hasta')
			->type('0.5', 'comision')
			->press('Crear');

		//Then
		$this->seeInDatabase('comisiones',[
			'tipo' => '1',
			'desde' => '0.00',
			'hasta' => '80.99',
			'comision' => '0.5'
		]);

		$this->seePageIs('admin/comision')
			->seeInElement('li ', '80.99');

	}


	public function test_comision_list_is_paginated()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);

 		factory(\App\TipoComision::class)->create([
 			'id' => '1',
 			'nombre' => 'Primera Comision'
 		]);

 		factory(\App\TipoComision::class)->create([
 			'id' => '2',
 			'nombre' => 'Ultima Comision'
 		]); 		

 		factory(\App\TipoComision::class)->create([
 			'id' => '3',
 		]);

		$first = factory(\App\Comision::class)->create([
			'tipo' => '1'
		]);

		factory(\App\Comision::class)->times(15)->create([
			'tipo' => '3'
		]);

		$last = factory(\App\Comision::class)->create([
			'tipo' => '2'
		]);

		//When
		$this->visit('/admin/comision');

		//Then
		$this->see('Primera Comision')	
			->dontSee('Ultima comision');
	}    

	public function test_comision_data_can_be_showed()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);

		$comision = factory(\App\Comision::class)->create();

		//When
		$this->visit("/admin/comision/{$comision->id}");

		//Then
		$this->see($comision->name )
			->see($comision->desde)
			->see($comision->hasta)
			->see($comision->comision);		
	}	

	public function test_menu_can_be_updated()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);

 		factory(\App\TipoComision::class)->create([
 			'id' => '1',
 			'nombre' => 'Tipo viejo'
 		]);

 		factory(\App\TipoComision::class)->create([
 			'id' => '2',
 			'nombre' => 'Tipo nuevo'
 		]); 		

		$comision = factory(\App\Comision::class)->create([
			'tipo' => '1'
		]);

		//When 
		$this->visit("/admin/comision/{$comision->id}")
			->select('2', 'tipo')
			->type('0.00', 'desde')
			->type('80.99', 'hasta')
			->type('15', 'comision')
			->press('Actualizar');

		//Then
		$this->seeInDatabase('comisiones',[
			'tipo' => '2', 
			'desde' => '0.00', 
			'hasta' => '80.99', 
			 'comision' => '15',
		]);

		$this->seePageIs('/admin/comision')
			->seeInElement('li ', 'Tipo nuevo');

	}

	public function test_comision_can_be_finded_by_name()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);

		factory(\App\Comision::class)->times(15)->create();		

		$searched = factory(\App\Comision::class)->create();	

		//When
		$this->visit('/admin/comision')
			->dontSeeInElement('li',"#{$searched->id}")
			->type($searched->name, 'search')
			->press('Buscar');

		//Then
		$this->seeInElement('li', $searched->name);
	}
}
