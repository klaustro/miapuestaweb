<?php

namespace Tests\Feature;

use Tests\FeatureTestCase;

class MenuTest extends FeatureTestCase
{
	protected $admin;

	protected function setUp()
	{		
		parent::setUp();

    	$this->admin = factory(\App\User::class)->create([
    		'role' => 'admin'
    	]);		
	}


	public function test_menu_can_be_created()
	{
	    	//Having	    	

	    	$this->actingAs($this->admin);


		//When 
		$this->visit(route('admin.menu.create'))
			->see('Crear categoria')
			->type('FUTBOL', 'nombre')
			->type('5', 'orden')
			->type('Nota actializada', 'nota')
			->press('Crear');

		//Then
		$this->seeInDatabase('menu',[
			'nombre' =>'FUTBOL',
			'orden' => '5',
			'nota' => 'Nota actializada'
		]);

		$this->seePageIs('admin/menu')
			->seeInElement('li ', 'FUTBOL');

	}


    public function test_if_menu_is_listed()
    {
    	//Having    	

    	$this->actingAs($this->admin);

    	$menu = factory(\App\Menu::class)->create();

    	$this->visit('/admin/menu')
    		->seeInElement('li', $menu->nombre);
    }

	public function test_menu_list_is_paginated()
	{
	    	//Having	    	

	    	$this->actingAs($this->admin);

		$first = factory(\App\Menu::class)->create([
			'nombre' => 'Primera categoria'
		]);

		factory(\App\Menu::class)->times(15)->create();

		$last = factory(\App\Menu::class)->create([
			'nombre' => 'Ultima categoria'
		]);

		//When
		$this->visit('/admin/menu');

		//Then
		$this->see($first->nombre)	
			->dontSee($last->nombre);
	}    

	public function test_menu_data_can_be_showed()
	{
	    	//Having	    	

	    	$this->actingAs($this->admin);

		$menu = factory(\App\Menu::class)->create();

		//When
		$this->visit("/admin/menu/{$menu->id}");

		//Then
		$this->see($menu->nombre )
			->see($menu->orden)
			->see($menu->nota);		
	}	

	public function test_menu_can_be_updated()
	{
	    	//Having	    	

	    	$this->actingAs($this->admin);


		$menu = factory(\App\Menu::class)->create([
			'nombre' => 'BEISBOL'
		]);

		//When 
		$this->visit("/admin/menu/{$menu->id}")
			->type('FUTBOL', 'nombre')
			->type('5', 'orden')
			->type('Nota actializada', 'nota')
			->press('Actualizar');

		//Then
		$this->seeInDatabase('menu',[
			'id' => $menu->id,
			'nombre' =>'FUTBOL',
			'orden' => '5',
			'nota' => 'Nota actializada'
		]);

		$this->seePageIs('/admin/menu')
			->seeInElement('li ', 'FUTBOL');

	}

	public function test_menu_can_be_finded_by_name()
	{
	    	//Having	    	

	    	$this->actingAs($this->admin);

		factory(\App\Menu::class)->times(15)->create();		

		$searched = factory(\App\Menu::class)->create();	

		//When
		$this->visit('/admin/menu')
			->dontSeeInElement('li',"#{$searched->id}")
			->type($searched->name, 'search')
			->press('Buscar');

		//Then
		$this->seeInElement('li', $searched->name);
	}

	public function test_menu_can_be_finded_by_user_name_like()
	{
	    	//Having	    	

	    	$this->actingAs($this->admin);

	    	factory(\App\Menu::class)->times(15)->create();	

		$searched1 = factory(\App\Menu::class)->create([			
            			'nombre' => 'Alan Brito'       		 
		]);	

		$searched2 = factory(\App\Menu::class)->create([
            			'nombre' => 'Alanis Morisete'
            	]);				

		//When
		$this->visit('/admin/menu')
			->dontSeeInElement('li', 'Alan Brito')
			->dontSeeInElement('li','Alanis Morisete')
			->type('alan', 'search')
			->press('Buscar');

		//Then
		$this->seeInElement('li', 'Alan Brito')
			->seeInElement('li','Alanis Morisete');
	}		

}
