<?php

namespace Tests\Feature;

use Tests\FeatureTestCase;

class SubMenuTest extends FeatureTestCase
{
    public function test_if_submenu_is_listed()
    {
    	//Having
    	$admin = factory(\App\User::class)->create([
    		'role' => 'admin'
    	]);

    	$this->actingAs($admin);

    	$submenu = factory(\App\Submenu::class)->create();

    	$this->visit('/admin/submenu')
    		->seeInElement('li', $submenu->id);
    }

	public function test_submenu_list_is_paginated()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);

		$first = factory(\App\Submenu::class)->create([
			'nombre' => 'MLB'
		]);

		factory(\App\Submenu::class)->times(15)->create();

		$last = factory(\App\Submenu::class)->create([
			'nombre' => 'MLS'
		]);

		//When
		$this->visit('/admin/submenu');

		//Then
		$this->see('MLB')	
			->dontSee('MLS');
	}    

	public function test_submenu_can_be_created()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);

		$menu = factory(\App\Menu::class)->create();

		//When 
		$this->visit(route('admin.submenu.create'))
			->see('Crear liga')
			->type('Liga BBVA', 'nombre')
			->select($menu->id, 'id_menu')
			->type('400000', 'limite_directa')
			->type('400000', 'limite_compuesta')
			->type('0', 'orden')
			->select('0', 'mitad')
			->press('Crear');

		//Then
		$this->seeInDatabase('submenu',[
			'nombre' =>'Liga BBVA',
			'id_menu' => $menu->id,
			'limite_directa' => '400000',
			'limite_compuesta' => '400000',
		]);

		$this->seePageIs('/admin/submenu')
			->seeInElement('li ', 'Liga BBVA');

	}	

	public function test_submenu_data_can_be_showed()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);

		$submenu = factory(\App\Submenu::class)->create();

		//When
		$this->visit("/admin/submenu/{$submenu->id}");

		//Then
		$this->see($submenu->nombre )
			->see($submenu->orden)
			->see($submenu->nota);		
	}	

	public function test_submenu_can_be_updated()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);



		$submenu = factory(\App\Submenu::class)->create([
			'nombre' => 'MLB'
		]);

		$menu = factory(\App\Menu::class)->create();	
		
		//When 
		$this->visit("/admin/submenu/{$submenu->id}")
			->see('Actualizar liga')
			->type('Liga BBVA', 'nombre')
			->select($menu->id, 'id_menu')
			->type('400000', 'limite_directa')
			->type('400000', 'limite_compuesta')
			->type('0', 'orden')
			->select('0', 'mitad')
			->press('Actualizar');



		//Then
		$this->seeInDatabase('submenu',[
			'nombre' =>'Liga BBVA',
			'id_menu' => $menu->id,
			'limite_directa' => '400000',
			'limite_compuesta' => '400000',
		]);

		$this->seePageIs('/admin/submenu')
			->seeInElement('li ', 'Liga BBVA');

	}

	public function test_submenu_can_be_finded_by_name()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);

		factory(\App\Submenu::class)->times(15)->create();		

		$searched = factory(\App\Submenu::class)->create();	

		//When
		$this->visit('/admin/submenu')
			->dontSeeInElement('li',"#{$searched->id}")
			->type($searched->name, 'search')
			->press('Buscar');

		//Then
		$this->seeInElement('li', $searched->name);
	}

	public function test_submenu_can_be_finded_by_name_like()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);

	    	factory(\App\Submenu::class)->times(15)->create();	

		$searched1 = factory(\App\Submenu::class)->create([			
            			'nombre' => 'Alan Brito'       		 
		]);	

		$searched2 = factory(\App\Submenu::class)->create([
            			'nombre' => 'Alanis Morisete'
            	]);				

		//When
		$this->visit('/admin/submenu')
			->dontSeeInElement('li', 'Alan Brito')
			->dontSeeInElement('li','Alanis Morisete')
			->type('alan', 'search')
			->press('Buscar');

		//Then
		$this->seeInElement('li', 'Alan Brito')
			->seeInElement('li','Alanis Morisete');
	}		

	public function test_submenu_can_be_finded_by_user_menu_name()
	{
	    	//Having
	    	$admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($admin);

		factory(\App\Submenu::class)->times(15)->create();
		
		$searched = factory(\App\Submenu::class)->create();		
			
		
				

		//When
		$this->visit('admin/submenu')
			->type($searched->menu->nombre, 'search')
			->press('Buscar');

		//Then
		$this->seeInElement('li', $searched->menu->nombre);		
	}
}
