<?php

namespace Tests\Feature;

use Tests\FeatureTestCase;

class EnterpriseTest extends FeatureTestCase
{
	protected $admin;

	function setUp()
	{
		parent::setUp();

	    	$this->admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($this->admin);		
	}
	public function test_enterprise_list_is_paginated()
	{
	    	//Having
		$first = factory(\App\Empresa::class)->create([
			'nombre' => 'Primera empresa'
		]);

		factory(\App\Empresa::class)->times(15)->create();

		$last = factory(\App\Empresa::class)->create([
			'nombre' => 'Ultima empresa'
		]);

		//When
		$this->visit('/admin/enterprise');

		//Then
		$this->see($first->nombre)	
			->dontSee($last->nombre);
	}  

	public function test_enterprise_can_be_created()
	{
		//When 
		$this->visit(route('admin.enterprise.create'))
			->see('Crear empresa')
			->type('Apple', 'nombre')
			->press('Crear');

		//Then
		$this->seeInDatabase('empresas',[
			'nombre' =>'Apple',
		]);

		$this->seePageIs('admin/enterprise')
			->seeInElement('li ', 'Apple');

	}		

	public function test_enterprise_data_can_be_showed()
	{
	    	//Having

		$enterprise = factory(\App\Empresa::class)->create();

		//When
		$this->visit("/admin/enterprise/{$enterprise->id}");

		//Then
		$this->see($enterprise->nombre );		
	}	

	public function test_enterprise_can_be_updated()
	{
	    	//Having
		$enterprise = factory(\App\Empresa::class)->create([
			'nombre' => 'Apple'
		]);

		//When 
		$this->visit("/admin/enterprise/{$enterprise->id}")
			->type('Microsoft', 'nombre')
			->press('Actualizar');

		//Then
		$this->seeInDatabase('empresas',[
			'id' => $enterprise->id,
			'nombre' =>'Microsoft',
		]);

		$this->seePageIs('/admin/enterprise')
			->seeInElement('li ', 'Microsoft');

	}

	public function test_enterprise_can_be_finded_by_name()
	{
	    	//Having
		factory(\App\Empresa::class)->times(15)->create();		

		$searched = factory(\App\Empresa::class)->create();	

		//When
		$this->visit('/admin/enterprise')
			->dontSeeInElement('li',"#{$searched->id}")
			->type($searched->nombre, 'search')
			->press('Buscar');

		//Then
		$this->seeInElement('li', $searched->nombre);
	}	
}
