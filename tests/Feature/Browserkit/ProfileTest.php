<?php

namespace Tests\Feature;

use Tests\FeatureTestCase;


class ProfileTest extends FeatureTestCase
{
    public function test_user_can_see_own_profile()
    {

    	$user = factory(\App\User::class)->create([
    		'name' => 'Juan Palencia'
    	]);	

    	$this->actingAs($user);

    	//when
    	$this->visit(route('profile.show'));

    	//Then
    	$this->see('Actualizar perfil')
    		->seeInField('name', 'Juan Palencia');
    }

	public function test_user_profile_can_be_updated(){
		//having
		$user = factory(\App\User::class)->create([
			'logro' => '0'
		]);	


		$this->actingAs($user);		
	
		//When
		$this->visit(route("profile.show"))
			->type('Juan Palencia', 'name')
			->type('klaustro@hotmail.com', 'email')
			->type('04149900552', 'phone')
			->select('1', 'printer')
			->select('1', 'logro')
			->press('Enviar');		

		//Then
		$this->seeInDatabase('users',[
			'name' => 'Juan Palencia',
			'email' => 'klaustro@hotmail.com',
			'phone' => '04149900552',
			'printer' => '1',
			'logro' => '1',
		]);

		$this->seePageIs(route('home'));
	}    
}
