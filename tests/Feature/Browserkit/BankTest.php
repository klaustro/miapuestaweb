<?php
namespace Tests\Feature;

use Tests\FeatureTestCase;

class BankTest extends FeatureTestCase
{
	protected $admin;

	function setUp()
	{
		parent::setUp();

	    	$this->admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($this->admin);		
	}

	public function test_bank_list_is_paginated()
	{
	    	//Having
		$first = factory(\App\Banco::class)->create([
			'nombre' => 'Primer banco'
		]);

		factory(\App\Banco::class)->times(15)->create();

		$last = factory(\App\Banco::class)->create([
			'nombre' => 'Ultimo banco'
		]);

		//When
		$this->visit('/admin/bank');

		//Then
		$this->see($first->nombre)	
			->dontSee($last->nombre);
	}  

	public function test_bank_can_be_created()
	{

		//When 
		$this->visit(route('admin.bank.create'))
			->see('Crear banco')
			->type('Wells Fargo', 'nombre')
			->press('Crear');

		//Then
		$this->seeInDatabase('banco',[
			'nombre' =>'Wells Fargo',
		]);

		$this->seePageIs('admin/bank')
			->seeInElement('li ', 'Wells Fargo');

	}	

	public function test_bank_data_can_be_showed()
	{
	    	//Having

		$bank = factory(\App\Banco::class)->create();

		//When
		$this->visit("/admin/bank/{$bank->id}");

		//Then
		$this->see($bank->nombre );		
	}	

	public function test_bank_can_be_updated()
	{
	    	//Having

		$bank = factory(\App\Banco::class)->create([
			'nombre' => 'Wells Fargo'
		]);

		//When 
		$this->visit("/admin/bank/{$bank->id}")
			->type('Bank of America', 'nombre')
			->press('Actualizar');

		//Then
		$this->seeInDatabase('banco',[
			'id' => $bank->id,
			'nombre' =>'Bank of America',
		]);

		$this->seePageIs('/admin/bank')
			->seeInElement('li ', 'Bank of America');

	}	

	public function test_bank_can_be_finded_by_name()
	{
	    	//Having
		factory(\App\Banco::class)->times(15)->create();		

		$searched = factory(\App\Banco::class)->create();	

		//When
		$this->visit('/admin/bank')
			->dontSeeInElement('li',"#{$searched->id}")
			->type($searched->nombre, 'search')
			->press('Buscar');

		//Then
		$this->seeInElement('li', $searched->nombre);
	}

}
