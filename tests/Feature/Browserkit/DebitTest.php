<?php
namespace Tests\Feature;

use Tests\FeatureTestCase;

class DebitTest extends FeatureTestCase
{
	protected $admin;

	protected function setUp()
	{		
		parent::setUp();

    	$this->admin = factory(\App\User::class)->create([
    		'role' => 'admin'
    	]);		
	}

    public function test_if_debits_is_listed()
    {
    	//Having
    	
    	$this->actingAs($this->admin);

    	$debit = factory(\App\Retiro::class)->create();

    	$this->visit('/admin/retiro')
    		->seeInElement('li', $debit->id);
    }

	public function test_debit_list_is_paginated()
	{
	    	//Having
	    	

	    	$this->actingAs($this->admin);

		$first = factory(\App\Retiro::class)->create();

		factory(\App\Retiro::class)->times(15)->create();

		$last = factory(\App\Retiro::class)->create();

		//When
		$this->visit('/admin/retiro');

		//Then
		$this->see($last->user->name)	
			->dontSee($first->user->name);
	}    

	public function test_debit_data_can_be_showed()
	{
	    	//Having
	    	

	    	$this->actingAs($this->admin);

		$retiro = factory(\App\Retiro::class)->create();

		//When
		$this->visit("/admin/retiro/{$retiro->id}");

		//Then
		$this->seeInElement('li',$retiro->id )
			->seeInElement('li', $retiro->user->name)
			->seeInElement('li', $retiro->monto);		
	}	

	public function test_debit_can_be_approved()
	{
	    	//Having
	    	

	    	$this->actingAs($this->admin);


		$debit = factory(\App\Retiro::class)->create([
			'status' => '0'
		]);

		//When 
		$this->visit("/admin/retiro/{$debit->id}")
			->press('Aprobar');

		//Then
		$this->seeInDatabase('retiros',[
			'id' => $debit->id,
			'status' =>'1'
		]);

		$this->seePageIs('/admin/retiro')
			->seeInElement('span ', 'Aprobado');

	}

	public function test_debit_can_be_finded_by_id()
	{
	    	//Having
	    	

	    	$this->actingAs($this->admin);

		$searched = factory(\App\Retiro::class)->create();	

		factory(\App\Retiro::class)->times(15)->create();		

		//When
		$this->visit('/admin/retiro')
			->dontSeeInElement('li',"#{$searched->id}")
			->type($searched->id, 'search')
			->press('Buscar');

		//Then
		$this->seeInElement('li', $searched->id);
	}

	public function test_debit_can_be_finded_by_user_name()
	{
	    	//Having
	    	

	    	$this->actingAs($this->admin);

		$searched = factory(\App\Retiro::class)->create();	
		
		factory(\App\Retiro::class)->times(15)->create();		

		//When
		$this->visit('/admin/retiro')
			->dontSeeInElement('li',$searched->user->name)
			->type($searched->user->name, 'search')
			->press('Buscar');

		//Then
		$this->seeInElement('li', $searched->user->name);
	}	

	public function test_debit_can_be_finded_by_user_name_like()
	{
	    	//Having
	    	

	    	$this->actingAs($this->admin);

		$searched1 = factory(\App\Retiro::class)->create([			
			'id_usuario' => function (){
            		return factory(\App\User::class)->create([
            			'name' => 'Alan Brito'
            		])->id;
       		 }
		]);	

		$searched2 = factory(\App\Retiro::class)->create([			
			'id_usuario' => function (){
            		return factory(\App\User::class)->create([
            			'name' => 'Alanis Morisete'
            		])->id;
       		 }
		]);	
		
		factory(\App\Retiro::class)->times(15)->create();		

		//When
		$this->visit('/admin/retiro')
			->dontSeeInElement('li', 'Alan Brito')
			->dontSeeInElement('li','Alanis Morisete')
			->type('alan', 'search')
			->press('Buscar');

		//Then
		$this->seeInElement('li', 'Alan Brito')
			->seeInElement('li','Alanis Morisete');
	}		

	public function test_debit_can_be_disapproved()
	{
	    	//Having
	    	

	    	$this->actingAs($this->admin);

		$debit = factory(\App\Retiro::class)->create([
			'status' => '1'
		]);

		//When 
		$this->visit("/admin/retiro/{$debit->id}")
			->press('Desaprobar');

		//Then
		$this->seeInDatabase('retiros',[
			'id' => $debit->id,
			'status' =>'0'
		]);

		$this->seePageIs('/admin/retiro')
			->seeInElement('span ', 'Pendiente');
	}
}
