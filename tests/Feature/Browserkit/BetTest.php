<?php

namespace Tests\Feature;

use Tests\FeatureTestCase;
use App\Bet;
use App\Stsbet;

class BetTest extends FeatureTestCase
{

	protected $admin;

	function setUp()
	{
		parent::setUp();

	    	$this->admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($this->admin);		
	}	

	public function test_if_bet_is_listed()
	{
		//Having
		$status = factory(\App\Stsbet::class)->create([
			'nombre' => 'Ganada'
		]);

		$bet = factory(\App\Bet::class)->create([
			'fecha' => '2017-05-21',
			'status' => $status->id
		]);

		$this->visit('/owner/bet')
			->seeInElement('li', $bet->id)
			->seeInElement('li', '21-05-2017')
			->seeInElement('li', $bet->userName)
			->seeInElement('span', 'Ganada');
	}

	public function test_bet_list_is_paginated()
	{
	  	//Having
		$first = factory(\App\Bet::class)->create([
			'monto' => '10000.00'
		]);

		factory(\App\Bet::class)->times(15)->create();

		$last = factory(\App\Bet::class)->create([
			'monto' => '9999.00'
		]);

		//When
		$this->visit('/owner/bet');

		//Then
		$this->see('9,999.00')	
			->dontSee('10,000.00');
	}    

	public function test_lost_bet_badge_is_showed_in_index()
	{
	  	//Having

	  	$status = factory(\App\Stsbet::class)->create([
	  		'id' => '3',
	  		'nombre' => 'Perdida'
	  	]);

		$first = factory(\App\Bet::class)->create([
			'status' => '3'
		]);

		//When
		$this->visit('/owner/bet')
			->seeInElement('span', 'Perdida');		
	}


	public function test_bet_data_can_be_showed()
	{
	  	//Having
	  	$game = factory(\App\Game::class)->create();

	  	$betType =  factory(\App\TipoApuesta::class)->create([
	  		'id' => '1',
	  		'nombre' => 'Ganador'
	  	]);

	  	$gamesProb = factory(\App\GamesProb::class)->times(2)->create([
	  		'idgame' => $game->id,
	  		'tipo_probabilidad' => $betType->id,
	  		'acierto' => '1',
	  	]);

		$bet = factory(\App\Bet::class)->create([
			'monto' => '10000.00',
			'fecha' => '2017-05-21'
		]);

		$betsDetail = factory(\App\BetsDetail::class)->create([
			'idbets' => $bet->id,
			'idgames_probs' => $gamesProb[0]->id
		]);

		//When
		$this->visit("/owner/bet/{$bet->id}");

		//Then
		//Bet
		$this->see($bet->id )
			->see('21-05-2017')
			->see($bet->userName)
			->see('10,000.00')
			->see($bet->ganacia);		

		//BetsDetail
		$this->seeInElement('li', $game->event)
			->seeInElement('li', 'Ganador')
			->seeInElement('li',  $betsDetail->selection)
			->seeInElement('li',  $gamesProb[0]->probabilidad)
			->seeInElement('span', 'Acertó');
	}	

	public function test_lost_bet_detail_badge_is_showed()
	{
	  	//Having
	  	$game = factory(\App\Game::class)->create();

	  	$betType =  factory(\App\TipoApuesta::class)->create([
	  		'id' => '1',
	  		'nombre' => 'Ganador'
	  	]);

	  	$gamesProb = factory(\App\GamesProb::class)->times(2)->create([
	  		'idgame' => $game->id,
	  		'tipo_probabilidad' => $betType->id,
	  		'acierto' => '0',
	  	]);

		$bet = factory(\App\Bet::class)->create([
			'monto' => '10000',
			'fecha' => '2017-05-21'
		]);

		$betsDetail = factory(\App\BetsDetail::class)->create([
			'idbets' => $bet->id,
			'idgames_probs' => $gamesProb[0]->id
		]);

		//When
		$this->visit("/owner/bet/{$bet->id}")
			->seeInElement('span', 'No acertó');;		
	}

	public function test_bet_can_be_anuled()
	{
	  	//Having	  	
		factory(\App\Stsbet::class)->create([
		  		'id' => '1',
		  		'nombre' => 'Pendiente'
		  	]);

		factory(\App\Stsbet::class)->create([
		  		'id' => '4',
		  		'nombre' => 'Anulada'
		  	]);		

		$bet = factory(\App\Bet::class)->create([
			'status' => '1'
		]);

		//When
		$this->visit('/owner/bet/'.$bet->id)
			->press('Anular');

		//Then
		$this->seeInDatabase('bets', [
			'id' => $bet->id,
			'status' => '4'
		]);

		$this->seeInElement('span', 'Anulada')
			->seePageIs('/owner/bet');				
	}

	public function test_anuled_bet_can_be_reverted()
	{
	  	//Having	  	
		factory(\App\Stsbet::class)->create([
		  		'id' => '1',
		  		'nombre' => 'Pendiente'
		  	]);

		factory(\App\Stsbet::class)->create([
		  		'id' => '4',
		  		'nombre' => 'Anulada'
		  	]);		

		$bet = factory(\App\Bet::class)->create([
			'status' => '4'
		]);

		//When
		$this->visit('/owner/bet/'.$bet->id)
			->press('Reanudar');

		//Then
		$this->seeInDatabase('bets', [
			'id' => $bet->id,
			'status' => '1'
		]);

		$this->seeInElement('span', 'Pendiente')
			->seePageIs('/owner/bet');				
	}	

	public function test_bet_can_be_finded_by_id()
	{

	  	//Having
		$searchedBet = factory(\App\Bet::class)->create([
			'monto' => '10000'
		]);

		factory(\App\Bet::class)->times(15)->create();

		//When
		$this->visit('/owner/bet')
			->type($searchedBet->id, 'search')
			->press('Buscar');

		//Then
		$this->seeInElement('li', $searchedBet->id);
	}	

	public function test_bet_can_be_finded_by_userName()
	{

	  	//Having
	  	$user = factory(\App\User::class)->create([
	  		'name' => 'Juan Palencia'
	  	]); 

		$searchedBet = factory(\App\Bet::class)->create([
			'monto' => '10000',
			'usuario' => $user->id
		]);

		factory(\App\Bet::class)->times(15)->create();

		//When
		$this->visit('/owner/bet')
			->type('Juan', 'search')
			->press('Buscar');

		//Then
		$this->seeInElement('li', $searchedBet->id);
	}	

	public function test_if_bets_can_be_filtered_by_status()
	{
	  	//Having

	  	$user = factory(\App\User::class)->create([
	  		'name' => 'Juan Palencia'
	  	]); 

		factory(\App\Stsbet::class)->create([
		  		'id' => '1',
		  		'nombre' => 'Pendiente'
		  	]);	    	

		$searchedBet = factory(\App\Bet::class)->create([
			'monto' => '10000',
			'usuario' => $user->id,
			'status' => '1'
		]);

		factory(\App\Bet::class)->times(15)->create();

		//When
		$this->visit('/owner/bet')
			->select('1', 'status')
			->press('Buscar');

		//Then
		$this->seeInElement('li', $searchedBet->id);
	}

	public function test_if_owner_can_list_the_bets_of_own_enterprise()
	{
		//Having
		$enterprise1 =  factory(\App\Empresa::class)->create();

		$enterprise2 =  factory(\App\Empresa::class)->create();

		$owner1 = factory(\App\User::class)->create([
	    		'role' => 'owner',
	    		'enterprise' => $enterprise1
	    	]);

		$owner2 = factory(\App\User::class)->create([
	    		'role' => 'owner',
	    		'enterprise' => $enterprise2
	    	]);		

		$status = factory(\App\Stsbet::class)->create([
			'nombre' => 'Ganada'
		]);

		$bet1 = factory(\App\Bet::class)->create([
			'fecha' => '2017-05-21', 
			'status' => $status->id,
			'usuario' => $owner1->id
		]);

		$bet2 = factory(\App\Bet::class)->create([
			'fecha' => '2017-05-21',
			'status' => $status->id,
			'usuario' => $owner2->id
		]);		

		$this->actingAs($owner1);

		$this->visit('/owner/bet')
			->seeInElement('li', $bet1->id)
			->dontSeeInElement('li',  $bet2->id);		
	}	
}
