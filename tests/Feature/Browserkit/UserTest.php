<?php

namespace Tests\Feature;

use Tests\FeatureTestCase;

class UserTest extends FeatureTestCase
{

	public function test_admin_can_see_user_list()
	{
		//Having
		$admin = factory(\App\User::class)->create([
			'role' => 'admin'
		]);

		$this->actingAs($admin);

		//when
		$this->visit('/admin/user');

		//Then
		$this->see('Lista de usuarios');
	}	 

	public function test_user_list_is_paginated()
	{
		//Having
		$first = factory(\App\User::class)->create([
			'name' => 'Primer Usuario',
		]);

		factory(\App\User::class)->times(15)->create();

		$last = factory(\App\User::class)->create([
			'name' => 'Ultimo Usuario',
		]);

		$this->seeinDatabase('users', [
			'name' => $first->name
		]);

		$this->seeinDatabase('users', [
			'name' => $last->name
		]);	

		//When
		$admin = factory(\App\User::class)->create([
			'role' => 'admin'
		]);

		$this->actingAs($admin);

		$this->visit('/admin/user');

		//Then
		$this->see($first->name)	
			->dontSee($last->name);
	}

	public function test_user_data_can_be_showed()
	{
		//having
		$user = factory(\App\User::class)->create();

		$admin = factory(\App\User::class)->create([
			'role' => 'admin'
		]);

		$this->actingAs($admin);
		//When
		$this->visit("/admin/user/{$user->id}");

		//Then
		$this->see($user->name)
			->see( $user->email)
			->see( $user->phone)
			->see('Activo');
	}

	public function test_user_can_be_updated(){
		//having
		$user = factory(\App\User::class)->create();	


		$admin = factory(\App\User::class)->create([
			'role' => 'admin'
		]);

		$this->actingAs($admin);		
	
		//When
		$this->visit("/admin/user/{$user->id}")
			->type('Juan Palencia', 'name')
			->type('klaustro@hotmail.com', 'email')
			->type('04149900552', 'phone')
			->select('0', 'status')
			->select('1', 'printer')
			->select('0', 'corredor')
			->select('admin', 'role')
			->press('Enviar');		

		//Then
		$this->seeInDatabase('users',[
			'name' => 'Juan Palencia',
			'email' => 'klaustro@hotmail.com',
			'phone' => '04149900552',
			'status' => '0',
			'printer' => '1',
			'corredor' => '0',
			'role' => 'admin',
		]);

		$this->see('Lista de usuarios');
	}

	public function test_user_balance_can_be_listed()
	{
		//having
		$user = factory(\App\User::class)->create([
			'corredor' => 0
		]);

		$admin = factory(\App\User::class)->create([
			'role' => 'admin'
		]);

		$this->actingAs($admin);				

		//When
		$this->visit('/admin/user');

		//Then
		$this->see('0.00');		
	}

	public function test_user_balance_can_be_viewed_in_user_update_form()
	{
		//having
		$user = factory(\App\User::class)->create([
			'corredor' => '0',
		]);


		$pago = factory(\App\Pago::class)->create([
			'id_usuario' => $user->id,
			'status' => '1',
			'monto' => 100000
		]);

		$bet_win = factory(\App\Bet::class)->create([
			'usuario' => $user->id,
			'status' => 2,
			'monto' => 5000,
			'ganancia' => 50000,
		]);


		$bet_lost = factory(\App\Bet::class)->create([
			'usuario' => $user->id,
			'status' => 3,
			'monto' => 2500,
		]);		

		
		$debit = factory(\App\Retiro::class)->create([
			'id_usuario' => $user->id,
			'monto' => 5000
		]);	

		//dd($pago->monto, $bet_win->ganancia, -$bet_win->monto -$bet_lost->monto, -$debit->monto, $user->saldo);

		$admin = factory(\App\User::class)->create([
			'role' => 'admin'
		]);


		$this->actingAs($admin);				

		//When
		$this->visit('/admin/user/'.$user->id);
		//Then
		$this->seeInElement('div','137,500.00');		
	}	

	public function test_user_can_be_finded_by_name()
	{
		//Having
		factory(\App\User::class)->times(15)->create();

		$user = factory(\App\User::class)->create([
			'name' => 'Juan Palencia'
		]);

		$this->seeInDatabase('users',[
			'name' => 'Juan Palencia'
		]);

		$admin = factory(\App\User::class)->create([
			'role' => 'admin'
		]);

		$this->actingAs($admin);				

		//When
		$this->visit('/admin/user')
			->dontSeeInElement('li', 'Juan Palencia')
			->type('Juan Palencia', 'name')
			->press('Buscar');

		//Then
		$this->seeInElement('li', 'Juan Palencia');
	}	

	public function test_corredor_user_not_show_balance()
	{
		//Having		
		$user = factory(\App\User::class)->create([
			'name' => 'Juan Palencia',
			'corredor' => '1'
		]);

		$admin = factory(\App\User::class)->create([
			'role' => 'admin'
		]);

		$this->actingAs($admin);				

		//when
		$this->visit('/admin/user');


		//Then

		$this->seeInElement('li', 'Juan Palencia')
			->type('Juan Palencia', 'name')
			->press('Buscar')
			->dontSeeInElement('li', '0.00');

		$this->seeInDatabase('users', [
			'name' => 'Juan Palencia'
		]);
	}

	public function test_user_can_be_seted_as_corredor()
	{
		//Having		
		$user = factory(\App\User::class)->create([
			'corredor' => '0'
		]);		

 		factory(\App\TipoComision::class)->create([
 			'id' => '1',
 		]);		

		$admin = factory(\App\User::class)->create([
			'role' => 'admin'
		]);

		$this->actingAs($admin);				

		//when
		$this->visit('/admin/user/'. $user->id)
			->select('1', 'corredor')
			->select('1', 'tipoComision')
			->select('1', 'ventas')
			->press('Enviar');

		//Then
		$this->seeInDatabase('users',[
			'id' => $user->id,
			'corredor' => '1'
		]);

		$this->type($user->name, 'name')
			->press('Buscar')
			->see('Lista de Usuarios')
			->dontSeeInElement('li', '0.00');
	}

	public function test_cancel_update(){
		//Having		
		$user = factory(\App\User::class)->create();	

		$admin = factory(\App\User::class)->create([
			'role' => 'admin'
		]);

		$this->actingAs($admin);				

		//when
		$this->visit('/admin/user/'. $user->id)
			->click('Cancelar');		

		//Then
		$this->seeInElement('h4', 'Lista de Usuarios');
	}

    public function test_user_id_can_be_showed()
    {
    	//Having
	$user = factory(\App\User::class)->create();	

	$admin = factory(\App\User::class)->create([
		'role' => 'admin'
	]);

	$this->actingAs($admin);

	//when
	$this->visit('/admin/user/'. $user->id);

	//Then
	$this->seeInElement('div', "Editar usuario #{$user->id}");		

    }	
}