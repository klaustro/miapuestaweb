<?php

namespace Tests\Feature;

use Tests\FeatureTestCase;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BetDetailTest extends FeatureTestCase
{
	protected $admin;
	protected $game;

	function setUp()
	{
		parent::setUp();

	    	$this->admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($this->admin);		

	  	$this->game = factory(\App\Game::class)->create();
	}	
	
	public function test_bet_runline_can_be_showed()
	{
	  	//Having

	  	$betType =  factory(\App\TipoApuesta::class)->create([
	  		'id' => '3',
	  		'nombre' => 'Runline'
	  	]);

	  	$gamesProb = factory(\App\GamesProb::class)->times(2)->create([
	  		'idgame' => $this->game->id,
	  		'tipo_probabilidad' => $betType->id,
	  		'cantidad' => '1.5',
	  		'acierto' => '1',
	  	]);

	  	\App\GamesProbsSigno::create([
	  		'games_prob_id' => $gamesProb[0]->id,
			'signo' => '+',
	  	]);

	  	\App\GamesProbsSigno::create([
	  		'games_prob_id' => $gamesProb[1]->id,
			'signo' => '-',
	  	]);	  	

		$bet = factory(\App\Bet::class)->create([
			'monto' => '10000.00',
			'fecha' => '2017-05-21'
		]);

		$betsDetail = factory(\App\BetsDetail::class)->create([
			'idbets' => $bet->id,
			'idgames_probs' => $gamesProb[0]->id
		]);

		//When
		$this->visit("/owner/bet/{$bet->id}");

		//Then

		//Bet
		$this->see($bet->id )
			->see('21-05-2017')
			->see($bet->userName)
			->see('10,000.00')
			->see($bet->ganacia);		

		//BetsDetail
		$this->seeInElement('li', $this->game->event)
			->seeInElement('li', 'Runline '. $gamesProb[0]->team->nombre .' +1.5')
			->seeInElement('li',  $gamesProb[0]->probabilidad)
			->seeInElement('span', 'Acertó');
	}	

	public function test_bet_alta_baja_can_be_showed()
	{
	  	//Having

	  	$betType =  factory(\App\TipoApuesta::class)->create([
	  		'id' => '4',
	  		'nombre' => 'Alta/Baja'
	  	]);

	  	$gamesProb = factory(\App\GamesProb::class)->times(2)->create([
	  		'idgame' => $this->game->id,
	  		'tipo_probabilidad' => $betType->id,
	  		'cantidad' => '1.5',
	  		'orden' => '1',
	  		'acierto' => '1',
	  	]);

		$bet = factory(\App\Bet::class)->create([
			'monto' => '10000.00',
			'fecha' => '2017-05-21'
		]);

		$betsDetail = factory(\App\BetsDetail::class)->create([
			'idbets' => $bet->id,
			'idgames_probs' => $gamesProb[0]->id
		]);

		//When
		$this->visit("/owner/bet/{$bet->id}");

		//Then

		//Bet
		$this->see($bet->id )
			->see('21-05-2017')
			->see($bet->userName)
			->see('10,000.00')
			->see($bet->ganacia);		

		//BetsDetail
		$this->seeInElement('li', $this->game->event)
			->seeInElement('li', 'Alta/Baja +1.5')
			->seeInElement('li',  $gamesProb[0]->probabilidad)
			->seeInElement('span', 'Acertó');
	}		

	public function test_bet_tied_can_be_showed()
	{
	  	//Having

	  	$betType =  factory(\App\TipoApuesta::class)->create([
	  		'id' => '6',
	  		'nombre' => 'Empate'
	  	]);

	  	$gamesProb = factory(\App\GamesProb::class)->create([
	  		'idgame' => $this->game->id,
	  		'tipo_probabilidad' => $betType->id,
	  		'acierto' => '1',
	  	]);  	

		$bet = factory(\App\Bet::class)->create([
			'monto' => '10000.00',
			'fecha' => '2017-05-21'
		]);

		$betsDetail = factory(\App\BetsDetail::class)->create([
			'idbets' => $bet->id,
			'idgames_probs' => $gamesProb->id
		]);

		//When
		$this->visit("/owner/bet/{$bet->id}");

		//Then

		//Bet
		$this->see($bet->id )
			->see('21-05-2017')
			->see($bet->userName)
			->see('10,000.00')
			->see($bet->ganacia);		

		//BetsDetail
		$this->seeInElement('li', $this->game->event)
			->seeInElement('li', 'Empate')
			->seeInElement('li',  $gamesProb->probabilidad)
			->seeInElement('span', 'Acertó');
	}	

	public function test_bet_super_runline_can_be_showed()
	{
	  	//Having

	  	$betType =  factory(\App\TipoApuesta::class)->create([
	  		'id' => '7',
	  		'nombre' => 'Super Runline'
	  	]);

	  	$gamesProb = factory(\App\GamesProb::class)->times(2)->create([
	  		'idgame' => $this->game->id,
	  		'tipo_probabilidad' => $betType->id,
	  		'cantidad' => '1.5',
	  		'acierto' => '1',
	  	]);

	  	\App\GamesProbsSigno::create([
	  		'games_prob_id' => $gamesProb[0]->id,
			'signo' => '+',
	  	]);

	  	\App\GamesProbsSigno::create([
	  		'games_prob_id' => $gamesProb[1]->id,
			'signo' => '-',
	  	]);	  	

		$bet = factory(\App\Bet::class)->create([
			'monto' => '10000.00',
			'fecha' => '2017-05-21'
		]);

		$betsDetail = factory(\App\BetsDetail::class)->create([
			'idbets' => $bet->id,
			'idgames_probs' => $gamesProb[0]->id
		]);

		//When
		$this->visit("/owner/bet/{$bet->id}");

		//Then

		//Bet
		$this->see($bet->id )
			->see('21-05-2017')
			->see($bet->userName)
			->see('10,000.00')
			->see($bet->ganacia);		

		//BetsDetail
		$this->seeInElement('li', $this->game->event)
			->seeInElement('li', 'Super Runline ' . $gamesProb[0]->team->nombre .' +1.5')
			->seeInElement('li',  $gamesProb[0]->probabilidad)
			->seeInElement('span', 'Acertó');
	}

	public function test_bet_score_first_can_be_showed()
	{
	  	//Having

	  	$betType =  factory(\App\TipoApuesta::class)->create([
	  		'id' => '8',
	  		'nombre' => 'Anota Primero'
	  	]);

	  	$gamesProb = factory(\App\GamesProb::class)->times(2)->create([
	  		'idgame' => $this->game->id,
	  		'tipo_probabilidad' => $betType->id,
	  		'cantidad' => '1.5',
	  		'acierto' => '1',
	  	]);


		$bet = factory(\App\Bet::class)->create([
			'monto' => '10000.00',
			'fecha' => '2017-05-21'
		]);

		$betsDetail = factory(\App\BetsDetail::class)->create([
			'idbets' => $bet->id,
			'idgames_probs' => $gamesProb[0]->id
		]);

		//When
		$this->visit("/owner/bet/{$bet->id}");

		//Then

		//Bet
		$this->see($bet->id )
			->see('21-05-2017')
			->see($bet->userName)
			->see('10,000.00')
			->see($bet->ganacia);		

		//BetsDetail
		$this->seeInElement('li', $this->game->event)
			->seeInElement('li', 'Anota Primero ' . $gamesProb[0]->team->nombre)			
			->seeInElement('li',  $gamesProb[0]->probabilidad)
			->seeInElement('span', 'Acertó');
	}	

	public function test_bet_score_in_first_inning_can_be_showed()
	{
	  	//Having

	  	$betType =  factory(\App\TipoApuesta::class)->create([
	  		'id' => '9',
	  		'nombre' => 'Anotan en el 1er Inning'
	  	]);

	  	$gamesProb = factory(\App\GamesProb::class)->times(2)->create([
	  		'idgame' => $this->game->id,
	  		'tipo_probabilidad' => $betType->id,
	  		'acierto' => '1',
	  		'orden' => '1',
	  	]);


		$bet = factory(\App\Bet::class)->create([
			'monto' => '10000.00',
			'fecha' => '2017-05-21'
		]);

		$betsDetail = factory(\App\BetsDetail::class)->create([
			'idbets' => $bet->id,
			'idgames_probs' => $gamesProb[0]->id
		]);

		//When
		$this->visit("/owner/bet/{$bet->id}");

		//Then

		//Bet
		$this->see($bet->id )
			->see('21-05-2017')
			->see($bet->userName)
			->see('10,000.00')
			->see($bet->ganacia);		

		//BetsDetail
		$this->seeInElement('li', $this->game->event)
			->seeInElement('li', 'Anotan en el 1er Inning SI ')			
			->seeInElement('li',  $gamesProb[0]->probabilidad)
			->seeInElement('span', 'Acertó');
	}

	public function test_bet_HRE_can_be_showed()
	{
	  	//Having

	  	$betType =  factory(\App\TipoApuesta::class)->create([
	  		'id' => '10',
	  		'nombre' => 'Hits Carreras y Errores (HRE)'
	  	]);

	  	$gamesProb = factory(\App\GamesProb::class)->times(2)->create([
	  		'idgame' => $this->game->id,
	  		'tipo_probabilidad' => $betType->id,
	  		'cantidad' => '27',
	  		'orden' => '1',
	  		'acierto' => '1',
	  	]);


		$bet = factory(\App\Bet::class)->create([
			'monto' => '10000.00',
			'fecha' => '2017-05-21'
		]);

		$betsDetail = factory(\App\BetsDetail::class)->create([
			'idbets' => $bet->id,
			'idgames_probs' => $gamesProb[0]->id
		]);

		//When
		$this->visit("/owner/bet/{$bet->id}");

		//Then

		//Bet
		$this->see($bet->id )
			->see('21-05-2017')
			->see($bet->userName)
			->see('10,000.00')
			->see($bet->ganacia);		

		//BetsDetail
		$this->seeInElement('li', $this->game->event)
			->seeInElement('li', 'Hits Carreras y Errores (HRE) +27')			
			->seeInElement('li',  $gamesProb[0]->probabilidad)
			->seeInElement('span', 'Acertó');
	}

	public function test_bet_win_half_time_can_be_showed()
	{
	  	//Having

	  	$betType =  factory(\App\TipoApuesta::class)->create([
	  		'id' => '11',
	  		'nombre' => 'Ganador Mitad de Juego'
	  	]);

	  	$gamesProb = factory(\App\GamesProb::class)->times(2)->create([
	  		'idgame' => $this->game->id,
	  		'tipo_probabilidad' => $betType->id,
	  		'acierto' => '1',
	  	]);

		$bet = factory(\App\Bet::class)->create([
			'monto' => '10000.00',
			'fecha' => '2017-05-21'
		]);

		$betsDetail = factory(\App\BetsDetail::class)->create([
			'idbets' => $bet->id,
			'idgames_probs' => $gamesProb[0]->id
		]);

		//When
		$this->visit("/owner/bet/{$bet->id}");

		//Then
		//Bet
		$this->see($bet->id )
			->see('21-05-2017')
			->see($bet->userName)
			->see('10,000.00')
			->see($bet->ganacia);		

		//BetsDetail
		$this->seeInElement('li', $this->game->event)
			->seeInElement('li', 'Ganador Mitad de Juego ' . $gamesProb[0]->team->nombre)
			->seeInElement('li',  $gamesProb[0]->probabilidad)
			->seeInElement('span', 'Acertó');
	}		

	public function test_bet_runline_half_time_can_be_showed()
	{
	  	//Having

	  	$betType =  factory(\App\TipoApuesta::class)->create([
	  		'id' => '12',
	  		'nombre' => 'Runline Mitad de Juego'
	  	]);

	  	$gamesProb = factory(\App\GamesProb::class)->times(2)->create([
	  		'idgame' => $this->game->id,
	  		'tipo_probabilidad' => $betType->id,
	  		'cantidad' => '1.5',
	  		'acierto' => '1',
	  	]);

	  	\App\GamesProbsSigno::create([
	  		'games_prob_id' => $gamesProb[0]->id,
			'signo' => '+',
	  	]);

	  	\App\GamesProbsSigno::create([
	  		'games_prob_id' => $gamesProb[1]->id,
			'signo' => '-',
	  	]);	  	

		$bet = factory(\App\Bet::class)->create([
			'monto' => '10000.00',
			'fecha' => '2017-05-21'
		]);

		$betsDetail = factory(\App\BetsDetail::class)->create([
			'idbets' => $bet->id,
			'idgames_probs' => $gamesProb[0]->id
		]);

		//When
		$this->visit("/owner/bet/{$bet->id}");

		//Then

		//Bet
		$this->see($bet->id )
			->see('21-05-2017')
			->see($bet->userName)
			->see('10,000.00')
			->see($bet->ganacia);		

		//BetsDetail
		$this->seeInElement('li', $this->game->event)
			->seeInElement('li', 'Runline Mitad de Juego '. $gamesProb[0]->team->nombre .' +1.5')
			->seeInElement('li',  $gamesProb[0]->probabilidad)
			->seeInElement('span', 'Acertó');
	}

	public function test_bet_alta_baja_half_time_can_be_showed()
	{
	  	//Having

	  	$betType =  factory(\App\TipoApuesta::class)->create([
	  		'id' => '13',
	  		'nombre' => 'Alta / Baja Mitad de Juego'
	  	]);

	  	$gamesProb = factory(\App\GamesProb::class)->times(2)->create([
	  		'idgame' => $this->game->id,
	  		'tipo_probabilidad' => $betType->id,
	  		'cantidad' => '1.5',
	  		'orden' => '1',
	  		'acierto' => '1',
	  	]);

		$bet = factory(\App\Bet::class)->create([
			'monto' => '10000.00',
			'fecha' => '2017-05-21'
		]);

		$betsDetail = factory(\App\BetsDetail::class)->create([
			'idbets' => $bet->id,
			'idgames_probs' => $gamesProb[0]->id
		]);

		//When
		$this->visit("/owner/bet/{$bet->id}");

		//Then

		//Bet
		$this->see($bet->id )
			->see('21-05-2017')
			->see($bet->userName)
			->see('10,000.00')
			->see($bet->ganacia);		

		//BetsDetail
		$this->seeInElement('li', $this->game->event)
			->seeInElement('li', 'Alta / Baja Mitad de Juego +1.5')
			->seeInElement('li',  $gamesProb[0]->probabilidad)
			->seeInElement('span', 'Acertó');
	}

	public function test_bet_tied_half_timecan_be_showed()
	{
	  	//Having

	  	$betType =  factory(\App\TipoApuesta::class)->create([
	  		'id' => '14',
	  		'nombre' => 'Empate Mitad de Juego'
	  	]);

	  	$gamesProb = factory(\App\GamesProb::class)->create([
	  		'idgame' => $this->game->id,
	  		'tipo_probabilidad' => $betType->id,
	  		'acierto' => '1',
	  	]);  	

		$bet = factory(\App\Bet::class)->create([
			'monto' => '10000.00',
			'fecha' => '2017-05-21'
		]);

		$betsDetail = factory(\App\BetsDetail::class)->create([
			'idbets' => $bet->id,
			'idgames_probs' => $gamesProb->id
		]);

		//When
		$this->visit("/owner/bet/{$bet->id}");

		//Then

		//Bet
		$this->see($bet->id )
			->see('21-05-2017')
			->see($bet->userName)
			->see('10,000.00')
			->see($bet->ganacia);		

		//BetsDetail
		$this->seeInElement('li', $this->game->event)
			->seeInElement('li', 'Empate Mitad de Juego')
			->seeInElement('li',  $gamesProb->probabilidad)
			->seeInElement('span', 'Acertó');
	}	

	public function test_bet_team_win_cup_can_be_showed()
	{
	  	//Having

	  	$betType =  factory(\App\TipoApuesta::class)->create([
	  		'id' => '15',
	  		'nombre' => 'Equipo Que Ganara La Copa'
	  	]);

	  	$gamesProb = factory(\App\GamesProb::class)->times(2)->create([
	  		'idgame' => $this->game->id,
	  		'tipo_probabilidad' => $betType->id,
	  		'acierto' => '1',
	  	]);

		$bet = factory(\App\Bet::class)->create([
			'monto' => '10000.00',
			'fecha' => '2017-05-21'
		]);

		$betsDetail = factory(\App\BetsDetail::class)->create([
			'idbets' => $bet->id,
			'idgames_probs' => $gamesProb[0]->id
		]);

		//When
		$this->visit("/owner/bet/{$bet->id}");

		//Then
		//Bet
		$this->see($bet->id )
			->see('21-05-2017')
			->see($bet->userName)
			->see('10,000.00')
			->see($bet->ganacia);		

		//BetsDetail
		$this->seeInElement('li', $this->game->event)
			->seeInElement('li', 'Equipo Que Ganara La Copa ' . $gamesProb[0]->team->nombre)
			->seeInElement('li',  $gamesProb[0]->probabilidad)
			->seeInElement('span', 'Acertó');
	}	

	public function test_bet_alternative_runline_can_be_showed()
	{
	  	//Having

	  	$betType =  factory(\App\TipoApuesta::class)->create([
	  		'id' => '16',
	  		'nombre' => 'Runline Alternativo'
	  	]);

	  	$gamesProb = factory(\App\GamesProb::class)->times(2)->create([
	  		'idgame' => $this->game->id,
	  		'tipo_probabilidad' => $betType->id,
	  		'cantidad' => '1.5',
	  		'acierto' => '1',
	  	]);

	  	\App\GamesProbsSigno::create([
	  		'games_prob_id' => $gamesProb[0]->id,
			'signo' => '+',
	  	]);

	  	\App\GamesProbsSigno::create([
	  		'games_prob_id' => $gamesProb[1]->id,
			'signo' => '-',
	  	]);	  	

		$bet = factory(\App\Bet::class)->create([
			'monto' => '10000.00',
			'fecha' => '2017-05-21'
		]);

		$betsDetail = factory(\App\BetsDetail::class)->create([
			'idbets' => $bet->id,
			'idgames_probs' => $gamesProb[0]->id
		]);

		//When
		$this->visit("/owner/bet/{$bet->id}");

		//Then

		//Bet
		$this->see($bet->id )
			->see('21-05-2017')
			->see($bet->userName)
			->see('10,000.00')
			->see($bet->ganacia);		

		//BetsDetail
		$this->seeInElement('li', $this->game->event)
			->seeInElement('li', 'Runline Alternativo '. $gamesProb[0]->team->nombre .' +1.5')
			->seeInElement('li',  $gamesProb[0]->probabilidad)
			->seeInElement('span', 'Acertó');
	}	

	public function test_bet_hom_runs_can_be_showed()
	{
	  	//Having

	  	$betType =  factory(\App\TipoApuesta::class)->create([
	  		'id' => '17',
	  		'nombre' => 'Carreras Home Club'
	  	]);

	  	$gamesProb = factory(\App\GamesProb::class)->times(2)->create([
	  		'idgame' => $this->game->id,
	  		'tipo_probabilidad' => $betType->id,
	  		'cantidad' => '1.5',
	  		'orden' => '1',
	  		'acierto' => '1',
	  	]);

		$bet = factory(\App\Bet::class)->create([
			'monto' => '10000.00',
			'fecha' => '2017-05-21'
		]);

		$betsDetail = factory(\App\BetsDetail::class)->create([
			'idbets' => $bet->id,
			'idgames_probs' => $gamesProb[0]->id
		]);

		//When
		$this->visit("/owner/bet/{$bet->id}");

		//Then

		//Bet
		$this->see($bet->id )
			->see('21-05-2017')
			->see($bet->userName)
			->see('10,000.00')
			->see($bet->ganacia);		

		//BetsDetail
		$this->seeInElement('li', $this->game->event)
			->seeInElement('li', 'Carreras Home Club +1.5')
			->seeInElement('li',  $gamesProb[0]->probabilidad)
			->seeInElement('span', 'Acertó');
	}

	public function test_bet_visitor_runs_can_be_showed()
	{
	  	//Having

	  	$betType =  factory(\App\TipoApuesta::class)->create([
	  		'id' => '18',
	  		'nombre' => 'Carreras Visitante'
	  	]);

	  	$gamesProb = factory(\App\GamesProb::class)->times(2)->create([
	  		'idgame' => $this->game->id,
	  		'tipo_probabilidad' => $betType->id,
	  		'cantidad' => '1.5',
	  		'orden' => '1',
	  		'acierto' => '1',
	  	]);

		$bet = factory(\App\Bet::class)->create([
			'monto' => '10000.00',
			'fecha' => '2017-05-21'
		]);

		$betsDetail = factory(\App\BetsDetail::class)->create([
			'idbets' => $bet->id,
			'idgames_probs' => $gamesProb[0]->id
		]);

		//When
		$this->visit("/owner/bet/{$bet->id}");

		//Then

		//Bet
		$this->see($bet->id )
			->see('21-05-2017')
			->see($bet->userName)
			->see('10,000.00')
			->see($bet->ganacia);		

		//BetsDetail
		$this->seeInElement('li', $this->game->event)
			->seeInElement('li', 'Carreras Visitante +1.5')
			->seeInElement('li',  $gamesProb[0]->probabilidad)
			->seeInElement('span', 'Acertó');
	}


}
