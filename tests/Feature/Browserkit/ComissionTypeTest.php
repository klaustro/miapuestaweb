<?php
namespace Tests\Feature;

use Tests\FeatureTestCase;
class ComissionTypeTest extends FeatureTestCase
{
	protected $admin;

	function setUp()
	{
		parent::setUp();
	    	$this->admin = factory(\App\User::class)->create([
	    		'role' => 'admin'
	    	]);

	    	$this->actingAs($this->admin);		
	}

	public function test_commission_type_list_is_paginated()
	{
	    	//Having
		$first = factory(\App\TipoComision::class)->create([
			'nombre' => 'Primer tipo de comision'
		]);

		factory(\App\TipoComision::class)->times(15)->create();

		$last = factory(\App\TipoComision::class)->create([
			'nombre' => 'Ultimo tipo de comision'
		]);

		//When
		$this->visit('/admin/type/comision');

		//Then
		$this->see($first->nombre)	
			->dontSee($last->nombre);
	}  

	public function test_commission_type_can_be_created()
	{
		//When 
		$this->visit(route('admin.type.comision.create'))
			->see('Crear tipo de comisión')
			->type('Agencia', 'nombre')
			->press('Crear');

		//Then
		$this->seeInDatabase('tipo_comision',[
			'nombre' =>'Agencia',
		]);

		$this->seePageIs('admin/type/comision')
			->seeInElement('li ', 'Agencia');

	}	

	public function test_commission_type_data_can_be_showed()
	{
	    	//Having
		$comissionType = factory(\App\TipoComision::class)->create();

		//When
		$this->visit("/admin/type/comision/{$comissionType->id}");

		//Then
		$this->see($comissionType->nombre );		
	}	

	public function test_commission_type_can_be_updated()
	{
	    	//Having
		$commissionType = factory(\App\TipoComision::class)->create([
			'nombre' => 'Agencia'
		]);

		//When 
		$this->visit("/admin/type/comision/{$commissionType->id}")
			->type('Corredor', 'nombre')
			->press('Actualizar');

		//Then
		$this->seeInDatabase('tipo_comision',[
			'id' => $commissionType->id,
			'nombre' =>'Corredor',
		]);

		$this->seePageIs('/admin/type/comision')
			->seeInElement('li ', 'Corredor');

	}	

	public function test_commission_type_can_be_finded_by_name()
	{
	    	//Having
		factory(\App\TipoComision::class)->times(15)->create();		

		$searched = factory(\App\TipoComision::class)->create();	

		//When
		$this->visit('/admin/type/comision')
			->dontSeeInElement('li',"#{$searched->id}")
			->type($searched->nombre, 'search')
			->press('Buscar');

		//Then
		$this->seeInElement('li', $searched->nombre);
	}		

}
