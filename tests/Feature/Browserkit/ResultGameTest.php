<?php

namespace Tests\Feature;

use Tests\FeatureTestCase;

class ResultGameTest extends FeatureTestCase
{
    protected $admin;
    protected $game;
    protected $soccerGame;    
    protected $draw;    
    protected $gameProbs;
    protected $male;
    protected $female;
    protected $soccerMale;
    protected $soccerFemale;

    protected function setUp()
    {
        parent::setUp();

        $this->admin = factory(\App\User::class)->create([
            'role' => 'admin'
        ]);

        $this->actingAs($this->admin);        

        $this->game =  factory(\App\Game::class)->create([
            'status' => 0,
            'categoria' => 1,
        ]);

        $this->soccerGame =  factory(\App\Game::class)->create([
            'status' => 0,
            'categoria' => 3
        ]);     

        $this->basketGame =  factory(\App\Game::class)->create([
            'status' => 0,
            'categoria' => 6,
        ]);          

        $this->draw = factory(\App\Game::class)->create([
            'status' => 0,
            'categoria' => 12,
        ]);

        $this->gamesProbs = factory(\App\GamesProb::class, 2)->create([
            'idgame' => $this->game->id,
                'tipo_probabilidad' => '1',
                'activa' => '1',
                'acierto' => 0,
            ]);

        $this->male = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '1',
            'probabilidad' => '1',
            'orden' => '1',
            'activa' => '1',
            'acierto' => 0,
        ]);     

        $this->female = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '1',
            'probabilidad' => '2',
            'orden' => '2',
            'activa' => '1',
            'acierto' => 0,
        ]);   

        $this->soccerMale = factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '1',
            'probabilidad' => '1',
            'orden' => '1',
            'activa' => '1',
            'acierto' => 0,
        ]);     

        $this->soccerFemale = factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '1',
            'probabilidad' => '2',
            'orden' => '2',
            'activa' => '1',
            'acierto' => 0,
        ]);  

        $this->half_male = factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '11',
            'equipo' => $this->soccerMale->equipo,
            'probabilidad' => '1',
            'orden' => '1',
            'activa' => '1',
            'acierto' => 0,
        ]);     

        $this->half_female = factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '11',
            'equipo' => $this->soccerFemale->equipo,
            'probabilidad' => '2',
            'orden' => '2',
            'activa' => '1',
            'acierto' => 0,
        ]);   


    }

	public function test_game_result_can_be_showed()
	{
	    	//Having	

		 factory(\App\Resultado::class)->create([
			'idgame' => $this->game->id,
			'equipo' => $this->gamesProbs[0]->equipo,
    			'tipo' => '1',
    		]);

		factory(\App\Resultado::class)->create([
			'idgame' => $this->game->id,
			'equipo' => $this->gamesProbs[1]->equipo,
    			'tipo' => '1',
    		]);		

		//When
		$this->visit("admin/game/{$this->game->id}");

		//Then
		$this->seeInField('resultF[0]', $this->game->finalResult[$this->game->teams[0]->id])
			->seeInField('resultF[1]', $this->game->finalResult[$this->game->teams[1]->id]);
	}

	public function test_game_half_result_can_be_showed()
	{
	    //Having 		

		 factory(\App\Resultado::class)->create([
			'idgame' => $this->game->id,
			'equipo' => $this->gamesProbs[0]->equipo,
    			'tipo' => '3',
    		]);

		factory(\App\Resultado::class)->create([
			'idgame' => $this->game->id,
			'equipo' => $this->gamesProbs[1]->equipo,
    			'tipo' => '3',
    		]);		

		//When
		$this->visit("admin/game/{$this->game->id}");

		//Then
		$this->seeInField('resultH[0]', $this->game->halfResult[$this->game->teams[0]->id])
			->seeInField('resultH[1]', $this->game->halfResult[$this->game->teams[1]->id]);
	}	

	public function test_game_hre_result_result_can_be_showed()
	{
	    //Having
		 factory(\App\Resultado::class)->create([
			'idgame' => $this->game->id,
			'equipo' => $this->gamesProbs[0]->equipo,
    			'tipo' => '5',
    			'score' => '31'
    		]);


		//When
		$this->visit("admin/game/{$this->game->id}");

		//Then
		$this->seeInField('hre', '31');
	}		

	public function review_test_game_who_scored_first_result_result_can_be_showed()
	{
	    //Having
		 factory(\App\Resultado::class)->create([
			'idgame' => $this->game->id,
			'equipo' => $this->gamesProbs[0]->equipo,
    			'tipo' => '2',
    		]);

		//When
		$this->visit("admin/game/{$this->game->id}");

		//Then
		$this->see('<option value="' .  $this->game->whoScoredFrist->equipo . '" selected>');
	}	

	public function test_game_scored_in_first_ining_result_can_be_showed()
	{
	    //Having
		 $resultado = factory(\App\Resultado::class)->create([
			'idgame' => $this->game->id,
			'equipo' => 0,
    			'tipo' => '4',
    			'score' => '2'
    		]);

		//When
			
		$this->visit("admin/game/{$this->game->id}");

		//Then
		$this->see('<option value="2" selected>');
	}		

	public function test_game_who_go_next_level_result_can_be_showed()
	{
	    //Having
		$gamesProbs =factory(\App\GamesProb::class)->times(2)->create([
			'idgame' => $this->soccerGame->id,
    			'tipo_probabilidad' => '1',
    			'activa' => '1',
                'acierto' => 0,
    		]);    		

		factory(\App\Resultado::class)->create([
			'idgame' => $this->soccerGame->id,
			'equipo' => $gamesProbs[0]->equipo,
    			'tipo' => '6',
    			'score' => '0'
    		]);

		//When
			
		$this->visit("admin/game/{$this->soccerGame->id}");
		
		//Then
		$this->see('<option value="' .  $this->soccerGame->whoGoNextLevel->equipo . '" selected>');
	}	


	public function test_result_can_be_create()
	{	    	
		
		//When 
		$this->visit("admin/game/{$this->game->id}")
			->type('3', 'resultF[0]')
			->type('2', 'resultF[1]')
			->type('1', 'resultH[0]')
			->type('2', 'resultH[1]')
			->select($this->gamesProbs[0]->equipo, 'who')
			->select('1', 'inning1')
			->type('21', 'hre')
			->press('Actualizar');



		//Then
		//Resultado Final
		$this->seeInDatabase('resultados',[
			'tipo' => '1',
			'idgame' => $this->game->id,
			'equipo' => $this->gamesProbs[0]->equipo,
			'score' => '3'
		]);

		$this->seeInDatabase('resultados',[
			'tipo' => '1',
			'idgame' => $this->game->id,
			'equipo' => $this->gamesProbs[1]->equipo,
			'score' => '2'
		]);		

		//Resultado mitrad de tiempo
		$this->seeInDatabase('resultados',[
			'tipo' => '3',
			'idgame' => $this->game->id,
			'equipo' => $this->gamesProbs[0]->equipo,
			'score' => '1'
		]);		

		$this->seeInDatabase('resultados',[
			'tipo' => '3',
			'idgame' => $this->game->id,
			'equipo' => $this->gamesProbs[1]->equipo,
			'score' => '2'
		]);		

		//Quien anoto primero
		$this->seeInDatabase('resultados',[
			'tipo' => '2',
			'idgame' => $this->game->id,
			'equipo' => $this->gamesProbs[0]->equipo,
			'score' => '0'
		]);

		//Anotaron en el 1er inning
		$this->seeInDatabase('resultados',[
			'tipo' => '4',
			'idgame' => $this->game->id,
			'equipo' =>'0',
			'score' => '1'
		]);			

		//HRE
		$this->seeInDatabase('resultados',[
			'tipo' => '5',
			'idgame' => $this->game->id,
			'equipo' => '0',
			'score' => '21'
		]);		

		$this->seePageIs('/admin/game');

	}

	public function review_test_go_next_level_result_can_be_create()
	{
	    	//Having
		$gamesProbs =factory(\App\GamesProb::class)->times(2)->create([
			'idgame' => $this->soccerGame->id,
    			'tipo_probabilidad' => '1',
    			'activa' => '1',
                'acierto' => 0,
    		]);    		

		
		//When 
		$this->visit("admin/game/{$this->soccerGame->id}")
			->type('3', 'resultF[0]')
			->type('2', 'resultF[1]')
			->type('1', 'resultH[0]')
			->type('2', 'resultH[1]')
			->select($gamesProbs[1]->equipo, 'next')
			->press('Actualizar');

		//Then
		//Resultado Final
		$this->seeInDatabase('resultados',[
			'tipo' => '1',
			'idgame' => $this->soccerGame->id,
			'equipo' => $gamesProbs[0]->equipo,
			'score' => '3'
		]);

		$this->seeInDatabase('resultados',[
			'tipo' => '1',
			'idgame' => $this->soccerGame->id,
			'equipo' => $gamesProbs[1]->equipo,
			'score' => '2'
		]);		

		//Resultado mitrad de tiempo
		$this->seeInDatabase('resultados',[
			'tipo' => '3',
			'idgame' => $this->soccerGame->id,
			'equipo' => $gamesProbs[0]->equipo,
			'score' => '1'
		]);		

		$this->seeInDatabase('resultados',[
			'tipo' => '3',
			'idgame' => $this->soccerGame->id,
			'equipo' => $gamesProbs[1]->equipo,
			'score' => '2'
		]);		

		//Avanza a siguiente fase
		$this->seeInDatabase('resultados',[
			'tipo' => '6',
			'idgame' => $this->soccerGame->id,
			'equipo' => $gamesProbs[1]->equipo,
			'score' => '0'
		]);

		$this->seePageIs('/admin/game');
	}

	public function test_result_can_be_replaced()
	{
	    //Having   		

		factory(\App\Resultado::class)->create([
			'idgame' => $this->game->id,
			'equipo' => $this->gamesProbs[0]->equipo,
    			'tipo' => '1',
    			'score' => '12'
    		]);		

		factory(\App\Resultado::class)->create([
			'idgame' => $this->game->id,
			'equipo' => $this->gamesProbs[1]->equipo,
    			'tipo' => '1',
    			'score' => '13'
    		]);				

		
		//When 
		$this->visit("admin/game/{$this->game->id}")
			->type('3', 'resultF[0]')
			->type('2', 'resultF[1]')
			->type('1', 'resultH[0]')
			->type('2', 'resultH[1]')
			->select($this->gamesProbs[0]->equipo, 'who')
			->select('1', 'inning1')
			->type('21', 'hre')
			->press('Actualizar');

		//Then

		$this->dontSeeInDatabase('resultados',[
			'tipo' => '1',
			'idgame' => $this->game->id,
			'equipo' => $this->gamesProbs[0]->equipo,
			'score' => '13'
		]);

		$this->dontSeeInDatabase('resultados',[
			'tipo' => '1',
			'idgame' => $this->game->id,
			'equipo' => $this->gamesProbs[1]->equipo,
			'score' => '12'
		]);			

		$this->seePageIs('/admin/game');

	}	

	public function test_result_is_showed_in_index()
	{
		factory(\App\Resultado::class)->create([
			'idgame' => $this->soccerGame->id,
			'equipo' => $this->gamesProbs[0]->equipo,
    			'tipo' => '1',
    			'score' => '12'
    		]);		

		factory(\App\Resultado::class)->create([
			'idgame' => $this->soccerGame->id,
			'equipo' => $this->gamesProbs[1]->equipo,
    			'tipo' => '1',
    			'score' => '13'
    		]);		

    		//When		
		$this->visit('/admin/game');

		//Then
		//$this-> seeInElement('li', '12 - 13');
	}

	public function test_game_is_showed_without_result()
	{	    	
    		//When		
		$this->visit('/admin/game');

		//Then
		$this-> seeInElement('li', '-');
	}


    function test_tied_game()
    {
        $tied =factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '6',       
            'activa' => '1',
            'acierto' => 0,
        ]);  

        //When      
        $this->visit('/admin/game/' . $this->game->id)
            ->type('0', 'resultF[0]')
            ->type('0', 'resultF[1]')            
            ->press('Actualizar');    

        $this->seeInDatabase('games_probs',[
            'id' => $tied->id,
            'acierto' => '1',
        ]);        
    }    

    function test_who_scored_first()
    {
        $who1 =factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'equipo' => $this->male->equipo,
            'tipo_probabilidad' => '8',       
            'activa' => '1',
            'acierto' => 0,
        ]);  

        $who2 =factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'equipo' => $this->female->equipo,
            'tipo_probabilidad' => '8',       
            'activa' => '1',
            'acierto' => 0,
        ]);          

        $this->visit('/admin/game/' . $this->game->id)
            ->type($who1->equipo, 'who')        
            ->press('Actualizar');        

            //dd($this->game->who_scored_first);              
        
        $this->seeInDatabase('games_probs',[
            'id' => $who1->id,
            'acierto' => '1',
        ]); 

        $this->seeInDatabase('games_probs',[
            'id' => $who2->id,
            'acierto' => '0',
        ]); 

    }

    function test_scored_in_first_inning()
    {
        $yes =factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '9',       
            'activa' => '1',
            'orden' => 1,
            'acierto' => 0,
        ]);  

        $no =factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '9',       
            'activa' => '1',
            'orden' => 2,
            'acierto' => 0,
        ]);          

        $this->visit('/admin/game/' . $this->game->id)
            ->type($yes->orden, 'inning1')        
            ->press('Actualizar');        

        //dd($this->game->first_ining_scored);              
        
        $this->seeInDatabase('games_probs',[
            'id' => $yes->id,
            'acierto' => '1',
        ]); 

        $this->seeInDatabase('games_probs',[
            'id' => $no->id,
            'acierto' => '0',
        ]); 

    }    

    function test_tied_game_half()
    {
        $tied =factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '14',       
            'activa' => '1',
            'acierto' => 0,
        ]);  

        //When      
        $this->visit('/admin/game/' . $this->game->id)
            ->type('0', 'resultH[0]')
            ->type('0', 'resultH[1]')            
            ->press('Actualizar');    

        $this->seeInDatabase('games_probs',[
            'id' => $tied->id,
            'acierto' => '1',
        ]);        
    }     

    function test_who_go_next_level()
    {
        $one =factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '15', 
            'equipo' => $this->soccerMale->equipo,
            'activa' => '1',
            'orden' => 1,
            'acierto' => 0,
        ]);  

        $two =factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '15', 
            'equipo' => $this->soccerFemale->equipo,
            'activa' => '1',
            'orden' => 2,
            'acierto' => 0,
        ]);          

        $this->visit('/admin/game/' . $this->soccerGame->id)
            ->type($one->equipo, 'next')        
            ->press('Actualizar');        

        //dd($this->soccerGame->who_go_next_level);              
        
        $this->seeInDatabase('games_probs',[
            'id' => $one->id,
            'acierto' => '1',
        ]); 

        $this->seeInDatabase('games_probs',[
            'id' => $two->id,
            'acierto' => '0',
        ]); 

    } 

    function test_who_win_draw()
    {

        $winner = factory(\App\GamesProb::class)->create([
            'idgame' => $this->draw->id,
            'tipo_probabilidad' => '19', 
            'activa' => '1',
            'orden' => 1,
            'acierto' => 0,
        ]);  

        $lossers = factory(\App\GamesProb::class, 36)->create([
            'idgame' => $this->draw->id,
            'tipo_probabilidad' => '19', 
            'activa' => '1',
            'orden' => 2,
            'acierto' => 0,
        ]);          

        //dd($this->draw->gamesProbs);              
        
        $this->visit('/admin/game/' . $this->draw->id)
            ->type($winner->equipo, 'draw')        
            ->press('Actualizar');        

        
        $this->seeInDatabase('games_probs',[
            'id' => $winner->id,
            'acierto' => '1',
        ]); 

        foreach ($lossers as $loser) {
            $this->seeInDatabase('games_probs',[
                'id' => $loser->id,
                'acierto' => '0',
            ]); 
        }


    } 

    function test_if_bet_is_setted_winner()
    {
        $draw = factory(\App\Game::class)->create([
            'status' => 0,
            'categoria' => 12,
        ]);


        $winner = factory(\App\GamesProb::class)->create([
            'idgame' => $draw->id,
            'tipo_probabilidad' => '19', 
            'activa' => '1',
            'orden' => 1,
            'acierto' => 0,
        ]);  

        $lossers = factory(\App\GamesProb::class, 36)->create([
            'idgame' => $draw->id,
            'tipo_probabilidad' => '19', 
            'activa' => '1',
            'orden' => 2,
            'acierto' => 0,
        ]); 

        $bet = factory(\App\Bet::class)->create([
            'monto' => 1000,
            'status' => 1,
        ]);

        $betDetail = factory(\App\BetsDetail::class)->create([
            'idbets' => $bet->id,
            'idgames_probs' => $winner->id
        ]);


        $this->visit('/admin/game/' . $draw->id)
            ->type($winner->equipo, 'draw')        
            ->press('Actualizar');  



        $this->seeInDatabase('bets',[
            'id' => $bet->id,
            'status' => '2',
        ]);               



    }

    function test_if_bet_is_pending()
    {
        $winner = factory(\App\GamesProb::class)->create([
            'idgame' => $this->draw->id,
            'tipo_probabilidad' => '19', 
            'activa' => '1',
            'orden' => 1,
            'acierto' => 0,
        ]);  

        $lossers = factory(\App\GamesProb::class, 36)->create([
            'idgame' => $this->draw->id,
            'tipo_probabilidad' => '19', 
            'activa' => '1',
            'orden' => 2,
            'acierto' => 0,
        ]); 

        $bet = factory(\App\Bet::class)->create([
            'monto' => 1000,
            'status' => 1,
        ]);

        $betDetail = factory(\App\BetsDetail::class)->create([
            'idbets' => $bet->id,
            'idgames_probs' => $lossers[0]->id,
        ]);

        factory(\App\BetsDetail::class)->create([
            'idbets' => $bet->id,
            'idgames_probs' => $this->male->id,
        ]);


        $this->visit('/admin/game/' . $this->draw->id)
            ->type($winner->equipo, 'draw')        
            ->press('Actualizar');  

        $this->seeInDatabase('bets',[
            'id' => $bet->id,
            'status' => '1',
        ]);     
    }    

    function test_if_bet_is_setted_loser()
    {
        $winner = factory(\App\GamesProb::class)->create([
            'idgame' => $this->draw->id,
            'tipo_probabilidad' => '19', 
            'activa' => '1',
            'orden' => 1,
            'acierto' => 0,
        ]);  

        $lossers = factory(\App\GamesProb::class, 36)->create([
            'idgame' => $this->draw->id,
            'tipo_probabilidad' => '19', 
            'activa' => '1',
            'orden' => 2,
            'acierto' => 0,
        ]); 

        $bet = factory(\App\Bet::class)->create([
            'monto' => 1000,
            'status' => 1,
        ]);

        $betDetail = factory(\App\BetsDetail::class)->create([
            'idbets' => $bet->id,
            'idgames_probs' => $lossers[0]->id,
        ]);


        $this->visit('/admin/game/' . $this->draw->id)
            ->type($winner->equipo, 'draw')        
            ->press('Actualizar');  

        $this->seeInDatabase('bets',[
            'id' => $bet->id,
            'status' => '3',
        ]);     
    }      

    function test_if_basket_bet_is_setted_winner()
    {
        $male = factory(\App\GamesProb::class)->create([
            'idgame' => $this->basketGame->id,
            'tipo_probabilidad' => '1', 
            'probabilidad' => 1,
            'activa' => '1',
            'orden' => 1,
            'acierto' => 0,
        ]); 

        $female = factory(\App\GamesProb::class)->create([
            'idgame' => $this->basketGame->id,
            'tipo_probabilidad' => '1', 
            'probabilidad' => 2,
            'activa' => '1',
            'orden' => 1,
            'acierto' => 0,
        ]); 


        $rl1 = factory(\App\GamesProb::class)->create([
            'idgame' => $this->basketGame->id,
            'tipo_probabilidad' => '3', 
            'equipo' => $male->equipo,
            'cantidad' => 6,
            'activa' => '1',
            'orden' => 1,
            'acierto' => 0,
        ]); 

        $rl2 = factory(\App\GamesProb::class)->create([
            'idgame' => $this->basketGame->id,
            'tipo_probabilidad' => '3', 
            'equipo' => $female->equipo,
            'cantidad' => 6,
            'activa' => '1',
            'orden' => 2,
            'acierto' => 0,
        ]);         

        $up = factory(\App\GamesProb::class)->create([
            'idgame' => $this->basketGame->id,
            'tipo_probabilidad' => '4', 
            'cantidad' => 222,
            'activa' => '1',
            'orden' => 1,
            'acierto' => 0,
        ]);        

        $low = factory(\App\GamesProb::class)->create([
            'idgame' => $this->basketGame->id,
            'tipo_probabilidad' => '4', 
            'cantidad' => 222,
            'activa' => '1',
            'orden' => 2,
            'acierto' => 0,
        ]); 

        $bet = factory(\App\Bet::class)->create([
            'monto' => 25000,
            'status' => 1,
        ]);

        $betRl = factory(\App\BetsDetail::class)->create([
            'idbets' => $bet->id,
            'idgames_probs' => $rl1->id
        ]);

        $betUp = factory(\App\BetsDetail::class)->create([
            'idbets' => $bet->id,
            'idgames_probs' => $up->id
        ]);        



        $this->visit("admin/game/{$this->basketGame->id}")
            ->type('141', 'resultF[0]')
            ->type('113', 'resultF[1]')
            ->type('74', 'resultH[0]')
            ->type('57', 'resultH[1]')
            ->press('Actualizar');            

        $this->seeInDatabase('bets',[
            'monto' => '25000',
            'status' => '2',
            'id' => $bet->id,
        ]);               



    }
}

