<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\FeatureTestCase;

class ResultRunlineTest extends FeatureTestCase
{
    use DatabaseTransactions;

 protected function setUp()
    {
        parent::setUp();

        $this->admin = factory(\App\User::class)->create([
            'role' => 'admin'
        ]);

        $this->actingAs($this->admin);        

        $this->game =  factory(\App\Game::class)->create([
            'status' => 0,
            'categoria' => 1,
        ]);

        $this->soccerGame =  factory(\App\Game::class)->create([
            'status' => 0,
            'categoria' => 3
        ]);       

        $this->gamesProbs =factory(\App\GamesProb::class, 2)->create([
            'idgame' => $this->game->id,
                'tipo_probabilidad' => '1',
                'activa' => '1',
                'acierto' => 0,
            ]);

        $this->male = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '1',
            'probabilidad' => '1',
            'orden' => '1',
            'activa' => '1',
            'acierto' => 0,
        ]);     

        $this->female = factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '1',
            'probabilidad' => '2',
            'orden' => '2',
            'activa' => '1',
            'acierto' => 0,
        ]);   

        $this->soccerMale = factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '1',
            'probabilidad' => '1',
            'orden' => '1',
            'activa' => '1',
            'acierto' => 0,
        ]);     

        $this->soccerFemale = factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '1',
            'probabilidad' => '2',
            'orden' => '2',
            'activa' => '1',
            'acierto' => 0,
        ]);  

        $this->half_male = factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '11',
            'equipo' => $this->soccerMale->equipo,
            'probabilidad' => '1',
            'orden' => '1',
            'activa' => '1',
            'acierto' => 0,
        ]);     

        $this->half_female = factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '11',
            'equipo' => $this->soccerFemale->equipo,
            'probabilidad' => '2',
            'orden' => '2',
            'activa' => '1',
            'acierto' => 0,
        ]);   


    }

    function test_runline_male()
    {

        $rl1 =factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '3',       
            'equipo' => $this->soccerMale->equipo,
            'cantidad' => '1',
            'orden' => '1',
            'activa' => '1',
            'acierto' => 0,
        ]);  

        $rl2 =factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '3',     
            'equipo' => $this->soccerFemale->equipo,
            'cantidad' => '1',
            'orden' => '2',
            'activa' => '1',
            'acierto' => 0,
        ]);    


        //When      
        $this->visit('/admin/game/' . $this->soccerGame->id)
            ->type('3', 'resultF[0]')
            ->type('0', 'resultF[1]')            
            ->press('Actualizar');    

        $this->seeInDatabase('games_probs',[
            'id' => $rl1->id,
            'acierto' => '1',
        ]) ;

        $this->seeInDatabase('games_probs',[
            'id' => $rl2->id,
            'acierto' => '0',
        ]) ;        
    }

    function test_runline_female_win()
    {
        //Having          

        $rl1 =factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '3',       
            'equipo' => $this->soccerMale->equipo,
            'cantidad' => '1',
            'orden' => '1',
            'activa' => '1',
            'acierto' => 0,
        ]);  

        $rl2 =factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '3',     
            'equipo' => $this->soccerFemale->equipo,
            'cantidad' => '1',
            'orden' => '2',
            'activa' => '1',
            'acierto' => 0,
        ]);    


        //When      
        $this->visit('/admin/game/' . $this->soccerGame->id)
            ->type('1', 'resultF[0]')
            ->type('3', 'resultF[1]')            
            ->press('Actualizar');   


        $this->seeInDatabase('games_probs',[
            'id' => $rl1->id,
            'acierto' => '0',
        ]) ;

        $this->seeInDatabase('games_probs',[
            'id' => $rl2->id,
            'acierto' => '1',
        ]) ;        
    }

    function test_runline_female_lose()
    {      

        $rl1 =factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '3',       
            'equipo' => $this->male->equipo,
            'cantidad' => '3',
            'orden' => '1',
            'activa' => '1',
            'acierto' => 0,
        ]);  

        $rl2 =factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '3',     
            'equipo' => $this->female->equipo,
            'cantidad' => '3',
            'orden' => '2',
            'activa' => '1',
            'acierto' => 0,
        ]);    


        //When      
        $this->visit('/admin/game/' . $this->game->id)
            ->type('2', 'resultF[0]')
            ->type('0', 'resultF[1]')            
            ->press('Actualizar');       
        $this->seeInDatabase('games_probs',[
            'id' => $rl1->id,
            'acierto' => '0',
        ]) ;

        $this->seeInDatabase('games_probs',[
            'id' => $rl2->id,
            'acierto' => '1',
        ]) ;        
    }   

    function test_runline_female_tied()
    {       

        $rl1 =factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '3',       
            'equipo' => $this->male->equipo,
            'cantidad' => '3',
            'orden' => '1',
            'activa' => '1',
            'acierto' => 0,
        ]);  

        $rl2 =factory(\App\GamesProb::class)->create([
            'idgame' => $this->game->id,
            'tipo_probabilidad' => '3',     
            'equipo' => $this->female->equipo,
            'cantidad' => '3',
            'orden' => '2',
            'activa' => '1',
            'acierto' => 0,
        ]);    


        //When      
        $this->visit('/admin/game/' . $this->game->id)
            ->type('0', 'resultF[0]')
            ->type('0', 'resultF[1]')            
            ->press('Actualizar');       
        $this->seeInDatabase('games_probs',[
            'id' => $rl1->id,
            'acierto' => '0',
        ]) ;

        $this->seeInDatabase('games_probs',[
            'id' => $rl2->id,
            'acierto' => '1',
        ]) ;        
    }   

         function test_half_runline_male()
    {

        $rl1 =factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '12',       
            'equipo' => $this->half_male->equipo,
            'cantidad' => '1',
            'orden' => '1',
            'activa' => '1',
            'acierto' => 0,
        ]);  

        $rl2 =factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '12',     
            'equipo' => $this->half_female->equipo,
            'cantidad' => '1',
            'orden' => '2',
            'activa' => '1',
            'acierto' => 0,
        ]);    


        //When      
        $this->visit('/admin/game/' . $this->soccerGame->id)
            ->type('5', 'resultH[0]')
            ->type('0', 'resultH[1]')            
            ->press('Actualizar');    

        $this->seeInDatabase('games_probs',[
            'id' => $rl1->id,
            'acierto' => '1',
        ]) ;

        $this->seeInDatabase('games_probs',[
            'id' => $rl2->id,
            'acierto' => '0',
        ]) ;        
    }

    function test_runline_female_win_half()
    {
        //Having          

        $rl1 =factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '12',       
            'equipo' => $this->soccerMale->equipo,
            'cantidad' => '1',
            'orden' => '1',
            'activa' => '1',
            'acierto' => 0,
        ]);  

        $rl2 =factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '12',     
            'equipo' => $this->soccerFemale->equipo,
            'cantidad' => '1',
            'orden' => '2',
            'activa' => '1',
            'acierto' => 0,
        ]);    


        //When      
        $this->visit('/admin/game/' . $this->soccerGame->id)
            ->type('1', 'resultH[0]')
            ->type('3', 'resultH[1]')            
            ->press('Actualizar');   


        $this->seeInDatabase('games_probs',[
            'id' => $rl1->id,
            'acierto' => '0',
        ]) ;

        $this->seeInDatabase('games_probs',[
            'id' => $rl2->id,
            'acierto' => '1',
        ]) ;        
    }    

    function test_runline_female_lose_half()
    {      

        $rl1 =factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '12',       
            'equipo' => $this->soccerMale->equipo,
            'cantidad' => '3',
            'orden' => '1',
            'activa' => '1',
            'acierto' => 0,
        ]);  

        $rl2 =factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '12',     
            'equipo' => $this->soccerFemale->equipo,
            'cantidad' => '3',
            'orden' => '2',
            'activa' => '1',
            'acierto' => 0,
        ]);    


        //When      
        $this->visit('/admin/game/' . $this->soccerGame->id)
            ->type('2', 'resultH[0]')
            ->type('0', 'resultH[1]')            
            ->press('Actualizar');   


        $this->seeInDatabase('games_probs',[
            'id' => $rl1->id,
            'acierto' => '0',
        ]) ;

        $this->seeInDatabase('games_probs',[
            'id' => $rl2->id,
            'acierto' => '1',
        ]) ;        
    }   

    function test_runline_female_tied_half()
    {       

        $rl1 =factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '12',       
            'equipo' => $this->soccerMale->equipo,
            'cantidad' => '3',
            'orden' => '1',
            'activa' => '1',
            'acierto' => 0,
        ]);  

        $rl2 =factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '12',     
            'equipo' => $this->soccerFemale->equipo,
            'cantidad' => '3',
            'orden' => '2',
            'activa' => '1',
            'acierto' => 0,
        ]);    


        //When      
        $this->visit('/admin/game/' . $this->soccerGame->id)
            ->type('0', 'resultH[0]')
            ->type('0', 'resultH[1]')            
            ->press('Actualizar');       

        $this->seeInDatabase('games_probs',[
            'id' => $rl1->id,
            'acierto' => '0',
        ]) ;

        $this->seeInDatabase('games_probs',[
            'id' => $rl2->id,
            'acierto' => '1',
        ]) ;        
    }

    function test_alternate_runline_female_win()
    {

        $rl1 =factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '16',       
            'equipo' => $this->soccerMale->equipo,
            'cantidad' => '1',
            'orden' => '1',
            'activa' => '1',
            'acierto' => 0,
        ]);  

        $rl2 =factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '16',     
            'equipo' => $this->soccerFemale->equipo,
            'cantidad' => '1',
            'orden' => '2',
            'activa' => '1',
            'acierto' => 0,
        ]);    


        //When      
        $this->visit('/admin/game/' . $this->soccerGame->id)
            ->type('0', 'resultF[0]')
            ->type('3', 'resultF[1]')            
            ->press('Actualizar');    

        $this->seeInDatabase('games_probs',[
            'id' => $rl1->id,
            'acierto' => '0',
        ]) ;

        $this->seeInDatabase('games_probs',[
            'id' => $rl2->id,
            'acierto' => '1',
        ]) ;        
    }    

    function test_alternate_runline_male_win()
    {
        //Having          

        $rl1 =factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '16',       
            'equipo' => $this->soccerMale->equipo,
            'cantidad' => '3',
            'orden' => '1',
            'activa' => '1',
            'acierto' => 0,
        ]);  

        $rl2 =factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '16',     
            'equipo' => $this->soccerFemale->equipo,
            'cantidad' => '3',
            'orden' => '2',
            'activa' => '1',
            'acierto' => 0,
        ]);    


        //When      
        $this->visit('/admin/game/' . $this->soccerGame->id)
            ->type('5', 'resultF[0]')
            ->type('1', 'resultF[1]')            
            ->press('Actualizar');  

        $this->seeInDatabase('games_probs',[
            'id' => $rl1->id,
            'acierto' => '1',
        ]) ;

        $this->seeInDatabase('games_probs',[
            'id' => $rl2->id,
            'acierto' => '0',
        ]) ;        
    }  

    function test_alternate_runline_male_lose()
    {      

        $rl1 =factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '16',       
            'equipo' => $this->soccerMale->equipo,
            'cantidad' => '3',
            'orden' => '1',
            'activa' => '1',
            'acierto' => 0,
        ]);  

        $rl2 =factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '16',     
            'equipo' => $this->soccerFemale->equipo,
            'cantidad' => '3',
            'orden' => '2',
            'activa' => '1',
            'acierto' => 0,
        ]);    


        //When      
        $this->visit('/admin/game/' . $this->soccerGame->id)
            ->type('0', 'resultF[0]')
            ->type('2', 'resultF[1]')            
            ->press('Actualizar');


        $this->seeInDatabase('games_probs',[
            'id' => $rl1->id,
            'acierto' => '1',
        ]) ;

        $this->seeInDatabase('games_probs',[
            'id' => $rl2->id,
            'acierto' => '0',
        ]) ;        
    }      

    function test_alternate_runline_male_tied()
    {       

        $rl1 =factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '16',       
            'equipo' => $this->soccerMale->equipo,
            'cantidad' => '3',
            'orden' => '1',
            'activa' => '1',
            'acierto' => 0,
        ]);  

        $rl2 =factory(\App\GamesProb::class)->create([
            'idgame' => $this->soccerGame->id,
            'tipo_probabilidad' => '16',     
            'equipo' => $this->soccerFemale->equipo,
            'cantidad' => '3',
            'orden' => '2',
            'activa' => '1',
            'acierto' => 0,
        ]);     


        //When      
        $this->visit('/admin/game/' . $this->soccerGame->id)
            ->type('0', 'resultF[0]')
            ->type('0', 'resultF[1]')            
            ->press('Actualizar');     

        $this->seeInDatabase('games_probs',[
            'id' => $rl1->id,
            'acierto' => '1',
        ]) ;

        $this->seeInDatabase('games_probs',[
            'id' => $rl2->id,
            'acierto' => '0',
        ]) ;        
    }   

}
