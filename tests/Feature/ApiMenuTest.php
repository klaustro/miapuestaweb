<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ApiMenuTest extends TestCase
{
	use DatabaseTransactions;
	
	protected $admin;

	public function setUp()
	{
		parent::setUp();

		$this->admin = factory(\App\User::class)->create([
			'role' => 'admin',
		]);		

	}

	public function test_api_menu_show()
	{
		$menu = factory(\App\Menu::class)->create([
			'nombre' => 'FUTBOL',
		]);
		
		$this->actingAs($this->admin)
			->get("/menu/{$menu->id}")
			->assertStatus(200)
			->assertJson([
				'nombre' => 'FUTBOL',
			]);

	}	

	public function test_api_menu_list()
	{

		$menu = factory(\App\Menu::class, 10)->create();

		$this->actingAs($this->admin)
			->get("/menu")
			->assertStatus(200);
	}	
}
