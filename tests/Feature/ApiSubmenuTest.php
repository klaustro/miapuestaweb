<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ApiSubmenuTest extends TestCase
{
	use DatabaseTransactions;
	
	public function test_api_submenu_list()
	{

		$user = factory(\App\User::class)->create();

		$this->actingAs($user);


		$menu = factory(\App\Menu::class)->create();
		$submenu = factory(\App\Submenu::class, 10)->create([
			'id_menu' => $menu->id,
		]);

		$this->get("/submenu?menu=" . $menu->id)
			->assertStatus(200)
			->assertJsonFragment([
				'id' => $submenu[0]->id,
			]);
	}	
}
