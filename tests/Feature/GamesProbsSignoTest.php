<?php

namespace Tests\Feature;

use App\Equipo;
use App\Game;
use App\Menu;
use App\Submenu;
use App\TipoApuesta;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class GamesProbsSignoTest extends TestCase
{
    use DatabaseTransactions; 

    protected $admin;
    protected $category;
    protected $league;
    protected $now;
    protected $win;
    protected $rl;
    protected $teams;

    protected function setUp()
    {
        parent::setUp();

        $this->admin = factory(\App\User::class)->create([
            'role' => 'admin'
        ]);        
        
        $this->category = factory(Menu::class)->create();

        $this->league = factory(Submenu::class)->create([
            'id_menu' => $this->category->id,
        ]);

        $this->win = factory(TipoApuesta::class)->create([
            'id' => 1,
            'nombre' => 'Ganador'
        ]);

        $this->rl = factory(TipoApuesta::class)->create([
            'id' => 3,
            'nombre' => 'Runline'
        ]);

        $this->total = factory(TipoApuesta::class)->create([
            'id' => 4,
            'nombre' => 'Alta/Baja'
        ]);

        $this->halfWin = factory(TipoApuesta::class)->create([
            'id' => 11,
            'nombre' => 'Ganador Mitad de Juego'
        ]);


        $this->halfRl = factory(TipoApuesta::class)->create([
            'id' => 12,
            'nombre' => 'Runline Mitad de juego'
        ]);        


        $this->alternateRl = factory(TipoApuesta::class)->create([
            'id' => 16,
            'nombre' => 'Runline Alternativo'
        ]);




        $this->teams = factory(Equipo::class, 2)->create();

        $this->now = date_format(Carbon::now(),"Y-m-d H:i:s");
    }    

    public function test_runline_sign()
    {
        $this->actingAs($this->admin);
        
        $this->post('admin/game', [
            'categoria' => $this->category->id,
            'subcategoria' => $this->league->id,
            'compuesta' => 1,
            'fecha_cierre' => $this->now,
            'status' => 0,
            'visible' => 1,
            'probs' => [
                '0' =>[
                    'tipo_probabilidad' => $this->win,
                    'equipo' => $this->teams[0], 
                    'orden' => '1',
                    'cantidad' => '0',  
                    'probabilidad' => '2.1',       
                    'activa' => '1',
                    'acierto' => '1',               
                ],
                '1' =>[
                    'tipo_probabilidad' => $this->win,
                    'equipo' => $this->teams[1], 
                    'orden' => '2',
                    'cantidad' => '0',
                    'probabilidad' => '1.9',       
                    'activa' => '1',
                    'acierto' => '1',               
                ],  
                '2' =>[
                    'tipo_probabilidad' => $this->rl,
                    'equipo' => $this->teams[0], 
                    'orden' => '1',
                    'cantidad' => '3',  
                    'probabilidad' => '1.9',       
                    'activa' => '1',
                    'acierto' => '1',               
                ],
                '3' =>[
                    'tipo_probabilidad' => $this->rl,
                    'equipo' => $this->teams[1], 
                    'orden' => '2',
                    'cantidad' => '3',
                    'probabilidad' => '1.9',       
                    'activa' => '1',
                    'acierto' => '1',               
                ],   
            ],
        ])->assertSuccessful();             


        $this->assertTrue(Game::first()->gamesProbs[2]->selection == $this->teams[0]->nombre . ' +3 1.9');
        $this->assertTrue(Game::first()->gamesProbs[3]->selection == $this->teams[1]->nombre . ' -3 1.9');
    }

    public function test_totals_sign()
    {

        $this->actingAs($this->admin);
        
        $this->post('admin/game', [
            'categoria' => $this->category->id,
            'subcategoria' => $this->league->id,
            'compuesta' => 1,
            'fecha_cierre' => $this->now,
            'status' => 0,
            'visible' => 1,
            'probs' => [
                '0' =>[
                    'tipo_probabilidad' => $this->win,
                    'equipo' => $this->teams[0], 
                    'orden' => '1',
                    'cantidad' => '0',  
                    'probabilidad' => '2.1',       
                    'activa' => '1',
                    'acierto' => '1',               
                ],
                '1' =>[
                    'tipo_probabilidad' => $this->win,
                    'equipo' => $this->teams[1], 
                    'orden' => '2',
                    'cantidad' => '0',
                    'probabilidad' => '1.9',       
                    'activa' => '1',
                    'acierto' => '1',               
                ],  
                '2' =>[
                    'tipo_probabilidad' => $this->total,
                    'equipo' => $this->teams[0], 
                    'orden' => '1',
                    'cantidad' => '3',  
                    'probabilidad' => '1.9',       
                    'activa' => '1',
                    'acierto' => '1',               
                ],
                '3' =>[
                    'tipo_probabilidad' => $this->total,
                    'equipo' => $this->teams[1], 
                    'orden' => '2',
                    'cantidad' => '3',
                    'probabilidad' => '1.9',       
                    'activa' => '1',
                    'acierto' => '1',               
                ],   
            ],
        ])->assertSuccessful();             


        $this->assertTrue(Game::first()->gamesProbs[2]->selection == '+3 1.9');
        $this->assertTrue(Game::first()->gamesProbs[3]->selection == '-3 1.9');
    }    

    public function test_alternate_runline_sign()
    {

        $this->actingAs($this->admin);
        
        $this->post('admin/game', [
            'categoria' => $this->category->id,
            'subcategoria' => $this->league->id,
            'compuesta' => 1,
            'fecha_cierre' => $this->now,
            'status' => 0,
            'visible' => 1,
            'probs' => [
                '0' =>[
                    'tipo_probabilidad' => $this->win,
                    'equipo' => $this->teams[0], 
                    'orden' => '1',
                    'cantidad' => '0',  
                    'probabilidad' => '2.1',       
                    'activa' => '1',
                    'acierto' => '1',               
                ],
                '1' =>[
                    'tipo_probabilidad' => $this->win,
                    'equipo' => $this->teams[1], 
                    'orden' => '2',
                    'cantidad' => '0',
                    'probabilidad' => '1.9',       
                    'activa' => '1',
                    'acierto' => '1',               
                ],  
                '2' =>[
                    'tipo_probabilidad' => $this->alternateRl,
                    'equipo' => $this->teams[0], 
                    'orden' => '1',
                    'cantidad' => '3',  
                    'probabilidad' => '1.9',       
                    'activa' => '1',
                    'acierto' => '1',               
                ],
                '3' =>[
                    'tipo_probabilidad' => $this->alternateRl,
                    'equipo' => $this->teams[1], 
                    'orden' => '2',
                    'cantidad' => '3',
                    'probabilidad' => '1.9',       
                    'activa' => '1',
                    'acierto' => '1',               
                ],   
            ],
        ])->assertSuccessful();             


        $this->assertTrue(Game::first()->gamesProbs[2]->selection == $this->teams[0]->nombre . ' -3 1.9');
        $this->assertTrue(Game::first()->gamesProbs[3]->selection == $this->teams[1]->nombre . ' +3 1.9');
    }    

    public function test_half_runline_sign()
    {
        $this->actingAs($this->admin);
        
        $this->post('admin/game', [
            'categoria' => $this->category->id,
            'subcategoria' => $this->league->id,
            'compuesta' => 1,
            'fecha_cierre' => $this->now,
            'status' => 0,
            'visible' => 1,
            'probs' => [
                '0' =>[
                    'tipo_probabilidad' => $this->halfWin,
                    'equipo' => $this->teams[0], 
                    'orden' => '1',
                    'cantidad' => '0',  
                    'probabilidad' => '2.1',       
                    'activa' => '1',
                    'acierto' => '1',               
                ],
                '1' =>[
                    'tipo_probabilidad' => $this->halfWin,
                    'equipo' => $this->teams[1], 
                    'orden' => '2',
                    'cantidad' => '0',
                    'probabilidad' => '1.9',       
                    'activa' => '1',
                    'acierto' => '1',               
                ],  
                '2' =>[
                    'tipo_probabilidad' => $this->halfRl,
                    'equipo' => $this->teams[0], 
                    'orden' => '1',
                    'cantidad' => '3',  
                    'probabilidad' => '1.9',       
                    'activa' => '1',
                    'acierto' => '1',               
                ],
                '3' =>[
                    'tipo_probabilidad' => $this->halfRl,
                    'equipo' => $this->teams[1], 
                    'orden' => '2',
                    'cantidad' => '3',
                    'probabilidad' => '1.9',       
                    'activa' => '1',
                    'acierto' => '1',               
                ],   
            ],
        ])->assertSuccessful();             


        $this->assertTrue(Game::first()->gamesProbs[2]->selection == $this->teams[0]->nombre . ' +3 1.9');
        $this->assertTrue(Game::first()->gamesProbs[3]->selection == $this->teams[1]->nombre . ' -3 1.9');
    }


}
