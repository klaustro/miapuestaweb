<?php

namespace Tests\Feature;

use App\Banco;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ApiCuentasTest extends TestCase
{
    use DatabaseTransactions;
    
    protected $admin;

    public function setUp()
    {
        parent::setUp();

        $this->admin = factory(\App\User::class)->create([
            'role' => 'admin',
        ]);     

    }
    /** @test */
    public function api_cuentas_index()
    {
        $bank = factory(Banco::class)->create();

        $account = factory(\App\CuentasBancaria::class)->create([
            'banco_id' => $bank->id,
            'user_id' => $this->admin,
        ]);
        
        $this->actingAs($this->admin)
            ->get("/cuentasbancarias")
            ->assertStatus(200)
            ->assertJsonFragment([
                'id' => $account->id,
            ]);

    }
}
