<?php

namespace Tests\Feature;

use App\Equipo;
use App\Game;
use App\Menu;
use App\Stsgame;
use App\Submenu;
use App\TipoApuesta;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ApiGameTest extends TestCase
{
	use DatabaseTransactions;
	protected $admin;
	protected $category;
	protected $league;
	protected $now;
	protected $tipoApuestas;
	protected $teams;

	protected function setUp()
	{
		parent::setUp();

		$this->admin = factory(User::class)->create([
            'role' => 'admin',
        ]);

		
		$this->category = factory(Menu::class)->create();

		$this->league = factory(Submenu::class)->create([
			'id_menu' => $this->category->id,
		]);

		$this->tipoApuesta = factory(TipoApuesta::class)->create([
			'nombre' => 'Ganador'
		]);

		$this->teams = factory(Equipo::class, 2)->create();

		$this->now = date_format(Carbon::now(),"Y-m-d H:i:s");
	}

	function test_if_game_list_is_paginated()
	{
	    	$status = factory(\App\Stsgame::class)->create([
	    		'id' => 0,
	    		'nombre' => 'pendiente'
	    	]);		

	    	$games =  factory(\App\Game::class, 16)->create([
	    		'status' => $status->id,
	    		'categoria' => $this->category->id,
	    	]);


	    	foreach ($games as $game) {
		     	factory(\App\GamesProb::class)->times(2)->create([
		    		'idgame' => $game->id,
		    		'tipo_probabilidad' => 1
		    	]);	
	    	}

	    	$this->actingAs($this->admin);

	    	$this->get( 'admin/game', ['HTTP_X-Requested-With' => 'XMLHttpRequest'])
	    		->assertSuccessful()
	    		->assertJsonFragment([			
			        'id' => $games[1]->id,
			        'categoria' => $games[1]->categoria,
			        'subcategoria' => $games[1]->subcategoria,
			        'compuesta' => $games[1]->compuesta,
			        'fecha_registro' => $games[1]->fecha_registro,
			        'fecha_cierre' => $games[1]->fecha_cierre,
			        'status' => $games[1]->status,
			        'visible' => $games[1]->visible,
           		 ]);
	}	

	function test_if_game_is_stored()
	{
		$this->actingAs($this->admin);

		$this->post('admin/game', [
			'categoria' => $this->category->id,
			'subcategoria' => $this->league->id,
			'compuesta' => 1,
			'fecha_registro' => $this->now,
			'fecha_cierre' => $this->now,
			'status' => 0,
			'visible' => 1,
			'probs' => [
				'0' =>[
					'fecha_registro' => $this->now,
					'tipo_probabilidad' => $this->tipoApuesta,
					'equipo' => $this->teams[0], 
					'orden' => '1',
					'cantidad' => '0',	
					'probabilidad' => '1.9',       
					'activa' => '1',
					'acierto' => '1',				
				],
				'1' =>[
					'fecha_registro' => $this->now,
					'tipo_probabilidad' => $this->tipoApuesta,
					'equipo' => $this->teams[1], 
					'orden' => '2',
					'cantidad' => '0',
					'probabilidad' => '1.9',       
					'activa' => '1',
					'acierto' => '1',				
				],				
			],
		])->assertSuccessful();		

		$this->assertDatabaseHas('games', [
			'categoria' => $this->category->id,
			'subcategoria' => $this->league->id,
			'compuesta' => 1,
			'fecha_registro' => $this->now,
			'fecha_cierre' => $this->now,
			'status' => 0,
			'visible' => 1,			
		]);

		$this->assertDatabaseHas('games_probs',[			
			'tipo_probabilidad' => $this->tipoApuesta->id,
			'equipo' => $this->teams[0]->id, 
			'orden' => '1',
			'cantidad' => '0',	
			'probabilidad' => '1.9',       
			'activa' => '1',
			'acierto' => '1',				
		]);

		$this->assertDatabaseHas('games_probs',[			
			'tipo_probabilidad' => $this->tipoApuesta->id,
			'equipo' => $this->teams[1]->id, 
			'orden' => '2',
			'cantidad' => '0',
			'probabilidad' => '1.9',       
			'activa' => '1',
			'acierto' => '1',			
		]);		

	}


	function test_if_game_can_be_updated()
	{
			$this->actingAs($this->admin);
		 
	    	$status = factory(\App\Stsgame::class)->create([
	    		'id' => 0,
	    		'nombre' => 'pendiente'
	    	]);		

	    	$game =  factory(\App\Game::class)->create([
	    		'status' => $status->id,
	    		'categoria' => $this->category->id,
	    	]);

	     	factory(\App\GamesProb::class)->times(2)->create([
	    		'idgame' => $game->id,
	    		'tipo_probabilidad' => 1,
	    		'activa' => 1,
	    	]);	

	    	$new_category = factory(\App\Menu::class)->create();
	    	
	    	$new_league = factory(\App\Submenu::class)->create();

			$new_teams = factory(\App\Equipo::class, 2)->create();	    	

	    	$this->put('admin/game/' . $game->id, [
			'categoria' => $new_category->id,
			'subcategoria' => $new_league->id,
			'compuesta' => 1,
			'fecha_registro' => $this->now,
			'fecha_cierre' => $this->now,
			'status' => 1,
			'visible' => 0,
			'probs' => [
				'0' =>[
					'tipo_probabilidad' => $this->tipoApuesta,
					'equipo' => $new_teams[0], 
					'orden' => '1',
					'cantidad' => '0',	
					'probabilidad' => '1.9',       
					'activa' => '1',
					'acierto' => '1',				
				],
				'1' =>[
					'tipo_probabilidad' => $this->tipoApuesta,
					'equipo' => $new_teams[1], 
					'orden' => '2',
					'cantidad' => '0',
					'probabilidad' => '1.9',       
					'activa' => '1',
					'acierto' => '1',				
				],				
			],
		])->assertSuccessful();	

		$this->assertDatabaseMissing('games', [
			'categoria' => $this->category->id,
			'subcategoria' => $this->league->id,		
		]);

		$this->assertDatabaseHas('games_probs',[			
			'idgame' => $game->id,
			'activa' => '0',

		]);

		$this->assertDatabaseHas('games_probs',[			
			'idgame' => $game->id,
			'activa' => '0',

		]);		

		$this->assertDatabaseHas('games', [
			'categoria' => $new_category->id,
			'subcategoria' => $new_league->id,		
		]);

		$this->assertDatabaseHas('games_probs',[			
			'equipo' => $new_teams[0]->id, 			
		]);

		$this->assertDatabaseHas('games_probs',[			
			'equipo' => $new_teams[1]->id, 
			
		]);		
	}

	function test_if_game_is_showed()
	{
	    	$status = factory(\App\Stsgame::class)->create([
	    		'id' => 0,
	    		'nombre' => 'pendiente'
	    	]);		

	    	$games =  factory(\App\Game::class, 2)->create([
	    		'status' => $status->id,
	    		'categoria' => $this->category->id,
	    	]);


	    	foreach ($games as $game) {
		     	factory(\App\GamesProb::class, 2)->create([
		    		'idgame' => $game->id,
		    		'tipo_probabilidad' => 1
		    	]);	
	    	}

	    	$this->actingAs($this->admin);

	    	$this->get( 'admin/game/' . $games[0]->id, ['HTTP_X-Requested-With' => 'XMLHttpRequest'])
	    		->assertSuccessful()
	    		->assertJsonFragment([			
		             'id' => $games[0]->id,
           		 ])
	    		->assertJsonMissing([
		              'id' => $games[1]->id,
           		 ]);
	}	
}
