<?php

namespace Tests\Feature;

use App\ApuestasCategoria;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ApiProbsTypeTest extends TestCase
{
	use DatabaseTransactions;
	
	public function test_type_bet_api_list()
	{
  	//Having
  	$admin = factory(\App\User::class)->create([
  		'role' => 'admin'
  	]);

  	$this->actingAs($admin);

		$probsType = factory(\App\TipoApuesta::class)->times(15)->create();


		//When
		$this->get('/admin/typebet', ['HTTP_X-Requested-With' => 'XMLHttpRequest'])
		    		->assertSuccessful()
		    		->assertJsonFragment([			
	             'nombre' => $probsType[0]->nombre,
	       		 ]);
	} 

		public function test_type_bet_api_list_by_category()
	{
  	//Having
  	$admin = factory(\App\User::class)->create([
  		'role' => 'admin'
  	]);

  	$this->actingAs($admin);

		$probsType = factory(\App\TipoApuesta::class)->times(15)->create();


		ApuestasCategoria::create([
			'tipo_apuesta' => $probsType[0]->id,
			'categoria' => 1
		]);


		ApuestasCategoria::create([
			'tipo_apuesta' => $probsType[6]->id,
			'categoria' => 1
		]);


		ApuestasCategoria::create([
			'tipo_apuesta' => $probsType[10]->id,
			'categoria' => 1
		]);

		
		//When
		$this->get('/admin/categoryTypeBet/1', ['HTTP_X-Requested-With' => 'XMLHttpRequest'])
		    		->assertSuccessful();
	}     
}
