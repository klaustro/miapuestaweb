<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;


class ApiTeamsTest extends TestCase
{

	use DatabaseTransactions;

    public function test_api_teams_list()
    {
        //Having
        $admin = factory(\App\User::class)->create([
            'role' => 'admin'
        ]);

        $this->actingAs($admin);

        $submenu = factory(\App\Submenu::class)->create([
        	'id' => 1,
        ]);

        $team = factory(\App\Equipo::class)->create();

        $rel = \App\EquipoSubcategoria::create([
        	'id_equipo' => $team->id,
					'id_submenu' => $submenu->id
        ]);




        //When
        $this->get('/admin/team?subcategory=1' , ['HTTP_X-Requested-With' => 'XMLHttpRequest'])
        	->assertSuccessful();

    }
}
