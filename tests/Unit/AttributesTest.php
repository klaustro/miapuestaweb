<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\IntegrationTestCase;

class AttributesTest extends IntegrationTestCase
{
    use DatabaseTransactions;

    public function test_leagues_team_attribute()
    {
    	$submenu1 = factory(\App\Submenu::class)->create();
    	$submenu2 = factory(\App\Submenu::class)->create();

	$team = factory(\App\Equipo::class)->create([
        	'nombre' => 'Inter de Milan'
        ]);

	$team->save();

        $team->equipoSubcategoria()->create([
        	'id_submenu' => $submenu1->id
        ]);

        $team->equipoSubcategoria()->create([
        	'id_submenu' => $submenu2->id
        ]);
        $this->assertSame($team->leagues, [$submenu1->id, $submenu2->id]);
    }

    function test_male_attribute()
    {
        $status = factory(\App\Stsgame::class)->create([
            'id' => 0,
            'nombre' => 'pendiente'
        ]);

        $category = factory(\App\Menu::class)->create();

        $game =  factory(\App\Game::class)->create([
            'status' => $status->id,
            'categoria' => $category->id,
        ]);

        $female = factory(\App\GamesProb::class)->create([
            'idgame' => $game->id,
            'tipo_probabilidad' => 1,
            'probabilidad' => 2,
        ]);

        $male = factory(\App\GamesProb::class)->create([
            'idgame' => $game->id,
            'tipo_probabilidad' => 1,
            'probabilidad' => 1,
        ]);

        $this->assertTrue($male->id == $game->male->id);

    }

    function test_female_attribute()
    {
        $status = factory(\App\Stsgame::class)->create([
            'id' => 0,
            'nombre' => 'pendiente'
        ]);

        $category = factory(\App\Menu::class)->create();

        $game =  factory(\App\Game::class)->create([
            'status' => $status->id,
            'categoria' => $category->id,
        ]);

        $female = factory(\App\GamesProb::class)->create([
            'idgame' => $game->id,
            'tipo_probabilidad' => 1,
            'probabilidad' => 2,
        ]);

        $male = factory(\App\GamesProb::class)->create([
            'idgame' => $game->id,
            'tipo_probabilidad' => 1,
            'probabilidad' => 1,
        ]);

        $this->assertTrue($female->id == $game->female->id);

    }

    function test_event_game_attribute()
    {
        $status = factory(\App\Stsgame::class)->create([
            'id' => 0,
            'nombre' => 'pendiente'
        ]);

        $category = factory(\App\Menu::class)->create();

        $subcategory = factory(\App\Submenu::class)->create([
            'id_menu' => $category->id
        ]);

        $game =  factory(\App\Game::class)->create([
            'status' => $status->id,
            'categoria' => $category->id,
            'subcategoria' => $subcategory->id,
            'fecha_cierre' => '2017-01-01 00:00:00'
        ]);

        $animals = factory(\App\GamesProb::class, 36)->create([
            'idgame' => $game->id,
            'tipo_probabilidad' => 1,
            'probabilidad' => 0.3,
        ]);
//      dd($game->event);
        $this->assertTrue(substr($subcategory->nombre, 0, 3) . ': 12' == $game->event);

    }

    function test_who_win_attribute()
    {
        $status = factory(\App\Stsgame::class)->create([
            'id' => 0,
            'nombre' => 'pendiente'
        ]);

        $category = factory(\App\Menu::class)->create();

        $game =  factory(\App\Game::class)->create([
            'status' => $status->id,
            'categoria' => $category->id,
        ]);

        $winner = factory(\App\GamesProb::class)->create([
            'idgame' => $game->id,
            'tipo_probabilidad' => 1,
            'probabilidad' => 1,
        ]);

        $loser = factory(\App\GamesProb::class)->create([
            'idgame' => $game->id,
            'tipo_probabilidad' => 1,
            'probabilidad' => 2,
        ]);

        \App\Resultado::create([
            'tipo' => 1,
            'idgame' => $game->id,
            'equipo' => $winner->id,
            'score' => 1
        ]);

        \App\Resultado::create([
            'tipo' => 1,
            'idgame' => $game->id,
            'equipo' => $loser->id,
            'score' => 0
        ]);

        $this->assertTrue($winner->id == $game->who_win);
    }

    function test_who_win_half_attribute()
    {
        $status = factory(\App\Stsgame::class)->create([
            'id' => 0,
            'nombre' => 'pendiente'
        ]);

        $category = factory(\App\Menu::class)->create();

        $game =  factory(\App\Game::class)->create([
            'status' => $status->id,
            'categoria' => $category->id,
        ]);

        $winner = factory(\App\GamesProb::class)->create([
            'idgame' => $game->id,
            'tipo_probabilidad' => 1,
            'probabilidad' => 1,
        ]);

        $loser = factory(\App\GamesProb::class)->create([
            'idgame' => $game->id,
            'tipo_probabilidad' => 1,
            'probabilidad' => 2,
        ]);

        \App\Resultado::create([
            'tipo' => 3,
            'idgame' => $game->id,
            'equipo' => $winner->id,
            'score' => 1
        ]);

        \App\Resultado::create([
            'tipo' => 3,
            'idgame' => $game->id,
            'equipo' => $loser->id,
            'score' => 0
        ]);

        $this->assertTrue($winner->id == $game->who_win_half);
    }

    function test_total_score_attribute()
    {
        $status = factory(\App\Stsgame::class)->create([
            'id' => 0,
            'nombre' => 'pendiente'
        ]);

        $category = factory(\App\Menu::class)->create();

        $game =  factory(\App\Game::class)->create([
            'status' => $status->id,
            'categoria' => $category->id,
        ]);

        $winner = factory(\App\GamesProb::class)->create([
            'idgame' => $game->id,
            'tipo_probabilidad' => 1,
            'probabilidad' => 1,
        ]);

        $loser = factory(\App\GamesProb::class)->create([
            'idgame' => $game->id,
            'tipo_probabilidad' => 1,
            'probabilidad' => 2,
        ]);

        \App\Resultado::create([
            'tipo' => 1,
            'idgame' => $game->id,
            'equipo' => $winner->id,
            'score' => 3
        ]);

        \App\Resultado::create([
            'tipo' => 1,
            'idgame' => $game->id,
            'equipo' => $loser->id,
            'score' => 2
        ]);

        $this->assertTrue($game->total_score == 5);
    }

    function test_half_score_attribute()
    {
        $status = factory(\App\Stsgame::class)->create([
            'id' => 0,
            'nombre' => 'pendiente'
        ]);

        $category = factory(\App\Menu::class)->create();

        $game =  factory(\App\Game::class)->create([
            'status' => $status->id,
            'categoria' => $category->id,
        ]);

        $winner = factory(\App\GamesProb::class)->create([
            'idgame' => $game->id,
            'tipo_probabilidad' => 11,
            'probabilidad' => 1,
        ]);

        $loser = factory(\App\GamesProb::class)->create([
            'idgame' => $game->id,
            'tipo_probabilidad' => 11,
            'probabilidad' => 2,
        ]);

        \App\Resultado::create([
            'tipo' => 3,
            'idgame' => $game->id,
            'equipo' => $winner->id,
            'score' => 2
        ]);

        \App\Resultado::create([
            'tipo' => 3,
            'idgame' => $game->id,
            'equipo' => $loser->id,
            'score' => 3
        ]);

        $this->assertTrue($game->half_score == 5);
    }

    function test_diff_score_attribute()
    {
        $status = factory(\App\Stsgame::class)->create([
            'id' => 0,
            'nombre' => 'pendiente'
        ]);

        $category = factory(\App\Menu::class)->create();

        $game =  factory(\App\Game::class)->create([
            'status' => $status->id,
            'categoria' => $category->id,
        ]);

        $winner = factory(\App\GamesProb::class)->create([
            'idgame' => $game->id,
            'tipo_probabilidad' => 11,
            'probabilidad' => 1,
        ]);

        $loser = factory(\App\GamesProb::class)->create([
            'idgame' => $game->id,
            'tipo_probabilidad' => 11,
            'probabilidad' => 2,
        ]);

        \App\Resultado::create([
            'tipo' => 1,
            'idgame' => $game->id,
            'equipo' => $winner->id,
            'score' => 2
        ]);

        \App\Resultado::create([
            'tipo' => 1,
            'idgame' => $game->id,
            'equipo' => $loser->id,
            'score' => 3
        ]);

        $this->assertTrue($game->difference == 1);
    }

    /** @test */
    function amount_gamed_by_team_in_a_game()
    {
        $user = factory(\App\User::class)->create();

        $this->actingAs($user);

        $status = factory(\App\Stsgame::class)->create([
            'id' => 0,
            'nombre' => 'pendiente'
        ]);

        $category = factory(\App\Menu::class)->create();

        $subcategory = factory(\App\Submenu::class)->create([
            'id_menu' => $category->id
        ]);

        $game =  factory(\App\Game::class)->create([
            'status' => $status->id,
            'categoria' => $category->id,
            'subcategoria' => $subcategory->id,
            'fecha_cierre' => '2017-01-01 00:00:00'
        ]);

        $animals = factory(\App\GamesProb::class, 36)->create([
            'idgame' => $game->id,
            'tipo_probabilidad' => 1,
            'probabilidad' => 0.3,
        ]);

        $bet1 = factory(\App\Bet::class)->create([
            'usuario' => $user->id,
            'monto' => 1000
        ]);

        $betsDetailil1 = factory(\App\BetsDetail::class)->create([
            'idgames_probs' => $animals[5]->id,
            'idbets' => $bet1->id,
        ]);

        $bet2 = factory(\App\Bet::class)->create([
            'usuario' => $user->id,
            'monto' => 2000
        ]);

        $betsDetailil2 = factory(\App\BetsDetail::class)->create([
            'idgames_probs' => $animals[5]->id,
            'idbets' => $bet2,
        ]);

        $bet3 = factory(\App\Bet::class)->create([
            'usuario' => $user->id,
            'monto' => 3000
        ]);

        $betsDetailil3 = factory(\App\BetsDetail::class)->create([
            'idgames_probs' => $animals[6]->id,
            'idbets' => $bet3,
        ]);

        $this->assertTrue($user->team_acum($animals[5]->id) == 3000);


        $this->assertTrue($user->team_acum($animals[6]->id) == 3000);
    }

    /** @test */
    function bet_amount()
    {
        $user = factory(\App\User::class)->create();

        $this->actingAs($user);

        $status = factory(\App\Stsgame::class)->create([
            'id' => 0,
            'nombre' => 'pendiente'
        ]);

        $category = factory(\App\Menu::class)->create();

        $subcategory = factory(\App\Submenu::class)->create([
            'id_menu' => $category->id
        ]);

        $game =  factory(\App\Game::class)->create([
            'status' => $status->id,
            'categoria' => $category->id,
            'subcategoria' => $subcategory->id,
            'fecha_cierre' => '2017-01-01 00:00:00'
        ]);

        $probs = factory(\App\GamesProb::class, 2)->create([
            'idgame' => $game->id,
            'tipo_probabilidad' => 1,
            'probabilidad' => 2.5,
        ]);

        $bet1 = factory(\App\Bet::class)->create([
            'usuario' => $user->id,
            'monto' => 1000
        ]);

        $betsDetailil1 = factory(\App\BetsDetail::class)->create([
            'idgames_probs' => $probs[1]->id,
            'idbets' => $bet1->id,
        ]);

        $betsDetailil2 = factory(\App\BetsDetail::class)->create([
            'idgames_probs' => $probs[1]->id,
            'idbets' => $bet1->id,
        ]);

        //dd($probs);

        $this->assertTrue($bet1->ganancia == 6250);
    }
}
