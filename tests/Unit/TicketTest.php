<?php

namespace Tests\Unit;

use App\Bet;
use App\BetsDetail;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TicketTest extends TestCase
{
    use DatabaseTransactions; 

    public function test_ticket_event()
    {
        $user = factory(User::class)->create();

        $status = factory(\App\Stsgame::class)->create([
            'id' => 0,
            'nombre' => 'pendiente'
        ]);   

        $category = factory(\App\Menu::class)->create();

        $subcategory = factory(\App\Submenu::class)->create([
            'id_menu' => $category->id
        ]);

        $game =  factory(\App\Game::class)->create([
            'status' => $status->id,
            'categoria' => $category->id,
            'subcategoria' => $subcategory->id,
            'fecha_cierre' => '2017-01-01 00:00:00'
        ]);

        $animals = factory(\App\GamesProb::class, 36)->create([
            'idgame' => $game->id,
            'tipo_probabilidad' => 1,
            'probabilidad' => 0.3,
        ]);  

        $bet = factory(BetsDetail::class)->create([
            'idgames_probs' => $animals[5]->id,
        ]); 

        $bets = Bet::with('bets_detail')
            ->with('bets_detail.games_probs.equipo')
            ->with('bets_detail.games_probs.tipoApuesta')
            ->with('bets_detail.games_probs.game.equipo')
            ->with('bets_detail.games_probs.games_probs_signo')
            ->first();      

        $this->actingAs($user);

        $this->get('bet/' . $bet->idbets)
            ->assertSuccessful();
    }
}
