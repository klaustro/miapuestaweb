<?php

namespace Tests\Unit;

use App\Equipo;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\IntegrationTestCase;

class ScopesTeamTest extends IntegrationTestCase
{
    use DatabaseTransactions;
    public function test_scope_name_works()
    {
    	//Having
        factory(\App\Equipo::class)->create([
        	'nombre' => 'Magallanes BBC',
        ]);

        factory(\App\Equipo::class)->create([
        	'nombre' => 'Magallanes FC',
        ]);

        //When
        $teamSearched = Equipo::name('maga')->get();

        //Then
        $this->assertSame($teamSearched[0]->nombre, 'Magallanes BBC');
        $this->assertSame($teamSearched[1]->nombre, 'Magallanes FC');
    }
}
