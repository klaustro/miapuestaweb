<?php

namespace Tests;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\BrowserKitTesting\TestCase as BaseTestCase;
use Tests\CreatesApplication;

class IntegrationTestCase extends BaseTestCase
{
	use CreatesApplication, DatabaseTransactions;	
}
