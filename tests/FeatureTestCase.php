<?php

namespace Tests;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Laravel\BrowserKitTesting\TestCase as BaseTestCase;
use Tests\CreatesApplication;

class FeatureTestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseTransactions;
}
