<?php 
return[
'main' =>[
	'bank' => [
		'title' => 'Bancos', 
		'route' => 'admin.bank.index'
		],
	'enterprise' => [
		'title' => 'Empresas', 
		'route' => 'admin.enterprise.index'		
		],
	'comisionType' => [
		'title' => 'Tipo de comisión', 
		'route' => 'admin.type.comision.index'		
		],		
	]
];