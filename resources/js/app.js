
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
 require('./bootstrap');

window.Vue = require('vue');

import vSelect from 'vue-select'
import flatPickr from 'vue-flatpickr-component';
import 'flatpickr/dist/flatpickr.css';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('gameIndex', require('./components/admin/game/Index.vue'));
Vue.component('gameShow', require('./components/admin/game/Show.vue'));
Vue.component('buttonGame', require('./components/admin/game/ButtonGame.vue'));

Vue.component('paginate', require('./components/helpers/Paginate.vue'));
Vue.component('DatePicker', require('./components/helpers/Datepicker.vue'));
Vue.component('search', require('./components/helpers/Search.vue'));
Vue.component('v-select', vSelect);
Vue.component('flatPickr', flatPickr);

//Filters

const app = new Vue({
    el: '#app'
});

