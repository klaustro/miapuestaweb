<div id="modal-confirmation" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="title-confirmation"></h4>
    </div>
    <div class="modal-body">
        <p id="message-confirmation"></p>
    </div>
    <div class="modal-footer">
        <button class="btn" onclick="borrar()">Si</button>
        <button class="btn" data-dismiss="modal">No</button>
    </div>
</div>