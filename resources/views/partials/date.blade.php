<div class="form-group">
    <label class="control-label" for="fecha_hora">{!! $title !!}</label>
	<div class='input-group date input-pago' id='{!! $selector !!}'>
		<input type='text' class="form-control common-date-picker"  id="{!! $id !!}" name="{!! $id !!}" readonly="true" required="" value="{!!  isset($_GET[$id]) ? $_GET[$id] : ''  !!}"/>
		<span class="input-group-addon">
			<span class="fa fa-calendar">
			</span>
		</span>
	</div>
</div>