<div id="bet-forbidden" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="forbiden-label"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> Apuesta Prohibida</h4>
    </div>
    <div class="modal-body">
        <p id="forbidden-message"></p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal">Close</button>
    </div>
</div>