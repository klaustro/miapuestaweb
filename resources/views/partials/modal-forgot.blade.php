{{-- Modal de Reset Password --}}
<script src="assets/js/forgot.js" type="text/javascript"></script>
<div class="modal fade" tabindex="-1" role="dialog" id="forgotModal" hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-user"></i> Recuperar contraseña</h4>
      </div>
        <form class="form-horizontal" id="formForgot" role="form" method="POST">
          <div class="modal-body">
              <div class="form-group" id="email-div">
              {{ csrf_field() }}
                <label for="email">Correo Electrónico</label>
                <input type="email" class="form-control" name="email1" id="email1" placeholder="Correo Electrónico">
              {{-- <div id="form-errors-email" class="has-error"></div> --}}
              <span class="help-block">
                  <strong id="form-errors-email"></strong>
              </span>
              </div>
              <div class="form-group" id="login-errors">
                <span class="help-block">
                  <strong id="loginerrors"></strong>
                </span>
             </div>
          </div>
          <div class="modal-footer">
            <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button> -->
            <button type="button" onclick="forgot();" class="btn btn-login btn-primary right">Recuperar contraseña</button>
          </div>
      </form>
   </div>
  </div>
</div>