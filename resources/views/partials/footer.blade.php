<div id="footerOuterSeparator"></div>

        <div id="divFooter" class="footerArea">

        <div class="divPanel">

            <div class="row-fluid">
                <div class="span12" id="footerArea1">
                    <p>
                        <a href="/rule" title="Reglas" id="reglas">Reglas</a>
                        <a> | </a>
                        <a href="/who" title="Nosotros" id="quienes">Nosotros</a>
                        <a> | </a>
                        <a href="/contact" title="Contacto">Contacto</a>
                        <a> | </a>
                        <a href="/help" title="Ayuda">Ayuda</a>                        
                    </p>
                </div>
            </div>

            <div class="row-fluid">
                <div class="span12">
                    <p class="copyright">
                        Copyright © 2016 Miapuestaweb. Todos los derechos reservados.
                    </p>

                    <p class="social_bookmarks">
                        <a href="#"><i class="social foundicon-facebook"></i> Facebook</a>
                        <a href=""><i class="social foundicon-twitter"></i> Twitter</a>

                    </p>
                </div>
            </div>

        </div>
    </div>
</div>       
