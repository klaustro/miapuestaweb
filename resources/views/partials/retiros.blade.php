{{-- Modal de Retiros --}}
<div class="modal fade" tabindex="-1" role="dialog" id="retiroModal" hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-download" aria-hidden="true"></i> Solicitar Retiro</h4>
        </div>
        <div class="modal-body">
            <div class="form-group" id="retiro-cuenta-group">
                <label class="control-label" for="retiro-cuenta">Cuenta</label>
                    <select class="form-control input-retiro" id="retiro-cuenta">
                    </select>
                <span class="help-block"><strong id="retiro-errors-cuenta"></strong></span>
            </div>
            <div class="form-group group-monto" id="retiro-monto">
                {{ csrf_field() }}
                <label class="control-label" for="retiro-monto">Monto</label>
                <input class="form-control input-number" id="retiro" required="" title="Por favor introduce el monto" type="text">

                <span class="help-block"><strong id="retiro-errors-monto"></strong></span>

            </div>
            <div class="form-group" id="login-errors">
                <span class="help-block"><strong id="form-login-errors"></strong></span>
            </div>

        </div>
        <div class="modal-footer">
            <button class="btn btn-login btn-primary right" id="btn-retiro" disabled="true">Registrar retiro</button>
        </div>
   </div>
  </div>
</div>
@include('partials.modal-message')
