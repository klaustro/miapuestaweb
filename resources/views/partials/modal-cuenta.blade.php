{{-- Modal de Cuentas --}}
<div class="modal fade" tabindex="-1" role="dialog" id="cuentasModal" hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-bank" aria-hidden="true"></i> <text class="title"></text></h4>
        </div>
        <div class="modal-body">
            <div class="form-group" id="group-banco-cuenta"> 
            {{ csrf_field() }}
                <label class="control-label" for="banco-cuenta">Banco</label>
                    <select class="form-control input-cuenta banco" id="banco-cuenta">
                    </select>
                <span class="help-block"><strong id="cuenta-errors-banco"></strong></span>
            </div>   
            <div class="form-group" id="group-tipo-cuenta">
                <label class="control-label" for="tipo-cuenta">Tipo de cuenta</label>
                    <select class="form-control input-cuenta tipo" id="tipo-cuenta">
                        <option value='0'>Seleccione el tipo de cuenta</option>
                        <option value='Ahorro'>Ahorro</option>
                        <option value='Corriente'>Corriente</option>
                        <option value='FAL'>FAL</option>
                    </select>
                <span class="help-block"><strong id="cuenta-errors-tipo"></strong></span>
            </div>                    
            <div class="form-group" id="cuenta-numero">               
                <label class="control-label" for="nrocuenta">Número de Cuenta</label>
                <input class="form-control input-number" id="nrocuenta" name="nrocuenta" required="" placeholder="Por favor introduce el Número de Cuenta" title="Por favor introduce el Número de Cuenta" type="text" maxlength="20">
                <span class="help-block"><strong id="cuenta-errors-monto"></strong></span>
            </div>
            <div class="form-group" id="login-errors">
                <span class="help-block"><strong id="form-login-errors"></strong></span>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-login btn-primary right title" onclick="register()" disabled="true" id="btn-cuenta"></button>
        </div>
   </div>
  </div>
</div>
@include('partials.modal-message')
