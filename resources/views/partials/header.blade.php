{{-- Modal de registro --}}
<div class="modal fade" tabindex="-1" role="dialog" id="registerModal" hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-keyboard-o"></i> Registro</h4>
        </div>
        <form action="{{url('/register')}}" id="registerForm" method="post" name="registerForm">
            <div class="modal-body">
                <div class="form-group" id="register-name">
                    <label class="control-label" for="name">Nombre</label> <input class="form-control" id="name" name="name"
                    placeholder="Nombre" required="" title="Por favor introduce tu nombre" type="text"> <span class=
                    "help-block"><strong id="register-errors-name"></strong></span>
                </div>
                <div class="form-group" id="register-email">
                    {{ csrf_field() }} <label class="control-label" for="email">Email</label> <input class="form-control" id="email" name="email" placeholder="ejemplo@gmail.com" required="" title="Por favor introduce tu email" type="email"
                    value=""> <span class="help-block"><strong id="register-errors-email"></strong></span>
                </div>
                <div class="form-group" id="register-password">
                    <label class="control-label" for="password">Contraseña</label> <input class="form-control" id="password" name=
                    "password" placeholder="******" required="" title="Please enter your password" type="password" value="">
                    <span class="help-block"><strong id="register-errors-password"></strong></span>
                </div>
                <div class="form-group">
                    <label class="control-label" for="password-confirm">Confirma Contraseña</label> <input class="form-control" id=
                    "password-confirm" name="password_confirmation" placeholder="******" type="password"> <span class=
                    "help-block"><strong id="form-errors-password-confirm"></strong></span>
                </div>
                <div class="form-group" id="register-identification">
                    <label class="control-label" for="identification">Cedula de Identidad</label> <input class="form-control" id="identification" name="identification"
                    placeholder="Cedula de Identidad" required="" title="Por favor introduce tu Cedula de Identidad" type="text"> <span class=
                    "help-block"><strong id="register-errors-identification"></strong></span>
                </div>

                @include('partials.date', ['id' => 'birth_date', 'selector' => 'group-fecha', 'title' => 'Fecha de Nacimiento'])                

                <div class="form-group" id="register-phone">
                    <label class="control-label" for="phone" style="margin-top: 10px;">Teléfono</label>
                    <input class="form-control input-number" id="phone" name="phone"
                    placeholder="Teléfono" required="" title="Por favor introduce tu teléfono" type="text"> <span class=
                    "help-block"><strong id="register-errors-phone"></strong></span>
                </div>
                <div class="form-group" id="login-errors">
                    <span class="help-block"><strong id="form-login-errors"></strong></span>
                </div>
                <div class="g-recaptcha" data-sitekey="6LflaxUUAAAAAFAmdEA2FqFnAhergfgMCaC0T2Oj" data-callback="enableBtn"></div>
                <div class="modal-footer">
                    <button class="btn btn-login btn-primary right" id="btn-register">Registrar</button>
                </div>
            </div>
        </form>
   </div>
  </div>
</div>

{{-- Modal de Inicio de sesión --}}
<div class="modal fade" tabindex="-1" role="dialog" id="loginModal" hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-user"></i> Inicio de Sesión</h4>
      </div>
      <form action="{{url('/login')}}" method="POST" id="loginForm" novalidate>
          <div class="modal-body">
              <div class="form-group" id="email-div">
              {{ csrf_field() }}
                <label for="email">Correo Electrónico</label>
                <input type="email" class="form-control" name="email" id="login-email" placeholder="Correo Electrónico">
              {{-- <div id="form-errors-email" class="has-error"></div> --}}
              <span class="help-block">
                  <strong id="form-errors-email"></strong>
              </span>
              </div>
              <div class="form-group" id="password-div">
                  <label for="password">Contraseña</label>
                  <input type="password" class="form-control" name="password" id="login-password" placeholder="Contraseña">
                  <span class="help-block">
                      <strong id="form-errors-password"></strong>
                  </span>
              </div>
              <div class="form-group" id="login-errors">
                <span class="help-block">
                  <strong id="loginerrors"></strong>
                </span>
             </div>
             <a href="password/email">Olvidaste tu contraseña?</a>
          </div>
          <div class="modal-footer">
            <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button> -->
            <button class="btn btn-login btn-primary right">Iniciar Sesión</button>
          </div>
      </form>
   </div>
  </div>
</div>

