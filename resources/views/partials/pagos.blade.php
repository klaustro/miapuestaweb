{{-- Modal de Pagos --}}
<div class="modal fade" tabindex="-1" role="dialog" id="pagoModal" hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-upload" aria-hidden="true"></i> Registrar Pago</h4>
        </div>
        <div class="modal-body">

            @include('partials.date', ['id' => 'fecha_hora', 'selector' => 'pago-group-fecha', 'title' => 'Fecha de pago'])

            <div class="form-group group-monto" id="pago-monto">
                {{ csrf_field() }}
                <label class="control-label" for="monto">Monto</label>
                <input class="form-control input-number" id="monto" name="monto" required="" title="Por favor introduce el monto" type="text">
                <span class="help-block"><strong id="pago-errors-monto"></strong></span>
            </div>
            <div class="form-group" id="pago-tipo">
                <label class="control-label" for="tipo">Tipo de Pago</label>
                    <select class="form-control input-pago" id="tipo">
                      <option value="0">Seleccione el tipo</option>
                      <option value="1">Despósito</option>
                      <option value="2">Transferencia</option>
                    </select>
                <span class="help-block"><strong id="pago-errors-tipo"></strong></span>
            </div>
            <div class="form-group" id="pago-referencia">
                <label class="control-label" for="referencia">Referencia</label>
                <input class="form-control" id="referencia" name="referencia" placeholder="Referencia" required="" title="Por favor introduce tu Fecha de Nacimiento" type="text">
                <span class="help-block"><strong id="pago-errors-referencia"></strong></span>
            </div>
            <div class="form-group" id="pago-banco">
                <label class="control-label" for="banco">Banco</label>
                    <select class="form-control input-pago banco" id="banco">
                    </select>
                <span class="help-block"><strong id="pago-errors-banco"></strong></span>
            </div>
            <div class="form-group" id="pago-titular">
                <label class="control-label" for="titular">Nombre del titular</label>
                <input class="form-control" id="titular" name="titular" placeholder="Titular" required="" title="Por favor introduce el titular" type="text">
                <span class="help-block"><strong id="pago-errors-titular"></strong></span>
            </div>
            <div class="form-group" id="login-errors">
                <span class="help-block"><strong id="form-login-errors"></strong></span>
            </div>
            <div class="modal-footer">
                <button class="btn btn-login btn-primary right" id="btn-pago" disabled="true">Registrar pago</button>
            </div>
        </div>
   </div>
  </div>
</div>
@include('partials.modal-message')