<div id="bet-result" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Resultado de la Apuesta</h4>
    </div>
    <div class="modal-body">
        <p id="bet-message"></p>
    </div>
    <div class="modal-footer">
        <button class="btn" id="btn-print" onclick="print_ticket()">Imprimir</button>
        <button class="btn" data-dismiss="modal">Cerrar</button>
    </div>
</div>