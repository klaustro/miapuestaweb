<div id="modal-bets" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="modal-betsLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><i class="fa fa-futbol-o"></i> Detalle de Apuesta</h4>
        @if (Carbon\Carbon::now() > '2018-08-20')
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <strong>Alerta!</strong> Recuerde que su apuesta debe ser expresada en Bs Soberanos (Bs.S 1,00 = Bs.F 100.000,00).
        </div>
        @endif
    </div>
    <div class="modal-body">
      <p>
      <div class="pull-left">
        <label class="switch">
          <input type="checkbox" class="sw-tipo" id="sw-tipo-apuesta-modal">
          <div class="slider"></div>
        </label><text class="lbl-tipo-apuesta">  Directa</text>
      </div>
       <div class="pull-right">
            <button class="btn btn-primary font-neuro btn-play" id="btn-bet" onclick="make_bet()" data-toggle="popover"><i class="fa fa-caret-square-o-right" aria-hidden="true"></i> Jugar </button>
       </div>
       <div class="clearfix"></div>
      </p>
      <p>
        <div class="form-group">
        <div class="pull-left">
          <input type="text" placeholder="Monto" class="bets-monto input-number" id="bets-monto-modal" value="1"/>
        </div>
        <div class="pull-right">
          <div class="lbl-aGanar"><strong class="label-total lbl-aGanar">A Gananar: </strong><strong  class="bets-total lbl-aGanar" id="bets-total"></strong></div>
        </div>
        <div class="clearfix"></div>
          <span class="help-block"><strong id="bet-errors-monto"></strong></span>
        </div>
      </p>
      <div class="pre-scrollable">
            <p id="bets-detalle">
            </p>
      </div>
      </div>
      <div class="modal-footer">
        <div class="pull-left">
            <strong class="label-total" id="bets-arriesgado"></strong>
        </div>
      </div>

</div>