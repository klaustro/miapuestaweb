﻿@extends('layouts.app')
@section('content')

    <div class="panel panel-default panel-bets">
        <div class="panel-heading">
			<h4 class="text-center">Reglas de Apuestas</h4>			
			<a class="pull-right" href="/reglas-miapuestaweb.pdf" target="_blank" rel="noopener noreferrer">
				<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M11.363 2c4.155 0 2.637 6 2.637 6s6-1.65 6 2.457v11.543h-16v-20h7.363zm.826-2h-10.189v24h20v-14.386c0-2.391-6.648-9.614-9.811-9.614zm4.811 13h-2.628v3.686h.907v-1.472h1.49v-.732h-1.49v-.698h1.721v-.784zm-4.9 0h-1.599v3.686h1.599c.537 0 .961-.181 1.262-.535.555-.658.587-2.034-.062-2.692-.298-.3-.712-.459-1.2-.459zm-.692.783h.496c.473 0 .802.173.915.644.064.267.077.679-.021.948-.128.351-.381.528-.754.528h-.637v-2.12zm-2.74-.783h-1.668v3.686h.907v-1.277h.761c.619 0 1.064-.277 1.224-.763.095-.291.095-.597 0-.885-.16-.484-.606-.761-1.224-.761zm-.761.732h.546c.235 0 .467.028.576.228.067.123.067.366 0 .489-.109.199-.341.227-.576.227h-.546v-.944z"/></svg> Descargar
			</a>
			<div class='clearfix'></div>
        </div>
            <div class="panel-body" >
            <img src="images/referee.jpg" class="img-polaroid" style="margin:5px 0px 15px;" alt="">
            <p>1.   El reglamento referente a Apuestas deportivas es una traducción del reglamento de la versión en lengua inglesa, que será controlada a todos los efectos y para todos los derechos y obligaciones de todas las partes por dicha versión del reglamento.</p>
            <p>2.  La dirección se reserva el derecho a cambiar, modificar o añadir reglas a este reglamento, así como modificar procedimientos internos, si lo consideran oportuno sin más aviso que el envío del reglamento actual al sitio Web de Miapuestaweb.com.</p>
            <p>3.  El cliente se compromete a aceptar estas condiciones al abrir una cuenta o realizar apuestas en Miapuestaweb.com.</p>
            <p>4.  Cualquier tipo de información proporcionada por los agentes de Atención al Cliente es a modo de consejo y está sujeta al Reglamento de Miapuestaweb.com arriba mencionado. Aunque haremos todo lo posible por asegurar que la información facilitada al cliente sea correcta, es en última instancia responsabilidad del cliente asegurarse que entiendan a qué están apostando, así como los términos y condiciones.</p>

            <blockquote>
            <h3 class="text-error">Realización de Apuestas</h3>
            </blockquote>

            <p>1.   Una apuesta se considerará nula si no se ha recibido por completo.</p>
            <p>2.   Una vez que un cliente confirma una apuesta recibirá un acuse de recibo (que incluye el número identificador de la apuesta), que NO es una confirmación de la aceptación definitiva por parte de Miapuestaweb.com.</p>
            <p>3.   En caso de que un cliente envíe múltiples copias de una misma apuesta, ésta será tratada como una sola. La compañía se reserva el derecho de anular apuestas en caso que haya sospecha de coincidencia de dos o más usuarios.</p>
            <p>4.   En caso de recibir varias apuestas combinadas, que incluyan idénticas selecciones o muy similares, de diferentes clientes y en un corto espacio de tiempo, Miapuestaweb.com se reserva el derecho de anular todas estas apuestas, incluso después de que las apuestas hayan sido resueltas.</p>
            <p>5.   Miapuestaweb.com se reserva el derecho de rechazar cualquier apuesta o parte de una apuesta en cualquier momento sin previa explicación. Además, Miapuestaweb.com se reserva el derecho de cerrar la cuenta de un usuario y recuperar el saldo de esta cuenta sin dar explicación. En este caso se cumplir con las apuestas pendientes.</p>
            <p>6.   Miapuestaweb.com se reserva el derecho, en cualquier momento (incluso después de que una apuesta haya sido resuelta), de (i) rechazar cualquier apuesta o parte de una apuesta; o (ii) anular cualquier apuesta o parte de una apuesta siempre que Miapuestaweb.com concluya, a su única discreción, que existe una complicidad entre cuentas y/o clientes. En este caso, las apuestas restantes que no sean susceptibles de ser rechazadas o anuladas serán válidas. Asimismo, Miapuestaweb.com se reserva el derecho a adoptar las medidas legales necesarias contra aquellos clientes respecto a los cuales una autoridad reguladora considere que ha existido colusión.</p>
            <p>7.   En el caso de que la integridad de un evento deportivo y/o partido o enfrentamiento está en duda, Miapuestaweb.com se reserva el derecho a anular todas las apuestas relacionadas con este evento (incluidas las combinadas). Las cantidades apostadas en dichos eventos serán reembolsadas en las cuentas de los clientes.</p>
            <p>8.   Es condición para que Miapuestaweb.com acepte las apuestas de un Cliente, y para poder realizar una apuesta con Miapuestaweb.com, que el cliente confirme que:</p>
            <blockquote>Usted, el cliente, no tiene prohibida la participación en la apuesta por ningún término de su contrato de trabajo o por alguna norma del Organismo de Gobierno Deportivo del que dependa;</blockquote>
            <blockquote>Usted, el cliente, no está al tanto de ninguna circunstancia que pudiera significar que la realización de la apuesta infrinja una norma de apuesta impuesta por un Organismo de Gobierno Deportivo; y que </blockquote>
            <blockquote>Cuando la apuesta es realizada al resultado de una carrera, competición u otro evento o proceso, o a la posibilidad de algo ocurra o no ocurra, usted, el cliente, no sabe el resultado del evento.</blockquote>
            <p>En el caso de que dicha confirmación hecha por usted, el cliente, sea probada como falsa, perder su cantidad apostada y Miapuestaweb.com no tendrá la obligación de pagar ninguna ganancia, que de otro modo podría haber sido pagada, respecto a la apuesta.</p>
            <p>9.   La apuesta mínima que se puede realizar en línea es de 5 Bs.F.</p>
            <p>10.  Cuando un cliente da instrucciones poco claras Miapuestaweb.com se reserva el derecho a dividir el total apostado entre los resultados más importantes. En el caso que esto no se pudiera hacer, Miapuestaweb.com se reserva el derecho a anular toda la apuesta. En cualquiera de ambas situaciones, la decisión de la compañía es definitiva.</p>
            <p>11.  Los clientes no podrán cancelar o cambiar una apuesta una vez haya sido realizada y aceptada.</p>
            <p>12.  Las apuestas múltiples son aceptadas, sin embargo no se aceptarán apuestas de este tipo si el resultado de una parte repercute en el resultado de la otra.</p>
            <p>13.  Todas las cotizaciones están sujetas a variación, quedando fijas una vez se haya confirmado y aceptado.</p>
            <p>14.  Miapuestaweb.com se reserva el derecho de rechazar o anular cualquier apuesta.</p>
            <p>15.  Miapuestaweb.com se reserva el derecho de retirar de la cuenta de un cliente cualquier importe que considere, a su única discreción, que ha sido obtenido como resultado de algún error, mediante alguna actividad fraudulenta o cualquier otra actividad de cualquier naturaleza que Miapuestaweb.com considere ha sido realizada de mala fe.</p>
            <p>16.  Las apuestas serán aceptadas hasta la hora anunciada. Si una apuesta queda aceptada inadvertidamente cuando un evento ya ha dado comienzo, Miapuestaweb.com se reserva el derecho de anular todas las referidas a dicho evento.</p>
            <p>17.  Todas las cotizaciones hechas en mensajes de texto promocionales están correctas en el momento de ser enviados. Las cotizaciones pueden subir o bajar entre el momento de enviar el mensaje y el comienzo del evento. La única cotización que Miapuestaweb.com aceptaría será la cotización ofrecida al momento de colocar la apuesta.</p>

            <blockquote>
            <h3 class="text-error">Reglas sobre ingresos</h3>
            </blockquote>

            <p>1.   Ningún trabajador de Miapuestaweb.com podrá ofrecer crédito alguno a un cliente. De hecho, es responsabilidad del mismo saber cuánto dinero le queda en cuenta para poder apostar correctamente. Miapuestaweb.com se reserva el derecho a anular cualquier apuesta que haya podido ser aceptada inadvertidamente estando la cuenta del usuario con fondos insuficientes para cubrir la totalidad de una apuesta.
           <p>2.   En el supuesto de que se valide crédito en la cuenta de un usuario por error, es responsabilidad del usuario el informar a Miapuestaweb.com de dicho error. Cualquier beneficio obtenido después del error sin que el usuario haya informado a la empresa será anulado y devuelto a la empresa, tenga relación o no con el saldo proveniente del error.</p>
           <p>3.   Las ganancias serán ingresadas en las cuentas de usuario una vez se haya confirmado el resultado final del evento.</p>
           <p>4.   Miapuestaweb.com en ningún caso, y bajo ninguna circunstancia, se hace responsable de los datos o pérdidas sin limitación que se consideren o declaren procedentes a causa de este sitio Web o su contenido, incluyendo sin limitación, retrasos o interrupciones en la operación o transmisión, errores en las líneas de comunicación, uso incorrecto del sitio o su contenido por parte de los usuarios, o cualquier error u omisión en el contenido</p>
           <p>5.   Miapuestaweb.com se reserva el derecho de anular alguna o todas las apuestas realizadas por un grupo de gente en un intento de hacer fraude a la compañía. Esto incluye a personas, parientes, organizaciones, corredores de apuestas y sus empleados.</p>
           <p>6.   La dirección se reserva el derecho de cancelar una solicitud de cobro de ganancias si el usuario no ha invertido (jugado) la totalidad de los depósitos realizados, con tal de prevenir acciones fraudulentas.</p>
           <p>7.   Miapuestaweb.com realiza inspecciones sobre los movimientos de cuentas bancarias, en caso de presentarse situaciones fraudulentas.</p>

            <blockquote>
            <h3 class="text-error">En Caso de Disputa</h3>
            </blockquote>

            <p>1.   Miapuestaweb.com no se responsabiliza de ningún error del texto o humano en la oferta de cotizaciones y apuestas, a excepción de las deliberadas. Miapuestaweb.com se reserva el derecho de corregir el error o bien anular las apuestas afectadas, en cuyo caso se devolver la cantidad de las apuestas simples y se anularán los eventos implicados en las combinadas y múltiples.</p>

            <blockquote>
            <h3 class="text-error">Máxima Ganancia Diaria</h3>
            </blockquote>
            <p>1.   Las apuestas son a ganador.</p>
            <p>2.   El cierre de las apuestas se basa en la cotización y posición en el momento en que se acepta la apuesta. Miapuestaweb.com se reserva el derecho de cambiar los términos de posición de las apuestas a posición antes o durante un evento o campeonato.</p>
            <p>3.   En caso de empate, la cantidad apostada se divide por el número de corredores que han empatado. La cantidad apostada se multiplica por la cotización del corredor dividida por el número de corredores empatados.</p>
            <p>4.   Cuando una apuesta simple se anula el saldo invertido en la misma se devuelve a la cuenta de usuario. En aquellos eventos que no se disputan por cualquier razón, el saldo puede ser, o no, devuelto, según el reglamento específico de cada deporte. En apuestas compuestas o parlay, las apuestas continuarán sin contar con la selección anulada (una doble pasa a apuesta sencilla, una triple pasa a apuesta doble, etc.)</p>
            <p>5.   El resultado oficial es determinante para el cierre de las apuestas excepto cuando existen reglas específicas que indican lo contrario. Posteriores cambios en los resultados, por acciones disciplinarias u otros motivos no se tendrán en cuenta. Cuando, a juicio de Miapuestaweb.com, no hay resultados oficiales disponibles, el resultado será: determinado por una autoridad independiente, el veredicto de la cual será final para todos los propósitos, o por otro lado, determinado en la opinión razonable de Miapuestaweb.com. Esta opinión será formada en función de todas las pruebas razonables disponibles.</p>
            <p>6.   El cierre definitivo de una apuesta se producirá cuando Miapuestaweb.com considere que el resultado final y el consiguiente cierre se ha producido satisfactoriamente en función de todas las pruebas razonables disponibles.</p>

            <blockquote>
            <h3 class="text-error">Apuestas Relacionadas</h3>
            </blockquote>
            <p>1.   No se aceptan apuestas combinadas sobre un mismo evento en el que el resultado de una elección afecta o queda afectado por el resultado de otra elección. Eventualmente se pueden ofrecer apuestas especiales que combinen estas opciones. Si no se oferta dicha apuesta especial, y se acepta una apuesta de este tipo por error, la apuesta será tratada como dos apuestas simples y la cantidad apostada será dividida entre las dos apuestas.</p>
            <p>2.   Cuando las partes relacionadas de una apuesta se resuelven en diferentes momentos, incluso dentro del mismo torneo, la apuesta quedará establecida tal y como estaba introducida. Se utilizará la cotización del evento una vez finalizada la primera parte del evento.</p>

            <blockquote>
            <h3 class="text-error">Cambio de línea</h3>
            </blockquote>
            <p>1.   Todas las cotizaciones están sujetas a variaciones en cualquier momento y sin previo aviso.</p>

            <blockquote>
            <h3 class="text-error">Tiempo de Aceptación</h3>
            </blockquote>
            <p>1.   Todas las apuestas seleccionadas con los precios fijados por Miapuestaweb.com serán aceptadas hasta el momento antes de la hora de inicio fijada en la página. En el supuesto de que se haya aceptado dicha apuesta por error, Miapuestaweb.com se reserva el derecho a anular las apuestas implicadas o parte de ellas. Si se ofrecen cotizaciones en directo para el partido, la apuesta será aceptada a la cotización indicada.</p>

        
            <blockquote>
            <h3 class="text-error">Apuestas en directo</h3>
            </blockquote>

            <p>1.   Miapuestaweb.com intenta realizar una cotización justa de los eventos que transcurren durante un partido en directo basándose en la buena fe del comentarista de TV. Pueden presentarse situaciones en las que no es posible determinar con precisión lo sucedido. En caso de que se requiera un examen más preciso, la retransmisión del locutor de televisión se interrumpe o no puede comenzar a tiempo o se produce un fallo técnico en los medios, se reserva el derecho a comprobar posteriormente el cierre de la apuesta o a anularla. Si no hay ningún movimiento posterior en el evento una vez aceptada la apuesta en directo, estas apuestas serán nulas a no ser que la reglamentación específica para ese deporte indique lo contrario. Si resulta que el locutor de TV falla en uno o más eventos o se cierra una apuesta incorrectamente, Miapuestaweb.com se reserva el derecho a realizar ajustes retrospectivos positivos o negativos en las cuentas. Confiamos en las actuaciones de los oficiales que actúan durante el curso del juego, partido o torneo. Estas decisiones serán finales sin tener en cuenta si varían posteriormente por ser reclamadas por la otra parte.</p>            
            </div>
    </div>

@endsection