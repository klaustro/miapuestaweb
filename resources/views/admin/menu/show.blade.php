@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span12">

        <div id="headerSeparator"></div>

<div class="panel panel-default panel-bets">
	<div class="panel-heading">		
	  <h4 class="text-center">Actualizar menu <strong>#{{$menu->id}}</strong></h4>
	  <div class="clearfix"></div>
	</div>
	  <div class="panel-body">	

	  {!! Form::open(['route' => ['admin.menu.update', $menu], 'method' => 'PUT']) !!}
		<ul class="list-group col-md-8 col-md-offset-2">
			{!! Field::text('nombre', $menu->nombre) !!}
			{!! Field::text('orden', $menu->orden) !!}
			{!! Field::textarea('nota', $menu->nota) !!}
			<br>
			<input type="submit" class="btn btn-login btn-primary btn-submit" value="Actualizar">				
			<a class="btn btn-primary btn-cancel" href="/admin/menu" role="button">Cancelar</a>
		</ul>
		{!!  Form::close() !!}
  	</div>
  	<div class="clearfix"></div>
</div>
        <div id="headerSeparator2"></div>
    </div>
</div>
@endsection