@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span12">

        <div id="headerSeparator"></div>

<div class="panel panel-default panel-bets">
	<div class="panel-heading">		
	  <h4 class="text-center">Actualizar tipo de apuesta <strong>#{{$typebet->id}}</strong></h4>
	  <div class="clearfix"></div>
	</div>
	  <div class="panel-body">	

	  {!! Form::open(['route' => ['admin.typebet.update', $typebet], 'method' => 'PUT']) !!}
		<ul class="list-group col-md-8 col-md-offset-2">
			{!! Field::text('nombre', $typebet->nombre) !!}
			{!! Field::text('max_apuesta', $typebet->max_apuesta) !!}		
			{!! Field::select('exotica', ['0' => 'No','1' => 'Si'],  $typebet->exotica) !!}
			{!! Field::select('cantidad', ['0' => 'No','1' => 'Si'],  $typebet->cantidad) !!}
			{!! Field::text('probabilidades', $typebet->probabilidades) !!}
			{!! Field::select('pk', ['0' => 'No','1' => 'Si'],  $typebet->pk) !!}
    			{!! Form::label('menu', 'Categorias') !!}
			{!! Form::select('menu[]', $menus,  isset($typebet->categories) ? $typebet->categories : [], [
				'class' => 'form-control selectpicker', 
				'multiple', 'data-live-search' => 'true', 
				'title' => 'Seleccione las categorias asosciadas',
				'data-width' => '100%',
			]) !!}	
			<br>				
			<br>
			<input type="submit" class="btn btn-login btn-primary btn-submit" value="Actualizar">				
			<a class="btn btn-primary btn-cancel" href="/admin/typebet" role="button">Cancelar</a>
		</ul>
		{!!  Form::close() !!}
  	</div>
  	<div class="clearfix"></div>
</div>
        <div id="headerSeparator2"></div>
    </div>
</div>
@endsection