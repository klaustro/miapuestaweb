@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span12">

        <div id="headerSeparator"></div>

<div class="panel panel-default panel-bets">
	<div class="panel-heading">		
	  <h4 class="text-center">Crear tipo de apuesta</strong></h4>
	  <div class="clearfix"></div>
	</div>
	  <div class="panel-body">	
	{{--*/$menu = ['nombre' => '', 'orden' => '', 'nota' => '']/*--}}
	  {!! Form::open(['route' => 'admin.typebet.store', 'method' => 'POST']) !!}
		<ul class="list-group col-md-8 col-md-offset-2">
			{!! Field::text('nombre') !!}
			{!! Field::text('max_apuesta') !!}		
			{!! Field::text('orden') !!}
			{!! Field::select('exotica', ['0' => 'No',' 1' => 'Si']) !!}
			{!! Field::select('cantidad', ['0' => 'No',' 1' => 'Si']) !!}
			{!! Field::text('probabilidades') !!}
			{!! Field::select('pk', ['0' => 'No',' 1' => 'Si']) !!}
    			{!! Form::label('menu', 'Categorias') !!}
			{!! Form::select('menu[]', $menus, null, [
				'class' => 'form-control selectpicker', 
				'multiple', 'data-live-search' => 'true', 
				'title' => 'Seleccione las categorias asosciadas',
				'data-width' => '100%',
			]) !!}	
			<br>			
			<br>
			<input type="submit" class="btn btn-login btn-primary btn-submit" value="Crear">				
			<a class="btn btn-primary btn-cancel" href="/admin/menu" role="button">Cancelar</a>			
		</ul>

		{!!  Form::close() !!}
  	</div>
  	<div class="clearfix"></div>
</div>
        <div id="headerSeparator2"></div>
    </div>
</div>
@endsection