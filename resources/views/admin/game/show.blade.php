@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span12">

        <div id="headerSeparator"></div>

<div class="panel panel-default panel-bets">
	{!! Form::open(['route' =>['admin.game.result', $game], 'method' => 'POST']) !!}
	<div class="panel-heading">		
	  <h6 class="text-center">Resultados #{{$game->id}} {{$game->event}} {{ $game->date }}</h6>
	  <div class="clearfix"></div>
	</div>
	<div class="panel-body panel-game col-md-10 col-md-offset-1">
		@if($game->categoria != 12)
		<div class="panel-body panel-game col-md-3">
			<ul class="list-group">
				<li class="list-group-item disabled">
					Resultado Final
				</li>				

					@foreach($game->teams as $key => $team)
					<li class="list-group-item">
						<div class="pull-left">
							{{$team->nombre}}
						</div>
						<div class="pull-right">
							{!! Form::text("resultF[$key]", isset($game->finalResult[$team->id]) ?  $game->finalResult[$team->id] : '', ['class' => 'form-control']) !!}
						</div>
						<div class="clearfix"></div>
					</li>
					@endforeach
					@if($game->categoria == 3)
					<li class="list-group-item">
						{!! Field::select('next', $game->arrayTeams, isset($game->whoGoNextLevel) ? $game->whoGoNextLevel->equipo : '', ['class' => 'form-control', 'label' =>'Avanza siguiente fase']) !!}
					</li>	
					@endif

				<br>			

				</ul>
			</div>
		@else		
		<div class="panel-body panel-game col-md-11">
			<ul class="list-group">
				<li class="list-group-item disabled">
					Ganador del sorteo
				</li>			
				<li class="list-group-item">
						{!! Field::select('draw', $game->arrayTeams, isset($game->winDraw) ? $game->winDraw : '', ['class' => 'form-control', 'label' =>'Ganador del Sorteo']) !!}
				</li>								
				</ul>
			</div>
		@endif
		@if($game->categoria == 1)
		<div class="panel-body panel-game col-md-3">
			<ul class="list-group">
				<li class="list-group-item disabled">
					Resultado exóticos
				</li>

					<li class="list-group-item">
						{!! Field::select('who', $game->arrayTeams, isset($game->whoScoredFrist) ? $game->whoScoredFrist->equipo : '', ['class' => 'form-control', 'label' =>'Anota Primero']) !!}
					</li>			

					<li class="list-group-item">
						{!! Field::select('inning1', ['1' => 'Si', '2' => 'No'], isset($game->firstIningScored->score) ? $game->firstIningScored->score : '', ['class' => 'form-control', 'label' =>'Anotaron en el 1° inning']) !!}
					</li>	

					<li class="list-group-item">
						{!! Field::text("hre", isset($game->hreResult) ?  $game->hreResult->score : '', ['class' => 'form-control',  'label' =>'Hits + Carreras + Errores']) !!}
					</li>											

				</ul>
			</div>  	
		@endif
		@if( in_array($game->categoria, ['1', '3', '6', '7'])  )
		<div class="panel-body panel-game col-md-3">
			<ul class="list-group">
				<li class="list-group-item disabled">
					Resultado mitad de juego
				</li>
					@foreach($game->teams as $key => $team)
					<li class="list-group-item">
						<div class="pull-left">
							{{$team->nombre}}
						</div>
						<div class="pull-right">
							{!! Form::text("resultH[$key]", isset($game->halfResult[$team->id]) ?  $game->halfResult[$team->id] : '', ['class' => 'form-control']) !!}
						</div>
						<div class="clearfix"></div>
					</li>
					@endforeach
				</ul>
			</div>  	
			@endif
		</div>

		
		<div class="clearfix"></div>	
		<ul class="list-group">
			<li class="list-group-item">
				<input type="submit" class="btn btn-login btn-primary btn-submit" value="Actualizar">				
				<a class="btn btn-primary btn-cancel" href="/admin/game" role="button">Cancelar</a>				
				
			</li>				
		</ul>
		{!!  Form::close() !!}	
</div>
        <div id="headerSeparator2"></div>
    </div>
</div>
@endsection


