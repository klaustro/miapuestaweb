@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span12">

        <div id="headerSeparator"></div>

<div class="panel panel-default panel-bets">
	<div class="panel-heading">
	<div class="col-md-8 col-md-offset-2">
	<h4 class="text-center">Lista de juegos</h4>	
	{!! Form::model(Request::all(),['route' => ['admin.game.index'], 'method' => 'GET', 'role' => 'search', 'class' => 'form-inline']) !!}

	<div class="form-group">		
		{!! Form::select('categoria', $menus, null, ['class' => 'form-control']) !!}		
	</div>
	<div class="form-group">		
		{!! Form::select('status', $status, null, ['class' => 'form-control']) !!}					
	</div>

	<div class="form-group">
		<div class="input-group">
			{!! Form::text('search', null, ['class' => 'form-control', 'placeholder' =>  'Buscar juegos...', 'id'=> 'search-game']) !!}
		   <span class="input-group-btn">
		<button type="submit" class="btn btn-primary btn-search">  
		<span class="fa fa-search text-center"></span> Buscar                                
		</button>	
		</span>
		</div>		
	</div>	

	{!!  Form::close() !!}
	</div>
	<div class="clearfix"></div>
	</div>
	  <div class="panel-body">				
	</div>	
		<ul class="list-group col-md-8 col-md-offset-2">
			@foreach($games as $game)
				<li class="list-group-item"> 
					 {{ $game->id }} - <strong>{{$game->event}}</strong>  {{"( {$game->submenu->nombre} - {$game->date}" }})
					<div class="pull-right">
						<a class="btn btn-default" role="button" href="{{url('admin/game/'.$game->id)}} " title="Resultados"><i class="fa fa-futbol-o" aria-hidden="true"> </i></a> 
					</div>
				</li>
				<div class="clearfix"></div>	
			@endforeach		
		</ul>	
		<div class="clearfix"></div>
		<div class="pagination pagination-small text-center">

		{{ $games->appends(Request::only(['search', 'role', 'categoria']))->render() }}	
		</div>
		<div class="clearfix"></div>
  	</div>


</div>
        <div id="headerSeparator2"></div>
    </div>
</div>
@endsection