@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span12">

        <div id="headerSeparator"></div>

<div class="panel panel-default panel-bets"> 
	<div class="panel-heading">
	
	  <h4 class="text-center">{{ ($retiro->status == 0) ? 'Aprobar' : 'Desaptobar' }} retiro <strong>#{{$retiro->id}}</strong></h4>
	  <div class="clearfix"></div>
	</div>
	  <div class="panel-body">	
	  {!! Form::open(['route' => ['admin.retiro.update', $retiro], 'method' => 'PUT']) !!}
		<ul class="list-group col-md-8 col-md-offset-2">
			<li class="list-group-item">Numero de retiro : {{ $retiro->id }}</li>
			<li class="list-group-item">Usuario: {{ $retiro->user->name}}</li>
			<li class="list-group-item">Cédula: {{ $retiro->user->identification }}</li>
			<li class="list-group-item">Banco: {{ $retiro->banco ? $retiro->banco : '' }}</li>
			<li class="list-group-item">N° Cuenta: {{ $retiro->account_number }}</li>
			<li class="list-group-item">Fecha: {{ $retiro->fecha}}</li>
			<li class="list-group-item">Monto: {{ $retiro->monto}}</li>				
			<input type="hidden" name="status" value="{{ ($retiro->status == 0) ? '1' : '0'}}">			
			<br>
			<input type="submit" class="btn btn-login btn-primary btn-submit"  value="{{ ($retiro->status == 0) ? 'Aprobar' : 'Desaprobar' }}">	
			<a class="btn btn-primary btn-cancel" href="/admin/retiro" role="button">Cancelar</a>
		</ul>
		{!!  Form::close() !!}
  	</div>
  	<div class="clearfix"></div>
</div>
        <div id="headerSeparator2"></div>
    </div>
</div>
@endsection