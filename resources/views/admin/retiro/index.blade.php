@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span12">

        <div id="headerSeparator"></div>

<div class="panel panel-default panel-bets">
	<div class="panel-heading">
	<div class="col-md-8 col-md-offset-2">
	<h4 class="text-center">Lista de retiros</h4>	
	{!! Form::model(Request::all(),['route' => ['admin.retiro.index'], 'method' => 'GET', 'role' => 'search']) !!}
          <div class="input-group">
          	{!! Form::text('search', null, ['class' => 'form-control', 'placeholder' =>  'Buscar retiro...', 'id'=> 'name-search']) !!}
              <span class="input-group-btn">
		<button type="submit" class="btn btn-primary btn-search">  
			<span class="fa fa-search text-center"></span> Buscar                                
		</button>	
         </span>
          </div>	
	{!!  Form::close() !!}
	</div>
	<div class="clearfix"></div>
	</div>
	  <div class="panel-body">			
		<ul class="list-group col-md-8 col-md-offset-2">
			@foreach($retiros as $retiro)
				<li class="list-group-item"> 
					{{ $retiro->user->name }}  #{{ $retiro->id}} 
					<strong><i class="fa fa-money" aria-hidden="true"></i> {{ number_format($retiro->monto, 2) }}</strong>		
					@if($retiro->status == 1) 
						<span class="badge badge-success">Aprobado</span> 
					@else 
						<span class="badge">Pendiente</span> 
					@endif
					<div class="pull-right">
						<a class="btn btn-default" role="button" href="{{url('admin/retiro/'.$retiro->id)}}"><i class="fa {{ ($retiro->status == 1) ? 'fa-eye' :  'fa-check' }}" aria-hidden="true"> </i></a> 
					</div>
				</li>
			@endforeach		
		</ul>	
		<div class="clearfix"></div>
		<div class="pagination pagination-small text-center">
		{{ $retiros->appends(Request::only(['search', 'role']))->render() }}	
		</div>
		<div class="clearfix"></div>
  	</div>
</div>
        <div id="headerSeparator2"></div>
    </div>
</div>
@endsection