@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span12">

        <div id="headerSeparator"></div>

<div class="panel panel-default panel-bets">
	<div class="panel-heading">		
	  <h4 class="text-center">Actualizar liga</strong></h4>
	  <div class="clearfix"></div>
	</div>
	  <div class="panel-body">	
	  {!! Form::open(['route' =>['admin.submenu.update', $submenu], 'method' => 'PUT']) !!}
		<ul class="list-group col-md-8 col-md-offset-2">
			{!! Field::text('nombre', $submenu->nombre) !!}
			<div class="form-group">
    			{!! Form::label('menu', 'Categoria') !!}
			{!! Form::select('id_menu', $menus, $submenu->id_menu, ['class' => 'form-control']) !!}	
			</div>
			{!! Field::text('limite_directa', $submenu->limite_directa) !!}
			{!! Field::text('limite_compuesta', $submenu->limite_compuesta) !!}
			{!! Field::textarea('orden', $submenu->orden) !!}	
			{!! Field::select('mitad', ['1' => 'Si', '0' => 'No'], $submenu->mitad) !!}		
			<br>
			<input type="submit" class="btn btn-login btn-primary btn-submit" value="Actualizar">				
			<a class="btn btn-primary btn-cancel" href="/admin/submenu" role="button">Cancelar</a>			
		</ul>
		{!!  Form::close() !!}
  	</div>
  	<div class="clearfix"></div>
</div>
        <div id="headerSeparator2"></div>
    </div>
</div>
@endsection