@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span12">

        <div id="headerSeparator"></div>

<div class="panel panel-default panel-bets">
	<div class="panel-heading">		
	  <h4 class="text-center">Crear liga</strong></h4>
	  <div class="clearfix"></div>
	</div>

	  <div class="panel-body">	
	  {!! Form::open(['route' => 'admin.submenu.store', 'method' => 'POST']) !!}
		<ul class="list-group col-md-8 col-md-offset-2">
			{!! Field::text('nombre') !!}
			<div class="form-group">
    			{!! Form::label('menu', 'Categoria') !!}
			{!! Form::select('id_menu', $menus, null, ['class' => 'form-control']) !!}	
			</div>
			{!! Field::text('limite_directa') !!}
			{!! Field::text('limite_compuesta') !!}
			{!! Field::textarea('orden') !!}	
			{!! Field::select('mitad', ['1' => 'Si', '0' => 'No']) !!}		
			<br>
			<input type="submit" class="btn btn-login btn-primary btn-submit" value="Crear">				
			<a class="btn btn-primary btn-cancel" href="/admin/submenu" role="button">Cancelar</a>			
		</ul>

		{!!  Form::close() !!}
  	</div>
  	<div class="clearfix"></div>
</div>
        <div id="headerSeparator2"></div>
    </div>
</div>
@endsection