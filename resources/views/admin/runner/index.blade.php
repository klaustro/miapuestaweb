@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span12">

        <div id="headerSeparator"></div>

<div class="panel panel-default panel-bets"> 
	<div class="panel-heading">
	<h4 class="text-center">Lista de comisiones de corredores</h4>	
	{!! Form::model(Request::all(),['route' => ['admin.runner.index'], 'method' => 'GET', 'role' => 'search', 'class' => 'form-inline']) !!}

	<div class="form-group">
		<div class="input-group">
		<label class="control-label" for="runner">Corredor</label>
			{!! Form::select('runner', $runners, null, ['class' => 'form-control']) !!}
		</div>		
	</div>

	<div class="form-group">
		<div class="input-group">
		<label class="control-label" for="runner">Estatus</label>
			{!! Form::select('status', $status, null, ['class' => 'form-control']) !!}
		</div>		
	</div>	

	@include('partials.date', ['id' => 'from', 'selector' => 'group-from', 'title' => 'Desde'])
	
	@include('partials.date', ['id' => 'to', 'selector' => 'group-to', 'title' => 'Hasta'])

	<div class="form-group">
		<button type="submit" class="btn btn-primary btn-search btn-alone">  
			<span class="fa fa-search"></span>Buscar                                
		</button>		

	{!!  Form::close() !!} 
	</div>
	<div class="clearfix"></div> 
	</div>
	  <div class="panel-body">			
	  	<div class="table-responsive col-md-1 col-md-offset-2 hidden-xs">
			@include('admin.runner.partials.table')
		</div>
		<ul class="list-group col-md-8 col-md-offset-2 visible-xs">
			@include('admin.runner.partials.list')
		</ul>
		<div class="clearfix"></div>
		<div class="pagination pagination-small text-center">
		{{ $bets->appends(Request::only(['runner', 'from', 'to']))->render() }}
		</div>
		
		<div class="clearfix"></div>
  	</div>
</div>
        <div id="headerSeparator2"></div>
    </div>
</div>
@endsection