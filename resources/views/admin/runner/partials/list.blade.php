@foreach($bets as $bet)		
	<li class="list-group-item"> 
		 <span class="badge {{$bet->classBadge}}">{{$bet->statusName}}</span> 
		 {{$bet->id}} - {{$bet->date}}
		 @if($bet->status != 4)
			 Monto:<strong>{{ number_format($bet->monto, 2) }}</strong>  
			 A ganar: <strong>{{number_format($bet->ganancia,2) }} </strong> 
			Comisión: <strong>{{number_format($bet->comision,2)}}</strong>  ({{$bet->percentage}}%)		 			 
		@endif		 
		<div class="pull-right">
			<a class="btn btn-default" role="button" href="{{url('admin/bet/'.$bet->id)}}"><i class="fa fa-eye" aria-hidden="true"> </i></a> 
		</div>
</li>
@endforeach
@if($total['comision'] > 0)
	<li class="list-group-item txt-number">
		<strong>
			Utilidad: {{ number_format($total['utilidad'],2) }}, 
			Ventas: {{ number_format($total['venta'],2) }}, 
			Comisiones: {{ number_format($total['comision'],2) }}
		</strong>
	</li>
@endif