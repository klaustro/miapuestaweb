<table class="table">
	<tr>
		<th>Ticket</th>
		<th>Estatus</th>
		<th>fecha</th>
		<th>Monto</th>
		<th>Ganancia</th>
		<th>Porcentaje</th>
		<th>Comisión</th>		
		<th></th>			
	</tr>
	@foreach($bets as $bet)			
	<tr>
		<td>{{$bet->id}}</td>  
		<td><span class="badge {{$bet->classBadge}}">{{$bet->statusName}}</span></td>
		<td>{{$bet->date}}</td>
		<td class="txt-number">{{$bet->humanAmount}}</td>
		<td class="txt-number">{{$bet->humanProfit}}</td>
		<td class="txt-number">{{$bet->percentage}}%</td>
		<td class="txt-number">{{number_format($bet->comision, 2)}}</td>
		<td><a class="btn btn-default" role="button" href="{{url('admin/bet/'.$bet->id)}}"><i class="fa fa-eye" aria-hidden="true"> </i></a></td>
	</tr>
	@endforeach
	@if($total['comision'] > 0)
		<tr>			
			<td colspan="3" class="txt-number"><strong>Utilidad: {{ number_format($total['utilidad'],2) }}</strong></td>
			<td colspan="2" class="txt-number"><strong>Ventas: {{ number_format($total['venta'],2) }}</strong></td>			
			<td colspan="3" class="txt-number">
				<strong>Comisiones: {{ number_format($total['comision'],2) }}</strong>	
			</td>			
			<td></td>	
		</tr>
	@endif

</table>
