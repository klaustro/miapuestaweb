@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span12">

        <div id="headerSeparator"></div>

<div class="panel panel-default panel-bets">
	<div class="panel-heading">
	<div class="col-md-8 col-md-offset-2">
	<h4 class="text-center">Lista de pagos</h4>	
	{!! Form::model(Request::all(),['route' => ['admin.pago.index'], 'method' => 'GET', 'role' => 'search']) !!}
          <div class="input-group">
          	{!! Form::text('search', null, ['class' => 'form-control', 'placeholder' =>  'Buscar pago...', 'id'=> 'name-search']) !!}
              <span class="input-group-btn">
		<button type="submit" class="btn btn-primary btn-search">  
			<span class="fa fa-search text-center"></span> Buscar                                
		</button>	
         </span>
          </div>	
	{!!  Form::close() !!}
	</div>
	<div class="clearfix"></div>
	</div>
	  <div class="panel-body">			
		<ul class="list-group col-md-8 col-md-offset-2">
			@foreach($pagos as $pago)
				<li class="list-group-item"> 
					{{ $pago->user->name }}  #{{ $pago->id}} 
					<strong><i class="fa fa-money" aria-hidden="true"></i> {{ number_format($pago->monto, 2) }}</strong>
					@if($pago->status == 1) 
						<span class="badge badge-success">Aprobado</span> 
					@else 
						<span class="badge">Pendiente</span> 
					@endif			

					<div class="pull-right">
						<a class="btn btn-default" role="button" href="{{url('admin/pago/'.$pago->id)}}"><i class="fa {{ ($pago->status == 1) ? 'fa-eye' : 'fa-check'  }}" aria-hidden="true"> </i></a> 
					</div>
				</li>
			@endforeach		
		</ul>	
		<div class="clearfix"></div>
		<div class="pagination pagination-small text-center">
		{{ $pagos->appends(Request::only(['search', 'role']))->render() }}	
		</div>
		<div class="clearfix"></div>
  	</div>
</div>
        <div id="headerSeparator2"></div>
    </div>
</div>
@endsection