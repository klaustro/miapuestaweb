@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span12">

        <div id="headerSeparator"></div>

<div class="panel panel-default panel-bets">
	<div class="panel-heading">	
	  <h4 class="text-center">{{($pago->status == 0) ? 'Aprobar' : 'Desaprobar'}} pago <strong>#{{$pago->id}}</strong></h4>
	  <div class="clearfix"></div>
	</div>
	  <div class="panel-body">	

	  {!! Form::open(['route' => ['admin.pago.update', $pago], 'method' => 'PUT']) !!}
		<ul class="list-group col-md-8 col-md-offset-2">
			<li class="list-group-item">Numero de pago : {{ $pago->id }}</li>
			<li class="list-group-item">Usuario: {{ $pago->user->name}}</li>
			<li class="list-group-item">Fecha: {{ $pago->fecha_hora}}</li>
			<li class="list-group-item">Tipo de Pago: @if($pago->tipo == 0) Depósito @else Transferencia @endif</li>
			<li class="list-group-item">Referencia: {{ $pago->referencia}}</li>
			<li class="list-group-item">Banco: {{ $pago->bank ? $pago->bank->nombre : ''}}</li>
			<li class="list-group-item">Titular: {{ $pago->titular}}</li>
			<li class="list-group-item">Monto: {{ $pago->monto}}</li>				
			<input type="hidden" name="status" value="{{ ($pago->status == 0) ? '1' : '0' }}">			
			<br>
			<input type="submit" class="btn btn-login btn-primary btn-submit" value="{{ ($pago->status == 0) ? 'Aprobar' : 'Desaprobar'}}">	
			<a class="btn btn-primary btn-cancel" href="/admin/pago" role="button">Cancelar</a>
		</ul>
		{!!  Form::close() !!}
  	</div>
  	<div class="clearfix"></div>
</div>
        <div id="headerSeparator2"></div>
    </div>
</div>
@endsection