@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span12">

        <div id="headerSeparator"></div>

<div class="panel panel-default panel-bets">
	<div class="panel-heading">
	<div class="col-md-8 col-md-offset-2">
	<h4 class="text-center">Lista de comisiones</h4>	
	{!! Form::model(Request::all(),['route' => ['admin.comision.index'], 'method' => 'GET', 'role' => 'search']) !!}
          <div class="input-group">
          	{!! Form::text('search', null, ['class' => 'form-control', 'placeholder' =>  'Buscar comision...', 'id'=> 'name-search']) !!}
              <span class="input-group-btn">
		<button type="submit" class="btn btn-primary btn-search">  
			<span class="fa fa-search text-center"></span> Buscar                                
		</button>	
         </span>
          </div>	
	{!!  Form::close() !!}
	</div>
	<div class="clearfix"></div>
	</div>
	  <div class="panel-body">			
		<ul class="list-group col-md-8 col-md-offset-2">
			<li class="list-group-item list-group-item-warning">
				Nueva Comisión
				<div class="pull-right">
					<a class="btn btn-default" role="button" href="{{url('admin/comision/create')}}"><i class="fa fa-plus" aria-hidden="true"></i></a>
				</div>
			</li>		
			@foreach($comisiones as $comision)
				<li class="list-group-item"> 
					 {{ $comision->id}} - {{ $comision->name }}  entre {{ $comision->desde }}  - {{ $comision->hasta }}  monto: {{ $comision->comision }} 
					<div class="pull-right">
						<a class="btn btn-default" role="button" href="{{url('admin/comision/'.$comision->id)}}"><i class="fa fa-pencil" aria-hidden="true"> </i></a> 
					</div>
				</li>
			@endforeach		
		</ul>	
		<div class="clearfix"></div>
		<div class="pagination pagination-small text-center">
		{{ $comisiones->appends(Request::only(['search', 'role']))->render() }}	
		</div>
		<div class="clearfix"></div>
  	</div>
</div>
        <div id="headerSeparator2"></div>
    </div>
</div>
@endsection