@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span12">

        <div id="headerSeparator"></div>

<div class="panel panel-default panel-bets">
	<div class="panel-heading">		
	  <h4 class="text-center">Actualizar comision <strong>#{{$comision->id}}</strong></h4>
	  <div class="clearfix"></div>
	</div>
	  <div class="panel-body">	

	  {!! Form::open(['route' => ['admin.comision.update', $comision], 'method' => 'PUT']) !!}
		<ul class="list-group col-md-8 col-md-offset-2">
			<div class="form-group">
	    			{!! Form::label('tipo', 'Tipo de comision') !!}
				{!! Form::select('tipo', $tipoComision, null, ['class' => 'form-control']) !!}	
			</div>
			{!! Field::text('desde', $comision->desde) !!}
			{!! Field::text('hasta', $comision->hasta) !!}
			{!! Field::text('comision', $comision->comision) !!}
			<br>
			<input type="submit" class="btn btn-login btn-primary btn-submit" value="Actualizar">				
			<a class="btn btn-primary btn-cancel" href="/admin/comision" role="button">Cancelar</a>
		</ul>
		{!!  Form::close() !!}
  	</div>
  	<div class="clearfix"></div>
</div>
        <div id="headerSeparator2"></div>
    </div>
</div>
@endsection