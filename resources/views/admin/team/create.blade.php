@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span12">

        <div id="headerSeparator"></div>

<div class="panel panel-default panel-bets">
	<div class="panel-heading">		
	  <h4 class="text-center">Crear equipo</strong></h4>
	  <div class="clearfix"></div>
	</div>

	  <div class="panel-body">	
	  {!! Form::open(['route' => 'admin.team.store', 'method' => 'POST']) !!}	  
		<ul class="list-group col-md-8 col-md-offset-2">
		{!! Alert::render() !!}
			{!! Field::text('nombre') !!}
			<div class="form-group">
    			{!! Form::label('submenu', 'Liga') !!}
			{!! Form::select('submenu[]', $submenus, null, [
				'class' => 'form-control selectpicker', 
				'multiple', 'data-live-search' => 'true', 
				'title' => 'Seleccione las ligas asosciadas',
				'data-width' => '100%',
			]) !!}	
			</div>	
			{!! Field::text('abbr') !!}
			<br><br>
			<input type="submit" class="btn btn-login btn-primary btn-submit" value="Crear">				
			<a class="btn btn-primary btn-cancel" href="/admin/team" role="button">Cancelar</a>			
		</ul>
		<div class="clearfix"></div>
		{!!  Form::close() !!}
  	</div>
  	<div class="clearfix"></div>
</div>
        <div id="headerSeparator2"></div>
    </div>
</div>
@endsection