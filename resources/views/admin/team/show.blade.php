@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span12">

        <div id="headerSeparator"></div>

<div class="panel panel-default panel-bets">
	<div class="panel-heading">		
	  <h4 class="text-center">Actualizar equipo</strong></h4>
	  <div class="clearfix"></div>
	</div>
	  <div class="panel-body">	
	  {!! Form::open(['route' =>['admin.team.update', $team], 'method' => 'PUT']) !!}
		<ul class="list-group col-md-8 col-md-offset-2">
			{!! Field::text('nombre', $team->nombre) !!}
			<div class="form-group">
    			{!! Form::label('submenu', 'Liga') !!}
			{!! Form::select('submenu[]', $submenus, isset($team->leagues) ? $team->leagues : [], [
				'class' => 'form-control selectpicker', 
				'multiple', 'data-live-search' => 'true', 
				'title' => 'Seleccione las ligas asosciadas',
				'data-width' => '100%',
			]) !!}	
			{!! Field::text('abbr', $team->abbr, ['label' => 'Abreviatura']) !!}
			<br><br>
			<input type="submit" class="btn btn-login btn-primary btn-submit" value="Actualizar">				
			<a class="btn btn-primary btn-cancel" href="/admin/team" role="button">Cancelar</a>			
		</ul>
		<div class="clearfix"></div>
		{!!  Form::close() !!}
  	</div>
  	<div class="clearfix"></div>
</div>
        <div id="headerSeparator2"></div>
    </div>
</div>
@endsection