@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span12">

        <div id="headerSeparator"></div>

<div class="panel panel-default panel-bets">
	<div class="panel-heading">
	<div class="col-md-8 col-md-offset-2">
	<h4 class="text-center">Lista de equipos</h4>	
	{!! Form::model(Request::all(),['route' => ['admin.team.index'], 'method' => 'GET', 'role' => 'search']) !!}
          <div class="input-group">
          	{!! Form::text('search', null, ['class' => 'form-control', 'placeholder' =>  'Buscar team...', 'id'=> 'name-search']) !!}
              <span class="input-group-btn">
		<button type="submit" class="btn btn-primary btn-search">  
			<span class="fa fa-search text-center"></span> Buscar                                
		</button>	
         </span>
          </div>	
	{!!  Form::close() !!}
	</div>
	<div class="clearfix"></div>
	</div>
	  <div class="panel-body">			
		<ul class="list-group col-md-8 col-md-offset-2">
			<li class="list-group-item list-group-item-warning">
				Nuevo Equipo
				<div class="pull-right">
					<a class="btn btn-default" role="button" href="{{url('admin/team/create')}}"><i class="fa fa-plus" aria-hidden="true"></i></a>
				</div>
			</li>		
			@foreach($teams as $team)
				<li class="list-group-item"> 
					 {{ $team->id}} - {{ $team->nombre }}
					<div class="pull-right">
						<a class="btn btn-default" role="button" href="{{url('admin/team/'.$team->id)}}"><i class="fa fa-pencil" aria-hidden="true"> </i></a> 
					</div>
				</li>
			@endforeach		
		</ul>	
		<div class="clearfix"></div>
		<div class="pagination pagination-small text-center">
		{{ $teams->render() }}	
		</div>
		<div class="clearfix"></div>
  	</div>
</div>
        <div id="headerSeparator2"></div>
    </div>
</div>
@endsection