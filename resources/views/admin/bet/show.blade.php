@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span12">

        <div id="headerSeparator"></div>

<div class="panel panel-default panel-bets">
	<div class="panel-heading">
	  <h5 class="text-center"><strong>#{{$bet->id}} -  {{ $bet->userName}} {{ $bet->date}}</strong></h5>
	  <div class="clearfix"></div>
	</div>
	  <div class="panel-body">
	  {!! Form::open(['route' => ['admin.bet.update', $bet], 'method' => 'PUT']) !!}
		<ul class="list-group col-md-8 col-md-offset-2">
			@foreach($betsDetail as $key => $detail)
				<li class="list-group-item" title="{{$detail->gamesProbs->idgame}}">
				{{ $detail->event}} <strong> {{$detail->betType}} - {{$detail->selection}} </strong>
				{!!$detail->tag!!}
				</li>
			@endforeach
		</ul>
		<div class="clearfix"></div>
		<br>
			<ul class="list-group col-md-8 col-md-offset-2">
				<li class="list-group-item">
					<div class="pull-left">
						<input type="submit" class="btn btn-login btn-primary btn-submit" id="btn-pago" value="{{($bet->status == 4) ? 'Reanudar' : 'Anular'}}">
						<a class="btn btn-primary btn-cancel" href="{{route(app('router')->getRoutes()->match(app('request')->create(URL::previous()))->getName())}}" role="button">Cancelar</a>
					</div>
					<div class="pull-right">
						<strong>Monto: {{ number_format($bet->monto,2) }} Ganancia: {{ number_format($bet->ganancia, 2) }}
						</strong>
					</div>
					<div class="clearfix"></div>
				</li>
			</ul>	<div class="clearfix"></div>
			<input type="hidden" name="status" value="{{ $bet->status}}">
		{!!  Form::close() !!}

  	</div>

</div>
        <div id="headerSeparator2"></div>
    </div>
</div>
@endsection