@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span12">

        <div id="headerSeparator"></div>

<div class="panel panel-default panel-bets">
	<div class="panel-heading">
	  <h5 class="text-center"><strong>Balance de Apuestas</strong></h5>
	  <div class="clearfix"></div>
	</div>
	<div class="panel-body">
		{!! Form::model(Request::all(),['route' => ['admin.bet.balance'], 'method' => 'GET', 'role' => 'search', 'class' => 'form-inline']) !!}		
			@include('partials.date', ['id' => 'start', 'selector' => 'pago-group-from', 'title' => 'Desde'])
			@include('partials.date', ['id' => 'end', 'selector' => 'pago-group-to', 'title' => 'hasta'])
			<div class="input-group">
				<label>Estatus</label>
				{!! Form::select('status', $status, null, ['class' => 'form-control']) !!}
			</div>
			<button type="submit" class="btn btn-primary btn-search" style="margin-top: 23px;">
				<span class="fa fa-search"></span> Buscar
			</button>	
		{!!  Form::close() !!}
		<div class="table-responsive">			
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>Ticket</th>
						<th>Fecha</th>
						<th>Usuario</th>
						<th>Monto apostado</th>
						<th>Monto a ganar</th>
						<th>Estaus</th>	
						<th>Acciones</th>				
					</tr>
				</thead>
				<tbody>
					@foreach($bets as $bet)		
					<tr>
						<td>{{$bet->id}}</td>
						<td>{{date_format(date_create($bet->fecha_registro), 'd-m-Y h:i A')}}</td>
						<td>{{$bet->user_name}}</td>
						<td>{{number_format($bet->monto, 2)}}</td>
						<td>{{number_format($bet->ganancia,2)}}</td>
						<td>{{$bet->status_name}}</td>
						<td>					
							<a class="btn btn-default" role="button" href="{{url('owner/bet/'.$bet->id)}}"><i class="fa fa-eye" aria-hidden="true"> </i></a>
						</td>
					</tr>
					@endforeach		
				</tbody>
				<tfoot>
					<tr>
						<td colspan="2"></td>
						<th>Total vendido</th>
						<td><b>{{number_format($total, 2)}}</b></td>
						<th>A pagar</th>
						<td><b>{{number_format($to_pay, 2)}}</b></td>
					</tr>
				</tfoot>
			</table>		
		</div>
		<div class="pagination pagination-small text-center">
		{{ $bets->appends(Request::only(['start', 'end']))->render() }}
		</div>
	</div>

</div>
        <div id="headerSeparator2"></div>
    </div>
</div>
@endsection