@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span12">

		<div id="headerSeparator"></div>

		<div class="panel panel-default panel-bets">
			<div class="panel-heading">
				<h4 class="text-center">Lista de apuestas</h4>
				{!! Form::model(Request::all(),['route' => ['admin.bet.index'], 'method' => 'GET', 'role' => 'search', 'class' => 'form-inline']) !!}

				<div class="col-md-2">
					<div class="input-group">
						<label>Estatus de apuesta</label>
						{!! Form::select('status', $status, null, ['class' => 'form-control']) !!}
					</div>
				</div>
				
				<div class="col-md-2">
					@include('partials.date', ['id' => 'from', 'selector' => 'pago-group-from', 'title' => 'Desde'])		
				</div>
				<div class="col-md-2">
					@include('partials.date', ['id' => 'to', 'selector' => 'pago-group-to', 'title' => 'Hasta'])				
				</div>

				<div class="col-md-2">
					<label>Buscar por usuario o ticket</label>
					{!! Form::text('search', null, ['class' => 'form-control', 'placeholder' =>  'Buscar apuesta...', 'id'=> 'search-game']) !!}
				</div>


				<button type="submit" class="btn btn-primary btn-search" style="margin-top: 23px;">
					<span class="fa fa-search text-center"></span> Buscar
				</button>		

				{!!  Form::close() !!}

				<div class="clearfix"></div>
			</div>
			<div class="panel-body">
				<ul class="list-group col-md-8 col-md-offset-2">
					@foreach($bets as $bet)
						<li class="list-group-item">
							{{$bet->id}} - {{$bet->date}} {{$bet->userName}} 
							@if($bet->status != 4)
								Monto:<strong>{{ number_format($bet->monto, 2) }}</strong>
								A ganar: <strong>{{number_format($bet->ganancia,2) }} </strong>
							@endif
							<span class="badge {{$bet->badge}}">{{$bet->statusName}}</span>
							<div class="pull-right">
								<a class="btn btn-default" role="button" href="{{url('owner/bet/'.$bet->id)}}"><i class="fa fa-eye" aria-hidden="true"> </i></a>
							</div>
						</li>
					@endforeach
				</ul>
				<div class="clearfix"></div>

				<div class="clearfix"></div>
			</div>
			<div class="panel-footer">
				@if($jugado > 0)
					<div class="col-md-3">
						<b>Total jugado</b> {{number_format($jugado, 2)}}  			
					</div>
					<div class="col-md-3">
						<b>Total ganado</b> {{number_format($ganancia, 2)}} 			
					</div>
					<div class="col-md-3">
						<b>Utilidad</b> {{number_format($ganancia - $jugado, 2)}}
					</div>
					<div class="clearfix"></div>
				@endif
				<div class="pagination pagination-small text-center">
				{{ $bets->appends(Request::only(['search', 'role', 'from', 'to', 'game', 'status']))->render() }}
				</div>
			</div>
		</div>
		<div id="headerSeparator2"></div>
    </div>
</div>
@endsection