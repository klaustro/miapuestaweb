@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span12">

        <div id="headerSeparator"></div>

<div class="panel panel-default panel-bets">
	<div class="panel-heading">		
	  <h4 class="text-center">Actualizar empresa <strong>#{{$enterprise->id}}</strong></h4>
	  <div class="clearfix"></div>
	</div>
	  <div class="panel-body">	

	  {!! Form::open(['route' => ['admin.enterprise.update', $enterprise], 'method' => 'PUT']) !!}
		<ul class="list-group col-md-8 col-md-offset-2">
			{!! Field::text('nombre', $enterprise->nombre) !!}
			<br>
			<input type="submit" class="btn btn-login btn-primary btn-submit" value="Actualizar">				
			<a class="btn btn-primary btn-cancel" href="/admin/enterprise" role="button">Cancelar</a>
		</ul>
		{!!  Form::close() !!}
  	</div>
  	<div class="clearfix"></div>
</div>
        <div id="headerSeparator2"></div>
    </div>
</div>
@endsection