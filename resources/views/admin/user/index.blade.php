@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span12">

        <div id="headerSeparator"></div>

<div class="panel panel-default panel-bets">
	<div class="panel-heading">
	<div class="col-md-8 col-md-offset-2">
	<h4 class="text-center">Lista de usuarios</h4>	
	{!! Form::model(Request::all(),['route' => ['admin.user.index'], 'method' => 'GET', 'role' => 'search']) !!}
          <div class="input-group">
          	{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' =>  'Buscar usuario...', 'id'=> 'name-search']) !!}
              <span class="input-group-btn">
		<button type="submit" class="btn btn-primary btn-search">  
			<span class="fa fa-search"></span> Buscar                                
		</button>	
         </span>
          </div>	
	{!!  Form::close() !!}
	</div>
	<div class="clearfix"></div>
	</div>
	  <div class="panel-body">			
		<ul class="list-group col-md-8 col-md-offset-2">
			@foreach($users as $user)
				<li class="list-group-item"> 
					{{$user->id}} - {{$user->name}} 
						@if($user->corredor == 0)
						<strong><i class="fa fa-money" aria-hidden="true"></i> {{ number_format($user->saldo, 2) }}</strong>
						@endif
					<div class="pull-right">
						<a class="btn btn-default" role="button" href="{{url('admin/user/'.$user->id)}}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
					</div>
				</li>
			@endforeach		
		</ul>	
		<div class="clearfix"></div>
		<div class="pagination pagination-small text-center">
		{{ $users->appends(Request::only(['name', 'role']))->render() }}	
		</div>
		<div class="clearfix"></div>
  	</div>
</div>
        <div id="headerSeparator2"></div>
    </div>
</div>
@endsection