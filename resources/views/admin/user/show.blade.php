@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span12">

        <div id="headerSeparator"></div>

<div class="panel panel-default panel-bets">
	<div class="panel-heading">
	  <div class="pull-left">Editar usuario #{{$user->id}} <strong>{{$user->name}}</strong></div>
		@if($user->corredor == 0)
			  <div class="pull-right"><strong><i class="fa fa-money" aria-hidden="true"></i> {{ number_format($user->saldo, 2) }}</strong></div>		
		@endif
	  <div class="clearfix"></div> 
	</div>
	  <div class="panel-body">		
	  {!! Form::open(['route' => ['admin.user.update', $user], 'method' => 'PUT']) !!}	  
		<ul class="list-group col-md-8 col-md-offset-2">
			{!! Alert::render() !!}
			{!! Field::text('name', $user->name) !!}
			{!! Field::text('email', $user->email) !!}
			{!! Field::text('phone', $user->phone) !!}			
			{!! Field::select('status', ['empty' => 'Seleccione', '0' => 'Inactivo', '1' => 'Activo' ], $user->status) !!}
			{!! Field::select('printer', ['1' => 'POS 58 Termica', '2' => 'EPSON TM-U325 (MATRIZ DE PUNTO)', ], $user->printer) !!}
			{!! Field::select('corredor', ['0' => 'No', '1' => 'Si', ], $user->corredor) !!}
			<ul id="corredor-comision" class="{{($user->corredor == '0') ? 'hidden' : ''}}">
				<li class="list-group-item">
					<div class="form-group">
		    			{!! Form::label('tipoComision', 'Tipo de comision') !!}
					{!! Form::select('tipoComision', $tipoComisiones,  $user->tipoComision , ['class' => 'form-control', 'onChange' => 'enableButton()']) !!}	
					</div>
					<div class="form-group">
		    			{!! Form::label('ventas', 'Calculo en base a') !!}
					{!! Form::select('ventas', ['' => 'Seleccione tipo de calculo', '0' => 'Ganancias', '1' => 'Ventas'],  $user->sells , ['class' => 'form-control', 'onChange' => 'enableButton()']) !!}	
					</div>										
				</li>			
			</ul>
			<br>
			{!! Field::select('role', ['user' => 'Usuario', 'owner' => 'Propietario', 'admin' => 'Administrador', ], $user->role, ['label' => 'Tipo de usuario']) !!}
			@if($user->accounts->count() > 0)
				<label>Cuentas Bancarias</label>
				@foreach($user->accounts as $account)
					<p class="text-uppercase"> {{$account->tipo . ' ' . $account->banco->nombre . $account->nrocuenta}}</p>
				@endforeach
			@endif
			{!! Field::select('enterprise', $empresas, $user->enterprise, ['label' => 'Empresa']) !!}
			<br>
			<input type="submit" class="btn btn-login btn-primary btn-submit" id="btn-update-user" value="Enviar">	
			<a class="btn btn-primary btn-cancel" href="/admin/user" role="button">Cancelar</a>
		</ul>
		{!!  Form::close() !!}
  	</div>
  	<div class="clearfix"></div>
</div>
        <div id="headerSeparator2"></div>
    </div>
</div>
@endsection