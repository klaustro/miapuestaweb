@extends('layouts.app')
@section('content')

<div class="row-fluid" id="panel-contact">
<div class="panel panel-default panel-bets">
    <div class="panel-heading">
        <h4 class="text-center">Contáctanos</h4>
    </div>
    <div class="panel-body">
        <div class="span8" id="divMain">
            <form name="enq">  
              <fieldset>
            	<input type="text" name="name" id="name" value=""  class="input-block-level" placeholder="Nombre" />
                <input type="text" name="email" id="email" value="" class="input-block-level" placeholder="Email" />
                <textarea rows="11" name="content" id="content" class="input-block-level" placeholder="Commentarios"></textarea>
                <div class="actions">
            	<input type="button" value="Enviar" name="btnContacto" id="btnContacto" class="btn btn-primary pull-right btn-login" title="Enviar" onclick="contacto();" />
            	</div>
            	</fieldset>
            </form>

        </div>


        <div class="span4 sidebar">

            <div class="sidebox">
                <h3 class="sidebox-title">Infromación de Contacto</h3>
            <p>
                <address><strong>Miapuestaweb.com</strong><br />
               Dirección<br/>
               Av Panteon<br/>
               Res Santa Ana de Coro<br/>
               Piso 5 Ofc 05<br/>
                <abbr title="Teléfono">T:</abbr> (0212) 564.6712<br>
                <abbr title="Teléfono">C:</abbr> (0412) 298.5469</address>
                <address>  <strong>Email</strong><br />
                <a href="mailto:mail.miapuestaweb.com@gmail.com">mail.miapuestaweb.com@gmail.com</a></address>
            </p>
            </div>
        </div>
    <div class="clearfix"></div>

    </div>

</div>
</div>

@endsection
@section('js')
    <script type="text/javascript" src="{{url('assets/scripts/contacto.js')}}"></script>
@endsection