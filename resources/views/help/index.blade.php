@extends('layouts.app')
@section('content')

	<div class="panel panel-default panel-bets">
		<div class="panel-heading">
		    <h4 class="text-center">Ayuda</h4>

		</div>
		  <div class="panel-body">
			<div class="col-md-6 col-md-offset-3">
				<div class="videoWrapper">
				    <iframe width="560" height="349" src="https://www.youtube.com/embed/vN7HsTpGXy8" frameborder="0" allowfullscreen></iframe>
				</div>				
			</div>
			
			<div class="col-md-6 col-md-offset-3">
				<div class="videoWrapper">
				    <iframe width="560" height="349" src="https://www.youtube.com/embed/nTYp5pSsUzI" frameborder="0" allowfullscreen></iframe>
				</div>				
			</div>	

			<div class="col-md-6 col-md-offset-3">
				<div class="videoWrapper">
				    <iframe width="560" height="349" src="https://www.youtube.com/embed/9tq7XoinjTw" frameborder="0" allowfullscreen></iframe>
				</div>				
			</div>	
			<div class="clearfix"></div>				
	  	</div>
	</div>

@endsection