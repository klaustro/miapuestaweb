<script src="assets/js/resultados.js"></script>

<div class="panel panel-default panel-bets">
	<div class="panel-heading">
	    <div class="form-group col-md-4" id="pago-fecha_hora">
	        <label class="control-label" for="fecha_hora">Fecha</label>
	    <div class='input-group date input-pago' id='group-fecha'>
	        <input type='text' class="form-control"  id="fecha" readonly="true" required=""/>
	        <span class="input-group-addon">
	            <span class="fa fa-calendar">
	            </span>
	        </span>
	    </div>
	    </div>
	<div class="clearfix"></div>
	</div>
	  <div class="panel-body" id="panel-resultados">
	  </div>
  </div>
</div>