<html>
	<body>
		<table>
				<tr><td align='center'><img src='http://new.miapuestaweb.com/images/logos.png'</td></tr>
				<tr><td align='center'><h2><font color='orange'>Aviso de Depósito Recibido</font></h2></td></tr>
				<tr><td>Estimado <strong>{{$name}}</strong></td></tr>
				<tr><td>Hemos recibido su aviso de depósito por <strong>{{number_format($monto,2,',','.')}}</strong></td></tr>
				<tr><td>Su transacción <strong>{{$id}}</strong> será procesada en un tiempo máximo de 60 minutos, si la efectúa dentro del horario de atención, que es Lunes a Domingo de 8am a 8:00pm.
				Si realizó una transferencia desde un banco tercero o realizó el depósito en cheque se le acreditará de 24-48 horas hábiles bancarias.
				Cualquier consulta por favor comunicarse a: <strong>0212-5611273</strong>
				</td></tr>
				<tr><td>Atentamente,</td></tr>
				<tr><td><font color='#81BEF7'>MIAPUESTAWEB</font></td></tr>

		</table>
	</body>
</html>