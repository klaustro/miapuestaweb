<html>
	<body>
		<table>
			<tr><td align='center'><img src='http://new.miapuestaweb.com/images/logos.png'</td></tr>
			<tr><td align='center'><h2><font color='orange'>Aviso de Retiro Recibido</font></h2></td></tr>
			<tr><td>Estimado <strong>{{ $user->name }}</strong></td></tr>
			<tr><td>Hemos recibido su solicitud de retiro por <strong>{{number_format($monto,2,',','.')}}</strong></td></tr>
			<tr><td>Su transacción <strong>{{$id}}</strong> será procesada en un lapso de 24 horas hábiles bancarias, si la efectúa dentro del horario de atención, que es de Lunes a Sabado de 8am a 8pm.
			Cuando sea acreditada su transacción recibirá un correo de confirmación.
			Es importante tomar en cuenta que las transferencias Banesco pueden tomar de 2 a 24 horas, los demás bancos hasta 48 horas en ser acreditados, y montos superiores a 1,000,000 Bs pueden ser procesados hasta en un lapso máximo de 72 horas hábiles bancarias.
			Recuerde que no realizamos transferencias a cuentas de ahorro del Banco Banesco.
			</td>
			<tr>
				<tr>
					<td><b>Su cedula:</b> {{$user->identification}}</td>
				</tr>
				<tr>
					<td><b>Banco:</b> {{$cuenta->banco->nombre}}</td>
				</tr>
				<tr>
					<td><b>Cuenta N°:</b> {{$cuenta->nrocuenta}}</td>
				</tr>
				<tr>
					<td><b>Tipo de cuenta:</b> {{$cuenta->tipo}}</td>
				</tr>
			</tr>
			<tr><td></td></tr>
			<tr><td>Atentamente,</td></tr>
			<tr><td><font color='#81BEF7'>MIAPUESTAWEB</font></td></tr>
		</table>
	</body>
</html>