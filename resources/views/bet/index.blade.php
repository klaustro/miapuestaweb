@extends('layouts.app')
@section('content')

<div class="panel panel-default panel-bets">
  <div class="panel-heading">
  <div class="pull-left">
    <label class="switch">
      <input type="checkbox" class="sw-tipo" id="sw-tipo-apuesta">
      <div class="slider"></div>
    </label><b class="lbl-tipo-apuesta">  Directa</b>
  </div>
  <div class="pull-right">
   @if (Auth::user()->corredor == 0)
  <a href="##" class="text-right txt-saldo" id="saldo" data-saldo="{{ Auth::user()->saldo }}" data-corredor="{{ Auth::user()->corredor }}" data-toggle="tooltip" data-placement="bottom" title="Monto arriesgado: {{ Auth::user()->arriesgado }}"><i class="fa fa-money" aria-hidden="true"></i> {{ number_format(Auth::user()->saldo, 2) }}</a>
  @endif
  </div>
  <div class="clearfix"></div>  
 </div>
  <div class="panel-body">
	<p id="lbl-time" class="pull-right"></p>
	<div class="clearfix"></div> 
    <div class="accordion" id="accordion2">

          @include('bet.category')

    </div>
  </div>
</div>
@include('partials.modal-bets')
@include('partials.modal-result')
@include('partials.modal-forbidden')


@endsection

@section('js')
<script src="scripts/bootstrap-notify/bootstrap-notify.min.js"></script>
  <script type="text/javascript" src="{{url('assets/scripts/bets.js')}}"></script>
  <script>
    $(function() {
      reset();
    });
  </script>
@endsection
