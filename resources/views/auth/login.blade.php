@extends('layouts.app')
@section('content')

<div class="panel panel-default panel-login">
	<div class="panel-heading">
		<h4 class="text-center">Iniciar sesión</h4> 
		<div class="clearfix"></div>
	</div>
	<div class="panel-body">
		<div class="wrapper">
			<form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
			{{ csrf_field() }}

				<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
					<label for="email" class="col-md-6 control-label">E-Mail Address</label>

					<div class="col-md-6">
						<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

						@if ($errors->has('email'))
							<span class="help-block">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
						@endif
					</div>
				</div>


				<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
					<label for="password" class="col-md-6 control-label">Password</label>

					<div class="col-md-6">
						<input id="password" type="password" class="form-control" name="password" required>
						
						@if ($errors->has('password'))
						<span class="help-block">
							<strong>{{ $errors->first('password') }}</strong>
						</span>
						@endif
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-md-8 col-md-offset-4">
						<button type="submit" class="btn btn-primary">
							Iniciar sesión
						</button>
					</div>
				</div>
			</form>
		</div>
	</div> 
	<div class="clearfix"></div>
</div>
           
@endsection
