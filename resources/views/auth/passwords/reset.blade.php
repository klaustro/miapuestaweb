@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span12">

        <div id="headerSeparator"></div>

<div class="panel panel-default panel-bets">
    <div class="panel-heading">     
      <h4 class="text-center">Recuperar contraseña</strong></h4>
      <div class="clearfix"></div>
    </div>

      <div class="panel-body">  

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                    <ul class="list-group col-md-8 col-md-offset-2">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">Correo Electrónico</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ $email ?? old('email') }}">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div> 
                        <br>
                        <div class="clearfix"></div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password">Contraseña</label>
                                <input id="password" type="password" class="form-control" name="password">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </div> 
                        <br>
                        <div class="clearfix"></div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm">Repetir contraseña</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                        </div> 
                        <br>
                        <div class="clearfix"></div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-login btn-primary btn-submit" value="Recuperar contraseña">    
                        </div>
                        </ul><div class="clearfix"></div>
                    </form>
                      
    </div>
    <div class="clearfix"></div>
</div>
        <div id="headerSeparator2"></div>
    </div>
</div>


@endsection