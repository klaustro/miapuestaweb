@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span12">

        <div id="headerSeparator"></div>

<div class="panel panel-default panel-bets">
    <div class="panel-heading">     
      <h4 class="text-center">Recuperar contraseña</strong></h4>
      <div class="clearfix"></div>
    </div>

      <div class="panel-body">  

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                    <ul class="list-group col-md-8 col-md-offset-2">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email">Correo electrónico</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif                            
                            </div>
                            <br>

                            <div class="clearfix"></div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-login btn-primary btn-submit" value="Enviar link de recuperación">    
                            </div>
                        </ul>
                        <div class="clearfix"></div>
                    </form>    
                      
    </div>
    <div class="clearfix"></div>
</div>
        <div id="headerSeparator2"></div>
    </div>
</div>

@endsection

