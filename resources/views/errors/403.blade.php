<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>.::Miapuestaweb::.</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0 minimum-scale=1">
    <meta name="description" content="">
    @if (Auth::guest())
    <meta http-equiv="refresh" content="3601">
    @endif
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link href="{{asset('scripts/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('scripts/bootstrap/css/bootstrap-responsive.min.css')}}" rel="stylesheet">
    <link href="{{url('scripts/bootstrap/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
    <link href="{{url('scripts/bootstrap/css/bootstrap-datetimepicker-standalone.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{url('scripts/fontawesome/css/font-awesome.min.css')}}">
    <link href="{{url('scripts/carousel/style.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('scripts/camera/css/camera.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('styles/custom.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('assets/css/style.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('scripts/bootstrap-select/css/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{url('scripts/jquery.min.js')}}" type="text/javascript"></script>

</head>
<body id="pageBody">
    <div class="panel panel-default panel-bets col-md-8 col-md-offset-2">
        <div class="panel-heading">
            <h5>Acceso no autorizado</h5>            
        </div>

        <div class="panel-body">
            <p>La pagina que intenta acceder no está autorizada, por favor consulte con el administrador, <a href="/">Ir al inicio</a></p>
        </div>
    </div>
</body>

</html>  

