﻿@extends('layouts.app')
@section('content')

    <div class="panel panel-default panel-bets">
        <div class="panel-heading">
            <h4 class="text-center">Nosotros</h4>
        </div>
            <div class="panel-body">
                <img src="images/500px-33918005.jpg" class="img-polaroid" style="margin:5px 0px 15px;" alt="">
                <blockquote>
                <h3 class="text-error">Miapuestaweb</h3>
                </blockquote>
                <p>Somos una organización especializada en apuestas online que ofrece un servicio absolutamente integral en tiempo real, a través del cual usted podrá realizar todo tipo de apuestas deportivas por medio del área interactiva de la página</p>

                <blockquote>
                <h3 class="text-error">Misión</h3>
                </blockquote>


                <p>Satisfacer las necesidades de nuestros clientes en el menor tiempo posible y manteniendo siempre la calidad de nuestra página, conservando la oportuna respuesta ante las necesidades de cada uno de ellos, manteniéndolos al día e informados de todo lo relacionado al mundo deportivo y las apuestas.</p>

                <blockquote>
                <h3 class="text-error">Visión</h3>
                </blockquote>

                <p>Consolidar nuestro posicionamiento en un mercado altamente competitivo como lo son las casas de apuestas, reconocidas por los años de experiencia, por la envergadura de sus clientes, excelencia de sus trabajos y solvencia económica, permitiéndonos cumplir eficientemente con todas sus exigencias y requerimientos; logrando la adquisición de nuevas cuentas tanto a nivel nacional como internacional y el desarrollo de nuevas alianzas.</p>
                </div>
    </div>

@endsection