<!DOCTYPE html>
	<html>
		<head>
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width,initial-scale=1">
			<title>Vue App</title>			
			<link href="/assets/app/css/about.css" rel="prefetch">
			<link href="/assets/app/css/about~contact~help~home~playground~rule~score.css" rel="prefetch">
			<link href="/assets/app/css/about~contact~help~home~rule.css" rel="prefetch">
			<link href="/assets/app/css/about~contact~help~playground~rule.css" rel="prefetch">			
			<link href="/assets/app/css/contact.c0f0f958.css" rel="prefetch">
			<link href="/assets/app/css/help.c0fc4ccb.css" rel="prefetch">
			<link href="/assets/app/css/home.29149bd4.css" rel="prefetch">
			<link href="/assets/app/css/playground.69943d43.css" rel="prefetch">
			<link href="/assets/app/css/rule.a8760f1f.css" rel="prefetch">
			<link href="/assets/app/css/score.04812c03.css" rel="prefetch">
			<link href="/assets/app/js/about.d574436f.js" rel="prefetch">
			<link href="/assets/app/js/about~contact~help~home~playground~rule~score.9b478018.js" rel="prefetch">
			<link href="/assets/app/js/about~contact~help~home~rule.8d783e15.js" rel="prefetch">
			<link href="/assets/app/js/about~contact~help~playground~rule.599e30a7.js" rel="prefetch">
			<link href="/assets/app/js/contact.c1b7a4c7.js" rel="prefetch">
			<link href="/assets/app/js/help.a0b9058f.js" rel="prefetch">
			<link href="/assets/app/js/home.32e8b5b7.js" rel="prefetch">
			<link href="/assets/app/js/playground.2db2e48e.js" rel="prefetch">
			<link href="/assets/app/js/rule.9be0d751.js" rel="prefetch">
			<link href="/assets/app/js/score.ab5742f4.js" rel="prefetch">
			<link href="/assets/app/css/app.23d19ba0.css" rel="preload" as="style">
			<link href="/assets/app/css/chunk-vendors.ba010541.css" rel="preload" as="style">
			<link href="/assets/app/js/app.b0a8bc6b.js" rel="preload" as="script">
			<link href="/assets/app/js/chunk-vendors.788d98a5.js" rel="preload" as="script">
			<link href="/assets/app/css/chunk-vendors.ba010541.css" rel="stylesheet">
			<link href="/assets/app/css/app.23d19ba0.css" rel="stylesheet">
		</head>
		<body>
			<div id="app"></div>
			<script src="/assets/app/js/chunk-vendors.js"></script><script src="/assets/app/js/app.b0a8bc6b.js"></script></body></html>