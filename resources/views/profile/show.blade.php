@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span12">

        <div id="headerSeparator"></div>

<div class="panel panel-default panel-bets">
	<div class="panel-heading">		
	  <h4 class="text-center">Actualizar perfil</strong></h4>
	  <div class="clearfix"></div>
	</div>
	  <div class="panel-body">		
	  {!! Form::open(['route' => ['profile.update', $user], 'method' => 'PUT']) !!}	  
		<ul class="list-group col-md-8 col-md-offset-2">
			{!! Alert::render() !!}
			{!! Field::text('name', $user->name) !!}
			{!! Field::text('email', $user->email) !!}
			{!! Field::text('phone', $user->phone) !!}						
			{!! Field::select('printer', ['1' => 'POS 58 Termica', '2' => 'EPSON TM-U325 (MATRIZ DE PUNTO)', ], $user->printer) !!}
			{!! Field::select('logro', ['' => 'Seleccione tipo de logro', '0' => 'Decimal', '1' => 'Americano' ], $user->logro, ['label' => 'Tipo de logro']) !!}			 
			<br>
			 
			<input type="submit" class="btn btn-login btn-primary btn-submit" id="btn-update-profile" value="Enviar">				
			<a class="btn btn-primary btn-cancel" href="/" role="button">Cancelar</a>
		</ul>
		<div class="clearfix"></div>
		{!!  Form::close() !!}
  	</div>
  	<div class="clearfix"></div>
</div>
        <div id="headerSeparator2"></div>
    </div>
</div>
@endsection