<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>.::Miapuestaweb::.</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0 minimum-scale=1">
    <meta name="description" content="">
    @if (Auth::guest())
    <meta http-equiv="refresh" content="3601">
    @endif
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link href="{{asset('scripts/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('scripts/bootstrap/css/bootstrap-responsive.min.css')}}" rel="stylesheet">
    <link href="{{url('scripts/bootstrap/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
    <link href="{{url('scripts/bootstrap/css/bootstrap-datetimepicker-standalone.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{url('scripts/fontawesome/css/font-awesome.min.css')}}">
    <link href="{{url('scripts/carousel/style.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('scripts/camera/css/camera.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('styles/custom.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('assets/css/style.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{url('scripts/bootstrap-select/css/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css" />


</head>
<body id="pageBody">
<div class="hidden-print">

    @include('menu.navbar')
<div id="app">
    @yield('content')
</div>

    @include('partials.header')
    @include('partials.pagos')
    @include('partials.retiros')
    @include('partials.modal-loading')
    @include('partials.footer')
</div>
@include('partials.ticket')
</body>

</html>


<script src="{{url('scripts/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>

 <script src="{{url('assets/js/app.js')}}" type="text/javascript"></script>

<script src="{{url('scripts/default.js')}}" type="text/javascript"></script>

<script src="{{url('scripts/easing/jquery.easing.1.3.js')}}" type="text/javascript"></script>
<script src="{{url('scripts/moment-with-locales.min.js')}}" type="text/javascript"></script>
<script src="{{url('scripts/bootstrap/js/transition.js')}}" type="text/javascript"></script>
<script src="{{url('scripts/bootstrap/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script src="{{url('scripts/bootstrap-select/js/bootstrap-select.min.js')}}"></script>

<script type="text/javascript" src="{{url('scripts/google_analytics.js')}}"></script>

    @if (Auth::guest())
    <script src='https://www.google.com/recaptcha/api.js?hl=es'></script>
    @endif
    <script type="text/javascript" src="{{url('assets/scripts/comun.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/scripts/pagos.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/scripts/retiros.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/scripts/admin.js')}}"></script>
@yield('js')