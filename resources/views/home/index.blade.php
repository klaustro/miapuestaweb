@extends('layouts.app')
@section('content')

<div class="row-fluid">
    <div class="span12">

        <div id="headerSeparator"></div>

        <div class="camera_full_width">
            <div id="camera_wrap">
                <div data-src="{{url('slider-images/baloncesto.jpg')}}" ><div class="camera_caption fadeFromBottom cap1 capitalize">Baloncesto...</div></div>
                <div data-src="{{url('slider-images/beisbol.jpg')}}" ><div class="camera_caption fadeFromBottom cap2 capitalize">Beísbol...</div></div>
                <div data-src="{{url('slider-images/futbol.jpg')}}" ><div class="camera_caption fadeFromBottom cap1 capitalize">Fútbol...</div></div>
            </div>
            <br style="clear:both"/><div style="margin-bottom:40px"></div>
        </div>

        <div id="headerSeparator2"></div>
    </div>
</div>

@endsection

@section('js')
<script src="{{url('scripts/carousel/jquery.carouFredSel-6.2.0-packed.js')}}" type="text/javascript"></script>
<script type="text/javascript">$('#list_photos').carouFredSel({ responsive: true, width: '100%', scroll: 2, items: {width: 320,visible: {min: 2, max: 6}} });</script>
<script src="{{url('scripts/camera/scripts/camera.min.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{url('scripts/carousel/carousel.js')}}"></script>
@endsection