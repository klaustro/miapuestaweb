<li class="dropdown {{ in_array(Route::currentRouteName(), trans('adminRoutes')) ? 'active' : '' }}">
    <a  role="button" aria-expanded="false" class="not-active"><i class="fa fa-wrench" aria-hidden="true"></i>
        Admin <span class="caret caret-menu"></span>
    </a> 
    <ul class="dropdown-menu sub-menu">

        <li class="dropdown">
            <a  role="button" aria-expanded="false" class="not-active"><i class="fa fa-gamepad" aria-hidden="true"></i>
                Juegos <i class="fa fa-caret-right" aria-hidden="true"></i></span>
            </a>
                <ul class="dropdown-menu sub-menu">           
                    </li>     
                    <li>
                        <a href="{{url('owner/bet')}}"><i class="fa fa-btn fa-ticket"></i> Apuestas</a>
                    </li>            
                    <li>
                        <a href="{{url('owner/betBalance')}}"><i class="fa fa-btn fa-ticket"></i> Balance Apuestas</a>
                    </li>                                             
                </ul>
        </li>       

        <li class="dropdown"> 
            <a  role="button" aria-expanded="false" class="not-active"><i class="fa fa-suitcase" aria-hidden="true"></i>
                Corredores <i class="fa fa-caret-right" aria-hidden="true"></i></span>
            </a>
                <ul class="dropdown-menu sub-menu">                                                                                                                      
                    <li>
                        <a href="{{url('/owner/runner')}}"><i class="fa fa-btn fa-dollar"></i> Comisiones por Corredor</a>
                    </li>                           
                </ul>
        </li>       
    </ul>
</li>
