<li class="tool-bar {{ Route::currentRouteNamed('contact') ? 'active' : '' }}" id="contacto">
    <a href="/contact"  id="contact"><i class="fa fa-phone" aria-hidden="true" class=""></i> Contacto</a>
</li>
<li class="tool-bar" id="li-login">
    <a href="#" data-toggle="modal" data-target="#loginModal"><i class="fa fa-user" aria-hidden="true"></i> Inicia sesión</a>
</li>

<li class="tool-bar" id="li-register">
    <a href="#" data-toggle="modal" data-target="#registerModal"><i class="fa fa-keyboard-o" aria-hidden="true"></i> Registro</a>
</li>
