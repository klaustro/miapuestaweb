<div id="divBoxed" class="container">

    <div class="divPanel notop nobottom">
            <div class="row-fluid">
                <div class="span12">

                    <div id="divLogo" class="pull-left">
                        <a href="/" id="divSiteTitle" class="title-logo"><img class="img-responsive logopru" src="{{url('images/logo.png')}}"></a><br />
                    </div>

                    <div id="divMenuRight" class="pull-right">
                    <div class="navbar">
                        <button type="button"
                            class="btn btn-navbar-highlight btn-large btn-primary"
                            id="btn-menu"
                            data-toggle="collapse"
                            data-target=".nav-collapse">
                                <i class="fa fa-bars" aria-hidden="true"></i> Menu
                        </button>
                        <div class="nav-collapse collapse">
                            <ul class="nav nav-pills ddmenu">

                            @include('menu.everyone')

                            @if (!Auth::guest())

                                @include('menu.auth')

                            @endif
                            </ul>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
<!-- @yield('content') -->
    </div>

<div id="modal-login"></div>