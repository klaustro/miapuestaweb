<li class="dropdown {{ in_array(Route::currentRouteName(), trans('adminRoutes')) ? 'active' : '' }}">
    <a  role="button" aria-expanded="false" class="not-active"><i class="fa fa-wrench" aria-hidden="true"></i>
        Admin <span class="caret caret-menu"></span>
    </a> 
    <ul class="dropdown-menu sub-menu">

        <li class="dropdown">
            <a  role="button" aria-expanded="false" class="not-active"><i class="fa fa-database" aria-hidden="true"></i>
                Datos <i class="fa fa-caret-right" aria-hidden="true"></i></span>
            </a>
                <ul class="dropdown-menu sub-menu">
                    <li>
                        <a href="{{url('admin/menu')}}"><i class="fa fa-btn fa-th-large"></i> Categorias</a>
                    </li>
                    <li>
                        <a href="{{url('admin/submenu')}}"><i class="fa fa-btn fa-th"></i> Ligas</a>
                    </li>           
                    <li>
                        <a href="{{url('admin/team')}}"><i class="fa fa-btn fa-sitemap"></i> Equipos</a>
                    </li>            
                    <li>
                        <a href="{{url('/admin/user')}}"><i class="fa fa-btn fa-users"></i> Usuarios</a>
                    </li> 				
                </ul>
        </li>         

        <li class="dropdown">
            <a  role="button" aria-expanded="false" class="not-active"><i class="fa fa-gamepad" aria-hidden="true"></i>
                Juegos <i class="fa fa-caret-right" aria-hidden="true"></i></span>
            </a>
                <ul class="dropdown-menu sub-menu">
                    <li>
                        <a href="{{url('admin/game')}}"><i class="fa fa-btn fa-gamepad"></i> Lista de juegos</a>
                    </li>                  
                    </li>     
                    <li>
                        <a href="{{url('owner/bet')}}"><i class="fa fa-btn fa-ticket"></i> Apuestas</a>
                    </li>        
                    <li>
                        <a href="{{url('owner/betBalance')}}"><i class="fa fa-btn fa-dollar"></i> Balance Apuestas</a>
                    </li>                                               
                </ul>
        </li>     

        <li class="dropdown">
            <a  role="button" aria-expanded="false" class="not-active"><i class="fa fa-bank" aria-hidden="true"></i>
                Finanzas <i class="fa fa-caret-right" aria-hidden="true"></i></span>
            </a>
                <ul class="dropdown-menu sub-menu">
                    <li>
                        <a href="{{url('/admin/pago')}}"><i class="fa fa-btn fa-upload"></i> Pagos</a>
                    </li>              
                    <li>
                        <a href="{{url('/admin/retiro')}}"><i class="fa fa-btn fa-download"></i> Retiros</a>
                    </li>                                                      
                </ul>
        </li> 

        <li class="dropdown">
            <a  role="button" aria-expanded="false" class="not-active"><i class="fa fa-cogs" aria-hidden="true"></i>
                Ajustes <i class="fa fa-caret-right" aria-hidden="true"></i></span>
            </a>
                <ul class="dropdown-menu sub-menu">
                    <li>
                        <a href="{{url('admin/typebet')}}"><i class="fa fa-newspaper-o"></i> Tipo de Apuestas</a>
                    </li>  
                    <li>
                        <a href="{{url('admin/bank')}}"><i class="fa fa-table"></i> Tablas del Sistema</a>
                    </li>      
                </ul>
        </li>           

        <li class="dropdown"> 
            <a  role="button" aria-expanded="false" class="not-active"><i class="fa fa-suitcase" aria-hidden="true"></i>
                Corredores <i class="fa fa-caret-right" aria-hidden="true"></i></span>
            </a>
                <ul class="dropdown-menu sub-menu">
                    <li>
                        <a href="{{url('/admin/comision')}}"><i class="fa fa-btn fa-percent"></i> Comisiones</a>
                    </li>                                                                                                                            
                    <li>
                        <a href="{{url('/owner/runner')}}"><i class="fa fa-btn fa-dollar"></i> Comisiones por Corredor</a>
                    </li>                           
                </ul>
        </li>
		<li>
			<a href="{{url('/logout')}}"><i class="fa fa-btn fa-users"></i> Cerrar Sesion</a>
		</li> 	
    </ul>
</li>


