<input type="hidden" id="tipoLogro" value="{{Auth::user()->logro}}">

@if(Auth::user()->role == 'admin')
    @include('menu.admin')
@elseif(Auth::user()->role == 'owner')
    @include('menu.owner')
@endif
