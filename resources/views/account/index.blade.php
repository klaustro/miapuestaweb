@extends('layouts.app')
@section('content')

	<div class="panel panel-default panel-bets">
		<div class="panel-heading">
		    <p class="pull-left">Mis Cuentas Bancarias</p>
		    <p class="pull-right">
		<a href="##" class="text-right txt-saldo" id="saldo" data-saldo="{{ Auth::user()->saldo }}" data-toggle="tooltip" data-placement="bottom" title="Monto arriesgado: {{ Auth::user()->arriesgado }}"><i class="fa fa-money" aria-hidden="true"></i> {{ number_format(Auth::user()->saldo, 2) }}</a>
		    <button class="btn btn-success btn-xs" onclick="add()" data-toggle="popover" title="Agregar cuenta"><i class="fa fa-plus" aria-hidden="true"></i> </button></p>
		    <div class='clearfix'></div>
		<div class="clearfix"></div>
		</div>
			<div class="panel-body" id="panel-cuentas">
			</div>
	</div>

@include('partials.modal-cuenta')
@include('partials.modal-confirmation')


@endsection
@section('js')
	<script type="text/javascript" src="{{url('assets/scripts/cuentas.js')}}"></script>
	<script>
		$(function() {
			cargar();

		});	
	</script>
@endsection