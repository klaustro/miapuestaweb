@extends('layouts.app')
@section('content')

	<div class="panel panel-default panel-bets">
		<div class="panel-heading">
			<div class="text-left col-md-3" id="west">
		<div class="input-group">
			<input type="text" class="form-control" placeholder="Buscar ticket" id="ticket-id">
			<span class="input-group-btn">
				<button class="btn btn-default search" type="button" onclick="searchHistory()"><span class="fa fa-search text-center"></span></button>
			</span>
		</div>
		<input type="hidden" id="corredor" value="{{ Auth::user()->corredor }}">
			</div>
		 	<div class="text-left col-md-3" id="west">
				<select class="form-control search" id="select-weeks" onChange="searchHistory()">
				  <option value="0" selected>Apuestas de la semana</option>
				  <option value="1">Apuestas de la semana pasada</option>
				  <option value="2">Apuestas de hace 2 semanas</option>
				  <option value="3">Apuestas de hace 3 semanas</option>
				  <option value="4">Apuestas de hace 4 semanas</option>
				  <option value="5">Apuestas de hace 5 semanas</option>
				</select>			
			</div>
			<div class="text-center col-md-3" id="west">
				<select class="form-control search" id="select-status" onChange="searchHistory()">
				  <option value="0" selected>Todas las Apuestas</option>
				  <option value="1">Apuestas Pendientes</option>
				  <option value="2">Apuestas Ganadas</option>
				  <option value="3">Apuestas Perdidas</option>
				  <option value="5">Apuestas Pagadas</option>
				</select>
			</div>

			<div class="text-right" id="east">
			@if(Auth::user()->corredor == 0)
			<a href="##" class="text-right txt-saldo" id="saldo" data-saldo="{{ Auth::user()->saldo }}" data-toggle="tooltip" data-placement="bottom" title="Monto arriesgado: {{ Auth::user()->arriesgado }}"><i class="fa fa-money" aria-hidden="true"></i> {{ number_format(Auth::user()->saldo, 2) }}</a>
			@endif
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="panel-body" id="panel-historial"></div>
	</div>


@endsection
@section('js')
	<script type="text/javascript" src="{{url('assets/scripts/historial.js')}}"></script>
	<script>
		$(function() {
			cargarHistory(0, 0, 1, '');
		});
	</script>
@endsection