<?php

use App\Http\Controllers\BetController;
use App\Http\Controllers\GameController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\PagoController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\BancoController;
use App\Http\Controllers\RetiroController;
use App\Http\Controllers\HistoryController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\CuentasBancariaController;
use App\Http\Controllers\CurrencyController;
use App\Http\Controllers\ContactController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [LoginController::class, 'authenticate']);
Route::post('register', [RegisterController::class, 'register']);
Route::get('logout', [LoginController::class, 'logout']);
Route::get('checkMail', [[UserController::class, 'checkMail']]);

Route::post('contacto', [ContactController::class, 'contactMail']);

Route::group(['middleware' => ['auth:sanctum']], function () {
	Route::get('/categories', [GameController::class, 'gameCategories']);
	Route::get('/submenu', ['as' => 'apiSubmenu', 'uses' => 'SubmenuController@index']);
	Route::get('/game', [GameController::class, 'index']);
	Route::get('/games_prob', ['as' => 'apiGamesProb', 'uses' => 'GamesProbController@index']);
	Route::post('/bet', ['as' => 'apiBet', 'uses' => 'BetController@store']);
	Route::get('/bet/{id}', [BetController::class, 'show']);
	Route::get('/rule', ['as' => 'apiRule', 'uses' => 'ReglaController@index']);
	Route::get('/banks', [BancoController::class, 'index']);
	Route::post('/payment', [PagoController::class, 'store']);
	Route::post('/withdrawal', [RetiroController::class, 'store']);
	Route::post('/account', [CuentasBancariaController::class, 'store']);
	Route::put('/account/{account}', [CuentasBancariaController::class, 'update']);
	Route::delete('/account/{account}', [CuentasBancariaController::class, 'destroy']);
	Route::put('/profile/{user}', [ProfileController::class, 'update']);
	Route::get('/history', [HistoryController::class, 'list']);
	Route::get('/balance', [UserController::class, 'balance']);
	Route::get('/currencies', [CurrencyController::class, 'index']);
});
