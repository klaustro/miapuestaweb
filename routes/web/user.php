<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//HOME

use App\Http\Controllers\ExchangeController;

Route::get('resultado', ['as' => 'resultado', 'uses' => 'ResultadoController@apiIndex']);

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');


//API
Route::group(['middleware' => ['auth',]], function () {
	Route::resource('bet', 'BetController');
	Route::resource('submenu', 'SubmenuController', ['only' => ['index', 'show']]);
	Route::get('betsubmenu', ['uses' => 'SubmenuController@index']);
	Route::resource('game', 'GameController');
	Route::resource('equipo', 'EquipoController');
	Route::resource('games_prob', 'GamesProbController');
	Route::resource('regla', 'ReglaController');
	Route::resource('menu', 'MenuController', ['only' => ['index', 'show']]);
	Route::resource('banco', 'BancoController');
	Route::resource('pago', 'PagoController',  ['only' => ['store']]);
	Route::resource('retiro', 'RetiroController',  ['only' => ['store']]);
	Route::get('history', ['as' => 'user_bets', 'uses' => 'HistoryController@index']);
	Route::get('ticket', ['as' => 'ticket', 'uses' => 'BetController@ticket']);
});

Route::get('exchange', [ExchangeController::class, 'getExchange']);

Route::auth();
Route::get('/logout', 'Auth\LoginController@logout');

//VUE ROUTER

//ATHORIZATIONS
Route::get('password/email', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset', 'Auth\ResetPasswordController@showResetForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::any('/{any}', 'FrontendController@app')->where('any', '^(?!api).*$');

//COMPATIBILITY
Route::get('/prototipo', ['as' => 'home', 'uses' => 'HomeController@index']);


//TEMPLATES
Route::get('logros', ['as' => 'logros', 'uses' => 'TemplateController@logros']);

//New
Route::get('result', ['as' => 'result', 'uses' => 'ResultadoController@index']);
Route::get('contact', ['as' => 'contact', 'uses' => 'ContactController@index']);

Route::get('help', ['as' => 'help', 'uses' => function () {
	return view('help.index');
}]);

Route::get('who', function () {
	return view('who.index');
});

Route::get('rule', function () {
	return view('rule.index');
});

Route::get('autogame', 'Admin\AutoGameController@mlbGames');

Route::get('historial', function () {
	return view('history.index');
});

Route::get('cuentas', function () {
	return view('account.index');
});

//PERFIL DE USUARIO
Route::get('profile', ['as' => 'profile.show', 'uses' => 'ProfileController@show']);
Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);




Route::resource('cuentasbancarias', 'CuentasBancariaController');
Route::get('resultado', ['as' => 'resultado', 'uses' => 'ResultadoController@apiIndex']);

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
