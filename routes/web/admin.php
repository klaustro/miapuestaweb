<?php
//Admin Routes
//Comision

use App\Http\Controllers\ExchangeController;

Route::get('/', ['as' => 'admin.menu.index', 'uses' => 'GameController@index']);
Route::get('comision', ['as' => 'admin.comision.index', 'uses' => 'ComisionController@index']);
Route::post('comision', ['as' => 'admin.comision.store', 'uses' => 'ComisionController@store']);
Route::get('comision/create', ['as' => 'admin.comision.create', 'uses' => 'ComisionController@create']);
Route::get('comision/{comision}', ['as' => 'admin.comision.show', 'uses' => 'ComisionController@show']);
Route::put('comision/{comision}', ['as' => 'admin.comision.update', 'uses' => 'ComisionController@update']);

//User
Route::get('user', ['as' => 'admin.user.index', 'uses' => 'UserController@index']);
Route::get('user/{user}', ['as' => 'admin.user.show', 'uses' => 'UserController@show']);
Route::put('user/{user}', ['as' => 'admin.user.update', 'uses' => 'UserController@update']);

//Retiro
Route::get('retiro', ['as' => 'admin.retiro.index', 'uses' => 'DebitController@index']);
Route::get('retiro/{retiro}', ['as' => 'admin.retiro.show', 'uses' => 'DebitController@show']);
Route::put('retiro/{retiro}', ['as' => 'admin.retiro.update', 'uses' => 'DebitController@update']);

//Game
Route::get('game', ['as' => 'admin.game.index', 'uses' => 'GameController@index']);
Route::get('game/{game}', ['as' => 'admin.game.show', 'uses' => 'GameController@show']);
Route::post('result/game{game}', ['as' => 'admin.game.result', 'uses' => 'GameController@storeResults']);
Route::post('game', 'GameController@store');
Route::put('game/{game}', 'GameController@update');
Route::put('cancel/{game}', 'GameController@cancel');
Route::put('hide/{game}', 'GameController@hide');
Route::put('suspend/{game}', 'GameController@suspend');
Route::put('legal/{game}', 'GameController@legal');
Route::put('resume/{game}', 'GameController@resume');


//Menu
Route::get('menu', ['as' => 'admin.menu.index', 'uses' => 'MenuController@index']);
Route::post('menu', ['as' => 'admin.menu.store', 'uses' => 'MenuController@store']);
Route::get('menu/create', ['as' => 'admin.menu.create', 'uses' => 'MenuController@create']);
Route::get('menu/{menu}', ['as' => 'admin.menu.show', 'uses' => 'MenuController@show']);
Route::put('menu/{menu}', ['as' => 'admin.menu.update', 'uses' => 'MenuController@update']);

//Pago
Route::get('pago', ['as' => 'admin.pago.index', 'uses' => 'PayController@index']);
Route::get('pago/{pago}', ['as' => 'admin.pago.show', 'uses' => 'PayController@show']);
Route::put('pago/{pago}', ['as' => 'admin.pago.update', 'uses' => 'PayController@update']);

//Submenu
Route::get('submenu', ['as' => 'admin.submenu.index', 'uses' => 'SubmenuController@index']);
Route::post('submenu', ['as' => 'admin.submenu.store', 'uses' => 'SubmenuController@store']);
Route::get('submenu/create', ['as' => 'admin.submenu.create', 'uses' => 'SubmenuController@create']);
Route::get('submenu/{submenu}', ['as' => 'admin.submenu.show', 'uses' => 'SubmenuController@show']);
Route::put('submenu/{submenu}', ['as' => 'admin.submenu.update', 'uses' => 'SubmenuController@update']);
Route::get('submenubymenu', ['uses' => 'SubmenuController@listByMenu']);

//Equipo
Route::get('team', ['as' => 'admin.team.index', 'uses' => 'TeamController@index']);
Route::post('team', ['as' => 'admin.team.store', 'uses' => 'TeamController@store']);
Route::get('team/create', ['as' => 'admin.team.create', 'uses' => 'TeamController@create']);
Route::get('team/{team}', ['as' => 'admin.team.show', 'uses' => 'TeamController@show']);
Route::put('team/{team}', ['as' => 'admin.team.update', 'uses' => 'TeamController@update']);

//Tipo Apuestas
Route::get('typebet', ['as' => 'admin.typebet.index', 'uses' => 'TypeBetController@index']);
Route::get('typebet/create', ['as' => 'admin.typebet.create', 'uses' => 'TypeBetController@create']);
Route::post('typebet', ['as' => 'admin.typebet.store', 'uses' => 'TypeBetController@store']);
Route::get('typebet/{typebet}', ['as' => 'admin.typebet.show', 'uses' => 'TypeBetController@show']);
Route::put('typebet/{typebet}', ['as' => 'admin.typebet.update', 'uses' => 'TypeBetController@update']);
Route::get('categoryTypeBet/{category}', ['as' => 'admin.typebet.category', 'uses' => 'TypeBetController@categoryTypeBet']);

//Bancos
Route::get('bank', ['as' => 'admin.bank.index', 'uses' => 'BankController@index']);
Route::post('bank', ['as' => 'admin.bank.store', 'uses' => 'BankController@store']);
Route::get('bank/create', ['as' => 'admin.bank.create', 'uses' => 'BankController@create']);
Route::get('bank/{bank}', ['as' => 'admin.bank.show', 'uses' => 'BankController@show']);
Route::put('bank/{bank}', ['as' => 'admin.bank.update', 'uses' => 'BankController@update']);

//Empresas
Route::get('enterprise', ['as' => 'admin.enterprise.index', 'uses' => 'EnterpriseController@index']);
Route::post('enterprise', ['as' => 'admin.enterprise.store', 'uses' => 'EnterpriseController@store']);
Route::get('enterprise/create', ['as' => 'admin.enterprise.create', 'uses' => 'EnterpriseController@create']);
Route::get('enterprise/{enterprise}', ['as' => 'admin.enterprise.show', 'uses' => 'EnterpriseController@show']);
Route::put('enterprise/{enterprise}', ['as' => 'admin.enterprise.update', 'uses' => 'EnterpriseController@update']);

//Tipo de comision
Route::get('type/comision', ['as' => 'admin.type.comision.index', 'uses' => 'TipoComisionController@index']);
Route::post('type/comision', ['as' => 'admin.type.comision.store', 'uses' => 'TipoComisionController@store']);
Route::get('type/comision/create', ['as' => 'admin.type.comision.create', 'uses' => 'TipoComisionController@create']);
Route::get('type/comision/{enterprise}', ['as' => 'admin.type.comision.show', 'uses' => 'TipoComisionController@show']);
Route::put('type/comision/{enterprise}', ['as' => 'admin.type.comision.update', 'uses' => 'TipoComisionController@update']);

//Exchange
Route::get('updateExchange', [ExchangeController::class, 'getExchange']);

//Logs
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
