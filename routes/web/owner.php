<?php 
//Bet
Route::get('bet', ['as' => 'admin.bet.index', 'uses' => 'BetController@index']);
Route::get('bet/{bet}', ['as' => 'admin.bet.show', 'uses' => 'BetController@show']);
Route::put('bet/{bet}', ['as' => 'admin.bet.update', 'uses' => 'BetController@update']);
Route::get('betBalance', ['as' => 'admin.bet.balance', 'uses' => 'BetController@balance']);


//Corredor 
Route::get('runner', ['as' => 'admin.runner.index', 'uses' => 'RunnerController@index']);   

