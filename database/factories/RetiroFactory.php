<?php

use Faker\Generator as Faker;

$factory->define(App\Retiro::class, function (Faker $faker) {
	return[
		'id_usuario' => function (){
        		return factory(\App\User::class)->create()->id;
   		 } ,
   		 'cuenta_id' => $faker->unique()->randomNumber,
		'fecha' =>  date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
		'fecha_hora' =>  date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
		'monto' => $faker->numberBetween(100, 9000),
		'status' => $faker->numberBetween(0, 1),
		'fecha_proceso' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
	];
});
