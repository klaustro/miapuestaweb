<?php

use Faker\Generator as Faker;

$factory->define(App\Comision::class, function(Faker $faker){
    return[
        'tipo' =>  function(){
            return factory(App\TipoComision::class)->create()->id;
        } ,
        'desde' => $faker->numberBetween(1, 100),
        'hasta' => $faker->numberBetween(1, 100),
        'comision' => $faker->randomFloat(2, $min = 0, $max = 15),  
    ];
});
