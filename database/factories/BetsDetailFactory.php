<?php

use Faker\Generator as Faker;

$factory->define(App\BetsDetail::class, function(Faker $faker){
    return[
        'idbets' => function(){
            return factory(App\Bet::class)->create()->id;
        }        ,
        'idgames_probs' => function(){
            return factory(App\GamesProb::class)->create()->id;
        } ,
    ];
});
