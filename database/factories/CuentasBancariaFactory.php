<?php

use App\CuentasBancaria;
use Faker\Generator as Faker;

$factory->define(CuentasBancaria::class, function (Faker $faker) {
    return [
        'banco_id' => $faker->numberBetween(),
        'user_id' => $faker->numberBetween(),
        'nrocuenta' => $faker->bankAccountNumber,
        'tipo' => $faker->randomElement(['Ahorro','Corriente','FAL']),          
        
    ];
});
