<?php

use Faker\Generator as Faker;

$factory->define(App\Pago::class, function (Faker $faker) {
	return[
		'id_usuario' => function (){
            		return factory(\App\User::class)->create()->id;
       		 } ,
		'fecha_hora' =>  date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
		'monto' => $faker->numberBetween(100, 9000),
		'status' => $faker->numberBetween(0, 1),
		'tipo' => $faker->numberBetween(1, 2),
		'referencia' => $faker->randomNumber(),
		'banco' => $faker->numberBetween(1, 20),
		'titular' => $faker->name(),
		'ultimos_digitos' => $faker->numberBetween(1000, 9999),
		'fecha_confirmacion' =>date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
	];
});
