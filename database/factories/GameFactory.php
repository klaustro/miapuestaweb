<?php

use Faker\Generator as Faker;

$factory->define(App\Game::class, function(Faker $faker){
    return[
        'categoria' => function (){
            return factory(\App\Menu::class)->create()->id;
        } ,
        'subcategoria' => function (){
            return factory(\App\Submenu::class)->create()->id;
        } ,
        'compuesta' => $faker->numberBetween(0, 1),
        'fecha_registro' =>date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
        'fecha_cierre' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
        'status' => function (){
            return factory(\App\Stsgame::class)->create()->id;
        },
        'visible' => $faker->numberBetween(0, 1),
    ];
});