<?php

use Faker\Generator as Faker;

$factory->define(App\GamesProb::class, function(Faker $faker){
    return[
        'idgame' => function (){
            return factory(\App\Game::class)->create()->id;
        } ,
        'fecha_registro' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
        'tipo_probabilidad' => function(){
            return factory(\App\TipoApuesta::class)->create()->id; 
        },
        'equipo' => function (){
            return factory(\App\Equipo::class)->create()->id;
        } , 
        'orden' => $faker->numberBetween(0, 1),
        'cantidad' => $faker->numberBetween(0, 10),
        'probabilidad' => $faker->randomFloat(2, $min = 0, $max = 1),       
        'activa' => $faker->numberBetween(0, 1),
        'acierto' => $faker->numberBetween(0, 1),
    ];
});