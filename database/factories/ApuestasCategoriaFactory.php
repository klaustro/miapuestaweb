<?php

use App\ApuestasCategoria;
use Faker\Generator as Faker;

$factory->define(ApuestasCategoria::class, function (Faker $faker) {
    return [
        'tipo_apuesta' => $faker->numberBetween(1,5),
        'categoria' => $faker->numberBetween(1,5),
    ];
});
