<?php

use Faker\Generator as Faker;

$factory->define(App\Stsgame::class, function(Faker $faker){
    return [
        'id' => $faker->randomNumber,
        'nombre' => $faker->word(),
    ];
});
