<?php

use Faker\Generator as Faker;

$factory->define(App\Submenu::class, function(Faker $faker){
    return[
        'nombre' => $faker->word(),
        'id_menu' => function (){
            return factory(\App\Menu::class)->create()->id;
        } ,
        'limite_directa' => $faker->numberBetween(0, 99999),
        'limite_compuesta' => $faker->numberBetween(0, 99999),
        'orden' => $faker->numberBetween(0, 1),
        'mitad' => $faker->numberBetween(0, 1),
    ];
});
