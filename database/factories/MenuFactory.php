<?php

use Faker\Generator as Faker;

$factory->define(App\Menu::class, function(Faker $faker){
    return[
        'nombre' => $faker->word(),
        'orden' => $faker->numberBetween(0, 1),
        'nota' => $faker->sentence(),
    ];
});