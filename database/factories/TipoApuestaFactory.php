<?php

use Faker\Generator as Faker;

$factory->define(App\TipoApuesta::class, function(Faker $faker){
    return[
           'nombre'  => $faker->word(),
           'max_apuesta'  => 400000,
           'exotica'  =>  $faker->numberBetween(0, 1),
           'cantidad'  =>  $faker->numberBetween(1, 30),
           'probabilidades'  =>  $faker->numberBetween(1, 2),
           'pk'  =>  $faker->numberBetween(0, 1),
    ];
});