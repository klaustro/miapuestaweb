<?php

use Faker\Generator as Faker;

$factory->define(App\Bet::class, function(Faker $faker){
    return[
                        'fecha_registro' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
                        'fecha' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
                        'usuario' => function (){
                            return factory(\App\User::class)->create()->id;
                        } ,
                        'compuesta' => $faker->numberBetween(0, 1),
                        'monto' => $faker->numberBetween(100, 9000),
                        'ganancia' =>  $faker->numberBetween(100, 9000),
                        'web' => $faker->numberBetween(0, 1),
                        'status' => function (){
                            return factory(\App\Stsbet::class)->create()->id;
                        } 
    ];
});