<?php

use Faker\Generator as Faker;

$factory->define(App\Equipo::class, function(Faker $faker){
    return[
           'nombre'  => $faker->word(),
           'id_menu'  => 0,
           'id_submenu'  => 0,
           'orden'  => 0
    ];
});