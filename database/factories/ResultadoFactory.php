<?php

use Faker\Generator as Faker;

$factory->define(App\Resultado::class, function(Faker $faker){
    return[
        'tipo'  => $faker->numberBetween(1,5),
        'idgame' => function (){
            return factory(\App\Game::class)->create()->id;
        } ,
        'equipo' => function (){
            return factory(\App\GamesProbs::class)->create()->equipo;
        } ,         
        'score'  => $faker->randomDigitNotNull,
    ];
});
