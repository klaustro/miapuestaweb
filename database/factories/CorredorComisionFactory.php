<?php

use Faker\Generator as Faker;

$factory->define(App\CorredorComision::class, function(Faker $faker){
    return[
            'idusuario' => $faker->unique->randomNumber,
            'idtipo_comision' => $faker->randomNumber,
            'ventas' => $faker->numberBetween(0, 1),    
    ];
});
