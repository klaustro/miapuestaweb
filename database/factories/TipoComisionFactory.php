<?php

use Faker\Generator as Faker;

$factory->define(App\TipoComision::class, function(Faker $faker){
    return[
        'nombre' => $faker->word()
    ];
});