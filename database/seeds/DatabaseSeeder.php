<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$this->call(MenuSeeder::class);
		$this->call(SubMenuSeeder::class);
		$this->call(AnimalSeeder::class);
		$this->call(TipoApuestaSeeder::class);
		$this->call(ApuestasCategoriaSeeder::class);
	}
}
