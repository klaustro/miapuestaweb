<?php

namespace Database\Seeders;

use App\TipoApuesta;
use Illuminate\Database\Seeder;

class TipoApuestaSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		factory(TipoApuesta::class)->create([
			'id' => 19,
			'nombre'  => 'SORTEO',
			'max_apuesta'  => 400000,
			'exotica'  =>  0,
			'cantidad'  =>  0,
			'probabilidades'  =>  2,
		]);
	}
}
