<?php

namespace Database\Seeders;

use App\Submenu;
use Illuminate\Database\Seeder;

class SubMenuSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		factory(Submenu::class)->create([
			'id' => 187,
			'id_menu' => 12,
			'nombre' => 'LOTTO ACTIVO',
		]);
	}
}
