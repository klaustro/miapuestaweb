<?php

namespace Database\Seeders;

use App\ApuestasCategoria;
use Illuminate\Database\Seeder;

class ApuestasCategoriaSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		factory(ApuestasCategoria::class)->create([
			'tipo_apuesta' => 19,
			'categoria' => 12,
		]);
	}
}
