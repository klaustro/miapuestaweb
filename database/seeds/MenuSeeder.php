<?php

namespace Database\Seeders;

use App\Menu;
use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		factory(Menu::class)->create([
			'id' => 12,
			'nombre' => 'JUEGOS DE AZAR',
		]);
	}
}
