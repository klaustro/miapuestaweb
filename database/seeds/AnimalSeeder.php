<?php

namespace Database\Seeders;

use App\Equipo;
use Illuminate\Database\Seeder;

class AnimalSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$team = factory(Equipo::class)->create(['nombre' => 'Araña', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Culebra', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Carnero', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Toro', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Cienpies', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Alacran', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Leon', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Sapo', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Loro', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Raton', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Aguila', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Tigre', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Gato', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Caballo', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Mono', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Paloma', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Zorro', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Oso', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Pavo', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Burro', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Cabra', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Puerco', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Gallo', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Camello', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Cebra', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Iguana', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Gallina', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Vaca', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Perro', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Zamuro', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Elefante', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Caiman', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Lapa', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Murcielago', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Panda', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Pulpo', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Tiburon', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);

		$team = factory(Equipo::class)->create(['nombre' => 'Hormiga', 'abbr' => '']);
		$team->equipoSubcategoria()->create(['id_submenu' => 187]);
	}
}
