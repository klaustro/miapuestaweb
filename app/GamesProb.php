<?php

namespace App;

use App\BetsDetail;
use Illuminate\Database\Eloquent\Model;

class GamesProb extends Model
{
	protected $table = 'games_probs';

	protected $fillable = [
		'idgame', 'fecha_registro', 'tipo_probabilidad', 'equipo', 'orden', 'cantidad', 'probabilidad', 'activa',
	];

	protected $appends = [];

	public $timestamps = false;

	public function equipo()
	{
		return $this->belongsTo(Equipo::class, 'equipo')->select(array('id', 'nombre'));
	}

	public function team()
	{
		return $this->belongsTo(Equipo::class, 'equipo');
	}

	public function tipoApuesta()
	{
		return $this->belongsTo(TipoApuesta::class, 'tipo_probabilidad');
	}

	public function reglas()
	{
		return $this->belongsTo(Regla::class, 'tipo_probabilidad', 'tipo_apuesta');
	}

	public function game()
	{
		return $this->belongsTo(Game::class, 'idgame');
	}

	public function games_probs_signo()
	{
		return $this->hasOne(GamesProbsSigno::class);
	}

	public function betsDetail()
	{
		return $this->hasMany(BetsDetail::class, 'idgames_probs');
	}
	//Attributes


	public function getSelectionAttribute()
	{
		if (in_array($this->tipo_probabilidad, ['1', '8', '11', '15'])) {
			return $this->team->nombre . ' ' . $this->probabilidad;
		} else if (in_array($this->tipo_probabilidad, ['3', '7', '12', '16'])) {
			$cantidad = $this->cantidad > 0 ? $this->games_probs_signo->signo . $this->cantidad : 'PK';
			return $this->team->nombre . ' ' .  $cantidad . ' ' . $this->probabilidad;
		} else if (in_array($this->tipo_probabilidad, ['4', '10', '13', '17', '18'])) {
			$signo = $this->orden == 1 ? '+' : '-';
			return  $signo . $this->cantidad . ' ' . $this->probabilidad;
		} else if (in_array($this->tipo_probabilidad, ['6', '14'])) {
			return $this->probabilidad;
		} else if (in_array($this->tipo_probabilidad, ['9'])) {
			$siNo = $this->orden == 1 ? 'SI' : 'NO';
			return $siNo . ' ' . $this->probabilidad;
		} else if (in_array($this->tipo_probabilidad, ['19'])) {
			return $this->team->nombre;
		} else {
			return '';
		}
	}

	public function getDescriptionAttribute()
	{
		if (in_array($this->tipo_probabilidad, ['1', '3', '7', '8', '11', '12', '15', '16'])) {
			return	$this->team->nombre;
		} else if (in_array($this->tipo_probabilidad, ['4', '10', '13', '17', '18'])) {
			return $this->orden == 1 ? '+' : '-';
		} else if (in_array($this->tipo_probabilidad, ['9'])) {
			return $this->orden == 1 ? 'SI' : 'NO';
		} else {
			return '';
		}
	}
}
