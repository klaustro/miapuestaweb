<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
	protected $fillable = [
		'categoria', 'subcategoria', 'compuesta', 'fecha_registro', 'fecha_cierre', 'status', 'visible',
	];

	protected $appends = ['event',];

	public $timestamps = false;

	//Relationship

	public function games_probs()
	{
		return $this->hasMany('App\GamesProb', 'idgame')
			->where('activa', '1')
			->whereIn('tipo_probabilidad', [1, 3, 4, 6, 19]);
	}



	public function gamesProbs()
	{
		return $this->hasMany(GamesProb::class, 'idgame');
	}

	public function probs()
	{
		return $this->hasMany(GamesProb::class, 'idgame')->where('activa', '1');
	}

	public function equipo()
	{
		return $this->belongsToMany('\App\Equipo', 'games_probs', 'idgame', 'equipo')
			->withPivot('id', 'orden')
			->wherePivot('activa', 1)
			->wherePivotIn('tipo_probabilidad', ['1', '19']);
	}

	public function teams()
	{
		return $this->belongsToMany('\App\Equipo', 'games_probs', 'idgame', 'equipo')
			->where('games_probs.activa', 1)
			->whereIn('games_probs.tipo_probabilidad', [1, 19]);
	}

	public function menu()
	{
		return $this->belongsTo('\App\Menu', 'categoria');
	}

	public function submenu()
	{
		return $this->belongsTo('\App\Submenu', 'subcategoria');
	}

	public function resultado()
	{
		return $this->hasMany('App\Resultado', 'idgame');
	}

	public function stsgame()
	{
		return $this->belongsTo('\App\Stsgame', 'status');
	}

	//Attributes

	public function getBetsCountAttribute()
	{
		$bets = 0;
		foreach ($this->gamesProbs as $games_prob) {
			$bets += $games_prob->betsDetail->count();
		}
		return $bets;
	}

	public function getMaleAttribute()
	{
		$max = $this->gamesProbs->where('tipo_probabilidad', 1)->where('activa', 1)->min('probabilidad');

		return $this->gamesProbs->where('tipo_probabilidad', 1)->where('activa', 1)->where('probabilidad', $max)->last();
	}

	public function getFemaleAttribute()
	{
		$max = $this->gamesProbs->where('tipo_probabilidad', 1)->where('activa', 1)->max('probabilidad');

		return $this->gamesProbs->where('tipo_probabilidad', 1)->where('activa', 1)->where('probabilidad', $max)->last();
	}

	public function getHalfMaleAttribute()
	{
		$min = $this->gamesProbs->where('tipo_probabilidad', 1)->where('activa', 1)->min('probabilidad');

		return $this->gamesProbs->where('tipo_probabilidad', 1)->where('activa', 1)->where('probabilidad', $min)->last();
	}

	public function getHalfFemaleAttribute()
	{
		$max = $this->gamesProbs->where('tipo_probabilidad', 1)->where('activa', 1)->max('probabilidad');

		return $this->gamesProbs->where('tipo_probabilidad', 1)->where('activa', 1)->where('probabilidad', $max)->last();
	}

	public function exoticasCount()
	{
		return $this->hasOne('App\GamesProb', 'idgame')
			->whereBetween('tipo_probabilidad', ['7', '18'])
			->where('activa', '1')
			->selectRaw('idgame, round(count(*)/2) as count')
			->groupBy('idgame');
	}

	public function getEventAttribute()
	{
		$event = '';
		if ($this->teams->count() > 2) {
			$date = date_create($this->fecha_cierre);
			$event = substr($this->submenu->nombre, 0, 3)  . ': ' . date_format($date, 'h');
		} else {
			foreach ($this->teams as $key => $team) {
				$score = isset($this->finalResult[$team->id]) ? $this->finalResult[$team->id] : '';
				if ($key == 0) {
					$event .= $team->nombre . ' ' . $score . ' - ';
				} else {
					$event .= $score . ' ' . $team->nombre;
				}
			}
		}
		return $event;
	}

	public function getSportAttribute()
	{
		return $this->menu->nombre;
	}

	public function getLeagueAttribute()
	{
		return $this->submenu->nombre;
	}

	public function getFinalResultAttribute()
	{
		$results = $this->resultado->where('tipo', 1);
		$teams = [];
		foreach ($results as  $result) {
			$teams += ["{$result->equipo}" => $result->score];
		}
		return $teams;
	}

	public function getHalfResultAttribute()
	{
		$results = $this->resultado->where('tipo', 3);
		$teams = [];
		foreach ($results as  $result) {
			$teams += ["{$result->equipo}" => $result->score];
		}
		return $teams;
	}

	public function getWhoWinAttribute()
	{
		$results = $this->resultado->where('tipo', 1);

		foreach ($results as $result) {
			if ($results->max('score') == $result->score &&  !$this->is_tied) {
				return $result->equipo;
			}
		}
	}

	public function getWhoWinHalfAttribute()
	{
		$results = $this->resultado->where('tipo', 3);

		foreach ($results as $result) {
			if ($results->max('score') == $result->score &&  !$this->half_is_tied) {
				return $result->equipo;
			}
		}
	}

	public function getDifferenceAttribute()
	{
		$diff = 0;
		foreach ($this->resultado->where('tipo', 1) as $key => $result) {
			$diff = $key == 0 ? $result->score : abs($diff) - abs($result->score);
		}
		return abs($diff);
	}

	public function getHalfDifferenceAttribute()
	{
		$diff = 0;
		$talfi = [];
		foreach ($this->resultado->where('tipo', 3) as $key => $result) {
			$diff = $key == 0 ? $result->score : abs($diff) - abs($result->score);
		}
		return abs($diff);
	}

	public function getIsTiedAttribute()
	{
		return $this->difference == 0;
	}

	public function getHalfIsTiedAttribute()
	{
		return $this->half_difference == 0;
	}

	public function getTotalScoreAttribute()
	{
		return $this->resultado->where('tipo', 1)->sum('score');
	}

	public function getHalfScoreAttribute()
	{
		return $this->resultado->where('tipo', 3)->sum('score');
	}


	public function getHreResultAttribute()
	{
		return $this->resultado->where('tipo', 5)->first();
	}


	public function getWhoScoredFirstAttribute()
	{
		return $this->resultado->where('tipo', 2)->first();
	}

	public function getArrayTeamsAttribute()
	{
		return $this->teams->pluck('id')->combine($this->teams->pluck('nombre'))->all();
	}

	public function getFirstIningScoredAttribute()
	{
		return $this->resultado->where('tipo', 4)->first();
	}

	public function getWhoGoNextLevelAttribute()
	{
		return $this->resultado->where('tipo', 6)->first();
	}

	public function getWinDrawAttribute()
	{
		return count($this->resultado) > 0
			? $this->resultado->where('tipo', 7)->first()['equipo']
			: null;
	}

	public function getDateAttribute()
	{
		$date = date_create($this->fecha_cierre);
		return date_format($date, "d-m-Y");
	}

	public function getStatusNameAttribute()
	{
		$this->stsgame->nombre;
	}

	public function getHomeScoreAttribute()
	{
		$home = $this->gamesProbs->where('orden', 1)->where('tipo_probabilidad', 1)->first()->equipo;
		return $this->resultado->where('equipo', $home)->where('tipo', 1)->first()['score'];
	}

	public function getVisitScoreAttribute()
	{
		$visit = $this->gamesProbs->where('orden', 2)->where('tipo_probabilidad', 1)->first()->equipo;
		return $this->resultado->where('equipo', $visit)->where('tipo', 1)->first()['score'];
	}


	//Scopes

	public function scopeId($query, $id)
	{
		if ($id != "" and is_numeric($id))
			$query->where('id', $id);
	}

	public function scopeEvent($query, $event)
	{
		if ($event != "" and !is_numeric($event)) {
			$teams = Equipo::name($event)->get();

			$gamesProbs = GamesProb::whereIn('equipo', $teams->pluck('id'))
				->where('fecha_registro', '>', Carbon::now()->subYear())
				->get();

			$query->whereIn('id', $gamesProbs->pluck('idgame'));
		}
	}

	public function scopeCategory($query, $id)
	{
		if ($id != "")
			$query->where('categoria', $id);
	}

	public function scopeSubmenu($query, $id)
	{
		if ($id != "")
			$query->where('subcategoria', '=', "$id");
	}

	public function scopeStatus($query, $status)
	{
		if ($status != "")
			$query->where('status', '=', "$status");
	}

	//Methods
	public function ownMale($prob)
	{
		$fecha = Carbon::parse($prob->fecha_registro)->subSeconds(20);

		$min = $this->gamesProbs->where('tipo_probabilidad', in_array($prob->tipo_probabilidad, [3, 7, 12, 16]) ? 1 : 11)
			->where('fecha_registro', '>', date_format($fecha, 'Y-m-d H:i:s'))
			->where('fecha_registro', '<=', $prob->fecha_registro)
			->min('probabilidad');



		return $this->gamesProbs
			->where('tipo_probabilidad', in_array($prob->tipo_probabilidad, [3, 7, 12, 16]) ? 1 : 11)
			->where('fecha_registro', '>', date_format($fecha, 'Y-m-d H:i:s'))
			->where('fecha_registro', '<=', $prob->fecha_registro)
			->where('probabilidad', $min)
			->first();
	}
	public function ownFemale($prob)
	{
		$fecha = Carbon::parse($prob->fecha_registro)->subSeconds(20);

		$max = $this->gamesProbs->where('tipo_probabilidad', in_array($prob->tipo_probabilidad, [3, 7, 12, 16]) ? 1 : 11)
			->where('fecha_registro', '>', date_format($fecha, 'Y-m-d H:i:s'))
			->where('fecha_registro', '<=', $prob->fecha_registro)
			->max('probabilidad');

		return $this->gamesProbs
			->where('tipo_probabilidad', in_array($prob->tipo_probabilidad, [3, 7, 12, 16]) ? 1 : 11)
			->where('fecha_registro', '>', date_format($fecha, 'Y-m-d H:i:s'))
			->where('fecha_registro', '<=', $prob->fecha_registro)
			->where('probabilidad', $max)
			->first();
	}
}
