<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoApuesta extends Model
{
	protected $table = 'tipo_apuesta';

	protected $fillable = ['nombre', 'max_apuesta', 'exotica', 'cantidad', 'probabilidades', 'pk'];

	public $timestamps = false;

	public function typeBetCategory()
	{
		return $this->hasMany('\App\ApuestasCategoria', 'tipo_apuesta');		
	}

	public function scopeName($query, $name)
	{
	    if ($name != "")
	        $query->where('nombre', 'LIKE', "%$name%");
	}   

	public function getCategoriesAttribute()	 
	{
		return $this->typeBetCategory()->pluck('categoria')->toArray();
	}

}
