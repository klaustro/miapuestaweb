<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comision extends Model
{
	protected $table = 'comisiones';

	protected $fillable = ['tipo', 'desde', 'hasta', 'comision'];

	public $timestamps = false;    

	public function tipoComision()
	{
		return $this->belongsTo('\App\TipoComision', 'tipo');
	}

	public function getNameAttribute()
	{
		return $this->tipoComision->nombre;
	}

    public function scopeTipo($query, $tipo)
    {
        if ($tipo != "" and !is_numeric($tipo)){            
            $types = TipoComision::where('nombre', 'LIKE', "%$tipo%")->get();
            $query->whereIn('tipo', $types->pluck('id'));
        }
    }  

}
