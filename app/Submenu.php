<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submenu extends Model
{
	protected $table = 'submenu';
	protected $fillable = ['nombre', 'id_menu', 'limite_directa', 'limite_compuesta', 'orden',];

	protected $appends = ['games_count',];

	public $timestamps = false;

	public function games()
	{
		return $this->hasMany('App\Game', 'subcategoria');
	}

	public function menu()
	{
		return $this->belongsTo('\App\Menu', 'id_menu');
	}

	public function getGamesCountAttribute()
	{
		return 1;
	}

	public function scopeMenu($query, $id)
	{
		if ($id != "")
			$query->where('id_menu', '=', "$id");
	}

	public function scopeName($query, $name)
	{
		if ($name != "")
			$query->where('nombre', 'LIKE', "%$name%");
	}

	public function scopeMenuName($query, $menus)
	{
		if (count($menus) > 0) {
			$menuId = [];
			foreach ($menus as $user) {
				array_push($menuId, $user->id);
			}
			$query->whereIn('id_menu', $menuId);
		}
	}
}
