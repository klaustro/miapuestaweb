<?php

namespace App\Notifications;

use App\Pago;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AdminPayment extends Notification
{
	use Queueable;
	private $payment;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct(Pago $payment)
	{
		$this->payment = $payment;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable)
	{
		return ['mail'];
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable)
	{
		$user = $this->payment->user;

		$monto = $user->currency->default 
			? $this->payment->monto 
			: exchangeDefault($this->payment->monto, $user->currency_id);

		return (new MailMessage)
			->subject("Depósito de {$user->name}")
			->greeting('Estimado ' . $notifiable->name)
			->line("El usuario <b>{$user->name}</b>, ha registrado un depósito por <b>" . $user->currency->symbol . number_format($monto, 2, ',', '.') . '</b>')
			->line("por favor revise y si es el caso apruebe el pago <b>#{$this->payment->id}</b>")
			->action('Aprobar Pago', url("/admin/pago/{$this->payment->id}"))
			->line('Gracias por usar la aplicación!');
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function toArray($notifiable)
	{
		return [
			//
		];
	}
}
