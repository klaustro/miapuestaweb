<?php

namespace App\Notifications;

use App\Retiro;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AdminWithdrawal extends Notification
{
	use Queueable;
	private $withdrawal;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct(Retiro $withdrawal)
	{
		$this->withdrawal = $withdrawal;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable)
	{
		return ['mail'];
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable)
	{
		$user = $this->withdrawal->user;

		$monto = $user->currency->default 
			? $this->withdrawal->monto 
			: exchangeDefault($this->withdrawal->monto, $user->currency_id);

		return (new MailMessage)
			->subject("Retiro de {$this->withdrawal->user->name}")
			->greeting('Estimado Administrador')
			->line("El usuario {$this->withdrawal->user->name}, ha solicitado un retiro por <b>" . $user->currency->symbol . number_format($monto, 2, ',', '.') . '</b>')
			->line("por favor realice el pago y apruebe el retiro <b>#{$this->withdrawal->id}</b>")
			->line('<b>Datos para la transferencia:</b>')
			->line("<b>Cédula:</b> {$this->withdrawal->user->identification}")
			->line("<b>Banco:</b> {$this->withdrawal->account->banco->nombre}")
			->line("<b>Cuenta N°:</b> {$this->withdrawal->account->nrocuenta}")
			->line("<b>Tipo de cuenta:</b> {$this->withdrawal->account->tipo}")
			->action('Aprobar Retiro', url("/admin/retiro/{$this->withdrawal->id}"))
			->line('Gracias por usar la aplicación!');
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function toArray($notifiable)
	{
		return [
			//
		];
	}
}
