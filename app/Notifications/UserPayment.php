<?php

namespace App\Notifications;

use App\Pago;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserPayment extends Notification
{
	use Queueable;
	private $payment;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct(Pago $payment)
	{
		$this->payment = $payment;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable)
	{
		return ['mail'];
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable)
	{
		$monto = $notifiable->currency->default 
			? $this->payment->monto 
			: exchangeDefault($this->payment->monto, $notifiable->currency_id);
		
		return (new MailMessage)
			->subject('Solicitud de Depósito')
			->greeting('Estimado ' . $notifiable->name)
			->line('Hemos recibido su aviso de depósito por <b>' . $notifiable->currency->symbol . number_format($monto, 2, ',', '.') . '</b>')
			->line("Su transacción <b>#{$this->payment->id}</b> será procesada en un tiempo máximo de 60 minutos, si la efectúa dentro del horario de atención, que es Lunes a Domingo de 8am a 8:00pm.")
			->line('Si realizó una transferencia desde un banco tercero o realizó el depósito en cheque se le acreditará de 24-48 horas hábiles bancarias. Cualquier consulta por favor comunicarse a: 0212-5611273')
			->action('Ir al inicio', url('/'));
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function toArray($notifiable)
	{
		return [
			//
		];
	}
}
