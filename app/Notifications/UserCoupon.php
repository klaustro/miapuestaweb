<?php

namespace App\Notifications;

use App\Coupon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserCoupon extends Notification
{
    use Queueable;
	private $coupon;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Coupon $coupon)
    {
        $this->coupon = $coupon;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
			->subject('Cupon Generado')
			->greeting('Estimado ' . $notifiable->name)
			->line('Hemos generado un cupon de regalo por <b>' . $notifiable->currency->symbol . number_format($this->coupon->amount, 2, ',', '.') . '</b>')
			->line('Ya el cupon fue asociado a su cuenta.')
			->line('Desde ya puede realizar apuestas con su cupon')
			->action('Ir al inicio', url('/'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
