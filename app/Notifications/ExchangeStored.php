<?php

namespace App\Notifications;

use App\Exchange;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ExchangeStored extends Notification
{
    use Queueable;
	private $exchange;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Exchange $exchange)
    {
        $this->exchange = $exchange;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
					->subject('Tasa de cambio actualizada')
					->greeting('Hola ' . $notifiable->name)
                    ->line('La tasa de cambio fue actualizada ')
					->line('<b>' . number_format($this->exchange->price, 2, ',', '.') . '<b>')
					->line('Si la tasa no corresponde, puede actualizarla en el siguiente link')
                    ->action('Actualizar', url('/admin/updateExchange'))
                    ->line('Gracias por usar la aplicación!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
