<?php

namespace App\Notifications;

use App\Retiro;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserWithdrawal extends Notification
{
	use Queueable;
	private $withdrawal;

	/**
	 * Create a new notification instance.
	 *
	 * @return void
	 */
	public function __construct(Retiro $withdrawal)
	{
		$this->withdrawal = $withdrawal;
	}

	/**
	 * Get the notification's delivery channels.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function via($notifiable)
	{
		return ['mail'];
	}

	/**
	 * Get the mail representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return \Illuminate\Notifications\Messages\MailMessage
	 */
	public function toMail($notifiable)
	{
		$monto = $notifiable->currency->default 
			? $this->withdrawal->monto 
			: exchangeDefault($this->withdrawal->monto, $notifiable->currency_id);

		return (new MailMessage)
			->subject('Solicitud de Retiro')
			->greeting('Estimado ' . auth()->user()->name)
			->line('Hemos recibido su solicitud de retiro por <b>' . $notifiable->currency->symbol . number_format($monto, 2, ',', '.') . '</b>')
			->line("Su transacción <b>#{$this->withdrawal->id}</b> será procesada en un lapso de 24 horas hábiles bancarias, si la efectúa dentro del horario de atención, que es de Lunes a Sabado de 8am a 8pm.")
			->line('Cuando sea acreditada su transacción recibirá un correo de confirmación.')
			->line('Es importante tomar en cuenta que las transferencias Banesco pueden tomar de 2 a 24 horas, los demás bancos hasta 48 horas en ser acreditados, y montos superiores a 350,000,000 Bs pueden ser procesados hasta en un lapso máximo de 72 horas hábiles bancarias.')
			->line('Recuerde que no realizamos transferencias a cuentas de ahorro del Banco Banesco.')
			->line("<b>Cédula:</b> {$this->withdrawal->user->identification}")
			->line("<b>Banco:</b> {$this->withdrawal->account->banco->nombre}")
			->line("<b>Cuenta N°:</b> {$this->withdrawal->account->nrocuenta}")
			->line("<b>Tipo de cuenta:</b> {$this->withdrawal->account->tipo}")
			->action('Ir al inicio', url('/'));
	}

	/**
	 * Get the array representation of the notification.
	 *
	 * @param  mixed  $notifiable
	 * @return array
	 */
	public function toArray($notifiable)
	{
		return [
			//
		];
	}
}
