<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regla extends Model
{
    public function mensaje()
	{
		return $this->belongsTo('App\ReglasMensaje', 'idregla', 'idregla');
	}
}
