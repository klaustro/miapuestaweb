<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoComision extends Model
{
	protected $table = 'tipo_comision';

	protected $fillable = ['nombre'];

	public $timestamps = false;

    public function scopeName($query, $name)
    {
        if ($name != "")
            $query->where('nombre', 'LIKE', "%$name%");
    }    	
}
