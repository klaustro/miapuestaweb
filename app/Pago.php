<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    protected $fillable = ['id_usuario' , 'fecha_hora', 'monto' , 'status', 'tipo', 'referencia', 'banco', 'titular', 'ultimos_digitos', 'fecha_confirmacion', 'exchange_id'];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User', 'id_usuario');
    }    

    public function bank()
    {
        return $this->belongsTo('App\Banco', 'banco');
    }    

    public function scopeId($query, $id)
    {
        if ($id != "")
            $query->where('id', $id);
    }    

    public function scopeUserId($query, $users)
    {
        if (count($users) > 0){
            $userId = [];
            foreach ($users as $user) {
                array_push($userId, $user->id);
            }
            $query->whereIn('id_usuario', $userId);
        }
    }
}