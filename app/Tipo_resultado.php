<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo_resultado extends Model
{
    protected $table = 'tipo_resultado';

    protected $fillable = ['id', 'nombre'];
}
