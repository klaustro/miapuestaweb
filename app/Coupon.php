<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use HasFactory;

    protected $fillable = ['hash', 'amount', 'user_id'];
	
	public function user()
	{
		return $this->belongsTo(User::class);
	}
	
	public function bets()
	{
		return $this->belongsToMany(Bet::class, 'coupon_bets');
	}

	//Methods
	public static function generate($amount, $user_id = null)
	{
		$coupon = Coupon::create([
			'hash' => str_random(40),
			'amount' => $amount,
			'user_id' => $user_id,
		]);

		$coupon->save();
		return $coupon;
	}	
}
