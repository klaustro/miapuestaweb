<?php

namespace App;

use App\BetsDetail;
use App\CuentasBancaria;
use App\Notifications\MyResetPassword;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
	use Notifiable;

	protected $fillable = [
		'name', 'email', 'password', 'identification', 'birth_date', 'phone', 'status', 'logro', 'enterprise', 'printer', 'corredor', 'role', 'currency_id'
	];

	protected $hidden = [
		'password', 'remember_token',
	];

	protected $appends = [
		'first_name', //'currency_symbol', 'saldo', 'bonus', 
	];

	//Relationships

	public function pagos()
	{
		return $this->hasMany(Pago::class, 'id_usuario');
	}

	public function retiros()
	{
		return $this->hasMany(Retiro::class, 'id_usuario');
	}
	public function bets()
	{
		return $this->hasMany(Bet::class, 'usuario');
	}

	public function corredorComision()
	{
		return $this->hasOne(CorredorComision::class, 'idusuario');
	}

	public function accounts()
	{
		return $this->hasMany(CuentasBancaria::class);
	}

	public function empresa()
	{
		return $this->belongsTo(Empresa::class, 'enterprise');
	}

	public function secrets()
	{
		return $this->hasMany(Secret::class);
	}

	public function coupons()
	{
		return $this->hasMany(Coupon::class);
	}

	public function currency() 
	{
		return $this->belongsTo(Currency::class);
	}

	//Attributes

	public function getSaldoAttribute()
	{
		$balance = $this->payApproved + $this->bets_win - ($this->bet_sum + $this->retiros->sum('monto'));
		
		// if ($this->currency && !$this->currency->default) {			
		// 	$balance = exchangeDefault($balance, $this->currency_id);
		// }
		
		return $balance;
	}

	public function getBetSumAttribute()
	{
		return $this->bets->where('bonus', 0)->sum('monto') - $this->bets->where('status', 4)->sum('monto');
	}

	public function getArriesgadoAttribute()
	{
		$arriesgado = number_format($this->bets()->where('status', '1')->sum('monto'), 2);
		return $arriesgado;
	}

	public function getTipoComisionAttribute()
	{
		return isset($this->corredorComision) ? $this->corredorComision->idtipo_comision : '';
	}

	public function getBetsWinAttribute()
	{
		$sum = DB::table('bets')->where('usuario', $this->id)->where('status', 2)->sum('ganancia');
		return $sum;
	}

	public function getPayApprovedAttribute()
	{
		return $this->pagos->whereIn('status', ['1', 1])->sum('monto');
	}

	public function getSellsAttribute()
	{
		return isset($this->corredorComision) ? $this->corredorComision->ventas : '';
	}

	public function getBankAccountAttribute()
	{
		return isset($this->corredorComision) ? $this->corredorComision->ventas : '';
	}

	public function getAccountNumberAttribute()
	{
		return isset($this->accounts->first()->nrocuenta) ? $this->accounts->first()->nrocuenta : '';
	}

	public function getAccountsAttribute()
	{
		return isset($this->accounts) ? $this->accounts() : [];
	}

	public function getBankAttribute()
	{
		return isset($this->accounts->first()->banco->nombre) ? $this->accounts->first()->banco->nombre : '';
	}

	public function getFirstNameAttribute()
	{
		$pos = strpos($this->name, ' ');

		if (! $pos) {
			return $this->name;
		}

		return substr($this->name, 0, $pos);
	}
	
	public function getBonusAttribute()
	{
		if (! $this->has('coupons')) {
			return null;
		}
		
		$coupons = $this->coupons;
		$sum = $coupons->sum('amount');
		$bets = $this->bets->where('bonus', 1)->sum('monto');
		$total = $sum - $bets;

		// if ($this->currency && !$this->currency->default) {
		// 	$total = exchangeDefault($total, $this->currency_id);
		// }

		return $total;
	}

	public function getCurrencySymbolAttribute()
	{
		// $currency = Currency::find($this->currency_id);
		// if ($currency && $currency->symbol) {
		// 	return $currency->symbol;
		// }

		return '';
	}

	//Scopes

	public function scopeName($query, $name)
	{
		if ($name != "")
			$query->where('name', 'LIKE', "%$name%");
	}

	//Methods
	public function sendPasswordResetNotification($token)
	{
		$this->notify(new MyResetPassword($token));
	}

	public function IsAdmin()
	{
		return $this->role == 'admin';
	}

	public function isOwner()
	{
		return in_array($this->role, ['admin', 'owner']);
	}

	public function team_acum($games_probs)
	{
		$amount = 0;
		$betsDetail = BetsDetail::where('idgames_probs', $games_probs)->get();
		foreach ($betsDetail as $detail) {
			$amount += Bet::where(['usuario' => auth()->user()->id, 'id' => $detail->idbets])->sum('monto');
		}

		return $amount;
	}

	public function fixBalance()
	{
		$start = Carbon::now();
		foreach ($this->bets as $bet) {
			$bet->ganancia = $bet->calculeProfit();
			$bet->save();
		}
		$end = Carbon::now()->diffForHumans($start);
		echo "User #{$this->id} actualizado $end \n";
		return '';
	}
}
