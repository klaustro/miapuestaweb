<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuariosRegistrado extends Model
{
	protected $fillable =  ['id', 'nombre','apellido','email','contrasena','identificacion','direccion','fecha_nacimiento','telefono','telefono_movil','pais','estado','ciudad','banco','cuenta','pregunta_seguridad','respuesta_seguridad','logro','corredor'];
	
	public $timestamps = false;
}
