<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resultado extends Model
{
	protected $fillable = ['tipo', 'idgame', 'equipo', 'score'];

	public $timestamps = false;

	public function equipo(){
		return $this->hasOne('App\Equipo', 'id', 'equipo');
	}
	
	public function tipo_resultado(){
		return $this->belongsTo('App\Tipo_resultado', 'tipo');
	}
}
