<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipo extends Model
{
	protected $table = 'submenu2';

	protected $fillable = ['nombre', 'orden', 'abbr'];

	public $timestamps = false;

	protected $hide = ['id_menu', 'id_submenu'];

	public function games()
	{
		return $this->belongsToMany('\App\Game', 'games_probs', 'equipo');
	}

	public function equipoSubcategoria()
	{
		return $this->hasMany('\App\EquipoSubcategoria', 'id_equipo');
	}

	public function scopeName($query, $name)
	{
		if ($name != "" and !is_numeric($name))
			$query->where('nombre', 'LIKE', "%$name%");
	}

	public function scopeId($query, $id)
	{
		if ($id != "" and is_numeric($id))
			$query->where('id', $id);
	}

	public function scopeSubcategory($query, $id)
	{
		if ($id != "") {
			$rel = EquipoSubcategoria::where('id_submenu', $id)->get()->pluck('id_equipo');
			$query->whereIn('id', $rel);
		}
	}

	public function getLeaguesAttribute()
	{
		return $this->equipoSubcategoria()->pluck('id_submenu')->toArray();
	}
}
