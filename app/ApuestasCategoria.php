<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApuestasCategoria extends Model
{
    protected $fillable = ['tipo_apuesta', 'categoria'];

    public $timestamps = false;
}
