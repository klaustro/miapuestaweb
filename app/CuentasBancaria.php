<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CuentasBancaria extends Model
{
    protected $table = 'cuentas_bancarias';

    protected $fillable = ['id', 'banco_id', 'user_id', 'nrocuenta', 'tipo'];
	
	public function user()
	{
		return $this->hasMany(User::class);
	}

	public function banco(){
		return $this->belongsTo(Banco::class);
	}
}