<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stsgame extends Model
{
    protected $fillable = ['nombre'];
    
    public $timestamps = false;
}
