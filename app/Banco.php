<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banco extends Model
{
    protected $table = 'banco';

    protected $fillable = ['nombre'];

    public $timestamps = false;

    public function scopeName($query, $name)
    {
        if ($name != "")
            $query->where('nombre', 'LIKE', "%$name%");
    }        
}
