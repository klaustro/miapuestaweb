<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BetsDetail extends Model
{
    protected $table = 'bets_detail';

    protected $fillable = ['idbets', 'idgames_probs'];

    public $timestamps = false;

    //Relationship

    public function games_probs(){
    	return $this->belongsTo('App\GamesProb', 'idgames_probs');
    }

    public function gamesProbs(){
    	return $this->belongsTo('App\GamesProb', 'idgames_probs');
    }

    public function bet(){
        return $this->belongsTo('App\Bet', 'idbets');
    }

    //Attributes
    public function getEventAttribute()
    {
    	return $this->gamesProbs->game->event;
    }

    public function getBetTypeAttribute()
    {
    	return $this->gamesProbs->tipoApuesta->nombre;
    }

    public function getSelectionAttribute()
    {
    	return $this->gamesProbs->selection;
    }

    public function getAssertAttribute()
    {
    	return $this->gamesProbs->acierto;
    }

    public function getIsGameOverAttribute()
    {
        return in_array($this->gamesProbs->game->status, [1, 3]);
    }    

    public function getTagAttribute()
    {
        $tag = '';

        if($this->assert == '1'  )
           $tag = '<span class="badge badge-success">Acertó</span> ';
        elseif($this->assert == '0' and $this->is_game_over)
           $tag = '<span class="badge badge-important">No Acertó</span>';

        return $tag;
    }

    public function getGameAttribute()
    {
        return $this->gamesProbs->game;
    }

    public function getTeamAttribute()
    {
        return $this->gamesProbs->equipo;
    }
}
