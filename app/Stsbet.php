<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stsbet extends Model
{
     protected $fillable = ['nombre'];
     public $timestamps = false;
}
