<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Bet extends Model
{
	protected $fillable = ['fecha_registro', 'fecha', 'usuario', 'compuesta', 'monto', 'ganancia', 'status', 'web', 'bonus', 'exchange_id',];

	public $timestamps = false;

	protected $appends = ['exchanged_amount', 'exchanged_profit',];

	//Relationship

	public function bets_detail()
	{
		return $this->hasMany(BetsDetail::class, 'idbets');
	}

	public function user()
	{
		return $this->belongsTo(User::class, 'usuario');
	}

	public function stsbets()
	{
		return $this->belongsTo(Stsbet::class, 'status');
	}

	public function corredorComision()
	{
		return $this->belongsTo(CorredorComision::class, 'usuario', 'idusuario');
	}

	//Attributes
	public function getComisionAttribute()
	{

		if (!$this->is_corredor || $this->monto == 0) {
			return 0;
		}

		$comision = $this->monto * $this->percentage / 100;

		return $comision;
	}

	public function getPercentageAttribute()
	{
		if ($this->monto > 0 && $this->user->corredor) {
			$percentage = (($this->ganancia - $this->monto) * 100) / $this->monto;
			$comision = $this->corredorComision->where('idusuario', $this->usuario)->with(['comision' =>
			function ($query) use ($percentage) {
				$query->where('desde', '<=', $percentage)->where('hasta', '>=', $percentage);
			}])->get()->all();

			return $comision[0]->comision[0]->comision;
		}
		return 0;
	}

	public function getDateAttribute()
	{
		$date = date_create($this->fecha);
		return date_format($date, "d-m-Y");
	}

	public function getUserNameAttribute()
	{
		return $this->user->name;
	}

	public function getStatusNameAttribute()
	{
		return $this->stsbets->nombre;
	}

	public function getIsCorredorAttribute()
	{
		$user = User::find($this->usuario);

		return $user->corredor;
	}

	public function getHumanAmountAttribute()
	{
		return number_format($this->monto, 2);
	}

	public function getHumanProfitAttribute()
	{
		return number_format($this->ganancia, 2);
	}

	public function getBadgeAttribute()
	{
		$classBadge = 'badge-default';
		if (in_array($this->status, ['2', '5']))
			$classBadge = 'badge-success';
		elseif ($this->status == 3)
			$classBadge = 'badge-important';
		elseif ($this->status == 4)
			$classBadge = 'badge-warning';

		return $classBadge;
	}

	public function getIsWinnerAttribute()
	{
		$win = 0;

		foreach ($this->bets_detail as $detail) {
			$win += intval($detail->games_probs->acierto);
		}

		$count = $this->bets_detail->count();
		return $count == $win;
	}

	public function getIsLoseAttribute()
	{
		$lose = 0;
		foreach ($this->bets_detail as $detail) {
			$lose += $detail->is_game_over && $detail->games_probs->acierto == 0 ? 1 : 0;
		}

		return $lose > 0;
	}

	public function getIsCompletedAttribute()
	{
		$result = 0;

		foreach ($this->bets_detail as $detail) {
			$result += $detail->games_probs->game->resultado->count()
				|| in_array($detail->games_probs->game->status, [2, 3]) ? 1 : 0;
		}
		$count = $this->bets_detail->count();

		return $count == $result;
	}

	public function getProfitAttribute()
	{
		$total = 0;
		foreach ($this->bets_detail as $key => $detail) {
			$key == 0
				? $total += $this->monto * $detail->games_probs->probabilidad
				: $total = $total * $detail->games_probs->probabilidad;
		}

		return $total;
	}


	public function getGanancia2Attribute()
	{
		return $this->calculeProfit();
	}

	public function getExchangedAmountAttribute()
	{
		$user = User::find($this->usuario);
		$currency = Currency::find($user->currency_id);

		if($currency->default) {
			return $this->monto;
		}

		return exchangeDefault($this->monto, $currency->id);	
	}

	public function getExchangedProfitAttribute()
	{
		$user = User::find($this->usuario);
		$currency = Currency::find($user->currency_id);

		if($currency->default) {
			return $this->ganancia;
		}

		return exchangeDefault($this->ganancia, $currency->id);	
	}

	//Scopes

	public function scopeTicketId($query, $id)
	{
		if ($id != "")
			$query->where('id', 'LIKE', "%$id%");
	}

	public function scopeId($query, $id)
	{
		if ($id != "" and is_numeric($id))
			$query->where('id', $id);
	}

	public function scopeUserName($query, $name)
	{
		if ($name != "" and !is_numeric($name)) {
			$users = User::where('name', 'LIKE', "%$name%")->get();

			$query->whereIn('usuario', $users->pluck('id'));
		}
	}

	public function scopeStatus($query, $status)
	{
		if ($status != "")
			$query->where('status', '=', "$status");
	}

	public function scopeRunner($query, $corredor)
	{
		$users = User::where('corredor', $corredor)->get();
		$query->whereIn('usuario', $users->pluck('id'));
	}

	public function scopeUser($query, $id)
	{
		if ($id != "") {
			$query->where('usuario', $id);
		}
	}

	public function scopeDateRange($query, $from, $to)
	{
		if ($from != '' && $to != '') {
			$query->whereBetween('fecha', [$from, $to]);
		}
	}

	public function scopeEnterprise($query, $id)
	{
		if ($id != "") {
			$users = User::where('enterprise', $id)->get();
			$query->whereIn('usuario', $users->pluck('id'));
		}
	}

	public function scopeGame($query, $game)
	{
		if ($game != "") {
			$games_probs = GamesProb::where('idgame', $game)->get();
			$betDetail = BetsDetail::whereIn('idgames_probs', $games_probs->pluck('id'));
			$query->whereIn('id', $betDetail->pluck('idbets'));
		}
	}

	//Methods
	public function calculeProfit()
	{
		$total = 0;

		foreach ($this->bets_detail as $key => $detail) {
			$key == 0
				? $total += $this->monto * $detail->games_probs->probabilidad
				: $total = $total * $detail->games_probs->probabilidad;
		}

		if ($this->bonus == 1) {
			$total = $total - $this->monto;
		}

		return $total;
	}
}
