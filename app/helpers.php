<?php

use App\User;
use App\Exchange;
use GuzzleHttp\Client;
use App\Notifications\ExchangeFailed;
use App\Notifications\ExchangeStored;
use Illuminate\Support\Facades\Notification;

const DOLARES = 1;
const BOLIVARES = 2;

function exchangeDefault($amount, $currency) 
{
	$exchange = Exchange::where('currency_id', $currency)->latest()->first();

	$price = $exchange->price;
	return $amount * $price;
}

function exchangeCurrency($amount, $currency) 
{
	$exchange = Exchange::where('currency_id', $currency)->latest()->first();
	
	$price = $exchange->price;
	$total = $amount / $price;
	return $total;
}



function getExchangeFromAPI() 
{
	$client = new Client(['base_uri' => 'https://exchangemonitor.net']);
		
	$res = $client->request('GET', '/ajax/widget-unique', [
		'query' => [
			'type' => 'enparalelovzla'
		]
	]);

	$status = $res->getStatusCode();

	$users = User::whereIn('id', [0, 16])->get();	

	if ($status == 200) {

		$body = $res->getBody();	
		$response = json_decode($body);	
		$exchange = new Exchange();
		$exchange->currency_id = 2;
		$exchange->price = str_replace(',', '.', str_replace('.', '', $response->price));
		$exchange->save();						

		Notification::send($users, new ExchangeStored($exchange));

		return "Tasa actualizada: {$response->price}";
	}

	Notification::send($users, new ExchangeFailed($status));
	
	return 'Error en respuesta: ' . $status . ', intente en unos minutos, si persiste el problema notifique al administrador';
}

function getBetsApi($dataApi)
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => "https://odds.incub.space/v1/$dataApi/en",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
			"package: testkey6042023"
		),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
		echo "cURL Error #:" . $err;
	} else {
		echo $response;
	}	
}