<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GamesProbsSigno extends Model
{
    protected $table = 'games_probs_signo';

    protected $fillable = ['games_prob_id', 'signo'];

    public $timestamps = false;

    public function games_probs(){
    	return $this->belongsTo('App\GamesProb');
    }
}
