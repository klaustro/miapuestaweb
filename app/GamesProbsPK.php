<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GamesProbsPK extends Model
{
    protected $table = 'games_probs_pk';
    
    protected $fillable = ['idgames_probs', 'probabilidad', 'acierto', ];

    public $timestamps = false;
}