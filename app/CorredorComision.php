<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorredorComision extends Model
{
	protected $table = 'corredor_comision';

	protected $fillable = ['idusuario', 'idtipo_comision', 'ventas'];

	public $timestamps = false;    

	public function comision()
	{
		return $this->hasMany('App\Comision', 'tipo', 'idtipo_comision');
	}
}
