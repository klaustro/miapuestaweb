<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
	protected $table = 'menu';

	protected $fillable = [
	  'nombre', 'orden', 'nota',
	];

	public $timestamps = false;
	
	public function submenu()
	{
		return $this->hasMany('App\Submenu', 'id_menu');
	}

	public function games()
	{
		return $this->hasMany('App\Game', 'categoria');
	}

	public function scopeName($query, $name)
	{
        if ($name != "")
            $query->where('nombre', 'LIKE', "%$name%");
	}
}
