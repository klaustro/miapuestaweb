<?php

namespace App\Console;

use App\Console\Commands\ExchangeCurrency;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ExchangeCurrency::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
		$morning = env('EXCHANGE_MORNING', '9:15');
		$afternoon = env('EXCHANGE_AFTERNOON', '13:15');
        $schedule->command('exchange:default')->weekdays()->at($morning)->timezone('America/Caracas');
        $schedule->command('exchange:default')->weekdays()->at($afternoon)->timezone('America/Caracas');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
