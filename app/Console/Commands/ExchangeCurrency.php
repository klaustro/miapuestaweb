<?php

namespace App\Console\Commands;


use Illuminate\Console\Command;

class ExchangeCurrency extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exchange:default';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Exchange Currency';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
		$this->info(getExchangeFromAPI());
    }
}
