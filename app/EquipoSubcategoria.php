<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EquipoSubcategoria extends Model
{
    protected $table = 'equipo_subcategoria';

    protected $fillable = ['id_equipo', 'id_submenu'];
    
    public $timestamps = false;
}
