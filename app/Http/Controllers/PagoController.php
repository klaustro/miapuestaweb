<?php

namespace App\Http\Controllers;

use Mail;

use App\Pago;
use App\User;
use App\Exchange;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Notifications\UserPayment;
use App\Notifications\AdminPayment;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PagoController extends Controller
{
	public function store(Request $request)
	{
		if (!$this->duplicado($request->referencia, $request->banco, $request->fecha_hora)) {
			$user = auth()->user();
			$monto = $user->currency->default 
				? $request->get('monto') 
				: exchangeCurrency($request->get('monto'), $user->currency_id);
			$exchange = Exchange::where('currency_id', $user->currency_id)->latest()->first();

			$pago = Pago::create([
				"id_usuario" => $user->id,
				"fecha_hora" => $request->get('fecha_hora'),
				"monto" => $monto,
				"status" => 0,
				"tipo" => $request->get('tipo'),
				"referencia" => $request->get('referencia'),
				"banco" => $request->get('banco'),
				"titular" => $request->get('titular'),
				"ultimos_digitos" => $request->get('ultimos_digitos'),
				"exchange_id" => $exchange ? $exchange->id : null,
			]);
			$pago->save();

			$user->notify(new UserPayment($pago));

			$admin = User::find(0);
			$admin->notify(new AdminPayment($pago));

			$message = 'Pago #' . $pago->id . ' por ' . $user->currency_symbol . number_format($request->get('monto'), 2) . ' registrado exitosamente, será revisado y aprobado en breve';
			return response()->json([
				'message' => $message
			]);
		} else {
			return response('Pago duplicado, por favor revise los datos', 406);
		}
	}

	protected function duplicado($referencia, $banco, $fecha)
	{
		$pago = Pago::where(['referencia' => $referencia, 'banco' => $banco, 'fecha_hora' => $fecha])->get();

		return $pago->count() > 0;
	}
}
