<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Mail;

class ContactController extends Controller
{
	public function contactMail(Request $request)
	{

		$data = [
			'name' => $request->name,
			'content' => $request->content
		];

		Mail::send('templates/mail/contact', $data, function ($message) use ($request) {
			$message->from(env('MAIL_FROM_ADDRESS', 'mail.miapuestaweb.com@gmail.com'), 'Contacto MiApuestaWeb');
			$message->sender(env('MAIL_FROM_ADDRESS', 'mail.miapuestaweb.com@gmail.com'), 'Contacto MiApuestaWeb');
			$message->to($request->email, $request->name);
			$message->bcc('mail.miapuestaweb.com@gmail.com', 'Contacto');
			$message->subject('Contacto MiApuestaWeb');

			$message->priority(1);
		});

		$message = "Mensaje enviado exitosamente, será revisado y respondido en breve";
		return response()->json([
			'message' => $message
		]);
	}

	public function index()
	{
		return view('contact.index');
	}
}
