<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TemplateController extends Controller
{
    public function quienes(){
    	return view('templates/quienes');
    }

    public function reglas(){
    	return view('templates/reglas');
    }

    public function home(){
    	return view('templates/home');
    }

    public function contacto(){
    	return view('templates.contacto');
    }

    public function bets(){
    	return view('templates/bets');
    }

    public function historial(){
        return view('templates/historial');
    }

    public function resultados(){
        return view('templates/resultados');
    }

    public function logros(){
        return view('templates/logros');
    }    

    public function cuentas(){
        return view('templates/cuentas');
    }

    public function tickets(){
        return view('templates/tickets');
    }
    public function help(){
        return view('templates/help');
    }  
}
