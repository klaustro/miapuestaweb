<?php

namespace App\Http\Controllers;

use Mail;
use App\User;
use App\Retiro;
use App\Exchange;
use Carbon\Carbon;
use App\Http\Requests;
use App\CuentasBancaria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Notifications\UserWithdrawal;
use App\Notifications\AdminWithdrawal;

class RetiroController extends Controller
{
	public function store(Request $request)
	{
		$now = Carbon::now('America/Caracas');
		$now = date_format($now, "Y-m-d H:i:s");
		$user = auth()->user();
		$saldo = $user->saldo;
		$monto = $user->currency->default 
			? $request->get('monto') 
			: exchangeCurrency($request->get('monto'), $user->currency_id);
		$exchange = Exchange::where('currency_id', $user->currency_id)->latest()->first();

		if ($monto <= $saldo) {
			$retiro = Retiro::create([
				"id_usuario" => $user->id,
				"cuenta_id" => $request->get("cuenta_id"),
				"fecha" => $now,
				"fecha_hora" => $now,
				"monto" => $monto,
				"status" =>  0,
				'exchange_id' => $exchange ? $exchange->id : null,
			]);
			
			$retiro->save();
			$saldo = $retiro->user->saldo;
			$message = 'Retiro #' . $retiro->id . ' por ' . $user->currency_symbol . number_format($request->get('monto'), 2) . ' registrado exitosamente, será revisado y aprobado en breve';

			$user->notify(new UserWithdrawal($retiro));

			$admin = User::find(0);
			$admin->notify(new AdminWithdrawal($retiro));
		} else {
			$message = "El monto a retirar supera su saldo disponible";
		}

		$saldo = $user->saldo;
		return response()->json([
			'message' => $message,
			'saldo' => $saldo
		]);
	}
}
