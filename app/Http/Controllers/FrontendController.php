<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class FrontendController extends Controller
{

	// For public application
	public function app()
	{
		
		switch (request()->path()) {
			case '/score':
				$page = 'Resultados';
				break;
			case '/contact':
				$page = 'Contacto';
				break;
			
			default:
				$page = 'Inicio';
				break;
		}

		$meta = [
			'title' => "Miapuestaweb - $page",
			'description' => 'Página de apuestas deportivas',
			'keywords' => 'parley, parlay, apuestas, deporte, deportes, futbol, beisbol, basket, apostar, ganar dinero, incrementa tus ingresos, apostar al parlay, apuestas deportivas, gana en dolares,logros, logros del dia, juegos, juegos del dia, calculadora de parley, calculadora de parlay, calcula tu parley, quiero vender parley, myapuesta, banca de parley, banca de parlay',
			'image' => url('/images/miapuestaweb-share.jpg'),
			'rrss' => '@Miapuestaweb',
 		];
		 

		return view('app', $meta);
	}
}
