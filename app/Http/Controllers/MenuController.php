<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Menu;
use Carbon\Carbon;

class MenuController extends Controller
{

	public function show($id)
	{
		return Menu::find($id);
	}

	public function index()
	{
		return Menu::all();
	}

	public function todayMenu()
	{
		$categories = [];
		$menus = Menu::withCount(['games' =>
		function ($query) {
			$now = Carbon::now('America/Caracas');
			$now = date_format($now, "Y-m-d H:i:s");
			$query->where('fecha_cierre', '>', $now);
			$query->where('visible', 1);
		}])
			->get()
			->all();

		foreach ($menus as $menu) {
			if ($menu->games_count > 0) {
				$categories[] = $menu;
			}
		}

		return $categories;
	}
}
