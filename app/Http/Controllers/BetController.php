<?php

namespace App\Http\Controllers;

use App\Bet;
use App\Game;

use App\Regla;

use App\Exchange;
use App\CouponBet;
use App\GamesProb;
use Carbon\Carbon;
use App\TipoApuesta;
use App\Http\Requests;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class BetController extends Controller
{
	protected $messages = [
		'Juego Cerrado' => 'Hay juegos en su jugada que ya cerraron, por favor vuelva a seleccionar su jugada',
		'Jugada Prohibida' => 'Hay jugadas prohibidas en su selección, por favor vuelva a seleccionar su jugada',
		'Saldo insuficiente' => 'Saldo insuficiente para realizar ésta apuesta, lo invitamos a registrar un nuevo pago',
		'Cambio de logro' => ':bets cambiado, por favor vuelva a seleccionar su jugada',
		'Máximo apuestas para un evento' => 'Ha superado el monto máximo de apuestas para éste evento',
		'Máximo apuestas para un logro' => 'Ha superado el monto máximo de apuestas para éste logro',
	];

	protected $compuesta;

	public function index(Request $request)
	{
		$bets = Bet::with('bets_detail.games_probs')->paginate();
		return $bets;
	}


	public function create()
	{
		$bets = Bet::all();
		return $bets;
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->compuesta = $request->get('compuesta');
		$id = '';

		$title = $this->invalidBet($request->bets);
		$message = $this->getMessage($title);

		if ($title == null) {
			$newBet = $this->makeBet($request);
			$title = $newBet['title'];
			$message = $newBet['message'];
			$id = $newBet['id'];
		}

		return response()->json([
			'title' => $title,
			'id' => $id,
			'message' => $message,
			'saldo' => Auth::user()->saldo - $this->totalBet($request->bets),
			'juego' => Auth::user()->arriesgado,
		]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$bet = Bet::with('bets_detail')
			->with('bets_detail.games_probs.equipo')
			->with('bets_detail.games_probs.tipoApuesta')
			->with('bets_detail.games_probs.game.equipo')
			->with('bets_detail.games_probs.games_probs_signo')
			->find($id);

		foreach ($bet->bets_detail as $item) {
			$item->games_probs->game['event'] =  $item->games_probs->game->event;
		}

		return response()->json([
			'bet' => $bet
		]);
	}

	public function ticket()
	{
		$bets = Bet::with('bets_detail')
			->with('bets_detail.games_probs.equipo')
			->with('bets_detail.games_probs.tipoApuesta')
			->with('bets_detail.games_probs.game.equipo')
			->with('bets_detail.games_probs.games_probs_signo')
			->whereIn('id', request()->bets)
			->get();

		foreach ($bets as $bet) {
			foreach ($bet->bets_detail as $item) {
				$item->games_probs->game['event'] =  $item->games_probs->game->event;
			}
		}

		return response()->json([
			'bet' => $bets
		]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$bet = Bet::findOrFail($id);
		return "Datos Enviados";
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$bet = Bet::findOrFail($id);
		$bet->fill($request->all());
		$bet->save();

		$accion = ($bet->status == 5) ? 'pagada' : 'actualizada';
		$message = "Apuesta {$bet->id} $accion";

		return response()->json([
			'message' => $message
		]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id, Request $request)
	{
		Bet::destroy($id);

		$message = "registro $id eliminado";

		if ($request->ajax()) {
			return response()->json([
				'message' => $message
			]);
		}
	}

	private function totalBet(array $bets)
	{
		$total = 0;
		foreach ($bets as $bet) {
			if ($this->compuesta == 1) {
				$total = $bet['monto'];
			} else {
				$total = $total + $bet['monto'];
			}
		}

		return $total;
	}

	private function isEndedGame($bets)
	{
		$now = date_format(Carbon::now('America/Caracas'), "Y-m-d H:i:s");
		foreach ($bets as $bet) {
			$game = Game::where('id', $bet['idgame'])
				->where('fecha_cierre', '<', $now)
				->get()
				->all();
			if (count($game) > 0) {
				return true;
			}
		}
		return false;
	}

	private function isForbiddenBet($bets)
	{
		$max = Regla::max('idregla');
		foreach ($bets as $i => $betOut) {
			$categoria = Game::find($betOut['idgame'])->categoria;
			foreach ($bets as $j => $betIn) {
				if ($i <> $j and $betOut["idgame"] == $betIn["idgame"] and $betOut["tipo_probabilidad"] == $betIn["tipo_probabilidad"]) {
					return true;
				} else if ($i <> $j and $betOut["idgame"] == $betIn["idgame"]) {
					for ($k = 1; $k <= $max; $k++) {
						$reglas = Regla::where('idcategoria', $categoria)
							->where('idregla', $k)
							->whereIn('tipo_apuesta', [$betOut["tipo_probabilidad"], $betIn["tipo_probabilidad"]])
							->get()
							->all();
						if (count($reglas) > 1) {
							return true;
							$k = $max;
						}
					}
				}
			}
		}
		return false;
	}

	private function changeProbability($bets)
	{
		$obsoletas = "";
		foreach ($bets as $bet) {
			$games_probs = GamesProb::find($bet['id']);
			if (!$games_probs->activa) {
				$obsoletas = 'Uno o varios logros han';
			}
		}

		return $obsoletas;
	}

	private function isMaxGames($bets)
	{
		foreach ($bets as $bet) {
			$games_probs = GamesProb::find($bet['id']);

			$tipoApuesta = TipoApuesta::find($bet["tipo_probabilidad"]);

			$betsGame = Bet::with('bets_detail.games_probs')
				->where('usuario',  Auth::user()->id)
				->where('status', 1)
				->get()
				->all();

			$total_game = 0;

			foreach ($betsGame as $betGame) {
				foreach ($betGame->bets_detail as $betDetail) {
					if ($betDetail->games_probs->idgame == $games_probs->idgame)
						$total_game += $betGame->monto;
				}
			}

			if ($total_game + $bet['monto'] >  $tipoApuesta->max_apuesta) {
				return true;
			}
		}
		return false;
	}

	private function invalidBet($bets)
	{
		$user = Auth::user();
		$total = $this->totalBet($bets);
		$tipoApuesta = TipoApuesta::find($bets[0]["tipo_probabilidad"]);
		if ($this->isEndedGame($bets)) {
			return 'Juego Cerrado';
		} else if ($this->isForbiddenBet($bets) && $this->compuesta) {
			return 'Jugada Prohibida';
		} else if ($total > $user->saldo && $user->corredor == 0) {
			return 'Saldo insuficiente';
		} else if ($this->changeProbability($bets) != '') {
			$this->messages['Cambio de logro'] = $this->replaceBets($this->messages['Cambio de logro'], $this->changeProbability($bets));
			return 'Cambio de logro';
		} else if ($this->isMaxGames($bets) && $user->corredor == 0) {
			return 'Máximo apuestas para un evento';
		} elseif ($this->dueMaxBets($bets, $tipoApuesta)) {
			return 'Máximo apuestas para un logro';
		}
	}

	protected function dueMaxBets($bets, $tipoApuesta)
	{
		foreach ($bets as $bet) {

			if (Auth::user()->team_acum($bet['id']) + $bet['monto'] > ($tipoApuesta->max_apuesta / 38) && $tipoApuesta->id == 19) {
				return true;
			}
		}

		return false;
	}

	private function getMessage($key)
	{
		return isset($this->messages[$key]) ? $this->messages[$key] : null;
	}

	private function replaceBets($text, $replace)
	{
		return str_replace(':bets', $replace, $text);
	}

	private function makeBet($request)
	{
		$ticket = "";
		$id = [];
		$bets = $request->bets;
		$now = date_format(Carbon::now('America/Caracas'), "Y-m-d H:i:s");
		$user = auth()->user();
		$exchange = Exchange::where('currency_id', $user->currency_id)->latest()->first();
		
		if ($this->compuesta == 1) {
			$monto = $user->currency->default 
				? $bets[0]['monto'] 
				: exchangeCurrency($bets[0]['monto'], $user->currency_id);

			$bet = Bet::create([
				'fecha_registro' => $now,
				'fecha' => $now,
				'usuario' => $user->id,
				'compuesta' => $this->compuesta,
				'monto' => $monto,
				'ganancia' =>  $request->ganancia,
				'web' => 0,
				'status' => 1,
				'bonus' => $request->bonus,
				'exchange_id' => $exchange ? $exchange->id : null,
			]);
			$bet->save();

			for ($i = 0; $i < count($bets); $i++) {
				$bet->bets_detail()->create(['idgames_probs' => $bets[$i]['id']]);
			}
			array_push($id, $bet->id);
			$title = 'valida';
			$message = "Apuesta  {$bet->id} registrada con exito <br> Suerte !!!";
		} else {
			for ($i = 0; $i < count($bets); $i++) {
				$monto = $user->currency->default 
					? $bets[$i]['monto']
					: exchangeCurrency($bets[$i]['monto'], $user->currency_id);

				$ganancia = $monto * $bets[$i]['probabilidad'];
				
				if ($request->bonus == 1) {
					$ganancia = $ganancia - $monto;
				}


				$bet = Bet::create([
					'fecha_registro' => $now,
					'fecha' => $now,
					'usuario' => Auth::user()->id,
					'compuesta' => $this->compuesta,
					'monto' => $monto,
					'ganancia' => $ganancia,
					'web' => 0,
					'status' => 1,
					'bonus' => $request->bonus,
					'exchange_id' => $exchange ? $exchange->id : null,
				]);
				$bet->save();
				$bet->bets_detail()->create(['idgames_probs' => $bets[$i]['id']]);
				$ticket .= " " . $bet->id;
				array_push($id, $bet->id);
			}

			$title = 'valida';			
			$s = count($bets) > 1 ? 's': '';
			$message = "Apuesta$s $ticket registrada$s con exito <br> Suerte !!!";
		}
		$newBet = ['title' => $title, 'message' => $message, 'id' => $id];
		
		return $newBet;
	}
}
