<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Resultado;
use App\Game;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class ResultadoController extends Controller
{

    public function adminIndex(Request $request)
    {
        $games = [];

        if (isset($request->search)) {

            $search = $request->search;     

            $games = Game::event($search)->get()->all();
        }

        $resultados = Resultado::gameId($games)->paginate();
            
        return view('admin.resultado.index', compact('resultados'));
    }

    public function apiIndex(Request $request){
    	if($request->fecha_cierre<> '')
    	{
			$fecha_from = date_format(Carbon::createFromFormat('Y-m-d', $request->fecha_cierre), 'Y-m-d');
			$fecha_to =date_format(Carbon::createFromFormat('Y-m-d', $request->fecha_cierre)->addDay(), 'Y-m-d');
    	}
    	else
    	{
			$fecha_from = Carbon::now('America/Caracas');
			$fecha_from = date_format($fecha_from,"Y-m-d");
			$fecha_to = Carbon::now('America/Caracas')->addDay();
			$fecha_to = date_format($fecha_to,"Y-m-d");
    	}
    	$resultados = Game::with('menu')
            ->with('submenu')
            ->with('equipo')
            ->with('resultado.equipo')
            ->with('resultado.tipo_resultado')
            ->has('resultado')
            ->whereBetween('fecha_cierre', [$fecha_from, $fecha_to])
            ->where('status', '1')
            ->orderBy('categoria')
            ->orderBy('subcategoria')
            ->orderBy('id')
            ->get()
            ->all();
    	return $resultados;
    }

    public function index()
    {
        return view('result.index');
    }
}
