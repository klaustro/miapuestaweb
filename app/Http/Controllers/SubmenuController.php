<?php

namespace App\Http\Controllers;

use App\Game;
use App\Menu;
use App\Submenu;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubmenuController extends Controller
{

	public function index(Request $request)
	{
		$now = Carbon::now('America/Caracas');
		$now = date_format($now, "Y-m-d H:i:s");

		$games = Game::where('fecha_cierre', '>', $now)->where(['visible' => 1, 'categoria' => $request->get('menu')])->get();
		$submenu_ids = $games->pluck('subcategoria');

		$submenus = Submenu::whereIn('id', $submenu_ids)->get();

		return $submenus;
	}

	public function show($id)
	{
		return Submenu::find($id);
	}
}
