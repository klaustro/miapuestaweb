<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use App\Bet;

class HistoryController extends Controller
{
	public function index(Request $request)
	{
		$rows = 10;
		$weeks = $request->weeks;
		if ($request->status == 0) {
			$status = ['1', '2', '3', '5'];
		} else {
			$status = [$request->status];
		}
		$id = $request->id;

		$monday = Carbon::now('America/Caracas');
		$sunday = Carbon::now('America/Caracas');
		$from = Carbon::now('America/Caracas');
		$to = Carbon::now('America/Caracas');

		$monday = $monday->startOfWeek();
		$monday = $monday->subWeek($weeks);

		$sunday = $sunday->endOfWeek();
		$sunday = $sunday->subWeek($weeks);

		$from = $from->startOfWeek();
		$from = $from->subWeek($weeks);

		$to = $to->startOfWeek();
		$to = $to->subWeek($weeks);
		$to = $to->addDay();

		$nombre_dia = Carbon::now()->format('l');
		$nombre_dia = Lang::get('custom.' . strtoupper($nombre_dia));


		$bets = Bet::with(
			'bets_detail.games_probs.equipo',
			'bets_detail',
			'bets_detail.games_probs.tipoApuesta',
			'bets_detail.games_probs.game.equipo',
			'bets_detail.games_probs.game.resultado',
			'bets_detail.games_probs.games_probs_signo'
		)
			->with(['bets_detail.games_probs.game.resultado' =>
			function ($query) {
				$query->where('tipo', '1');
			}])
			->whereBetween('fecha_registro',  [$monday, $sunday])
			->where('usuario', Auth::user()->id)
			->whereIn('status', $status)
			->ticketId($id)
			->orderBy('fecha_registro', 'desc')
			->paginate($rows);

		foreach ($bets as $key => $bet) {
			$bets[$key]->comision = $bet->comision;
		}


		$balance = [];
		$week_gana = 0;
		$week_movimiento = 0;
		$week_saldo = 0;
		$week_comision = 0;
		$week_ventas = 0;

		for ($i = 0; $i < 7; $i++) {

			$bets_com = Bet::where('usuario', Auth::user()->id)
				->where('status', '<>', '4')
				->whereBetween('fecha_registro',  [$from, $to])
				->get()
				->all();

			$bets_win = Bet::where('usuario', auth()->user()->id)
				->whereIn('status', ['2', '5'])
				->whereBetween('fecha_registro',  [$from, $to])
				->get();

			$win_sum = 0;

			foreach ($bets_win as $win) {
				$win_sum += $win->profit;
			}

			$bets_lose = Auth::user()
				->bets()
				->where('status', '<>', '4')
				->whereBetween('fecha_registro',  [$from, $to])
				->sum('monto');

			//$gana = $bets_win - $bets_lose;
			$gana = $win_sum - $bets_lose;

			$pagos = Auth::user()
				->pagos()
				->where('status', '1')
				->where('fecha_hora',  $from)
				->sum('monto');

			$retiros = Auth::user()
				->retiros()
				->where('fecha',  $from)
				->sum('monto');
			$movimiento = $pagos - $retiros;

			$comision = 0;
			$ventas = 0;


			$pagos_bal = Auth::user()->pagos()->where('status', '1')->where('fecha_hora', '<', $to)->sum('monto');
			$retiros_bal = Auth::user()->retiros()->where('fecha', '<', $to)->sum('monto');
			$bets_bal = Auth::user()->bets()->where('fecha_registro', '<', $to)->sum('monto');
			$bets_win_bal = Auth::user()->bets()->where('status', '2')->where('fecha_registro', '<', $to)->sum('ganancia');

			$saldo_bal = $pagos_bal + $bets_win_bal - $retiros_bal - $bets_bal;

			foreach ($bets_com as $bet) {
				$comision += $bet->comision;
				$ventas += $bet->monto;
			}


			$nombre_dia = $from->format('l');
			$nombre_dia = Lang::get('custom.' . strtoupper($nombre_dia));
			array_push(
				$balance,
				[
					'dia' => $nombre_dia,
					'gana' => number_format($gana, 2),
					'movimiento' => number_format($movimiento, 2),
					'saldo' => number_format($saldo_bal, 2),
					'ventas' => number_format($ventas, 2),
					'comision' => number_format($comision, 2)
				]
			);
			$from = $from->addDay();
			$to = $to->addDay();
			$week_gana += $gana;
			$week_movimiento += $movimiento;
			$week_comision += $comision;
			$week_ventas += $ventas;
			$week_saldo = $saldo_bal;
		}

		$totales = [
			'gana' => number_format($week_gana, 2),
			'movimiento' => number_format($week_movimiento, 2),
			'comision' => number_format($week_comision, 2),
			'ventas' => number_format($week_ventas, 2),
			'saldo' => number_format($week_saldo, 2),
		];

		return response()->json([
			'bets' => $bets,
			'balance' => $balance,
			'totales' => $totales,
			'rango' => date_format($monday, "d-m-Y") . " al " . date_format($sunday, "d-m-Y"),
		]);
	}

	public function list()
	{
		$status = ['1', '2', '3', '5'];
		$start = request()->start;
		$end =  Carbon::createFromFormat('Y-m-d', request()->end)->addDay();

		$totals = Bet::whereBetween('fecha_registro',  [$start, $end])
			->where('usuario', Auth::user()->id)
			->whereIn('status', $status)
			->get();

		foreach ($totals as $bet) {
			$bet->comision = $bet->comision;
		}

		$win = $totals->sum(function ($bet) {
			if ($bet->status == 2 || $bet->status == 5) {
				return $bet->ganancia;
			}
		});

		$sells = $totals->sum(function ($bet) {
			if ($bet->status != 4) {
				return $bet->monto;
			}
		});

		$comision = $totals->sum(function ($bet) {
			if ($bet->status != 4) {
				return $bet->comision;
			}
		});

		$profit = $win - $sells;

		$bets = Bet::with(
			'bets_detail.games_probs.equipo',
			'bets_detail',
			'bets_detail.games_probs.tipoApuesta',
			'bets_detail.games_probs.game.equipo',
			'bets_detail.games_probs.game.resultado',
			'bets_detail.games_probs.games_probs_signo'
		)
			->with(['bets_detail.games_probs.game.resultado' =>
			function ($query) {
				$query->where('tipo', '1');
			}])
			->whereBetween('fecha_registro',  [$start, $end])
			->where('usuario', Auth::user()->id)
			->whereIn('status', $status)
			//->ticketId($id)
			->orderBy('fecha_registro', 'desc')
			->paginate(10);

		$data = [
			'bets' => $bets,
			'profit' => $profit,
			'sells' => $sells,
			'comision' => $comision,
		];

		return $data;
	}
}
