<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Banco;
class BancoController extends Controller
{
    public function index(){
    	$bancos = Banco::all();
    	return $bancos;
    }
}
