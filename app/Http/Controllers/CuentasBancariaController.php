<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\CuentasBancaria;
use Illuminate\Support\Facades\Auth;

class CuentasBancariaController extends Controller
{
	public function index()
	{
		$cuentas = CuentasBancaria::with('banco')->where('user_id', Auth::user()->id)->get()->all();
		return $cuentas;
	}


	public function store(Request $request)
	{
		$cuentas = CuentasBancaria::create([
			'banco_id' => $request->banco,
			'user_id' => Auth::user()->id,
			'nrocuenta' => $request->nrocuenta,
			'tipo' => $request->tipo
		]);
		$cuentas->save();

		return response()->json([
			'title' => 'Cuenta <br> Registrada',
			'message' => 'Su cuenta fue registrada satisfactoriamente',
			'account' => $cuentas,
		]);
	}

	public function show($id)
	{
		$cuenta = CuentasBancaria::with('banco')->find($id);
		return $cuenta;
	}

	public function update(Request $request, $id)
	{
		$cuenta = CuentasBancaria::findOrFail($id);
		$cuenta->fill([
			'banco_id' => $request->banco,
			'nrocuenta' => $request->nrocuenta,
			'tipo' => $request->tipo
		]);
		$cuenta->save();
		return response()->json([
			'title' => 'Cuenta<br> Modificada',
			'message' => 'Su cuenta fue modificada satisfactoriamente',
		]);
	}

	public function destroy($id)
	{
		CuentasBancaria::destroy($id);
		return response()->json([
			'title' => 'Cuenta<br> Eliminada',
			'message' => 'Su cuenta fue eliminada satisfactoriamente',
		]);
	}
}
