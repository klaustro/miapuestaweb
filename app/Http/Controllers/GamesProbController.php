<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\GamesProb;

class GamesProbController extends Controller
{
  public function index(Request $request)
  {

      $games_probs = GamesProb::where('idgame',($request->get('idgame')))
      ->where('tipo_probabilidad', '>', '6')
      ->where('activa', '1')
      ->with('equipo', 'tipoApuesta', 'reglas', 'games_probs_signo')
      ->get()
      ->all();

      //dd($games_probs);
      return $games_probs;

  }

	public function show($id)
	{
		return GamesProb::find($id);
	}

}
