<?php

namespace App\Http\Controllers\Admin;

use App\Bet;
use App\Equipo;
use App\Game;
use App\GamesProb;
use App\GamesProbsPK;
use App\GamesProbsSigno;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Menu;
use App\Resultado;
use App\Stsgame;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class GameController extends Controller
{
	public function index(Request $request)
	{

		$menus = ['' => 'Categoria'] + Menu::pluck('nombre', 'id')->all();
		$status = ['' => 'Estatus'] + Stsgame::pluck('nombre', 'id')->all();


		$games = Game::category($request->categoria)
			->status($request->status)
			->event($request->search)
			->id($request->search)
			->with(['menu', 'submenu', 'games_probs', 'resultado', 'teams', 'stsgame'])
			->orderBy('id', 'DESC')->paginate();

		foreach ($games as $game) {
			$game['event'] = $game->event;
			$game['bets_count'] = $game->bets_count;
			foreach ($game->games_probs as $prob) {
				$prob['description'] = $prob->description;
			}
		}

		if ($request->ajax()) {
			return  $games;
		}
		return view('admin.game.index', compact('games', 'menus', 'status'));
	}

	public function store()
	{
		$game = new Game();
		$game->categoria = request()->categoria;
		$game->subcategoria = request()->subcategoria;
		$game->compuesta = request()->compuesta;
		$game->fecha_registro = Carbon::now();
		$game->fecha_cierre = request()->fecha_cierre;
		$game->status = request()->status;
		$game->visible = request()->visible;
		$game->save();

		$probs = request()->only('probs')['probs'];
		foreach ($probs as $prob) {
			$this->saveProb($game->id, $prob);
		}

		$this->setSigno($game);

		return ['message' => 'Juego Salvado'];
	}

	public function update($id)
	{
		$game = Game::find($id);
		$game->fill(request()->except('probs'));
		$game->save();

		$games_probs = GamesProb::where('idgame', $id)->get();
		foreach ($games_probs as $prob) {
			$this->disableProbs($prob);
		}

		$probs = request()->only('probs')['probs'];

		foreach ($probs as $probToSave) {
			$this->saveProb($game->id, $probToSave);
		}

		$this->setSigno($game);

		return ['message' => "Juego #$id actualizado satisfactoriamente"];
	}

	public function storeResults(Request $request)
	{

		$game = Game::find($request->game);

		$results = Resultado::where('idgame', $game->id)->get();

		if ($results->count() > 0) {
			foreach ($results as $result) {
				$result->delete();
			}
		}

		//Resultado Final
		if (isset($request->resultF)) {
			foreach ($request->resultF as $key => $result) {
				$this->saveResult('1', $game->id, $game->teams[$key]->id, $result);
			}
		}

		//Quien anoto primero
		$this->saveResult('2', $game->id,  $request->who, 0);

		//Resultado medio tiempo
		if (isset($request->resultH)) {
			foreach ($request->resultH as $key => $result) {
				$this->saveResult('3', $game->id, $game->teams[$key]->id, $result);
			}
		}

		//Anotaron en primer inning
		$this->saveResult('4', $game->id,  0, $request->inning1);

		//HRE
		$this->saveResult('5', $game->id, 0, $request->hre);

		//Quien anoto primero
		$this->saveResult('6', $game->id,  $request->next, 0);

		//Sorteo
		$this->saveResult('7', $game->id,  $request->draw, 0);

		$this->updateGamesProbs($game);


		$game->status = $game->status != 3 ? 1 : $game->status;

		$game->save();

		$this->updateBets($game);

		return redirect(route('admin.game.index'));
	}

	protected function saveResult($type, $gameId, $team, $score)
	{
		if (isset($team)  and isset($score)) {
			Resultado::create([
				'tipo' => $type,
				'idgame' => $gameId,
				'equipo' => $team,
				'score' => $score
			]);
		}
	}

	public function show($id)
	{
		$game = Game::with(['menu', 'submenu', 'probs', 'resultado', 'teams'])->findOrFail($id);

		if (request()->ajax()) {
			return $game;
		}

		return view('admin.game.show', compact('game'));
	}

	protected function disableProbs(GamesProb $games_probs)
	{
		$games_probs->activa = 0;
		$games_probs->save();
	}

	protected function saveProb($idgame, $prob)
	{
		$gamesProbs = new GamesProb;
		$gamesProbs->idgame = $idgame;
		$gamesProbs->tipo_probabilidad =   $prob['tipo_probabilidad']['id'];
		$gamesProbs->equipo =  $prob['equipo']['id'];
		$gamesProbs->orden =   $prob['orden'];
		$gamesProbs->cantidad =    $prob['cantidad'];
		$gamesProbs->probabilidad =    $prob['probabilidad'];
		$gamesProbs->activa =  $prob['activa'];
		$gamesProbs->acierto = $prob['acierto'];
		$gamesProbs->save();
	}

	public function setSigno(Game $game)
	{

		foreach ($game->gamesProbs as $gamesProb) {

			if ($gamesProb->activa == 1) {

				$signo = '';
	
				//DETERMINA SIGNO EN EL RL
				if (in_array($gamesProb->tipo_probabilidad, [3, 7])) {
					$signo = ($gamesProb->equipo == $game->female->equipo) ? '+' : '-';
				}
	
				//DETERMINA SIGNO EN EL RL ALTERNATIVO
				if ($gamesProb->tipo_probabilidad == 16) {
					$signo = ($gamesProb->equipo == $game->female->equipo) ? '-' : '+';
				}
	
				//DETERMINA SIGNO EN EL RL MITAD DE JUEGO
				if ($gamesProb->tipo_probabilidad == 12) {
					$signo = ($gamesProb->equipo == $game->halfFemale->equipo) ? '+' : '-';
				}
	
				//DETERMINA EL SIGNO DE ALTAS Y BAJAS
				if (in_array($gamesProb->tipo_probabilidad, [4, 10, 13, 17, 18])) {
					$signo = ($gamesProb->orden == 1) ? '+' : '-';
				}
	
				if ($signo != '') {
					$signo = GamesProbsSigno::create([
						'games_prob_id' => $gamesProb->id,
						'signo' => $signo,
					]);
				}
			}

		}
	}

	protected function updateGamesProbs(Game $game)
	{
		$gamesProbs = GamesProb::where('idgame', $game->id)->get();
		
		foreach ($gamesProbs as $prob) {
			$prob->acierto = false;

			if ($prob->tipo_probabilidad == 1) {
				$prob->acierto = $prob->equipo == $game->who_win;
			}

			if (in_array($prob->tipo_probabilidad, [3, 7])) {
				//Game Tied an PK
				if ($game->is_tied && $prob->cantidad == 0) {
					$this->setPk($prob);
				}
				//Male win
				elseif (
					$prob->equipo == $game->ownMale($prob)->equipo &&
					$game->difference > $prob->cantidad &&
					$prob->equipo == $game->who_win
				) {
					$prob->acierto = true;
				}
				//Male Lose
				elseif ($prob->equipo == $game->ownMale($prob)->equipo && $prob->equipo != $game->who_win) {
					$prob->acierto = false;
				}
				//Female tied
				elseif ($prob->equipo == $game->ownFemale($prob)->equipo && $game->isTied) {
					$prob->acierto = true;
				}
				//Female win
				elseif ($prob->equipo == $game->ownFemale($prob)->equipo && $prob->equipo == $game->who_win) {
					$prob->acierto = true;
				}
				//Female lose
				elseif (
					$prob->equipo == $game->ownFemale($prob)->equipo &&
					$game->difference < $prob->cantidad &&
					$prob->equipo != $game->who_win
				) {
					$prob->acierto = true;
				}
				//Difference 
				elseif ($game->difference == $prob->cantidad) {
					$this->setPk($prob);
				}

				$this->restorePk($prob);
			}

			if ($prob->tipo_probabilidad == 4) {
				if ($prob->orden == 1) {
					$prob->acierto = $prob->cantidad < $game->total_score;
				} else {
					$prob->acierto = $prob->cantidad > $game->total_score;
				}

				if ($prob->cantidad == $game->total_score) {
					$this->setPk($prob);
				}

				$this->restorePk($prob);
			}

			if ($prob->tipo_probabilidad == 6) {
				$prob->acierto = $game->isTied;
			}

			if ($prob->tipo_probabilidad == 8) {
				$prob->acierto = optional($game->who_scored_first)->equipo == $prob->equipo;
			}

			if ($prob->tipo_probabilidad == 9) {
				$prob->acierto = optional($game->first_ining_scored)->score == $prob->orden;
			}

			if ($prob->tipo_probabilidad == 10) {
				if ($prob->orden == 1) {
					$prob->acierto = $prob->cantidad < optional($game->hre_result)->score;
				} else {
					$prob->acierto = $prob->cantidad > optional($game->hre_result)->score;
				}

				if ($prob->cantidad == optional($game->hre_result)->score) {
					$this->setPk($prob);
				}

				$this->restorePk($prob);
			}

			if ($prob->tipo_probabilidad == 11) {
				if ($game->categoria == 3) {/** FUTBOL */
					$prob->acierto = $prob->equipo == $game->who_win_half;
				} else {
					
					if ($game->half_is_tied) {
						$this->setPk($prob);
					} else {
						$prob->acierto = $prob->equipo == $game->who_win_half;
					}
	
					$this->restorePk($prob);
				}
								
			}

			if (in_array($prob->tipo_probabilidad, [12])) {
				//Game Tied an PK
				if ($game->half_is_tied && $prob->cantidad == 0) {
					$this->setPk($prob);
				}
				//Male win
				elseif (
					$prob->equipo == $game->ownMale($prob)->equipo &&
					$game->half_difference > $prob->cantidad &&
					$prob->equipo == $game->who_win_half
				) {
					$prob->acierto = true;
				}
				//Male Lose
				elseif ($prob->equipo == $game->ownMale($prob)->equipo && $prob->equipo != $game->who_win_half) {
					$prob->acierto = false;
				}
				//Female tied
				elseif ($prob->equipo == $game->ownFemale($prob)->equipo && $game->half_is_tied) {
					$prob->acierto = true;
				}
				//Female win
				elseif ($prob->equipo == $game->ownFemale($prob)->equipo && $prob->equipo == $game->who_win_half) {
					$prob->acierto = true;
				}
				//Female lose
				elseif (
					$prob->equipo == $game->ownFemale($prob)->equipo &&
					$game->half_difference < $prob->cantidad &&
					$prob->equipo != $game->who_win_half
				) {
					$prob->acierto = true;
				}
				//Difference tied
				elseif ($game->half_difference == $prob->cantidad) {
					$this->setPk($prob);
				}

				$this->restorePk($prob);
			}

			if ($prob->tipo_probabilidad == 13) {
				if ($prob->orden == 1) {
					$prob->acierto = $prob->cantidad < $game->half_score;
				} else {
					$prob->acierto = $prob->cantidad > $game->half_score;
				}

				if ($prob->cantidad == $game->half_score) {
					$this->setPk($prob);
				}

				$this->restorePk($prob);
			}

			if ($prob->tipo_probabilidad == 14) {
				$prob->acierto = $game->half_is_tied;
			}

			if ($prob->tipo_probabilidad == 15) {
				$prob->acierto = $game->who_go_next_level->equipo == $prob->equipo;
			}

			if ($prob->tipo_probabilidad == 16) {
				//FeMale win
				//$prob->acierto = false;
				if (
					$prob->equipo == $game->ownFemale($prob)->equipo &&
					$game->difference > $prob->cantidad &&
					$prob->equipo == $game->who_win
				) {
					$prob->acierto = true;
				}
				//Male tied
				elseif ($prob->equipo == $game->ownMale($prob)->equipo && $game->isTied) {
					$prob->acierto = true;
				}
				//Male win
				elseif ($prob->equipo == $game->ownMale($prob)->equipo && $prob->equipo == $game->who_win) {
					$prob->acierto = true;;
				}
				//Male lose
				elseif (
					$prob->equipo == $game->ownMale($prob)->equipo &&
					$game->difference < $prob->cantidad &&
					$prob->equipo != $game->who_win
				) {
					$prob->acierto = true;
				}
			}

			if ($prob->tipo_probabilidad == 17) {
				if ($prob->orden == 1) {
					$prob->acierto = $prob->cantidad < $game->home_score;
				} else {
					$prob->acierto = $prob->cantidad > $game->home_score;
				}

				if ($prob->cantidad == $game->home_score) {
					$this->setPk($prob);
				}

				$this->restorePk($prob);
			}

			if ($prob->tipo_probabilidad == 18) {
				if ($prob->orden == 1) {
					$prob->acierto = $prob->cantidad < $game->visit_score;
				} else {
					$prob->acierto = $prob->cantidad > $game->visit_score;
				}

				if ($prob->cantidad == $game->visit_score) {
					$this->setPk($prob);
				}

				$this->restorePk($prob);
			}

			if ($prob->tipo_probabilidad == 19) {
				$prob->acierto = $prob->equipo == $game->win_draw;
			}

			$prob->save();
		}
	}

	protected function updateBets(Game $game)
	{
		$gamesProbs = $game->gamesProbs;

		foreach ($gamesProbs as $prob) {
			$details = $prob->betsDetail;

			foreach ($details as $detail) {
				$bet = Bet::find($detail->idbets);
				if (isset($bet)) {
					if ($bet->status != 4) {
						if ($bet->is_completed) {
							$bet->status = $bet->is_winner ? '2' : '3';
							$bet->ganancia = $bet->calculeProfit();
							$bet->save();
						} else {
							Log::info('id => ' . $bet->id . ', status => ' . $bet->status . ', is_completed => ' . $bet->is_completed . ', is_lose => ' . $bet->is_lose);
							$bet->status = $bet->is_lose ? '3' : '1';
							$bet->ganancia = $bet->calculeProfit();
							$bet->save();
						}
					}
				}
			}
		}
	}

	public function cancel($id)
	{
		$game = Game::find($id);
		$game->status = 9;
		$game->save();
		return ['message' => "Juego $id cancelado exitosamente"];
	}

	public function hide($id)
	{
		$game = Game::find($id);
		$game->visible = !$game->visible;
		$game->save();
		return ['message' => "Juego $id actualizado exitosamente"];
	}

	public function suspend($id)
	{
		$game = Game::find($id);
		$game->status = 2;
		$game->save();


		$games_probs = GamesProb::where('idgame', $game->id)->get();

		$this->playPk($games_probs);

		$this->updateBets($game);

		return ['message' => "Juego $id suspendido exitosamente"];
	}

	public function legal($id)
	{
		$game = Game::find($id);
		$game->status = 3;
		$game->save();

		$games_probs = GamesProb::where('idgame', $game->id)->whereIn('tipo_probabilidad', ['3', '4', '5', '7', '10', '16', '17', '18'])->get();

		$this->playPk($games_probs);

		$this->updateBets($game);

		return ['message' => "Juego $id legal suspendido exitosamente"];
	}

	public function resume($id)
	{
		$game = Game::find($id);
		$game->status = 0;
		$game->save();

		$games_probs = GamesProb::where('idgame', $game->id)->get();

		foreach ($games_probs as $prob) {
			$pk = GamesProbsPK::where('idgames_probs', $prob->id)->get();
			$prob->probabilidad = $pk[0]->probabilidad;
			$prob->acierto = $pk[0]->acierto;
			$prob->save();

			GamesProbsPK::where('idgames_probs', $prob->id)->delete();
		}

		$this->updateBets($game);

		return ['message' => "Juego $id reanudado exitosamente"];
	}

	public function playPk($games_probs)
	{
		foreach ($games_probs as $prob) {
			$this->setPk($prob);
			$prob->save();
		}
	}

	protected function setPk(GamesProb $prob)
	{
		$pk = GamesProbsPK::create([
			'idgames_probs' => $prob->id,
			'probabilidad' => $prob->probabilidad,
			'acierto' => $prob->acierto,
		]);

		$pk->save();

		$prob->acierto = true;
		$prob->probabilidad = 1;
	}

	protected function restorePk(GamesProb $prob)
	{
		if ($prob->isClean('probabilidad') && $prob->probabilidad == 1) {

			$pk = GamesProbsPK::where('idgames_probs', $prob->id)->first();

			if ($pk) {
				$prob->probabilidad = $pk->probabilidad;
				$pk->delete();
			}
		}
	}
}
