<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TipoApuesta;
use App\Menu;
use App\ApuestasCategoria;

class TypeBetController extends Controller
{

    public function index(Request $request)
    {            
        if ($request->ajax()) {
            return TipoApuesta::all();
        }

    	$typebets = TipoApuesta::name($request->search)->paginate();
    	return view('admin.typebet.index', compact('typebets'));
    }

    public function categoryTypeBet($category_id)
    {
        $categoryTypeBet = ApuestasCategoria::where('categoria', $category_id)->get()->pluck('tipo_apuesta');
        return TipoApuesta::whereIn('id', $categoryTypeBet)->get();
    }

    public function create()
    {
            $menus = Menu::pluck('nombre', 'id');
    	return view('admin.typebet.create', compact('menus'));
    }

    public function store(Request $request)
    {
        $typebet = TipoApuesta::create([
            'nombre' => $request->nombre,
            'max_apuesta' => $request->max_apuesta,
            'exotica' => $request->exotica,
            'cantidad' => $request->cantidad,
            'probabilidades' => $request->probabilidades,
            'pk' => $request->pk,
        ]);
        $typebet->save();

        foreach ($request->menu as $key => $menu) {
            $typebet->typeBetCategory()->create(['categoria' => $menu]);
        }

      return redirect('/admin/typebet');      
    }

    public function show($id)
    {
    	$typebet = TipoApuesta::find($id);
             $menus = Menu::pluck('nombre', 'id');
    	return view('admin.typebet.show', compact('typebet', 'menus'));
    }

    public function update(Request $request, $id)
    {
        $typebet  = TipoApuesta::findOrFail($id);
        $typebet->fill($request->all());
        $typebet->save();

        $betsCategories = ApuestasCategoria::where('tipo_apuesta', $id)->get();

        if ($betsCategories->count() > 0) {
            foreach ($betsCategories as $betsCategory) {
                $betsCategory->delete();
            }
        }

        foreach ($request->menu as $key => $menu) {
            $typebet->typeBetCategory()->create(['categoria' => $menu]);
        }             
        return redirect('/admin/typebet');
    }
}
