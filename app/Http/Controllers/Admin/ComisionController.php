<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Comision;
use App\TipoComision;

class ComisionController extends Controller
{
	public function create()
	{
		$tipoComision = TipoComision::pluck('nombre', 'id');
		return view('admin.comision.create', compact('tipoComision'));
	}

	public function store(Request $request)
	{
		$comision = Comision::create($request->all());
		return redirect(route('admin.comision.index'));
	}

	public function index(Request $request)
	{
		$comisiones = Comision::tipo($request->search)
			->with(['tipoComision'])
			->paginate();
		return view('admin.comision.index', compact('comisiones'));
	}

	public function show($id)
	{
		$tipoComision = TipoComision::pluck('nombre', 'id');
		$comision = Comision::find($id);
		return view('admin.comision.show', compact('comision', 'tipoComision'));		
	}

	public function update (Request $request, $id)
	{
		$comision  = Comision::findOrFail($id);
		$comision->fill($request->all());
		$comision->save();
		return redirect('/admin/comision');		
	}
}
