<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Menu;

class MenuController extends Controller
{
  public function create()
  {
      return view('admin.menu.create');
  }  

  public function store(Request $request)
  {
      $menu = Menu::create($request->all());
      $menu->save();
      return redirect('/admin/menu');      
  }

  public function index(Request $request)
  {

  $search = $request->search;   

  $menus = Menu::name($search)->paginate();
  return view('admin.menu.index', compact('menus'));
  }

  public function show($id)
  {
    $menu = Menu::find($id);
    return view('admin.menu.show', compact('menu'));
  }

  public function update(Request $request, $id)
  {
    $menu  = Menu::findOrFail($id);
    $menu->fill($request->all());
    $menu->save();
    return redirect('/admin/menu');
  }
}
