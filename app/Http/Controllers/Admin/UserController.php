<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\User;
use App\TipoComision;
use App\CorredorComision;
use App\Empresa;

class UserController extends Controller
{
	public function index(Request $request)
	{
		$users = User::name($request->name)
			->where('status', 1)
			->paginate();

		return view('admin.user.index', compact('users'));
	}

	public function show($id)
	{
		$tipoComisiones = ['' => 'Seleccione el tipo de comision'] + TipoComision::pluck('nombre', 'id')->all();
		$empresas = ['' => 'Seleccione la empresa'] + Empresa::pluck('nombre', 'id')->all();
		$user = User::findOrFail($id);
		return view('admin.user.show', compact('user', 'tipoComisiones', 'empresas'));
	}

	public function update(Request $request, $id)
	{
		if ($request->corredor == 1) {
			$this->validate($request, [
				'tipoComision' => 'required',
				'ventas' => 'required'
			]);
		}
		$user  = User::findOrFail($id);
		$user->fill($request->all());
		$user->save();

		$corredorComisiones = CorredorComision::where('idusuario', $user->id)->get();

		if ($corredorComisiones->count() > 0) {
			foreach ($corredorComisiones as $comision) {
				$comision->delete();
			}
		}

		if ($request->corredor == 1) {
			CorredorComision::create([
				'idusuario' => $user->id,
				'idtipo_comision' => $request->tipoComision,
				'ventas' => $request->ventas,
			]);
		}


		return redirect('/admin/user');
	}
}
