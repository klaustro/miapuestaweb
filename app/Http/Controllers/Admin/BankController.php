<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Banco;

class BankController extends Controller
{
	public function index(Request $request)
	{
		$banks = Banco::name($request->search)->paginate();
		return view('admin.bank.index', compact('banks'));
	}

	public function create()
	{
		return view('admin.bank.create');
	}

	public function store(Request $request)
	{
		$bank = Banco::create($request->all());
		$bank->save();
      		return redirect('/admin/bank');      		
	}

	public function show($id)
	{
		$bank = Banco::find($id);
		return view('admin.bank.show', compact('bank'));
	}

	public function update(Request $request, $id)
	{
		$bank = Banco::find($id);
		$bank->fill($request->all());
		$bank->save();

		return redirect('/admin/bank');
	}
}
