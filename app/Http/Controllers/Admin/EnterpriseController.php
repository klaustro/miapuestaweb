<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Empresa;

class EnterpriseController extends Controller
{
	public function index(Request $request)
	{
		$enterprises = Empresa::name($request->search)->paginate();
		return view('admin.enterprise.index', compact('enterprises'));
	}

	public function create()
	{
		return view('admin.enterprise.create');
	}

	public function store(Request $request)
	{
		$enterprise = Empresa::create($request->all());
		$enterprise->save();
		return redirect('admin/enterprise');
	}

	public function show($id)
	{
		$enterprise = Empresa::find($id);
		return view('admin.enterprise.show', compact('enterprise'));
	}

	public function update(Request $request, $id)
	{
		$enterprise = Empresa::find($id);
		$enterprise->fill($request->all());
		$enterprise->save();

		return redirect('admin/enterprise');
	}

	public function test_enterprise_can_be_finded_by_name()
	{
	    	//Having
		factory(\App\Empresa::class)->times(15)->create();		

		$searched = factory(\App\Empresa::class)->create();	

		//When
		$this->visit('/admin/enterprise')
			->dontSeeInElement('li',"#{$searched->id}")
			->type($searched->nombre, 'search')
			->press('Buscar');

		//Then
		$this->seeInElement('li', $searched->nombre);
	}	
}
