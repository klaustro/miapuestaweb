<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Menu;
use App\Submenu;

class SubmenuController extends Controller
{

	public function index(Request $request)
	{
		$search = '';
		$menus = [];

		if (isset($request->search)) {

			$search = $request->search;

			$menus = Menu::name($search)->get()->all();

			if (count($menus) > 0) {
				$search = '';
			}
		}

		$submenus = Submenu::name($search)
			->menuName($menus)
			->with('menu')
			->paginate();

		return view('admin.submenu.index', compact('submenus'));
	}

	public function create()
	{
		$menus = Menu::pluck('nombre', 'id');
		return view('admin.submenu.create', compact('menus'));
	}

	public function store(Request $request)
	{
		$submenu = Submenu::create($request->all());
		$submenu->save();
		return redirect('/admin/submenu');
	}

	public function show($id)
	{
		$menus = Menu::pluck('nombre', 'id');
		$submenu = Submenu::find($id);
		return view('admin.submenu.show', compact(['submenu', 'menus']));
	}

	public function update(Request $request, $id)
	{
		$submenu  = Submenu::findOrFail($id);
		$submenu->fill($request->all());
		$submenu->save();
		return redirect('/admin/submenu');
	}

	public function listByMenu(Request $request)
	{
		$submenus = Submenu::where('id_menu', $request->get('menu'))->get();

		return $submenus;
	}
}
