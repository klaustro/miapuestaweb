<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Equipo;
use App\Submenu;
use App\EquipoSubcategoria;

class TeamController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $teams = Equipo::Subcategory($request->subcategory)->get();
            return $teams;
        }

    	$teams = Equipo::name($request->search)
    		->id($request->search)
    		->paginate();
    	return view ('admin.team.index', compact('teams'));
    }

    public function create()
    {
        $submenus = Submenu::pluck('nombre', 'id');
        return view('admin.team.create', compact('submenus'));
    }

    public function store(Request $request)
    {
        
        $this->validate($request, [
            'nombre' => 'required',
            'submenu' => 'required',
        ]);

        $team = Equipo::create([
            'nombre' => $request->nombre,
            'abbr' => $request->abbr,
        ]);

        $team->save();
        
        foreach ($request->submenu as $key => $submenu) {
            $team->equipoSubcategoria()->create(['id_submenu' => $submenu]);
        }

        return redirect(route('admin.team.index'));
    }

    public function show($id)
    {
        $team = Equipo::find($id);
        $submenus = Submenu::pluck('nombre', 'id');
        return view('admin.team.show', compact('team', 'submenus'));
    }
    
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre' => 'required',
            'submenu' => 'required',
        ]);

        $team = Equipo::find($id);
        $team->nombre = $request->nombre;
        $team->abbr = $request->abbr;
        $team->save();

        $teamSubmenus = EquipoSubcategoria::where('id_equipo', $id)->get();
        
        if ($teamSubmenus->count() > 0) {
            foreach ($teamSubmenus as $teamSubmenu) {
                $teamSubmenu->delete();
            }
        }

        foreach ($request->submenu as $key => $submenu) {
            $team->equipoSubcategoria()->create(['id_submenu' => $submenu]);
        }        

        return redirect()->route('admin.team.index');
    }
}
