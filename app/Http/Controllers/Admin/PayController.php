<?php

namespace App\Http\Controllers\Admin;

use App\Coupon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Notifications\UserCoupon;
use App\Pago;
use App\User;

class PayController extends Controller
{
 	public function index(Request $request)
	{
		$search = '';
		$users = [];

		if (isset($request->search)) {

		$search = $request->search;		

		$users = User::name($search)->get()->all();
		
		if (count($users) > 0){
				$search = '';
			}
		}

		$pagos = Pago::id($search)
			->userId($users)
			->orderBy('id', 'DESC')
			->with('user')
			->paginate();
			
		return view('admin.pago.index', compact('pagos'));
	}

	public function show($id)
	{
	        $pago = Pago::findOrFail($id);
	        return view('admin.pago.show', compact('pago'));
	}

	public function update(Request $request, $id)
	{
	        $pago  = Pago::find($id);
	        $pago->status = $request->status;
	        $pago->save();			

			if ($request->status == 1 ) {
				$user = $pago->user;
				$countPayments = Pago::where('id_usuario', $user->id)->count();

				if ($countPayments == 1) {
					$amount = $pago->monto * 0.3;
					$coupon = Coupon::generate($amount, $pago->id_usuario);
					$user->notify(new UserCoupon($coupon));
				}
			}
	        return redirect(route('admin.pago.index'));
	}


}
