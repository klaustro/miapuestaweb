<?php

namespace App\Http\Controllers\Admin;

use App\Bet;
use App\BetsDetail;
use App\Http\Controllers\Controller;
use App\Stsbet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BetController extends Controller
{
	public function index(Request $request)
	{
		$status = ['' => 'Todas las apuestas'] + Stsbet::pluck('nombre', 'id')->all();

		$bets = Bet::id($request->search)
			->userName($request->search)
			->status($request->status)
			->game($request->game)
			->enterprise(Auth::user()->enterprise)
			->dateRange($request->from, $request->to)
			->orderBy('id', 'DESC')
			->with(['user', 'stsbets']);

		$jugado = 0;
		$ganancia = 0;


		if (($request->from && $request->to) || $request->game) {
			$total = $bets->get();

			$jugado = $total->whereIn('status', [1, 2, 3, 5])->sum('monto');

			$ganancia = $total->whereIn('status', [2, 5])->sum('ganancia');
		}

		$bets = $bets->paginate();

		return view('admin.bet.index', compact('bets', 'status', 'jugado', 'ganancia'));
	}

	public function show($id)
	{
		$bet = Bet::findOrFail($id);
		$betsDetail = BetsDetail::where('idbets', $id)->get()->all();
		//dd($betsDetail);
		return view('admin.bet.show', compact(['bet', 'betsDetail']));
	}

	public function update(Request $request, $id)
	{
		$bet = Bet::findOrFail($id);
		if ($bet->status != 4) {
			$bet->status = 4;
		} else {
			$bet->status = 1;
		}
		$bet->save();
		return redirect(route('admin.bet.index'));
	}

	public function balance()
	{
		//dd(request()->all());
		$status = ['' => 'Todas las apuestas'] + Stsbet::pluck('nombre', 'id')->all();
		$start = request()->start ? Carbon::parse(request()->start) :  Carbon::now();
		$end = request()->end ? Carbon::parse(request()->end) : Carbon::now();


		$bets = Bet::whereBetween('fecha_registro', [date_format($start, 'Y-m-d'), date_format($end->addDay(), 'Y-m-d')])
			->enterprise(Auth::user()->enterprise)
			->status(request()->status)
			->paginate();

		$amounts = Bet::whereBetween('fecha_registro', [date_format($start, 'Y-m-d'), date_format($end, 'Y-m-d')])
			->enterprise(Auth::user()->enterprise);

		$total = $amounts->whereNotIn('status', ['4'])->sum('monto');
		$to_pay = $amounts->whereIn('status', [2, 5])->sum('monto');

		return view('admin.bet.balance', compact('bets', 'total', 'to_pay', 'status'));
	}
}
