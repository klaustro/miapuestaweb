<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Retiro;
use App\User;

class DebitController extends Controller
{
	public function index(Request $request)
	{
		$search = '';
		$users = [];

		if (isset($request->search)) {

		$search = $request->search;		

		$users = User::name($search)->get()->all();
		
		if (count($users) > 0){
				$search = '';
			}
		}

		$retiros = Retiro::id($search)
			->userId($users)			
			->orderBy('id', 'DESC')
			->with('user')
			->paginate();
		return view('admin.retiro.index', compact('retiros'));
	}

	public function show($id)
	{
	        $retiro = Retiro::findOrFail($id);
	        return view('admin.retiro.show', compact('retiro'));
	}	

	public function update(Request $request, $id)
	{
	        $retiro  = Retiro::findOrFail($id);
	        $retiro->fill($request->all());
	        $retiro->save();
	        return redirect('/admin/retiro');
	}	
}
