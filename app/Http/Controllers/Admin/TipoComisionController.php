<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TipoComision;

class TipoComisionController extends Controller
{
	public function index(Request $request)
	{
		$comissionTypes = TipoComision::name($request->search)->paginate();
		return view('admin.typecomision.index', compact('comissionTypes'));
	}

	public function create()
	{
		return view('admin.typecomision.create');
	}

	public function store(Request $request)
	{
		$commissionType = TipoComision::create($request->all());
		$commissionType->save();
      		return redirect('/admin/type/comision');      		
	}

	public function show($id)
	{
		$commissionType = TipoComision::find($id);
		return view('admin.typecomision.show', compact('commissionType'));
	}

	public function update(Request $request, $id)
	{
		$commissionType = TipoComision::find($id);
		$commissionType->fill($request->all());
		$commissionType->save();

		return redirect('/admin/type/comision');
	}
}
