<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AutoGameController extends Controller
{
    public function mlbGames()
    {
    	$day = '21';
    	$month = '06';
    	$year = '2017';
    	$data = file_get_contents("http://gd2.mlb.com/components/game/mlb/year_$year/month_$month/day_$day/miniscoreboard.json");
    	return json_decode($data, true);
    }
}
