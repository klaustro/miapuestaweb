<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Bet;
use App\User;
use App\Stsbet;
use Illuminate\Support\Facades\Auth;

class RunnerController extends Controller
{
    public $total = [
        'comision' => 0,
        'venta' => 0,
        'utilidad' => 0,
    ];

    public function index(Request $request)
    {
        $runners = [''=>'Todos los Cooredores'] 
            + User::where('corredor', '1')
                ->where('enterprise', Auth::user()->enterprise)
				->orderBy('name')
                ->pluck('name', 'id')->all();

        $status = [''=>'Todos los Status'] +Stsbet::pluck('nombre', 'id')->all();

        $this->setTotal($request);
        $total = $this->total;    	
        $bets = Bet::runner('1')
                    ->user($request->runner)
                    ->dateRange($request->from, $request->to)
                    ->status($request->status)
                    ->enterprise(Auth::user()->enterprise)
                    ->orderBy('id','DESC')
                    ->with(['stsbets', 'corredorComision', 'user'])
                    ->paginate();

        return view('admin.runner.index', compact('bets', 'runners', 'total', 'status'));
    }

    private function setTotal($request)
    {        
        if ($request->runner != '' && $request->from != '' && $request->to != '') {
            $bets = Bet::runner('1')
                ->user($request->runner)
                ->dateRange($request->from, $request->to)
                ->status($request->status)
                ->enterprise(Auth::user()->enterprise)
                ->get()
                ->all();

            foreach ($bets as $bet) {
                if ($bet->status != 4) {
                    $this->total['utilidad'] +=  $this->profit($bet);
                    $this->total['venta'] +=  $bet->monto;
                    $this->total['comision'] +=  $bet->comision;
                }
            }
            $this->total['utilidad'] = $this->total['utilidad']  - $this->total['comision'] ;
        }        
    }

    protected function profit(Bet $bet)
    {
        $profit = $bet->monto;
        if (in_array($bet->status, ['2', '5'])) {
            $profit = $profit - $bet->ganancia;
        }

        return $profit;
    }
}
