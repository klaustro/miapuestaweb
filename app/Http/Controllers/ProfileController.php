<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
	public function show()
	{
		$user = User::findOrFail(Auth::user()->id);
		return view('profile.show', compact('user'));
	}

	public function update(Request $request)
	{
		$user  = User::findOrFail(Auth::user()->id);
		$user->fill($request->all());
		$user->save();

		if ($request->wantsJson()) {
			return [
				'title' => 'Usuario <br> Actualizado',
				'message' => 'Sus datos fueron actualizados correctamente.',
			];
		}

		return redirect()->route('home');
	}
}
