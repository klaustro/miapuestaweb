<?php

namespace App\Http\Controllers;

use App\Menu;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	if(Auth::guest()){            
	    return view('home.index');

	}

	$menus = Menu::withCount(['games'=>
						function($query) {
					$now = Carbon::now('America/Caracas');
					$now = date_format($now,"Y-m-d H:i:s");
	                             $query->where('fecha_cierre', '>', $now);
						$query->where('visible', 1);
					}])
	                          ->get()
	                          ->all();        

	return view('bet.index', compact('menus'));
    }
}
