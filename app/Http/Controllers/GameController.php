<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Game;
use Carbon\Carbon;

class GameController extends Controller
{
	public function index(Request $request)
	{
		$now = Carbon::now('America/Caracas');
		$now = date_format($now, "Y-m-d H:i:s");
		$games = Game::where('fecha_cierre', '>', $now)
			->where('status', '<>', '9')
			->where('visible', '1')
			->with('games_probs.games_probs_signo', 'games_probs.tipoApuesta', 'equipo', 'exoticasCount')
			->submenu($request->get('submenu'))
			->orderBy('fecha_cierre')
			->get();

		return $games;
	}

	public function show($id)
	{
		return Game::with('categoria', 'subcategoria')
			->with(['games_probs' =>
			function ($query) {
				$query->where('activa', '1');
			}])
			->with('equipo')
			->find($id);
	}

	public function gameCategories()
	{
		$now = Carbon::now('America/Caracas');
		$now = date_format($now, "Y-m-d H:i:s");
		$categories = Game::with('menu')
			->where('fecha_cierre', '>', $now)
			->where('visible', 1)
			->orderBy('categoria')
			->distinct()
			->get('categoria');

		$menus = [];

		foreach ($categories as $category) {
			$menus[] = $category->menu;
		}

		return $menus;
	}
}
