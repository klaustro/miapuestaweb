<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
	public function checkMail()
	{
		$user = User::where('email', request()->email)->first();

		$message = $user ? 'El correo ingresado ya se encuentra en nuestra Base de datos' : 'valid';

		return [
			'message' => $message,
		];
	}

	public function balance()
	{
		$data = [
			'balance' => auth()->user()->saldo,
			'bonus' => auth()->user()->bonus,
			'currency_symbol' => auth()->user()->currency_symbol,
		];
		return $data;
	}
}
