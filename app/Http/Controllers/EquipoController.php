<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Equipo;

class EquipoController extends Controller
{
  public function index()
  {
      $equipos = Equipo::all();
      return $equipos;
  }


  public function show($id)
  {
      return Equipo::find($id);
  }
}
