<?php

namespace App\Http\Controllers;

use App\Coupon;
use Illuminate\Http\Request;

class CouponController extends Controller
{
	
	public function applyCoupon($coupon_id)
	{
		$coupon = Coupon::find($coupon_id);
		$coupon->user_id = auth()->user()->id;
		$coupon->save();
	}
}
