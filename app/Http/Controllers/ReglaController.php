<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Regla;
use App\Game;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ReglaController extends Controller
{
	public function index(Request $request)
	{
		Log::info($request);
		$categoria = Game::find($request->get('game'))->categoria;
		$message = "Valida";
		$tipos_apuesta = $request->get('tipos_apuesta');
		$max = Regla::max('idregla');
		for ($i = 1; $i <= $max; $i++) {
			$reglas = Regla::with('mensaje')
				->where('idcategoria', $categoria)
				->where('idregla', $i)
				->whereIn('tipo_apuesta', $tipos_apuesta)
				->get()
				->all();
			if (count($reglas) > 1) {
				Log::info($reglas[0]);
				$message = $reglas[0]->mensaje->mensaje;
				$i = $max + 1;
			}
		}
		return response()->json([
			'message' => $message
		]);
	}
}
