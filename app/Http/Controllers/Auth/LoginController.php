<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
	/*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

	use AuthenticatesUsers;

	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */
	protected $redirectTo = '/home';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest')->except('logout');
	}

	/**
	 * Handle an authentication attempt.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function authenticate()
	{
		$credentials = request()->only('email', 'password');

		if (Auth::attempt($credentials)) {
			if (auth()->user()->status == 1) {
				$user = auth()->user();				
				request()->session()->regenerate();
				return $user->load('accounts');
			} else {
				Auth::logout();			
			}
		}

		return response()->json([
			'error' => 	'Unauthenticated.'
		], 401);
	}

	    /**
     * Show the application's login form.
     *
     * @return \Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }
}
