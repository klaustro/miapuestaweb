<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Retiro extends Model
{
	protected $fillable = ["id_usuario", "cuenta_id", "fecha", "fecha_hora", "monto", "status", "fecha_proceso", 'exchange_id',];

	public $timestamps = false;

	public function user()
	{
		return $this->belongsTo('App\User', 'id_usuario');
	}

	public function account()
	{
		return $this->belongsTo(CuentasBancaria::class, 'cuenta_id');
	}

	public function scopeId($query, $id)
	{
		if ($id != "")
			$query->where('id', $id);
	}

	public function scopeUserId($query, $users)
	{
		if (count($users) > 0) {
			$userId = [];
			foreach ($users as $user) {
				array_push($userId, $user->id);
			}
			$query->whereIn('id_usuario', $userId);
		}
	}

	//Atributes
	public function getBancoAttribute()
	{
		return $this->account ? $this->account->banco->nombre : '';
	}

	public function getAccountNumberAttribute()
	{
		return $this->account ? $this->account->nrocuenta : '';
	}
}
