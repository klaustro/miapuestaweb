<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::if('admin', function(){
            return auth()->check() && auth()->user()->isAdmin();
        });

        Blade::if('owner', function(){
            return auth()->check() && auth()->user()->isOwner();
        });        
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
